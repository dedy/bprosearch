drop view if exists popular_regulasi;

create view popular_regulasi as 
	select regulasi_id, count(regulasi_id) as total_views from 
	regulasi_view_histories a
where a.time >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
	GROUP BY a.regulasi_id;