ALTER TABLE  `regulasi` ADD  `nama_file_regulasi_en` VARCHAR( 255 ) NULL AFTER  `type_file_lampiran` ,
ADD  `ukuran_file_regulasi_en` INT UNSIGNED NULL AFTER  `nama_file_regulasi_en` ,
ADD  `type_file_regulasi_en` VARCHAR( 100 ) NULL AFTER  `ukuran_file_regulasi_en` ,
ADD  `nama_file_lampiran_en` VARCHAR( 255 ) NULL AFTER  `type_file_regulasi_en` ,
ADD  `ukuran_file_lampiran_en` INT( 11 ) UNSIGNED NULL AFTER  `nama_file_lampiran_en` ,
ADD  `type_file_lampiran_en` VARCHAR( 100 ) NULL AFTER  `ukuran_file_lampiran_en` ;

---- ALTER TABLE  `regulasi_file` ADD  `regulasi_file_utama_en` MEDIUMBLOB NULL AFTER  `regulasi_file_lampiran` ,
---- ADD  `regulasi_file_lampiran_en` MEDIUMBLOB NULL AFTER  `regulasi_file_utama_en` ;
CREATE TABLE IF NOT EXISTS `regulasi_file_en` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `regulasi_id` int(11) unsigned NOT NULL,
  `regulasi_file_utama_en` mediumblob,
  `regulasi_file_lampiran_en` mediumblob,
  PRIMARY KEY (`id`),
  KEY `fk_regulasi_file_en_regulasi_id` (`regulasi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `regulasi_file_en`
  ADD CONSTRAINT `fk_regulasi_file_en_regulasi_id` FOREIGN KEY (`regulasi_id`) REFERENCES `regulasi` (`id`) ON DELETE CASCADE;
