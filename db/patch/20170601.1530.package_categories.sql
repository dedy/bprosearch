
CREATE TABLE IF NOT EXISTS `package_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `package_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


ALTER TABLE `package_categories`
  ADD CONSTRAINT `FK_package_categories_package_id` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_packages_categories_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

ALTER TABLE  `package_categories` DROP  `updated_by` ,
DROP  `updated_on` ;