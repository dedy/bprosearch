alter table `category` add column `slug` char(255) ;
UPDATE `category` SET slug = lower(name), slug = replace(slug, ' ', '-');