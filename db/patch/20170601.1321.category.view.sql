create view category_view as
select a.*,
b.name as parent_name,
case a.id_parent 
	when 0 then 
		CONCAT(
			LPAD(a.level, 4, '0')
			,LPAD(a.display_order, 4, '0')
			,UPPER(SUBSTR(a.name, 1,2))
			,LPAD(a.id, 4, '0')
			,LPAD('0', 4, '0')
			,'0'
		) 
	else  CONCAT(
		LPAD(b.level, 4, '0')
		,LPAD(b.display_order, 4, '0')
		,UPPER(SUBSTR(b.name, 1,2))
		,LPAD(b.id, 4, '0')
		,LPAD(a.display_order, 4, '0')
		,UPPER(SUBSTR(a.name, 1,2))
	) 
end as level_order
from category a
left outer join category b on b.id = a.id_parent
order by level_order
