ALTER TABLE  `users` ADD  `package_id` INT( 11 ) UNSIGNED NULL AFTER  `mobile` ;
ALTER TABLE  `users` ADD  `package_enddate` DATE NULL AFTER  `package_id` ;
ALTER TABLE `users`
  ADD CONSTRAINT `FK_user_package_id` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ;
