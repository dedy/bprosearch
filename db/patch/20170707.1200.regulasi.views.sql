drop view if exists popular_regulasi;

create view popular_regulasi as 
	select regulasi_id, count(regulasi_id) as total_views from 
	regulasi_view_histories a
LEFT JOIN regulasi on regulasi.id = a.regulasi_id 
where a.time >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
	and regulasi.category_id_parents not like '%;39;%'
	GROUP BY a.regulasi_id;