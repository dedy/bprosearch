
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `duration_months` tinyint(4) NOT NULL,
  `active` enum('0','1') NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_packages_created_by` (`created_by`),
  KEY `FK_packages_updated_by` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `price`, `duration_months`, `active`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'PAKET FREE TRIAL', 0, 3, '1', 1, '2017-06-01 00:00:00', 1, '2017-06-01 08:50:49'),
(2, 'PAKET SILVER', 400000, 12, '1', 1, '2017-06-01 00:00:00', 1, '2017-06-01 08:50:49'),
(3, 'PAKET GOLD', 750000, 12, '1', 1, '2017-06-01 00:00:00', 1, '2017-06-01 08:51:51'),
(4, 'PAKET PLATINUM', 20000000, 12, '1', 1, '2017-06-01 00:00:00', 1, '2017-06-01 08:51:51');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `packages`
--
ALTER TABLE `packages`
  ADD CONSTRAINT `FK_packages_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_packages_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

ALTER TABLE  `packages` ADD  `all_category` ENUM(  '0',  '1' ) NOT NULL DEFAULT  '0' AFTER `duration_months` ;