delete from users_groups where user_id not in (select id from users);

ALTER TABLE `users_groups`
  ADD CONSTRAINT `FK_ugroups_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
  ADD CONSTRAINT `FK_ugroups_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

