
ALTER TABLE  `regulasi` ADD  `number_of_views` INT( 11 ) UNSIGNED NOT NULL AFTER  `active` ;

CREATE TABLE IF NOT EXISTS `regulasi_view_histories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `regulasi_id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `time` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;


ALTER TABLE `regulasi_view_histories`
  ADD CONSTRAINT `FK_view_history_regulasi_id` FOREIGN KEY (`regulasi_id`) REFERENCES `regulasi` (`id`);

ALTER TABLE  `regulasi_view_histories` ADD  `user_id` INT( 11 ) UNSIGNED NULL AFTER  `ip_address` ;

ALTER TABLE `regulasi_view_histories`
  ADD CONSTRAINT `FK_view_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

-- untuk auto update increase views
CREATE TRIGGER ins_history AFTER INSERT ON regulasi_view_histories 
FOR EACH ROW
  UPDATE regulasi
     SET number_of_views = number_of_views+1
   WHERE id = NEW.regulasi_id;

   ALTER TABLE  `regulasi_view_histories` ADD  `page` VARCHAR( 25 ) NOT NULL AFTER  `user_id` ;


create view popular_regulasi as 
    SELECT a.id, a.judul,a.nomor_dokumen , b.time,
          count(b.id) as total_views , DATE_FORMAT(a.tanggal,'%d/%m/%Y') as tanggal_fmt
    FROM 
      regulasi a LEFT OUTER JOIN regulasi_view_histories b on b.regulasi_id = a.id
    WHERE a.active = '1' AND 
    b.time BETWEEN NOW() - INTERVAL 30 DAY AND NOW()
    GROUP BY a.id;