CREATE TABLE IF NOT EXISTS `import_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `import_logs_detail`
--

CREATE TABLE IF NOT EXISTS `import_logs_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `import_log_id` int(11) unsigned NOT NULL,
  `nomor_dokumen` varchar(255) NOT NULL,
  `status` enum('update','insert','error') NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `import_logs_detail`  
ADD CONSTRAINT `FK_import_logs_id` FOREIGN KEY (`import_log_id`) REFERENCES `import_logs` (`id`) ON DELETE CASCADE;
ALTER TABLE  `import_logs` CHANGE  `created_at`  `created_on` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;
ALTER TABLE  `import_logs_detail` ADD  `created_on` TIMESTAMP NOT NULL ;
