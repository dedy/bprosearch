ALTER TABLE `regulasi_view_histories`
	DROP FOREIGN KEY `FK_view_history_regulasi_id`;
ALTER TABLE `regulasi_view_histories`
  ADD CONSTRAINT `FK_view_history_regulasi_id` FOREIGN KEY (`regulasi_id`) REFERENCES `regulasi` (`id`) ON DELETE CASCADE;
