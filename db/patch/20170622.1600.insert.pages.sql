INSERT INTO  `pages` (
`id` ,
`title` ,
`slug` ,
`description` ,
`meta_keyword` ,
`meta_description` ,
`content` ,
`date_posted` ,
`active` ,
`created_on` ,
`created_by` ,
`updated_on` ,
`updated_by`
)
VALUES (
NULL ,  'Please login',  'please-login', 'If user has not logged in ,and want to access regulation search result page 2 , then display this page',  'Please Login', 'Please Login',  'Please login first<br>
or if you don''t have any account, please register ', NULL ,  '1',  '2017-06-22 00:00:00',  '1',  '2017-06-22 00:00:00',  '1'
);

INSERT INTO `pages` (`id`, `title`, `slug`, `description`, `meta_keyword`, `meta_description`, `content`, `date_posted`, `active`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES (NULL, 'Membership Expired', 'membership-expired', 'halaman ditampilkan ketika member sudah login , mengakses halman 2 ke atas dari search result, tetapi masa aktif member sudah lewat', NULL, NULL, 'Maaf, masa aktif membership anda sudah lewat.<br>
Harap memperpanjang masa aktif 
anda dengan memilih paket yang disediakan.<Br><BR>

Terima kasih', '2017-06-22 00:00:00', '1', '2017-06-22 00:00:00', '1', '2017-06-22 00:00:00', '1'), (NULL, 'You are not Authorized', 'not-authorized', 'halaman ini ditampilkan ketika member sudah login, membership masih aktif, tapi kategori yang diakses tidak termasuk di paket', NULL, NULL, 'Maaf, paket anda tidak termasuk untuk kategori ini. <br>
Harap hubungi administrator untuk keterangan lebih lanjut.<Br><BR>

Terima kasih', '2017-06-22 00:00:00', '1', '2017-06-22 00:00:00', '1', '2017-06-22 00:00:00', '1');