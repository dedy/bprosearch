
ALTER TABLE `category`
  ADD CONSTRAINT `FK_category_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_category_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);


ALTER TABLE `country`
  ADD CONSTRAINT `FK_country_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

ALTER TABLE  `emails` ENGINE = INNODB;
ALTER TABLE `emails`
  ADD CONSTRAINT `FK_email_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `url` varchar(150) NOT NULL,
  `display_order` tinyint(3) unsigned NOT NULL,
  `active` 	enum('0', '1') DEFAULT '1' ,
  `updated_on` datetime NOT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `menus`
  ADD CONSTRAINT `FK_menu_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);


CREATE TABLE IF NOT EXISTS `group_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` mediumint(8) unsigned NOT NULL,
  `menu_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `group_menus`
  ADD CONSTRAINT `FK_group_menus_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `FK_group_menus_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`);

ALTER TABLE  `group_menus` ADD  `permissions` VARCHAR( 25 ) NOT NULL ;

ALTER TABLE  `groups` ADD  `updated_by` INT( 11 ) UNSIGNED NOT NULL ,
ADD  `updated_on` DATETIME NOT NULL ;
