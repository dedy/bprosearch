###################
What is BProsearch
###################

BProsearch is an Web Search Engine. Built on Codeigniter Framework 3.1.0

*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Please see the doc/readme.1st .

*********
Resources
*********

Report security issues to our `Dedy <mailto:dedy@geraiweb.com>` , thank you.
