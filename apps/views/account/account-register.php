<script >
	$(document).ready(function(){
    	$("#tc").modal();
        $('#submit-btn-register').click(function(e){
			e.preventDefault(); 
			dopost($(this));
		});
			
	
	});
	
	function dopost(obj){
		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();

		$.post('<?=base_url();?>account/doregister', 
				$("#wf-form-Create-Account-Form").serialize(),
				function(returData) {
					
					$('#loadingmessage').hide();
					obj.removeAttr('disabled');
     				
					$('#show_message').slideUp('normal',function(){
						if(returData.error){
                            var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							//var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                            var rv = '<div class="alert alert-success"><span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span>'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, <?=config_item('message_delay')?>);
							});	
						}	
					});
					
				},'json');
	}
	</script>	
    
<div class="block-1">
	<h2>REGISTER NOW</h2>
</div>

<div class="block-2">
    <div class="content">
	<div class="row">
		<div class="col-sm-push-3 col-sm-6 left">
        
      <div class="w-form form-wrapper">
            <?php 
              echo form_open("account/doregister",array('data-name'=>"Create Account Form" ,'id'=>"wf-form-Create-Account-Form",'name'=>"wf-form-Create-Account-Form"));
              ?>
          <div class="row row-form registration">
          <div class="col-sm-12">
            <div class=" form-registration-column1">
              <div class="create-and-account-copy"><p><strong><?=lang('liwant_to_create_account')?></strong></p></div>
                  <div class="form-group input-wrapper">
                    <span class="menu-icon"> <i class="append-icon fa fa-fw fa-user"></i> </span><input class="w-input field" data-name="Name" id="Name" maxlength="256" name="Name" required="required" type="text" placeholder="<?=lang('lfullname')?> *">
                  </div>
                  <div class="form-group input-wrapper">
                      <span class="menu-icon"> <i class="append-icon icon-mail"></i> </span><input class="w-input field" data-name="Email" id="Email" maxlength="256" name="Email" required="required" type="email" placeholder="<?=lang('lemail')?> *">
                  </div>
                  <div class="form-group input-wrapper">
                      <span class="menu-icon"> <i class="append-icon icon-mobile"></i> </span><input class="w-input field" data-name="Phone" id="Phone" maxlength="256" name="Phone" required="required" type="text" placeholder="<?=lang('lphone')?> *">
                  </div>
                  <div class="form-group input-wrapper">
                      <span class="menu-icon"> <i class="append-icon fa fa-fw fa-lock"></i> </span><input class="w-input field" data-name="Password" id="Password" maxlength="256" name="Password" required="required" type="password"  placeholder="<?=lang('lpassword')?> *">
                  </div>
                  <div class="form-group input-wrapper">
                      <span class="menu-icon"> <i class="append-icon fa fa-fw fa-lock"></i> </span><input class="w-input field" data-name="Password2" id="Password-3" maxlength="256" name="Password2" required="required" type="password"  placeholder="<?=lang('lretype_password')?> *">
                  </div>
                  <div class="form-group">
                  	<div class="vd_radio radio-success row">
						<div class="col-sm-2 ">
                    				<input type="radio" name="searchMethod" id="optionsRadios1" value="g1" class="vd_checkbox" checked="checked">
                                    <label  for="optionsRadios1" > Pria </label>
                   		</div>
						<div class="col-sm-2 ">
                    				<input type="radio" name="searchMethod" id="optionsRadios2" value="g2" class="vd_checkbox">
                                    <label  for="optionsRadios2"> Wanita </label>
                   		</div>
                     </div>                 
                  </div>
              <div class="vd_checkbox checkbox-success left">
                <input checked="checked" class="w-checkbox-input vd_checkbox" id="node-2" required="required" type="checkbox">
                <label class="w-form-label vd_checkbox-label" for="node-2">I have read and agree to the <a class="link-t-and-c" data-toggle="modal" data-target="#tc">Terms and Conditions</a> of www.bprosearch.com</label>
              </div>
              
              <div id='show_message' style="display: none;"></div> 
				
               <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
                    <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
                </div>
              
               <div class="container-button-submit register">
                <input class="w-button submit-ad register btn vd_btn vd_bg-blue vd_white" data-wait="Please wait..." type="submit" id='submit-btn-register' value="REGISTER">
              
              </div>
              
            </div>
            
          </div>
        </form>
        
		</div>
      </div>
  
        </div>
    	
	</div>
    </div>
</div>

<!-- Modal -->
<div id="tc" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?=$terms_title?></h4>
            </div>
            <div class="modal-body">
                <p><?=$terms_content?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
