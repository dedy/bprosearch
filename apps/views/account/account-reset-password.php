<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){

        $('#submit-btn-register').click(function(e){
            e.preventDefault();
            dopost($(this));
        });


    });

    function dopost(obj){
        obj.attr('disabled','disabled');
        $('#loadingmessage').show();

        $.post('<?=base_url();?>account/doreset_password',
            $("#wf-form-Reset-Password-Form").serialize(),
            function(returData) {

                $('#loadingmessage').hide();
                obj.removeAttr('disabled');

                $('#show_message').slideUp('normal',function(){

                    if(returData.error){
                        var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal');

                    }else{
                        var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal',function(){
                            setTimeout(function() {
                                $('#show_message').slideUp('normal',function(){
                                    if(returData.redirect){
                                        window.location.replace(returData.redirect);
                                    }
                                });
                            }, <?=config_item('message_delay')?>);
                        });
                    }
                });

            },'json');
    }
</script>
<div class="w-section section-create-ad">
    <div class="w-container">
        <div class="main-title-page create-ad"><?=lang('lreset_password')?></div>
        <div class="w-form form-wrapper">
            <?php
            echo form_open("account/doreset_password",array('data-name'=>"Reset Password" ,'id'=>"wf-form-Reset-Password-Form"),array("code"=>$code));
            ?>
            <div class="w-row row-form registration">
                <div class="w-col w-col-6 form-reset-password-column1">
                    <label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
                    <?php echo form_input($new_password);?>
                    <label for="confim_password"><?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?></label>
                    <?php echo form_input($new_password_confirm);?>

                    <?php echo form_input($user_id);?>
                    <?php echo form_hidden($csrf); ?>

                    <div id='show_message' style="display: none;"></div>

                    <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
                        <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
                    </div>

                    <div class="container-button-submit register">
                        <input class="w-button submit-ad register" data-wait="Please wait..." type="submit" id='submit-btn-register' value="<?=lang('forgot_password_submit_btn')?>">
                    </div>

                </div>
                <div class="w-col w-col-6 column2-form">

                </div>
            </div>
            </form>

            <div class="w-form-done success-message" id="success_msg">
                <p class="message">Success MEssage</p>
            </div>
            <div class="w-form-fail error-message" id="error_msg">
                <p class="message">Error Message</p>
            </div>
        </div>
    </div>
</div>