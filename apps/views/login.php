<?php
$this->load->helper('form');
?>
    <script type="text/javascript" language="javascript">
    function createlightbox()
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block'
    }
    function closelightbox()
    {
        document.getElementById('light').style.display='none';
        document.getElementById('fade').style.display='none'
    }
    </script>


<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
        $('.forgot-password').click(function(e){
            e.preventDefault();
            $('#wf-form-Login-Form').hide();
            $('#wf-form-Forgot-Password-Form').show();
            $('.main-title-page').html('<?=lang('lforgot_password')?>');
        });

        $('.back-login').click(function(e){
            e.preventDefault();
            $('#wf-form-Login-Form').show();
            $('#wf-form-Forgot-Password-Form').hide();
            $('.main-title-page').html('Log into your account');
        });

		$('#submit-btn-login').click(function(e){
			e.preventDefault(); 
			$('#loadingmessage-login').show();
			
			$.post('<?=site_url('account/dologin');?>', 
				$("#wf-form-Login-Form").serialize(),
				function(returData) {
					$('#loadingmessage-login').hide();
					$('#show_message-login').slideUp('normal',function(){
                      //  alert(returData.message);
						if(returData.error){
							//var rv = '<div class="alert alert-danger"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_red"></i></span>'+returData.message+'</div>';
                            var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
							$('#show_message-login').html(rv);
							$('#show_message-login').slideDown('normal');	
						}else{
							var rv = '<div class="alert alert-success"><span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span>'+returData.message+'</div>';
							$('#show_message-login').html(rv);
							$('#show_message-login').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message-login').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  },  <?=config_item('message_delay')?>);
							});	
						}	
					});
					
				},'json');
		});

        $('#submit-btn-forgot-password').click(function(e){
            e.preventDefault();
            $('#loadingmessage-forgot-password').show();

            $.post('<?=site_url('account/doforgot');?>',
                $("#wf-form-Forgot-Password-Form").serialize(),
                function(returData) {
                    $('#loadingmessage-forgot-password').hide();
                    $('#show_message-login').slideUp('normal',function(){
                        //  alert(returData.message);
                        if(returData.error){
                            //var rv = '<div class="alert alert-danger"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_red"></i></span>'+returData.message+'</div>';
                            var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
                            $('#show_message-forgot-password').html(rv);
                            $('#show_message-forgot-password').slideDown('normal');
                        }else{
                            var rv = '<div class="alert alert-success"><span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span>'+returData.message+'</div>';
                            $('#show_message-forgot-password').html(rv);
                            $('#show_message-forgot-password').slideDown('normal',function(){
                                setTimeout(function() {
                                    $('#wf-form-Login-Form').show();
                                    $('#wf-form-Forgot-Password-Form').hide();
                                    $('.main-title-page').html('Log into your account');
                                    /**$('#show_message-forgot-password').slideUp('normal',function(){
                                        if(returData.redirect){
                                            //window.location.replace(returData.redirect);
                                        }
                                    });**/
                                },  <?=config_item('message_delay')?>);
                            });
                        }
                    });

                },'json');
        });
		
	});
	</script>	
    
    <!-- login box -->
  <div class="w-section section-lightbox" id="light">
    <div class="w-container container-lightbox">

      <div class="w-clearfix div-login">
        <div class="w-clearfix "><a href = "javascript:void(0)" onclick = "closelightbox()" class="close-button">X</a>
        </div>
        <div class="main-title-page login"><?=lang('llog_into_your_account')?></div>
        <div class="w-form login-form">
            <?php echo form_open("account/dologin",array('class'=>'login-form-inside',"data-name" => "Login Form","id" =>  "wf-form-Login-Form", "name" => "wf-form-Login-Form"  ));?>
            <label class="label" for="name"><?=lang('lemail')?> <span class="asterisk">*</span>
            </label>
            <input class="w-input field login" data-name="email" id="email" maxlength="256" name="email" placeholder="Enter your Email" type="text">
            <label class="label" for="Password"><?=lang('lpassword')?> <span class="asterisk">*</span>
            </label>
            <input class="w-input field login" data-name="password" id="password" maxlength="256" name="password" placeholder="Enter your Password" required type="password">
            <div class="forgot-password"><a href="#"><?=lang('lforgot_password')?></a></div>
            
            <input id='submit-btn-login' class="w-button submit-ad login" data-wait="Please wait..." type="submit" value="<?=lang('llogin')?>">
         
            <div id='loadingmessage-login' style="display: none">
                  <img src='<?=base_url()?>/assets/images/spinner-mini.gif'/>
            </div>
            <div id='show_message-login' style="margin:5px 0 0 0"></div>

            <?php echo form_close();?>

            <?php echo form_open("account/doforgot",array('class'=>'login-form-inside',"data-name" => "Login Form","id" =>  "wf-form-Forgot-Password-Form", "name" => "wf-form-Login-Form"  ));?>
            <label class="label" for="name"><?=lang('lemail')?> <span class="asterisk">*</span>
            </label>
            <input class="w-input field login" data-name="identity" id="identity" maxlength="256" name="identity" placeholder="Enter your Email" type="text">
            <div class="back-login"><a href="#"><?=lang('lback_to_login')?></a></div>
            <input id='submit-btn-forgot-password' class="w-button submit-ad login" data-wait="Please wait..." type="submit" value="<?=lang('lsubmit')?>">
            <div id='loadingmessage-forgot-password' style="display: none">
                <img src='<?=base_url()?>/assets/images/spinner-mini.gif'/>
            </div>
            <div id='show_message-forgot-password' style="margin:5px 0 0 0"></div>
            <?php echo form_close();?>
            
        </div>
      <a class="no-account-yet" href="<?=site_url('account/register')?>">Don't have an account yet? Sign Up!</a>
      </div>
    </div>
  </div>
  <div id="fade" class="black_overlay"></div>
 <!-- /end login box -->