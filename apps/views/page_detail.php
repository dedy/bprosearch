<div class="block-1">
	<div class="content">
        <h2><?=$data->page_title?></h2>
    </div>
</div>

<div class="body">
	<div class="content">
        <div class="row">
            <div class="col-sm-12 left">
                <?=$data->page_content?>
            </div>
        </div>
    </div>
</div>
