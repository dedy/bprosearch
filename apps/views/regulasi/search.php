    <script src="<?=base_url();?>assets/js/jquery/scrollTo/jquery.scrollTo.min.js"></script>

 
 <script>
  $(document).ready(function(){

         <?php
          //default : display detail ID
          if($total_data > 0){?>
              loaddetail(<?=$datas[0]->regulasi_id?>,'id','1');
          <?}?>
        //datepicker
        $('.datepicker').datepicker({ dateFormat: "yy-mm-dd",changeMonth: true, changeYear: true,yearRange: '1950:' + new Date().getFullYear()});
        
        $("#_date_from").click(function(){
               $("#date_from").val("");
           });
        $("#_date_to").click(function(){
               $("#date_to").val("");
           });
        
        $('#input-form input').on('change', function() {
            var optionTanggal = $('input[name=optiontanggal]:checked', '#input-form').val();

            if(optionTanggal == "tahun"){
                //reset tanggal
                $("#date_to").val("");
                $("#date_from").val("");

                //disable tanggal
                $("#date_to").prop('disabled', true);
                $("#date_from").prop('disabled', true);

                //enable tahun
                $("#tahun").prop('disabled', false);

            }else
            if(optionTanggal == "tanggal"){
                //enable tanggal
                $("#date_to").prop('disabled', false);
                $("#date_from").prop('disabled', false);

                //reset tahun
                $("#tahun").val('');

                //disable tahun
                $("#tahun").prop('disabled', true);


            }
        });

      });

      /* load detail */
      function loaddetail(id,lang,offset){
          $("#loadingMessage").show();
          $("#iframe").attr("src", "<?=site_url('regulasi/detail')?>/"+id+"/?lang="+lang+"&offset="+offset);
          $.scrollTo('.body', 800);
          $('iframe').load(function(){
            $("#loadingMessage").hide();
          }).show();


       }

      function detail(id,lang){
          $("#loadingMessage").show();
          $("#iframe").attr("src", "<?=site_url('regulasi/detail')?>/"+id+'/?lang='+lang);
          $.scrollTo('.body', 800);
          $('iframe').load(function(){
            $("#loadingMessage").hide();
          }).show();
       }

       /* view pdf */
      function loadvp(id){
          $("#loadingMessage").show();
          $("#iframe").attr("src", "<?=site_url('regulasi/getfile/pdf')?>/"+id);
          $.scrollTo('.body', 800);
          $('iframe').load(function(){
            $("#loadingMessage").hide();
          }).show();
      }

      /* view history */
       function loadh(id){
          $("#loadingMessage").show();
          $("#iframe").attr("src", "<?=site_url('regulasi/history')?>/"+id);
          $.scrollTo('.body', 800);
          $('iframe').load(function(){
            $("#loadingMessage").hide();
          }).show();
       }

       /* view regulasi terkait */
       function loadr(id){
          $("#loadingMessage").show();
          $("#iframe").attr("src", "<?=site_url('regulasi/rterkait')?>/"+id);
          $.scrollTo('.body', 800);
          $('iframe').load(function(){
            $("#loadingMessage").hide();
          }).show();
       }

        /* view regulasi terkait */
       function loadl(id,lang){
          //$("#loadingMessage").show();
          if(lang ==  "en"){
            $("#iframe").attr("src", "<?=site_url('regulasi/getfile/lampiran')?>/"+id);
          }else{
            $("#iframe").attr("src", "<?=site_url('regulasi/getfileen/lampiran')?>/"+id);
          }

            $.scrollTo('.body', 800);
          $('iframe').load(function(){
            $("#loadingMessage").hide();
          }).show();
       }

</script> 
            



    <div class="block-1">
        <div class="content">
            <p>
              <a href="<?=site_url()?>">Home</a> 
            <?php
            if(isset($data)) { ?>
              / <a href="<?=site_url('regulasi/search')?>/<?=$data->category_slug?>/<?=$data->category_id?>/?s=c"><?=$data->category_name?></a>
              <?}?>
        
              <div>
        
              <!-- ini taruh di kanan --> 
              Total data : <?=number_format($total_data,0,"",".")?>
              <br>
              Upload terakhir : <?=$last_updated?>
              </div>
            </p>
        </div>
    </div><!--block1-->

<div class="block-2">
	<div class="content">
			<div class="row">
            	<div class="col-sm-12">
				  <?php
                  if(isset($data)) { ?>
                      <h2>Pencarian <?=$data->category_name?></h2>
                       <?php 
                              $hidden = array(
                                  "s"         => $s
                              );
                              echo form_open("regulasi/search/".$slug."/".$id."/",array('id'=>'input-form','method'=>'get',"class"      => "form-horizontal",'role'=>"form"),$hidden);
                              ?>

						<?php
                    //display sub category if exists
                    //especially pajak and kurs
                    if(isset($display_category) && $display_category) { ?>          
                      <div class="form-group">
                         <div class="col-sm-2 control-label left">Kategori</div>
                           <div class="col-sm-4 left">
                                <select name='scat' id='scat' >
                                <option value=''>ALL</option>
                                <?php
                                foreach($subcats as $subcat){?>
                                      <option value='<?=$subcat->category_id?>'  <?=($subcat->category_id == $scat)? "selected":"" ?> ><?=$subcat->category_name?></option>
                                  <?}?>
                                </select>
                                       
                        </div>
                      </div><!--group4-->
                    <?}?>
					
                    <?php
                    //display jenis dokumen if exist 
                    if(count($types)) { ?>
                    <div class="form-group">
                         <div class="col-sm-2 control-label left">Jenis Dokumen</div>
                           <div class="col-sm-5 left">
                                <select name='type' id='type' >
                                <option value=''>ALL</option>
                                <?php
                                foreach($types as $type ){?>
                                      <option value='<?=$type->type_id?>'   <?=($type->type_id == $stype)? "selected":"" ?>><?=$type->type_name?></option>
                                  <?}?>
                                </select>
                                       
                        </div>
                      </div>
                     <?}?>
                     

                    <!--group4-->
                   
                   <h2>Metode Pencarian Kata</h2>
	                <div class="form-group">
                      <div class=" controls">

                                  <div class="vd_radio radio-success">
                                  	<div class="col-sm-2 ">
                                    <input type="radio" name="searchMethod" id="optionsRadios1" value="m1" 
                                    <?=$m1_checked?> >
                                    <label  for="optionsRadios1"> Kalimat </label>
                                    </div>
                                    
									<div class="col-sm-3">
                                    <input type="radio" name="searchMethod" id="optionsRadios2" value="m2" <?=$m2_checked?> >
                                    <label  for="optionsRadios2"> Cari Hanya Judul </label>
                                    </div>
									
                                    <div class="col-sm-2">
                                    <input type="radio" name="searchMethod" id="optionsRadios3" value="m3" <?=$m3_checked?> >
                                    <label  for="optionsRadios3"> Semua </label>
                                    </div>
                                  </div>
						</div>          
                    </div><!--group 5-->
                    <div class="form-group"  >
							<div class="col-sm-2 control-label left">Kata Kunci</div>
							<div class="col-sm-3 left"><input type="text" name='q' id='q' value='<?=$q?>'></div>
                    </div>
					<hr />


                    <div class="form-group">
                         <div class="col-sm-2 control-label"> 
                              <div class="vd_radio radio-success left">
                                     <input type="radio" value="tanggal" name="optiontanggal" id="optionTanggal" <?=$optiontanggal_checked?> >
                                     <label for="optionTanggal">Tanggal</label>
                                  </div>
                         </div>
                         <div class="col-sm-2">
                                <div class="input-group">
                                    <input class="input-small datepicker" type="text" name='date_from' id='date_from' value="<?=$date_from?>" <?=$date_from_disabled?> >
                                     <img id="_date_from" border="0" src="<?=base_url()?>assets/admin/img/b_del.gif">
                             
                                     
                                </div>
                         </div>
                         <div class="col-sm-2 control-label right">Sampai</div>
                         <div class="col-sm-2">
                         		<div class="input-group">
                                    <input class="input-small datepicker" type="text" name='date_to' id='date_to' value="" <?=$date_to_disabled?> value="<?=$date_to?>">
                                     <img id="_date_to" border="0" src="<?=base_url()?>assets/admin/img/b_del.gif">
                               </div>
                      </div>
                      
                    </div><!--group1-->
					
                    
                    <div class="form-group">
                         <div class="col-sm-2 control-label">
                              <div class="vd_radio radio-success"> 
                                     <input type="radio" value="tahun" name="optiontanggal" id="optionTahun" <?=$optiontahun_checked?>>
                                     <label for="optionTahun">Tahun</label>
                                  </div>
                         </div>
                         <div class="col-sm-2 left">
                                <div class="input-group">
                                  <select name='tahun' id='tahun' <?=$tahun_disabled?>>
                                    <option value=''></option>
                                    <?php
                                    for($i=date("Y");$i>=1950;$i--){ ?>
                                      <option value='<?=$i?>' <? if($i == $tahun) echo "selected"; ?> ><?=$i?></option>
                                    <?}?>
                                  </select>
                                </div>
                         </div>
                    
                    </div><!--group3-->
                    <hr />
                    <div class="form-group">
                       <div class="col-sm-2 control-label left">Nomor</div>
                       <div class="col-sm-3">
                              <div class="input-group">
                                   <input type="text" placeholder="" name="nomor_start" id='nomor_start' value='<?=$nomor_start?>' ></div>
                    </div>
                       <div class="col-sm-1 control-label left">Sampai</div>
                       <div class="col-sm-3">
                              <div class="input-group">
                                    <input type="text" placeholder="" name="nomor_end" id='nomor_end' value='<?=$nomor_end?>' > </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2">
                          <button class="btn vd_btn vd_bg-blue vd_white" type="submit"><i class="icon-ok"></i> Cari</button> 
                        </div>
                        <div class="col-sm-2">
                          <button class="btn vd_btn vd_black" type="button">Batal</button>
                        </div>
                    </div>
                  
                  </form>

              <?}else{
                if($q){?>
                  <h2>Pencarian dengan kata kunci : "<?=$q?>" </h2>
              <?}
              }?>


            
			</div><!--col12-->
      </div><!--row-->
      </div>
            
            
</div><!--block2-->
  		
<!-- search result here -->      
<div class="body">
	<div class="content">
  <h2>Search Result</h2> 

   <div class="row">
    <?php
          if($total_data > 0) { ?>
            
       		<div class="col-sm-3">
       	      <?php
             
                  $i = ($offset+1);
                  foreach($datas as $data){?>
                      <?=$i?>. 
                      <?php
                      if(!$data->berlaku) { 
                        $font_color = "red";
                      }else{
                        $font_color = "black";
                      }?>

                      <font color="<?=$font_color?>"><?=word_limiter($data->judul, config_item('regulasi_title_word'),' ...');?></font>
                      <br>
                      <?php
                      //echo $data->category_id_parents."|".$data->category_name_parents.";".$data->category_ids.";".$data->category_names."<BR>";
                        //get category parents and category 
                        $cat_ids        = explode(';',$data->category_ids);
                        $cat_names      = explode(';',$data->category_names);
                        $cat_id_parents = explode(';',$data->category_id_parents);
                        $cat_name_parents = explode(';',$data->category_name_parents);
                        $d=0;
                        for($j=0;$j<count($cat_id_parents);$j++){
                            if($cat_id_parents[$j] && $cat_name_parents[$j]){
                                if($d > 0 ){
                                    echo ", ";
                                 }
                                 $d++;
                              ?>
                                <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c' target='_blank'><?=$cat_name_parents[$j]?></a>&nbsp;-&nbsp;<a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c&scat=<?=$cat_ids[$j]?>' target='_blank'><?=$cat_names[$j]?></a>

                            <?php   
                               } else {
                              /* has no parent */
                                if($cat_ids[$j] && $cat_names[$j]) {
                                    if($d > 0 ){
                                        echo ", ";
                                     }
                                     $d++;

                                   ?>
                                 <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_names[$j]))?>/<?=$cat_ids[$j]?>/?s=c' target='_blank'><?=$cat_names[$j]?></a>
                             <?php 
                                 
                                }
                             }

                             ?>

                  <?php } ?>
                        <br>
                        <?=$data->nomor_dokumen?> <br>
                        
                        <?=$data->tanggal_fmt?><br>

                      <a href='javascript:loaddetail("<?=$data->regulasi_id?>","id","<?=$i?>");'>View Detail</a><br>

                      <a href='<?=site_url('regulasi/download/pdf')?>/<?=$data->regulasi_id?>'>Download PDF</a>
                       <?php 
                       /* 
                          Download lampiran kalau ada file lampiran 
                          View kalau pdf, download kalau bukan pdf
                        */
                        if($data->nama_file_lampiran && $data->ukuran_file_lampiran) { ?>
                        <br>
                             
                            <?php    if($data->type_file_lampiran == "application/pdf") {
                              ?>
                                <a href='javascript:loadl("<?=$data->regulasi_id?>","id");'>View Lampiran</a>
                            <?}else{?>
                                <a href='<?=site_url('regulasi/download/lampiran')?>/<?=$data->regulasi_id?>'>Download lampiran</a>
                            <?php }
                        }?>

                      <?php
                      //check if english version is exists
                      if($data->judul_en){ ?>
                          <br>
                          <a href='javascript:loaddetail("<?=$data->regulasi_id?>","en","<?=$i?>");'>View Detail (Eng)</a>
                          <br><a href='<?=site_url('regulasi/downloaden/pdf')?>/<?=$data->regulasi_id?>'>Download PDF (Eng)</a>
                     
                        <?php }
                         /* 
                            Download lampiran kalau ada file lampiran ENGLISH
                            View kalau pdf, download kalau bukan pdf
                          */
                          if($data->nama_file_lampiran_en && $data->ukuran_file_lampiran_en) { ?>
                          <br>
                          <?php    if($data->type_file_lampiran_en == "application/pdf") {
                            ?>
                              <a href='javascript:loadl("<?=$data->regulasi_id?>","en");'>View Lampiran (Eng)</a>
                         <?}else{?>
                              <a href='<?=site_url('regulasi/downloaden/lampiran')?>/<?=$data->regulasi_id?>'>Download lampiran</a>
                         <?php }
                       }?>
                      
                        <?
                        $terkait_exists = $this->regulasi_model->regulasi_terkait_exists($data->regulasi_id);
                        if($terkait_exists) { ?>
                          <br>
                           <a href='javascript:loadr("<?=$data->regulasi_id?>");'>View Peraturan Terkait</a>
                        <?php } ?>
                       <hr>
                  <?
                  $i++;
                  }?>
                  
                  <nav aria-label="Page navigation">
                  <ul class="pagination">
                    <li>
                      <?php echo $this->pagination->create_links(); ?>
                    </li>
                  </ul>
                  </nav>
                  
               </div>
              <div class="col-sm-9" id='regulasi-container'>
                <div id="loadingMessage" style="display:none">Loading...</div>
                 <iframe id="iframe" src="" width="100%" height="350%" frameBorder="0">></iframe>
                </div>
              
      <?php }else{
        //no result found 
        ?>
            No result found
        <?}?>

   </div> <!-- /row -->
   </div>
</div><!--body-->