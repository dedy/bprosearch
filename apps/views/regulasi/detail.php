<script src="<?=base_url();?>assets/js/jquery/scrollTo/jquery.scrollTo.min.js"></script>
 
<?php
//display notifikasi jika sudah tidak berlaku

if(!$data->berlaku) { 
    $font_color = "red";
    if($lang <> "en") {?>
      <font color="<?=$font_color?>">Peraturan tidak berlaku , untuk peraturan pengganti, bisa dilihat di <a href='#dvHistory' id='lnkHistory'>bawah ini</a></font>
    <?php 
    }else{ ?>
      <font color="<?=$font_color?>">Regulation do not apply, for the replacement regulation, can be seen <a href='#dvHistory' id='lnkHistory'>below</a></font>
    <?php ;
    }?>
    <br><br>

<?}else{
    $font_color = "black";
}?>

<strong><?=$offset?>.</strong>

<?php
if($lang <> "en"){ ?>
    <font color="<?=$font_color?>"><?=$data->judul?></font>
<?php } else  { ?>
    <font color="<?=$font_color?>"><?=$data->judul_en?></font>
<?php } ?>
  
  <br>
<?php
 // echo $related_data->category_id_parents."|".$related_data->category_name_parents.";".$related_data->category_ids.";".$related_data->category_names."<BR>";
    //get category parents and category 
    $cat_ids        = explode(';',$data->category_ids);
    $cat_names      = explode(';',$data->category_names);
    $cat_id_parents = explode(';',$data->category_id_parents);
    $cat_name_parents = explode(';',$data->category_name_parents);
    $d=0;
    for($j=0;$j<count($cat_id_parents);$j++){
        if($cat_id_parents[$j] && $cat_name_parents[$j]){
            if($d > 0 ){
                echo ", ";
             }
             $d++;
          ?>
            <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c' target='_blank'><?=$cat_name_parents[$j]?></a>&nbsp;-&nbsp;
    
              <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c&scat=<?=$cat_ids[$j]?>' target='_blank'><?=$cat_names[$j]?></a>

        <?php   
           } else {
          /* has no parent */
            if($cat_ids[$j] && $cat_names[$j]) {
                if($d > 0 ){
                    echo ", ";
                 }
                 $d++;

               ?>
             <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_names[$j]))?>/<?=$cat_ids[$j]?>/?s=c' target='_blank'><?=$cat_names[$j]?></a>
         <?php 
             
            }
         }

         ?>

<?php } ?>
<br>

<?=$data->nomor_dokumen?><br>
<?=$data->tanggal_fmt?><br>

<?php
//display pdf 
if($lang <> "en"){ ?>
    <object data="<?=site_url('regulasi/getfile/pdf')?>/<?=$data->id?>" type="application/pdf" width="100%" height="40%">
      alt : <a href="<?=site_url('regulasi/download/pdf')?>/<?=$data->id?>">Download</a>
    </object>
<?php }else { ?>
  <object data="<?=site_url('regulasi/getfileen/pdf')?>/<?=$data->id?>" type="application/pdf" width="100%" height="40%">
    alt : <a href="<?=site_url('regulasi/downloaden/pdf')?>/<?=$data->id?>">Download</a>
  </object>

<?php } ?> 

    <?php
    if(count($regulasi_histories)) { ?>
      <div id='dvHistory'>
      <h3>History :</h3> 
      <br>

      <?php
      foreach($regulasi_histories as $regulasi_history) {                       
          if(!$regulasi_history->berlaku) { 
            $font_color = "red";
          }else{
            $font_color = "black";
          }?>

            <?php 
            if($lang <> "en"){ ?>
                <font color="<?=$font_color?>"><?=$regulasi_history->judul?></font>
            <?php } else { ?>
                <font color="<?=$font_color?>"><?=$regulasi_history->judul_en?></font>
            <?php }?>
            <br>

            <?php
                         // echo $related_data->category_id_parents."|".$related_data->category_name_parents.";".$related_data->category_ids.";".$related_data->category_names."<BR>";
                            //get category parents and category 
                            $cat_ids        = explode(';',$regulasi_history->category_ids);
                            $cat_names      = explode(';',$regulasi_history->category_names);
                            $cat_id_parents = explode(';',$regulasi_history->category_id_parents);
                            $cat_name_parents = explode(';',$regulasi_history->category_name_parents);
                            $d=0;
                            for($j=0;$j<count($cat_id_parents);$j++){
                                if($cat_id_parents[$j] && $cat_name_parents[$j]){
                                    if($d > 0 ){
                                        echo ", ";
                                     }
                                     $d++;
                                  ?>
                                    <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c' target='_blank'><?=$cat_name_parents[$j]?></a>&nbsp;-&nbsp;<a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c&scat=<?=$cat_ids[$j]?>' target='_blank'><?=$cat_names[$j]?></a>

                                <?php   
                                   } else {
                                  /* has no parent */
                                    if($cat_ids[$j] && $cat_names[$j]) {
                                        if($d > 0 ){
                                            echo ", ";
                                         }
                                         $d++;

                                       ?>
                                     <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_names[$j]))?>/<?=$cat_ids[$j]?>/?s=c' target='_blank'><?=$cat_names[$j]?></a>
                                 <?php 
                                     
                                    }
                                 }

                                 ?>

                      <?php } ?>

                        <br>
                          <a href='<?=site_url('regulasi/detail')?>/<?=$regulasi_history->regulasi_id?>'><?=$regulasi_history->nomor_dokumen?></a> <br>
                          <?=$regulasi_history->tanggal_fmt?><br>
                          <hr>
      <?}?>
  </div>    
<?}?>    


<?php
if(count($related_datas)) { ?>

<BR><BR>
related  : <BR><BR>

  <?php
  foreach($related_datas as $related_data) {                       
      if(!$related_data->berlaku_terkait) { 
        $font_color = "red";
      }else{
        $font_color = "black";
      }?>
    
      <?php 
      if($lang <> "en"){ ?>
          <font color="<?=$font_color?>"><?=$related_data->judul_terkait?></font>
      <?php } else { ?>
          <font color="<?=$font_color?>"><?=$related_data->judul_terkait_en?></font>
      <?php } ?>
        <br>

        <?php
                     // echo $related_data->category_id_parents."|".$related_data->category_name_parents.";".$related_data->category_ids.";".$related_data->category_names."<BR>";
                        //get category parents and category 
                        $cat_ids        = explode(';',$related_data->category_ids);
                        $cat_names      = explode(';',$related_data->category_names);
                        $cat_id_parents = explode(';',$related_data->category_id_parents);
                        $cat_name_parents = explode(';',$related_data->category_name_parents);
                        $d=0;
                        for($j=0;$j<count($cat_id_parents);$j++){
                            if($cat_id_parents[$j] && $cat_name_parents[$j]){
                                if($d > 0 ){
                                    echo ", ";
                                 }
                                 $d++;
                              ?>
                                <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c' target='_blank'><?=$cat_name_parents[$j]?></a>&nbsp;-&nbsp;<a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_name_parents[$j]))?>/<?=$cat_id_parents[$j]?>/?s=c&scat=<?=$cat_ids[$j]?>' target='_blank'><?=$cat_names[$j]?></a>

                            <?php   
                               } else {
                              /* has no parent */
                                if($cat_ids[$j] && $cat_names[$j]) {
                                    if($d > 0 ){
                                        echo ", ";
                                     }
                                     $d++;

                                   ?>
                                 <a href='<?=site_url('regulasi/search')?>/<?=strtolower(url_title($cat_names[$j]))?>/<?=$cat_ids[$j]?>/?s=c' target='_blank'><?=$cat_names[$j]?></a>
                             <?php 
                                 
                                }
                             }

                             ?>

                  <?php } ?>

                    <br>
                      <a href='<?=site_url('regulasi/detail')?>/<?=$related_data->regulasi_id_terkait?>'><?=$related_data->nomor_terkait?></a> <br>
                      <?=$related_data->tanggal_terkait_fmt?><br>
                      <hr>
  <?}?>
<?}?>                               
