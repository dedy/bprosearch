
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script>
    function doLogout(){
      $.confirm({
            title:"<?=lang('llogout')?>",
            text: "<?=lang('lconfirm_logout')?> ",
            confirmButton: "<?=lang('lok')?>",
            cancelButton: "<?=lang('lcancel')?>",
            confirm: function(button) {
                window.location.href =  "<?=site_url("auth/logout")?>/";
            },
            cancel: function(button) {
               // alert("You cancelled.");
            }
        });
    }
</script>
<div class="row">
	<div class="col-md-2"><a href='<?=site_url()?>'><img src="<?php echo base_url(); ?>assets/images/logo-bpro-240x120.png" /></a></div>
    <div class="col-md-10 navmenu">
    	<a href="<?=base_url()?>about-us">About Us</a> 
    	| <a href="http://www.bpro-training.com" target="_blank">B-PRO training</a>
    	<?php
    	if(!isLoggedIn()){ ?> 
            | <a href="javascript:void(0)" onclick="createlightbox()">Login</a>
            | <a href="<?=site_url('account/register/')?>">Register</a>
    	<?}else{
        //display logout button
         ?>
            | <a href="<?=site_url('my-profile')?>">My Profile</a>
            | <a href="javascript:void(0)" onclick="doLogout();">Logout</a>

        <?php } ?>

    </div>
</div>
        