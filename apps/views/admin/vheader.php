
<!-- jquery confirm -->
<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="<?=base_url()?>assets/admin/js/bootstrap.min.js"></script>
<script>

	 jQuery().ready(function (){
            $("#logoutid").confirm({
                title:"<?=lang('llogout_confirmation_title')?>",
                text:"<?=lang('lconfirm_logout')?>",
                confirm: function(button) {
                    //alert("You just confirmed.");
                    window.location.href = '<?=site_url('auth/logout')?>';
                },
                cancel: function(button) {
                   // alert("You aborted the operation.");
                },
                confirmButton: "OK",
                cancelButton: "Cancel"
            });
        
    
    });
        
        


</script>
    
<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="<?=site_url('admin');?>"><span><?=config_item('backend_header_title')?></span></a>
			
			<!-- user dropdown starts -->
			<div class="btn-group pull-right" >
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="icon-user"></i><span class="hidden-phone"> <?=_UserFullName()?> </span>
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
                    <li><a href="<?=site_url('admin/user/change_password')?>"><?=lang('lchange_password')?></a></li>
					<li class="divider"></li>
					<li><a id="logoutid">Logout</a></li>
				</ul>
			</div>
			<!-- user dropdown ends -->
			
		</div>
	</div>
</div>