<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title><?=config_item('backend_header_title')?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  	<!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">-->
	
    <!-- The styles -->
	<link id="bs-css" href="<?=base_url();?>assets/admin/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="<?=base_url();?>assets/admin/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/admin/css/charisma-app.css?v2" rel="stylesheet">
	<link href="<?=base_url();?>assets/admin/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
    
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="<?=base_url();?>assets/js/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url();?>assets/js/jquery/jquery.cookie.js"></script>
    
    <script src="http://code.jquery.com/jquery-migrate-1.3.0.js"></script>
    
	<!-- jQuery UI -->
	<script src="<?=base_url();?>assets/admin/js/jquery-ui-1.8.21.custom.min.js"></script>
    
    <!-- custom dropdown library -->
	<script src="<?=base_url();?>assets/admin/js/bootstrap-dropdown.js"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(function() {
            $.ajaxSetup({
                data: {
                    csrf_token : $.cookie('csrf_cookie')
                }
            });

            $(document).ajaxError(function() {
                alert('There is something wrong with the system. \r\n If the problem persist, please contact administrator');
                //window.location.reload(true);
            });

			 $(document).ajaxStart(function() {
			 		$.post('<?=site_url('auth/checkLogin');?>/'+Math.random(), function(returData) {
			 			 if(returData.isLoggedIn == false){
					   	 	alert('<?=lang('lsession_timeout_please_relogin')?>');
					      	window.location.replace('<?=site_url('auth/logout');?>');
					    };
					},'json');
			});
		});
	</script>
</head>

<body>
		<!-- topbar starts -->
		<?=$header?>
		<!-- topbar ends -->
		<div class="container-fluid">
			<div class="row-fluid">
				
				<!-- left menu starts -->
				<?=$menunavigation?>
				<!-- left menu ends -->
				
				<noscript>
					<div class="alert alert-block span10">
						<h4 class="alert-heading">Warning!</h4>
						<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
					</div>
				</noscript>
			
				<div id="content" class="span10">
				<!-- content starts -->
					<?=$content?>
				<!-- content ends -->
				</div><!--/#content.span10-->
			</div><!--/fluid-row-->
		<hr>
		<footer>
			<p class="pull-left"><?=config_item('copyright')?></p>
			<p class="pull-right">&nbsp;</p>
		</footer>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- application script for Charisma demo -->
	<script src="<?=base_url();?>assets/admin/js/charisma.js"></script>
	
		
</Body>
</html>
