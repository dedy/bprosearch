<div class="span2 main-menu-span">
	<div class="well nav-collapse sidebar-nav">
		<ul class="nav nav-tabs nav-stacked main-menu">
                <li class="nav-header hidden-tablet">Back end Menu</li>
                <li>
                        <a class="ajax-link" href="<?=site_url('admin/dashboard')?>"><i class="icon-list"></i><span class="hidden-tablet"><?=lang('ldashboard')?></span></a>
                </li>

                <?php
                if(count($menus)){
                        foreach($menus as $menu){
                                $displayed = true;
                                
                                //menu with read access
                                if(!isAdmin()){
                                    $permissions = json_decode($menu->permissions);
                                    if($permissions->R){
                                        $displayed = true;
                                     }else{
                                        $displayed = false;
                                     }
                                }

                                if($displayed) {
                                ?>
                                <li>
                                        <a class="ajax-link" href="<?=site_url('admin')?>/<?=$menu->menu_url?>"><i class="icon-list"></i><span class="hidden-tablet"><?=$menu->menu_title?></span></a>
                                </li>
                                <?php }
                        }
                }?>
                </ul>
	</div><!--/.well -->
</div><!--/span-->
