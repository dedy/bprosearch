<html>
<head>
<title><?=$website_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="description" content="<?=$website_description?>">
<meta name="keywords" content="<?=$website_keyword?>">

	<link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.png">
    <!-- CSS -->
    <link href="<?=base_url()?>assets/css/style-bpro.css?v=0.5" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/css/webflow.css" rel="stylesheet" type="text/css">
    
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.min.css?v=0.2" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome-ie7.min.css"><![endif]-->
    <link href="<?=base_url()?>assets/css/font-entypo.css" rel="stylesheet" type="text/css">    

    <!-- Fonts CSS -->
    <link href="<?=base_url()?>assets/css/fonts.css"  rel="stylesheet" type="text/css">
               
    <!-- Plugin CSS -->
    <link href="<?=base_url()?>assets/plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="<?=base_url()?>assets/plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="<?=base_url()?>assets/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="<?=base_url()?>assets/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="<?=base_url()?>assets/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="<?=base_url()?>assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->
	<link href="<?=base_url()?>assets/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css"><link href="<?=base_url()?>assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/plugins/bootstrap-wysiwyg/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet" type="text/css">    
     
    <!-- Theme CSS -->
    <link href="<?=base_url()?>assets/css/theme.min.css?v=0.2" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="<?=base_url()?>assets/css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="<?=base_url()?>assets/css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    
        
    <!-- Responsive CSS -->
	<link href="<?=base_url()?>assets/css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 

    <!-- for specific page in style css -->
        
    <!-- for specific page responsive in style css -->
           
    <!-- Custom CSS -->
    <link href="<?=base_url()?>assets/custom/custom.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="<?=base_url();?>assets/js/jquery/jquery-1.9.1.min.js"></script>
    <script src="<?=base_url();?>assets/js/jquery/jquery.cookie.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>

   <!-- jQuery UI -->
    <script src="<?=base_url();?>assets/admin/js/jquery-ui-1.8.21.custom.min.js"></script>
    
   <script type="text/javascript" charset="utf-8">
        $(function() {
            $.ajaxSetup({
                data: {
                    csrf_token : $.cookie('csrf_cookie')
                }
            });

            $(document).ajaxError(function() {
                alert('There is something wrong with the system. \r\n If the problem persist, please contact administrator');
                //window.location.reload(true);
            });

        });
    </script>

</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<?=$login_box?>
<div id="outer">
	<div id="inner">
    	<div id="header">
        	<?=$header?>
        </div><!--header-->

   	  <div id="body">
   	  	<?=$content?>
   	  </div><!-- id body -->

	    <div id="footer">
      		<p>Copyright 2016 - Yayasan Berdikari Bagi Bangsa</p>
      	</div>
	</div><!-- /inner -->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101353801-1', 'auto');
  ga('send', 'pageview');

</script>
</div>
</body>
</html>