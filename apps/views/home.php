<script type="text/javascript" charset="utf-8">
	$('.SeeMore2').click(function(){
		var $this = $(this);
		$this.toggleClass('SeeMore2');
		if($this.hasClass('SeeMore2')){
			$this.text('&or;');			
		} else {
			$this.text('&and;');
		}
	});

    $(function() {

      $("input").on('keydown', function(evt) {
          if (evt.which === 13 && this.value.length < 2) {
              return false;
          }
      });
    });
</script>
      	<div class="block-1">	
        	<div class="row">
            	<div class="col-md-12">
                	<?

              					if(isset($data)){
              						echo $data->page_content;
              					}
              					?>
              </div>

            </div>
        </div><!--block1-->
        <div class="block-2">
          	     <?php 
                  $hidden = array(
                      "s"             => "g" // s = source , c(ategory) or g(lobal)/home

                  );
                  echo form_open("regulasi/search/",array('id'=>'input-form','method'=>'get'),$hidden);
                  ?>
                       <div class="row">
                  		<div class="col-md-4"><h2>B-PRO ALL SEARCH OF REGULATIONS</h2></div>
                          <div class="col-md-8">
                            <div class="controls">
                            	
                                   <input type="text" id='q' name='q' placeholder="Keyword">
                             
                            </div>
                          </div>
      					<!--row 8-->
              	</div>
                <!-- 
                <div class="row">
                	<div class="col-md-4"><p class="right">Sort By</p></div>
                	<div class="col-md-8">
    	                <div class="form-group row">
                          <div class=" controls">
                                      <div class="vd_radio radio-success">
                                      	<div class="col-sm-3 ">
                                          <input type="radio" name="sortBy" id="sortBy1" value="tanggal" checked>
                                          <label  for="optionsRadios1">Latest Date </label>
                                        </div>
                                        
    									                 <div class="col-sm-3">
                                          <input type="radio" name="sortBy" id="sortBy2" value="category">
                                          <label  for="optionsRadios2"> Category </label>
                                        </div>
    									
                                      </div>
    						          </div>          
                        </div><!--group 5-->
                   <!--  </div>
                </div>
                -->
            </form>

        </div><!--block2-->
  		<div class="body">
           <div class="row">
           		<div class="col-md-12">
                
                <!--acc-->
                
                <div class="accordion" id="accordion2">
  
                   <div class="accordion-group">
                     <h2>Pilih Kategori</h2>
                      
                      <div class="accordion-heading row">
                            <?
                            $displayed = 16;
                            if(count($categories) > $displayed){
                               $i_counter = $displayed;
                            }else{
                              $i_counter = count($categories);
                            }
                            for($i=0;$i<$i_counter;$i++){
                                $category = $categories[$i];
                             ?>
                                <div class="col-sm-3"> 
                                    <a class="btn vd_btn vd_bg-yellow vd_white" href="<?=site_url('regulasi/search')?>/<?=$category->category_slug?>/<?=$category->category_id?>/?s=c" title='<?=$category->category_name?>'><?=$category->category_name?></a> 
                                </div>
                           <? }?>
                      </div>

                      <?php
                      //kalau lebih dari yang mau di tampilkan, munculkan more
                      if(count($categories) > $displayed){ ?>
                        <div id="collapseTwo" class="accordion-body collapse">
                            <div class="row">
                             <?
                              for($i=$i_counter;$i<count($categories);$i++){
                                  $category = $categories[$i];  ?>
                                        <div class="col-sm-3"> 
                                            <a class="btn vd_btn vd_bg-yellow vd_white" href="<?=site_url('regulasi/search')?>/<?=$category->category_slug?>/<?=$category->category_id?>/?s=c" title='<?=$category->category_name?>'><?=$category->category_name?></a> 
                                        </div>
                                   <? }?>
                              </div>
                          </div>
                          <div class="clear"></div>
                        <div class="row">  
                            <div class="col-sm-12"><button class="SeeMore2 btn btn-primary vd_bg-blue" data-toggle="collapse" href="#collapseTwo">More</button></div>
                        </div>
                      <?php }?>
                  </div>
                
                </div>
                
                
                <!--end-->
                
                

       
           
           		</div>
           
           
           
           		
           </div><!--row-->
          
           <div class="row">
            <hr />
           		<div class="col-md-6">
                	<h2>Peraturan Terpopuler</h2>
                  <?php
                  if(count($popular_regulation)){ 
                    foreach($popular_regulation as $pop){ ?>
                      <div class="forum">
                        <div class="col-md-12">
                              <p>
                              <a href='<?=base_url()?>regulasi/search/?s=g'><?=word_limiter($pop->judul, config_item('regulasi_title_word'),' ...');?></a><br />
                              <a href='<?=base_url()?>regulasi/search/?s=g'><?=$pop->nomor_dokumen?></a><br/>
                              <a href='<?=base_url()?>regulasi/search/?s=g'><?=$pop->tanggal_fmt?></a><!-- <a href="#" class="read">Read more</a>--></p>
                          </div>
                      </div>
                    <?php } 
                  }?>
                  <!--end forum-->
                </div><!--end col6-->

           		<div class="col-md-6">
                	<h2>Peraturan Terbaru</h2>
                  <?php
                  if(count($latest_regulation)){ 
                    foreach($latest_regulation as $reg){ ?>
                      <div class="forum">
                      	<div class="col-md-12">
                              <p>
                              <a href='<?=base_url()?>regulasi/search/?s=g'><?=word_limiter($reg->judul, config_item('regulasi_title_word'),' ...');?></a><br />
                              <a href='<?=base_url()?>regulasi/search/?s=g'><?=$reg->nomor_dokumen?></a><br/>
  								            <a href='<?=base_url()?>regulasi/search/?s=g'><?=$reg->tanggal_fmt?></a><!-- <a href="#" class="read">Read more</a>--></p>
                          </div>
                      </div>
                    <?php } 
                  }?>
                </div><!--end col6-->
           </div><!--end row-->
        </div><!--class body-->
        
<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>