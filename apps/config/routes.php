<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'auth';
$route['default_controller'] = 'home';
$route['about-us'] = 'page/detail/about-us';
$route['please-login'] = 'page/detail/please-login';
$route['not-authorized'] = 'page/detail/not-authorized';
$route['membership-expired'] = 'page/detail/membership-expired';

$route['my-profile'] = 'account/profile';

//$route['404_override'] = 'page/show_404';
//$route['page-not-found'] = 'page/show_404';

$route['translate_uri_dashes'] = false;

$route['admin/([a-zA-Z_-]+)/(:any)'] = '$1/admin/$2';
$route['admin/([a-zA-Z_-]+)/(:any)/(:any)'] = '$1/admin/$2/$3';
$route['admin/([a-zA-Z_-]+)/(:any)/(:any)/(:any)'] = '$1/admin/$2/$3/$4';
$route['admin/([a-zA-Z_-]+)/(:any)/(:any)/(:any)/(:any)'] = '$1/admin/$2/$3/$4/$5';

$route['admin/login'] = 'auth/login';
$route['admin/([a-zA-Z_-]+)'] = '$1/admin/index';
$route['admin'] = 'auth/login';
$route['forgot_password'] = 'auth/forgot_password';

// Users
//$route['admin/users/user_edit/(:any)'] = "admin/user/users/user_edit/$1";