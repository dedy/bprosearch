<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['website_title']			= "B-Pro Search Application";
$config['website_description']		= "B-Pro Search Application";
$config['backend_header_title']		= "B-Pro Search Application";
$config['backend_welcome_title']		= "Welcome to B-Pro Search Admin Panel";
$config['copyright']        =   "&copy; <a href='http://www.bpro-training.com' target='_blank'>B-Pro</a> 2016";
    
/* grid option */
$config['rowNum']		= 25; //number of row per page
$config['rowList']		= "25,50,75"; //option for row per page
$config['rowHeight']	= 480;
$config['rowWidth']	= 1024;

//transmit file config
$config['start_year']   = 2010;
$config['number_of_uploads']    = 5;

$config['temp_path'] = APPPATH.'../temp/';
$config['upload_path'] = APPPATH.'../upload/';

//ckeditor config
$config['ck_height']    = 500;
$config['ck_width']     = 1024;

$config['message_delay']     = 2500;
$config['number_of_category']   = 6; //total category

$config['pdf_author']   = "Bpro-Training";
$config['regulasi_file_type_allowed']   = "pdf|doc|docx|zip|xls|xlsx|rar";
$config['max_size_regulasi_upload']   = 1024*100; // 5MB

$config['months'] = array(
	"Januari"
	,"Februari"
	,"Maret"
	,"April"
	,"Mei"
	,"Juni"
	,"Juli"
	,"Agustus"
	,"September"
	,"Oktober"
	,"November"
	,"Desember"
);

//number of data per page Front end
$config['limit_perpage'] = 10;

$config['regulasi_title_word']	= 15;

$config['default_package']	= 1; // default package id when registeration

$config['limit_latest_home'] = 25; //number of latest regulation at home 

$config['limit_popular_home'] = 25; //number of popular regulation at home 