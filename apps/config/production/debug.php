<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['debug'] = false;
$config['debug_email'] = false;

/*
 * sending_mail_log = true, then sending email to dev for error
 */
$config['sending_mail_log'] = false;

/*
 * Transmit sending email enable 
 */
$config['sending_mail'] = true;
$config['enable_recaptcha']     = false;

