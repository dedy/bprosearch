<?php

function isLoggedIn(){
	$CI =& get_instance();
	return $CI->ion_auth->logged_in();
}

function isAdmin(){
	$CI =& get_instance();
	return $CI->ion_auth->is_admin();
}

// user group = 2 is member
function isMember(){
    $CI =& get_instance();
	if ($CI->ion_auth->in_group(2)){
		return true;
    }
    return false;
}

// user group = 2 is member
function isStaff(){
    $CI =& get_instance();
	if ($CI->ion_auth->in_group(3)){
		return true;
    }
    return false;
}

function userGroupId(){
    $CI =& get_instance();
	$user_groups = $CI->ion_auth->get_users_groups()->result();
	return $user_groups[0]->id;
}

/* 
 * Get actice user country
 */
/*function _userCountryName(){
    $CI =& get_instance();
    return $CI->session->userdata('country_name');
}


/* 
 * Get actice user country
 */
/*function _userCountryId(){
    $CI =& get_instance();
    return $CI->session->userdata('country_id');
}

function _userCountryCode(){
    $CI =& get_instance();
    return $CI->session->userdata('country_code');
}
*/

function _UserName(){
	$CI =& get_instance();
	$user = $CI->ion_auth->user()->row();
	return $user->username;

}

function _UserFullName(){
	$CI =& get_instance();
	$user = $CI->ion_auth->user()->row();
	return $user->full_name;

}

function _UserId(){
	$CI =& get_instance();
	$user = $CI->ion_auth->user()->row();
	return $user->id;
}

function _UserPackage(){
	$CI =& get_instance();
	return $CI->session->userdata('user_package_id');
        echo ";".$this->session->userdata('user_package_enddate');
}

function _UserPackageEnddate(){
	$CI =& get_instance();
	return $CI->session->userdata('user_package_enddate');
}

?>