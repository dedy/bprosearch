<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_image($path){
	$size = getimagesize($path);
	if($size[0] > 0)
		return true;
	else
		return false;
}

function resize_image($source_filename,$destination_filename,$width='',$height='',$source_path=''){
	$CI =& get_instance();
	
	unset($config);
  	$config['width']     		= ($width)?$width:config_item('thumbnail_width') ;
   	$config['height']     		= ($height)?$height:config_item('thumbnail_height') ;
  	$config['image_library'] 	= 'gd2';
	$config['source_image']		= $source_path.'/'.$source_filename;
	$config['create_thumb'] 	= false;
	$config['new_image'] 		= $source_path.'/'.$destination_filename;
	$config['maintain_ratio'] 	= TRUE;
	log_message('debug', 'dedy : create image '.$width.";".$height.";".$config['new_image']);
	$CI->image_lib->initialize($config); 
	$CI->image_lib->resize();
	$CI->image_lib->clear();
}

function get_thumbname($filename,$path=''){
	return "thumb_".$filename;
}

function get_resizename($filename,$path=''){
	return "res_".$filename;
}