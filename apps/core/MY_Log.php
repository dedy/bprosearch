<?php

class MY_Log extends CI_Log {

    public function __construct(){

        parent::__construct();

    }

    function write_log($level = 'error', $msg, $php_error = FALSE){
        
        $result = parent::write_log($level, $msg, $php_error);
        if ($result == TRUE && strtoupper($level) == 'ERROR') {
            $CI =& get_instance();
            $sending_mail_log = $CI->config->item('sending_mail_log');
 
            if($sending_mail_log) {
                $message = "An error occurred: <br><br><br>";
                $message .= $level.' - '.date($this->_date_fmt). ' --> '.$msg."<br><br>";
                $message .= "url : ".base_url();

                $to = $CI->config->item('admin_email_address');
                $subject = $CI->config->item('admin_email_error_subject');
                $from_name = $CI->config->item('sender_name');
                $from_address = $CI->config->item('sender_mail');
              
                //email the error
                $CI->load->library('email');

                //email configuration
                $CI->email->from($from_name,$from_address);
                $CI->email->to($to);
                $CI->email->subject($subject);
                $CI->email->message($message);

                if($CI->email->send()){
                     $sendmail = true;
                }
                //mail($to, $subject, $message, $headers);
            }

        }

        return $result;

    }
}

?>