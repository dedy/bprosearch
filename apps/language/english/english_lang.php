<?php

$lang['ladd_group']		= "Add User Group";
$lang['llist_group']	= "User Group List";
$lang['lmodify_group']	= "Modify User Group";
$lang['lpermissions']	= "Permission";

$lang['lplease_select_package']	= "Please select package";
$lang['lmember_package']		= "Member Package";
$lang['lpackage_end_date']			= "Package End date";
$lang['lregister'] = "Register";
$lang['linvalid_permission']	= "Invalid Permission";
$lang['ldata_failed_updated']	= "Data Gagal diubah";
$lang['lpackage']		= "Paket";
$lang['lall_category']		= "Semua Kategori";
$lang['lmonth']		= "Bulan";
$lang['ladd_package']		= "Tambah Paket";
$lang['lmodify_package']	= "Ubah Paket";
$lang['lduration']			= "Durasi";
$lang['lprice']			= "Harga";
$lang['ladd_package']		= "Tambah Paket";
$lang['llist_packages']		= "Daftar Paket";
$lang['lmember_register_success']	= "Member Registration Successful";
$lang['lfullname']					= "Full Name";
$lang['liwant_to_create_account']	= "I want to create an account with my Email";
$lang['lback_to_login']			= "Login Page";
$lang['llog_into_your_account']	= "Log into your account";
$lang['ldelete_terkait']	= "Hapus Peraturan terkait";
$lang['lregulasi_terkait_exists']  = "Peraturan Terkait sudah ada";
$lang['lnomor']		= "Nomor";
$lang['ldata_is_protected']		= "Data ini tidak bisa dihapus";
$lang['lmodify_page']		= "Ubah Halaman";
$lang['limport_time']      = "Waktu Import";
$lang['limport_by']      = "Diimport Oleh";
$lang['limport_detail']      = "Detail Import";
$lang['lview_detail']      = "Lihat Detil";
$lang['lfilename']      = "Nama File";
$lang['lcreated_by']   = "Dilakukan oleh";
$lang['lfinished_on']   = "Waktu Selesai";
$lang['lstarted_on']   = "Waktu Mulai";
$lang['limport_log']   = "Import Log";
$lang['limport_confirmation']   = "Apakah anda yakin ingin mengimport data dalam file(s) ?";
$lang['lfiles_found']   = "File(s) ditemukan";
$lang['limport']   = "Import Regulasi";
$lang['ldelete_page']   = "Hapus Page";
$lang['ldelete_confirmation']   = "Apa anda yakin ingin menghapus data ini?";

$lang['form_validation_lnama_file_dokumen']       = "Nama File Dokumen";
$lang['ldokumen_lampiran']       = "Dokumen Lampiran";
$lang['lperaturan_pengganti']       = "Peraturan Pengganti";
$lang['lplease_select_regulasi_pengganti']  = "Peraturan Pengganti harap dipilih";
$lang['lplease_select_category']    = "Harap Pilih Kategori Peraturan";
$lang['ladd_regulasi']              = "Tambah Regulasi";
$lang['ltidak_berlaku']              = "Tidak Berlaku";
$lang['lperihal']              = "Perihal";
$lang['lview_pdf']              = "View PDF";
$lang['ldownload_pdf']              = "Download PDF";
$lang['lsave']              = "Save";
$lang['lplease_select_regulasi_terkait']              = "Harap pilih regulasi terkait";
$lang['lperaturan_terkait']              = "Regulasi terkait";
$lang['ldokumen_regulasi']              = "Dokumen Regulasi";
$lang['linactive']              = "Tidak Aktif";

$lang['ljenis_dokumen_exists']          = "Jenis Dokumen sudah ada";
$lang['lplease_select_jenis_dokumen']   = "Harap pilih jenis dokumen";

$lang['lno']			= "Tidak";
$lang['lnama_file_dokumen']  = "Nama File Dokumen";
$lang['lisi_dokumen']       = "Isi Dokumen";
$lang['ljenis_dokumen']     = "Jenis Dokumen";
$lang['lberlaku']     = "Berlaku";
$lang['lperihal']     = "Perihal";
$lang['lnomor_dokumen']     = "Nomor Dokumen";
$lang['lnama_jenis_dokumen']     = "Nama Jenis Dokumen";
$lang['llist_jenis_dokumen']    = "Daftar Jenis Dokumen";
$lang['ladd_jenis_dokumen']     = "Tambah Jenis Dokumen";
$lang['lmodify_jenis_dokumen']     = "Ubah Jenis Dokumen";
$lang['ldelete_jenis_dokumen']     = "Hapus Jenis Dokumen";
$lang['ladd_sub_kategory']     = "Tambah Sub Kategori Peraturan";
$lang['ledit_sub_kategory']     = "Ubah Sub Kategori Peraturan";
$lang['lsub_kategory']     = "Sub Kategori Peraturan";
$lang['lconfirm_delete']				= "Apa anda yakin ingin menghapus data ini?";

$lang['lregulasi']              = "Peraturan";
$lang['llist_regulasi']              = "Daftar Peraturan";
$lang['ladd_regulasi']              = "Tambah Peraturan";

$lang['lcategory_peraturan']    = "Kategori Peraturan";
$lang['ldashboard']			= "Dashboard";
$lang['lplease_login'] 		= "Please login with your Email and Password.";
$lang['lusername']			= "User Name";
$lang['lpassword']			= "Password";
$lang['lsubmit']			= "Submit";
$lang['llogin']				= "Login";
$lang['lemail']				= "Email";
$lang['login_username_label']       = "Username";
$lang['ltoo_many_login_attempts']   = "You have too many login attempt";
$lang['lfield']         = "Field";
$lang['lregister_user'] = "Register New User";
$lang['luser_name']     = "User Name";        
$lang['linstitution']   = "Institution";
$lang['lfields']        = "Field";
$lang['lplease_select_fields']  = "Please select fields";
$lang['lplease_select_field']  = "Please select field";
$lang['lplease_select_country']  = "Please select country";
$lang['lsupervisor_name']  = "Supervisor name";
$lang['lsupervisor_email']  = "Supervisor email";
$lang['lusername_exists']  = "Username already exists";
$lang['lset_inactive_confirmation'] 	= "Apa anda yakin mau men-nonaktifkan data ini?";
$lang['lset_active_confirmation'] 		= "Apa anda yakin mau mengaktifkan data ini?";
$lang['lchange_status_confirmation']    = "Konfirmasi perubahan status aktif";
$lang['lstatus_is_changed']    = "Status untuk data ini sudah berubah";

$lang['luser_fullname']                 = "Full Name";
$lang['lfields_required']                 = "The Fields field is required";
$lang['lset_inactive']			= "Set Inactive";
$lang['lset_active']			= "Set Active";
$lang['lplease_fill_in_email']		= "Please fill in the email!";
$lang['lplease_fill_in_password']	= "Please fill in the password!";
$lang['linvalid_login_pass']		= "Invalid email or password!";
$lang['llogin_successful']			= "Login successful";
$lang['lconfirm_logout']			= "Are you sure to logout?";
$lang['llogout']			= "Logout";

$lang['ltitle']			= "Judul";
$lang['lstatus']		= "Status";
$lang['llast_updated']	= "Last update";
$lang['lupdated_by']	= "Updated by";

$lang['lpublish']		= "Publish";
$lang['lunpublish']		= "Unpublish";

$lang['lconfirm_set_active']	= "Are you sure to publish this data?";
$lang['lconfirm_set_inactive']	= "Are you sure to unpublish this data?";

$lang['ldata_isset_active']				= "Data has been set to published";
$lang['ldata_isset_inactive']			= "Data has been set to unpublished";

$lang['lactive']					= "Active";
$lang['lview']			= "View";
$lang['ledit']			= "Edit";
$lang['ldel']			= "Del";
$lang['ldelete']		= "Delete";
$lang['ldata_is_deleted']				= "Data deleted";

$lang['lupload']       = "Upload";
$lang['lreporting_country'] = "Reporting Country";
$lang['lperiod']    = "Period";
$lang['lcomment']   = "Comment";
$lang['lupload_data_to_asean']  = "Upload data to ASEAN Secretariat";
$lang['lplease_select_type']    = "Please select a type";
$lang['lreport']    = "Report";
$lang['lfields_are_mandatory']  = "Fields with (*) are mandatory";
$lang['lplease_select_at_least_1_file'] = "Please enter the information for at least one file";
$lang['ltransmission_result']   = "Transmission Result";
$lang['lparent']		= "Parent";
$lang['lcategory_name']	= "Category name";
$lang['lnone']			= "None";
$lang['lplease_fill_in_title']	= "Please fill in the title!";
$lang['lplease_fill_in_category_name']	= "Please fill in the category name!";
$lang['ldata_success_inserted']	= "Data insertion successful"; 
$lang['ldata_failed_inserted']	= "Data insertion failed";

$lang['ldata_success_updated']	= "Data update successful";

$lang['ldisplay_order']				= "Order";
$lang['lsession_timeout_please_relogin']	= "Session timeout, please relogin!";
$lang['ldescription']		= "Description";

$lang['lstring_name']		= "Nama %s";
$lang['lstring_list']		= "Daftar %s";
$lang['lstring_add']		= "Tambah %s";
$lang['lstring_modify']		= "Ubah %s";

$lang['lsave_changes_and_close'] = "Save & Close";
$lang['lsave_changes_and_add_new'] = "Save & Add New";
$lang['lcancel']		= "Cancel";
$lang['lok']		= "Ok";

$lang['lmenu']			= "Menu";
$lang['lmenu_category'] = "Menu Category";
$lang['limage']			= "Image";
$lang['lonly_numeric']	= "Only numeric input are allowed!";
$lang['ldate_publish']	= "Date publish";
$lang['luntil']			= "Until";
$lang['lurl']			= "Url";
$lang['lurl_tooltips']	= "Example : http://www.detik.com";
$lang['lopen_window']	= "Window open";
$lang['lsame_window']	= "Same window";
$lang['lnew_window']	= "New window";
$lang['lplease_select']	= "--- Please Select -------------";	

$lang['lempty_status']  = "Empty Status";
$lang['lall_status']    = "All Statuses";
$lang['lall_type']      = "All Types";
$lang['lall_fields']    = "All Fields";
$lang['lall_year']      = "All Years";
$lang['lempty_year']    = "Empty Year";
$lang['lall_period']      = "All Periods";
$lang['lempty_period']    = "Empty Period";
$lang['lfilename_original'] = "Original Filename";
$lang['ldtt_filename'] = "DTT Filename";
$lang['lall_country']   = "All Countries";
$lang['lall_user']   = "All Users";
$lang['lyear']      = "Tahun";
$lang['llist_email']    = "Email List";
$lang['lmodify_email']  = "Modify Email";
$lang['lall_types']     = "All Types";
$lang['lchange_password']   = "Change Password";
$lang['llist_status']       = "List Status";
$lang['lorder']				= "Order";
$lang['ladd_status']        = "Add Status";
$lang['lmodify_status']        = "Modify Status";
$lang['ltransmitted']       = "Transmitted";
$lang['ltransmitted_at']       = "Transmitted at";
$lang['ltransmitted_from']       = "Transmitted from";
$lang['ldtt_uploading_report']  = "DTT Uploading Report";
$lang['ldate_of_the_report']    = "Date of the report";
$lang['lplease_enter_your_username']    = "Please enter your username";
$lang['forgot_password_username_label'] = "Username";
$lang['linvalid_forgot_password_code']  = "Invalid Forgot Password Code";
$lang['ladd_field']     = "Add Field";
$lang['lfield_list']    = "Fields List";
$lang['lpic_email']    = "Email PIC";
$lang['lmodify_field']    = "Modify Field";
$lang['ltype_list']    = "Type List";

$lang['lmodify_type']    = "Ubah Jenis Dokumen";
$lang['ladd_type']    = "Tambah Jenis Dokumen";
$lang['ltype_name']    = "Nama Jenis Dokumen";

$lang['ltype_description']    = "Type Description";
$lang['lemail_log']             = "Email Log";
$lang['lcc']                = "CC";
$lang['ladd_country']       = "Add Country";
$lang['lcountry_code']      = "Country Code";
$lang['lmodify_country']    = "Modify Country";
$lang['lcountry_list']      = "Country List";

$lang['lfile']			= "File";
$lang['lname']			= "Name";
$lang['lstart_date']			= "Start date";
$lang['lend_date']			= "End date";
$lang['lfield_is_required']	= "The field is required.";
$lang['ldata_is_failed_deleted']	= "Data deletion failed";
$lang['lcode']			= "Code";
$lang['lsearch']		= "Search";
$lang['lslug']			= "Slug";
$lang['lcontent']		= "Content";

$lang['lsettings']		= "Setting";
$lang['lsave_changes']	= "Save Changes";
$lang['lyes']			= "Ya";

$lang['lsalutation']	= "Salutation";
$lang['laddress']		= "Address";
$lang['ltown']			= "Town";
$lang['lpostcode']		= "Postal Code";
$lang['lprovince']		= "Province";
$lang['lcountry']		= "Country";
$lang['lcountries']		= "Countries";

$lang['lphone']			= "Phone";
$lang['lmobile']		= "Mobile";
$lang['lretype_password']	= "Retype password";
$lang['lsubmit_order']		= "Submit order";
$lang['lemail_exists_alert']= "The email is already registered, please input an another email.";
$lang['lupdate_profile_succeed']	= "Profile update successful";
$lang['ltotal']					= "Total";
$lang['lopen']					= "Open";
$lang['lclosed']				= "Closed";
$lang['lprofile']               = "Profile";
$lang['ledit_profile']			= "Edit Profile";

$lang['llast_login']            = "Last Login";

$lang['luser']				= "Pengguna";
$lang['lmodify_user']		= "Ubah Pengguna";
$lang['llist_user']			= "Daftar Pengguna";
$lang['ladd_user']			= "Tambah Pengguna";

$lang['lmr']    = "Mr";
$lang['lmrs']    = "Mrs";
$lang['lmiss']    = "Miss";
$lang['lreset_password']	= "Reset Password";

$lang['luser_group']    = "User Group";
$lang['lselect_user_group'] = "Please select a user group";
$lang['lpage']              = "Page";
$lang['lpage_category']     = "Page Category";
$lang['ladd_page_category'] = "Add Page Category";
$lang['llist_page']     = "List Page";
$lang['ladd_page']      = "Add Page";

$lang['ldate_posted']   = "Date Posted";
$lang['lmeta_keyword']  = "Meta Keywords";
$lang['lmeta_description']  = "Meta Description";

$lang['lmenu_list'] = "Menu List";
$lang['ltype']      = "Type";
$lang['lcontainer'] = "Container";
$lang['llink']      = "Links";
$lang['lposition']  = "Position";

$lang['ldata_is_used']	= "Data is in used by an another table";
$lang['luser_already_has_data']	= "User already has transmitted data";
$lang['lsemua']			= "All";

$lang['lhome']		= "Home";
$lang['lcreated_on']    = "Created On";

$lang['llast_name']				= "Last Name";
$lang['lfirst_name']			= "First Name";

$lang['lwebsite_title']				= "Website Title";
$lang['lwebsite_description']		= "Website Description";
$lang['lwebsite_keyword']			= "Website Keywords";
$lang['lreported_by']               = "Reported By";
$lang['llist_type']                 = "Type List";



$lang['lmessage']	= "Message";
$lang['lsubmit']			= "Submit";
$lang['llanguage']			= "Language";
$lang['land']					= "And";

$lang['lforgot_password']		= "Forgot Password";
$lang['lforgot_password_info']	= "Please input your email to reset your password!";
$lang['lemail_is_not_found']	= "The Email can not be found.";
$lang['lemail_forgot_is_sent']	= "A notification has been sent to your email to reset your password.";
$lang['lreset_password']		= "Reset Password";
$lang['lreset_password_info']	= "Please input a new password to reset the old password";
$lang['lerror_minimal_password']	= "Password must be minimal %d char";
$lang['lforgot_password_check_link']	= "Email and code don't match, please check again the link.";

$lang['lemail_not_exists']			= "The Email is not registered";

$lang['lpassword_is_reset']			= "Reset Password successful";
$lang['lsave_changes']			= "Save Changes";
$lang['lplease_fill_in_image']	= "Please choose image";

$lang['lnumber']				= "No.";

$lang['lremove_navigation_bar_toggle'] 	= "Remove Navigation Bar Toggle";
$lang['lremove_top_menu_toggle']		= "Remove Top Menu Toggle";
$lang['lremove_navigation_bar_and_top_menu_toggle']	= "Remove Navigation Bar and Top Menu Toggle";

$lang['lenglish']	 = "English";
$lang['lindonesia']	 = "Indonesia";
		
$lang['lconfirmdelete']	 = "Are you sure to delete this data?";

$lang['lajax_error']		= "An error occured, please contact the administrator.";

$lang['ladd_links_info']	= "Please save the current data first in order to add some links.";
$lang['llink_info']				= "example : http://www.xxxxx.com ";

$lang['ltopic']			= "Topics";
$lang['lsub_topic']			= "Sub Topic";

$lang['llist_topic']		= "List Topic";
$lang['ladd_topic']			= "Add Topic";

$lang['lnews'] 			= "News";
$lang['ladd_news'] 		= "Add News";
$lang['llist_news'] 	= "List News";

$lang['ladd_links'] 		= "Add Link";
$lang['llist_links'] 		= "List Links";
$lang['llinks']      		= "Links";

$lang['lno_data_found']		= "There is no result for the selected criteria of the search.";
$lang['lblock']				= "Block";
$lang['llist_block']		= "Block List";
$lang['ladd_block']			= "Add Block";

$lang['lsetting']			= "Settings";
$lang['lemail_login']	= "Email/Login";
$lang['ldisplay']			= "Published";

$lang['ldate_create']	= "Creation date";


$lang['linvalid_date_format'] = "Invalid Date Format, YYYY-MM-DD";

$lang['ltitle_of_document'] = "Title Of Document";
$lang['labstract'] 			= "Abstract";
$lang['ldocument_number_assigned_by_asean'] = "Document number assigned by ASEAN";
$lang['lcategory_of_standards']				= "Category of Standards";
$lang['lregulation_related_to_standards']	= "MRA/Harmonised Regulations related to the Standard";
$lang['lasean_body']						= "ASEAN Body";
$lang['ladd_aseanbody']						= "Add ASEAN Body";
$lang['llist_aseanbody']					= "List ASEAN Body";

$lang['llist_has']		= "List HS";
$lang['ladd_has']		= "Add HS";

$lang['lprevious_version']	= "Previous Version";
$lang['llist_ics']		= "List ICS";
$lang['ladd_ics']		= "Add ICS";

$lang['lbrowse_by_has']	= "Browse By HS";
$lang['lbrowse_by_ics']	= "Browse By ICS";
$lang['lbrowse_by_standards_body']	= "Browse By ASEAN BODY";
$lang['lmore_detail']	= "[More Details+]";

$lang['lhas_child']		= "%s has child";
$lang['lics_has_child']	= "Some of ICS has child, please recheck";
$lang['lhs_has_child']	= "Some of HS has child, please recheck";

$lang['lmra']			= "MRA";
$lang['llist_mra']		= "List MRA";
$lang['ladd_mra']		= "Add MRA";
$lang['lhas_range']		= "HS Range";
$lang['lhs_is_same']	 	= "HS Code range, should be different";

$lang['ltotal_leafs_topic_without_link'] 	= "Total Leafs Topic without link";
$lang['lnon_empty_leafs_topic']			= "Total Leafs Topic with links";
$lang['llinks_last_updated']			= "Last Updated Links";

$lang['lwithdrawn']= "Withdrawn";
$lang['lmissing_page']			= "Looks like the page you're trying to visit doesn't exist.<br />Please check the URL dan try again.";

$lang['lmember_states']         = "Member States";
$lang['lexport']                = "Export";
$lang['lexport_confirm']        = "Are you sure to export the data?";
$lang['lcategory']		= "Category";

$lang['lcomplaint_form']    = "Complaint Form";
$lang['ladd_email']     = "Add Email";
$lang['llist_email']    = "Email List";
$lang['lbusiness_organization']     = "Business Organization Within Asean";
$lang['laddress']     = "Address";
$lang['lbusiness_info'] = "Business Information";
$lang['lcontact_person_info']   = "Contact Person";
$lang['llegal_registration_country'] = "Country of Legal Registration";
$lang['ltype_of_business']  = "Type of Business";
$lang['lbusiness_sector']   = "Business Sector";
$lang['lcountry_encounter_problem']  = "Country where the company encountered problem";
$lang['ltype_of_problem_encounter']  = "Type of Problem Encountered";
$lang['linformation']   = "Description";
$lang['lattachment']    = "Attachment";
$lang['lcomplaint_list']    = "Complaint List";
$lang['lcreated_on']        = "Created On";
$lang['lcomplaint']         = "Complaint";
$lang['lcomplaint_detail']  = "Complaint View";
$lang['ltracking_id']               = "Tracking ID";
$lang['lzip_code']                  = "ZIP code";
$lang['lwebsite']                   = "Website";
$lang['lcontact_person_phone']      = "Contact Person Phone";
$lang['ldestination_country']       = "Destination Country";
$lang['llegal_registration_number'] = "Registration Number";
$lang['linfo_attachment']           = "Please upload any document that you consider appropriate to further explain the trade problem being encountered or provide support to your complaint : jpg, zip, pdf, docx, xlsx.";
$lang['lgender']                    = "Gender";
$lang['lcity']                      = "City";
$lang['linvalid_captcha']           = "Invalid Captcha, please refresh to try again";
$lang['lplease_check_captcha']      = "Please recheck the captcha";

$lang['lplease_validate_email']	= "The Member's email is not yet validated, please check your email notification for activation.";
$lang['lemail_confirmed']       = "Email Confirmed";
$lang['lconfirmed']             = "Confirmed";
$lang['lnot_confirmed']         = "Not Confirmed";
$lang['lplease_fill_in_tracking_id']    = "Please fill in your Tracking ID!";
$lang['linvalid_email_tracking']    = "Invalid email or Tracking ID!";

$lang['laccept']    = "Accept";
$lang['lreject']    = "Reject";
$lang['lincomplete']    = "Incomplete";
$lang['lemail_exists']  = "This Email record already exists.";
$lang['lnotes']         = "Notes";
$lang['lapprove_complaint']     = "Approve Complaint";
$lang['laccept_complaint']     = "Accept Complaint";
$lang['lincomplete_complaint']  = "Incomplete Complaint";
$lang['lreject_complaint']      = "Reject Complaint";
$lang['linvalid_profile']       = "Invalid user profile";
$lang['lhistory']       = "History";
$lang['laction']        = "Action";
$lang['laction_by']     = "Action By";
$lang['ldate']          = "Tanggal";
$lang['lsolution']      = "Solution";
$lang['lsubmit_solution']      = "Submit Solution";
$lang['laccept_solution']   = "Accept Solution";
$lang['lsolution_by_ra']    = "Solution by RA/DCP";
$lang['lclick_to_edit']     = "[Click to Edit]";
$lang['lsolution_attachment']   = "Solution Attachment";
$lang['ltotal_complaints']      = "Total Complaints";
$lang['ltotal_ongoing_ae']      = "Total Ongoing AE";
$lang['ltotal_ongoing_dcp']      = "Total Ongoing DCP";
$lang['ltotal_ongoing_ca']      = "Total Ongoing CA";
$lang['ltotal_solution_ca']      = "Total Solution CA";
$lang['ltotal_solution_dcp']      = "Total Solution DCP";
$lang['ltotal_no_solution']      = "Total no Solution > 35 days";
$lang['ltotal_overdue_ca']      = "Total Overdue CA";
$lang['ltotal_overdue_dcp']      = "Total Overdue DCP";
$lang['ltotal_complaints_email_confirmed'] = "Total email confirmed";
$lang['ltotal_complaints_email_not_confirmed'] = "Total email not confirmed";
$lang['longoing']       = "Ongoing";
$lang['loverdue']       = "Overdue";

//complaint form information 
$lang['lconfirmdeleteattachment']   = "Are you sure to delete the attachment?";

$lang['lsubmit_complaint']          = "SUBMIT YOUR COMPLAINT";
$lang['lcomplaint_description']     = "Complaint description";

$lang['ltotal_satisfy_solution']    = "Total satisfy solution";

$lang['lsatisfied']     = "Satisfied";
$lang['lnot_satisfied'] = "Not Satisfied";
$lang['lfollowup_login']    = "Follow a complaint";
$lang['lmy_complaint']      = "My Complaint";

$lang['lfile_a_complaint'] = "File a complaint";

$lang['lms']    = "Ms";
$lang['lemail_logs']   = "Email Logs";
$lang['lsubject']       = "Subject";
$lang['lto']            = "To";
$lang['lbcc']           = "Bcc";
$lang['lfrom']          = "From";
$lang['lemail_content'] = "Email content";
$lang['lback']  = "Back";


$lang['lred_info']      = "Late Reply";
$lang['lyellow_info']   = 'Warning Reply';
$lang['lgreen_info']    = "Ok";

$lang['lpic_latest_status'] = "PIC / Latest Status";
$lang['lcomments']          = "Comments";
$lang['lremarks']          = "Remarks";

$lang['lunspecified_year']  = "Unsp.";
$lang['lunspecified_period']    = "Unspecified";

$lang['lforgot_password_message']   = "A link to reset the password has been sent to your email account";

$lang['lchange_user_access_confirmation']    = "Change user access confirmation";

$lang['lthis_user_cannot_be_deleted']   = "This user can not be deleted";
$lang['lplease_select_group']       = "Please select user group";
$lang['ldelete_user_confirmation']  = "Are you sure to permanently delete this user?";
$lang['llogout_confirmation_title'] = "Logout confirmation";
$lang['lplease_enter_your_email']   = "Please enter your email";

$lang['llist_str'] = "Daftar %s";
$lang['ladd_str']  = "Tambah %s";
$lang['lmodify_str']  = "Ubah %s";

$lang['ldelete_category']   = "Hapus Kategori";
$lang['lcategory']		= "Kategori";

$lang['lset_unpublish']	= "Set Unpublish";
$lang['lset_publish']	= "Set Publish";

/*
$lang['lprice']			= "Price";

$lang['lproduct']			= "Product";
$lang['lproduct_category']	= "Product Category";
$lang['lbanner_category']	= "Banner Category";
$lang['lpost_category']		= "Post Category";
$lang['ladd_product']		= "Add Product";
$lang['llist_product']		= "Product List";

$lang['lnew_order']			= "New Order";
$lang['lconfirm_order']		= "Payment";
$lang['lall_order']			= "All Order";
$lang['lconfirm_delivery'] 	= "Delivery";
$lang['lpost']				= "Post";
$lang['ladd_post']			= "Add Post";
$lang['lbanner']			= "Banner";
$lang['ladd_banner']		= "Add Banner";
$lang['luser']				= "User";
$lang['ladd_user']			= "Add User";
$lang['llist_user']         = "List User";
$lang['ltotal_data']		= "Total Data";
$lang['lbanner_name']	= sprintf($lang['lstring_name'], $lang['lbanner']);
$lang['lmodify_banner']	= sprintf($lang['lstring_modify'], $lang['lbanner']);
$lang['lproduct_name']	= sprintf($lang['lstring_name'], $lang['lproduct']);
$lang['lmodify_product']	= sprintf($lang['lstring_modify'], $lang['lproduct']);

$lang['lcategory_list']	= "List Category";
$lang['lpost_category'] = "Post Category";
$lang['lcategory']		= "Category";
$lang['ladd_product_category']	= "Add Product Category";
$lang['ladd_banner_category']	= "Add Banner Category";
$lang['ladd_post_category']		= "Add Post Category";

$lang['lmodify_product_category'] 	= "Modify Product Category";
$lang['lmodify_post_category']		= "Modify Post Category";
$lang['lmodify_banner_category']	= "Modify Banner Category";
$lang['llist_banner']		= "Banner List";

$lang['lproduct_code']		= "Product Code";
$lang['lprice1']		= "Price 1";
$lang['lprice2']		= "Price 2";
$lang['lmain_image']		= "Main Image";
$lang['ladditional_image']	= "Additional Image";
$lang['lproduct_image_info']	= "Can upload additional image after saved";
$lang['linfo']			= "Info";
$lang['ldisplay_on_home']	= "Display on Home";
$lang['lrelated_products']	= "Related Products";
$lang['lproduct_related_info'] = "Can set related products after saved";
$lang['lsubmit_order_succeed']	= "Submit order successful, please check your email notification for confirmation!";
$lang['lregister_succeed']		= "Registration successful, please check your email notification for activation!";


$lang['halo']	= "hello";
$lang['lmaster_data']			= "Master Data";
$lang['lpage_category']			= "Page Category";
$lang['lcheckin']	= "Check in";
$lang['lcheckout']	= "Check out";
$lang['ladult']		= "Adult";
$lang['ladults']	= "Adults";
$lang['lkid']		= "Kid";
$lang['lkids']		= "Kids";
$lang['linquiry']				= "Inquiry";
$lang['lfind_a_place_to_stay']	= "Find a place to stay";
$lang['lrent_in_bali']			= "Rent from people in over 34,000 neighborhoods in Bali";
$lang['lneighbourhood_guide']	= "Neighborhood Guides";
$lang['lsuggested_villa']			= "Suggested Villa";
$lang['lneighbourhood_guide_intro']	= "Not sure where to stay? We've created neighborhood guides for popular areas in Bali.";


$lang['ltake_me_home']			= "Take me home";

$lang['lalbum']					= "Album";
$lang['llist_album']			= "List Album";
$lang['ladd_album']				= "Add Album";

$lang['lagent']					= "Agent";
$lang['lorder']					= "Order";
$lang['lswimmingpool']		 	= "Swimming Pool";
$lang['ltotal_rooms']			= "Total Room(s)";

$lang['limage_info']			= "(jpeg)";

$lang['term_config']			= array("1"=>"Short Term","2"=>"Long Term","3"=>"Short&Long Term");
$lang['lterm']					= "Term";
$lang['lwebsite']				= "Website";
$lang['lservices']				= "Services";
$lang['ltransport']				= "Transport";
$lang['lsize']					= "Size";
$lang['lmap']					= "Map";
 $lang['llowprice']				= "Low Price";
$lang['lhighprice']				= "High Price";
$lang['llow_season']			= "Low Season Price";
$lang['lhigh_season']			= "High Season Price";
$lang['lcurrency']				= "Currency";

$lang['ladd_gallery_info']		= "Please save the album first to add more gallery";
$lang['lgallery']				= "Gallery";
$lang['lenlarge_photo']			= "Enlarge Photo";
$lang['ledit_photo']			= "Edit Photo";
$lang['larea']						= "Area";
$lang['llist_area']					= "List Area";
$lang['ladd_area']					= "Add Area";
$lang['ledit_album']			= "Edit Album";
$lang['lrooms']		= "Room(s)";
$lang['lupto']		= "Up to";
$lang['llist_inquiry']	 = "Inquiry List";

$lang['lset_unpublish_confirmation'] 	= "Are you sure to unpublish this data?";
$lang['lset_publish_confirmation'] 		= "Are you sure to publish this data?";
$lang['ldata_isset_publish']			= "Data has been set to Published";
$lang['ldata_isset_unpublish']			= "Data has been set to UnPublished";

 

 */