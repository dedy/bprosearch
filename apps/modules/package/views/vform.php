<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){
        
        $("#name").focus();
        $('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/package');?>");
		});
		
		<?php
		if(isAdmin()){ ?>
			$('#submit-add-btn').click(function(e){
				e.preventDefault(); 
				$('#addnew').val(1);
				dopost($(this));
			});
			
			$('#submit-btn').click(function(e){
				//$('form#login-form').submit();
				//return;
				e.preventDefault(); 
				dopost($(this));
			});
		<?php } ?>
			
	});
	
	<?php 
	if(isAdmin()){ ?>
		function dopost(obj){

			obj.attr('disabled','disabled');
	 		$('#loadingmessage').show();
			
			//$('form#input-form').submit();
			//return;
			//alert('lie');
			
			$('form#input-form').ajaxSubmit({
			 	URL:'<?=site_url('admin/package/doupdate');?>',
			 	dataType: 'json', 
			   	success: function(returData){
			   		//alert('hi');
			   		$('#loadingmessage').hide();
					obj.removeAttr('disabled');
	 				
					$('#show_message').slideUp('normal',function(){
						
						if(returData.error){
							var rv = '<div class="alert alert-error">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							var rv = '<div class="alert alert-success">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, 2000);
							});	
						}	
					});
			      }
			  });
		}	
	<?php } ?>	
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/package')?>"><?=lang('llist_packages')?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
             <?php 
              $hidden = array(
                  "id"              => (isset($data->id))? $data->id:0
              );
              echo form_open("admin/package/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
                 <div class="row-fluid">
                       <div class="span6">
               			  <div class="control-group">
							<label class="control-label" for="name"><b>*&nbsp;<?=lang('lname');?></b></label>
							<div class="controls">
							<input class="input-xlarge" type="text" name='name' id='name' value="<?php if(isset($data->name) && $data->name) echo $data->name ?>" maxlength='255'>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="name">&nbsp;<?=lang('lprice');?></label>
							<div class="controls">
							<input class="input-medium" type="text" name='price' id='price' value="<?php if(isset($data->price) && $data->price) echo $data->price; else echo '0'; ?>" maxlength='10'>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="name"><b>*&nbsp;<?=lang('lduration');?> (<?=lang('lmonth')?>)</b></label>
							<div class="controls">
							<input class="input-small" type="text" name='duration' id='duration' value="<?php if(isset($data->duration_months) && $data->duration_months) echo $data->duration_months ?>" maxlength='2'>
							</div>
						</div>
						</div>
						<div class='span6'>

						  <div class="control-group" >
                                <label class="control-label" for='status'><b>*&nbsp;<?=lang('lall_category')?></b></label>
                                <div class="controls">

                                    <label class="radio inline">
                                        <input type="radio" name="all_category" id="all_category1" value="1" <? if(!isset($data->all_category) || (isset($data->all_category) && $data->all_category == 1)) echo "checked"; ?> > <?=lang('lyes')?>
                                    </label>
                                    <label class="radio inline">
                                    <input type="radio" name="all_category" id="all_category0" value="0" <? if(isset($data->all_category) && $data->all_category <> 1) echo "checked"; ?> > <?=lang('lno')?>
                                    </label>

                                </div>
                              </div>
		
						    <div class="control-group">
								<label class="control-label" for='status'><?=lang('lstatus')?></label>
								<div class="controls">
								  <label class="checkbox inline">
									<input type="checkbox" id="status" name='active' value="1" <? if( (isset($data->active) && $data->active) || !isset($data))echo "checked"?>> <?=lang('lactive')?>
								  </label>
								</div>
							  </div>
						</div>
					</div>

	                 <div class="row-fluid">

						<div class='span12'>
								<div class="control-group">
									<label class="control-label" for='status'><?=lang('lcategory')?></label>
									<div class="controls">

										<?php
										if(count($categories)) { ?>
											<div class="row-fluid">
	                 						
	                 						<?php 
											$i=0;
											foreach($categories as $c) { 
												$i++;
												$is_checked = "";
												if(isset($data_categories) && count($data_categories)){
													foreach($data_categories as $d){
														if($d->category_id == $c->category_id){
															$is_checked = "checked";
														}
													}
												}
											?>
												<div class='span4'>
												  <label class="checkbox inline">
													<input type="checkbox" id="categories<?=$i?>" name='categories[]' value="<?=$c->category_id?>" <?=$is_checked?>> <?=$c->category_name?>
												  </label>
												</div>
									
												<?php if($i%3 == 0 && $i <> 0) {?>
	                 								</div>
	                 								<div class="row-fluid">
	                 							<?php }?>
												
										<?php ;
											} ?>
										<?php }?>
									</div>
								  </div>
							</div>
					</div>
					<div class='row-fluid'>
						<div class='span12'>
		
							<div id='show_message' style="display: none;"></div> 
						
							<div class="form-actions">
							   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
								  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
							  </div>
							  <?php 
							  if(isAdmin()){ ?> 
							  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
							  	<?php } ?> 
							  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
							</div><!--/form-actions-->
						</div>
					</div>
				</form>   <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->