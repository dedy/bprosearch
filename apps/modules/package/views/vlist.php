
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>


<script type="text/javascript">
	 jQuery().ready(function (){
      <?php
      if($error_message) {
        ?>
            var rv = '<div class="alert alert-error"><?=$error_message?></div>';
            $('#show_message').html(rv);
            $('#show_message').slideDown('normal');

      <?php }
      ?>
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url('admin/package/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','active','<?=lang('lname')?>','<?=lang('lprice')?>','<?=lang('lduration')?>','<?=lang('lactive')?>','<?=lang('ledit')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:1, align:"right",sortable:false},
                {name:'id',index:'id', hidden: true},
                {name:'active',index:'active', hidden: true},
                {name:'name',index:'name',align:"left",stype:'text',width:8,hidden:false,
                	formatter: function (cellvalue, options, rowObject) {
					   return '<a href="<?=site_url('admin/package/edit')?>/' + rowObject[1] + '"><u>'+cellvalue+'</u></a>';
					},sortable:false
               	},
                {name:'price',index:'price',align:"left",stype:'numeric',width:8},
                {name:'duration_months',index:'duration_months',align:"left",stype:'numeric',width:8},
                  
                {name:'status_icon', index: 'active', width: 1,align:"center",sortable:false,
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '#';
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                            	if(cellValue == "1")
            						return "<img src='<?=base_url()?>assets/admin/img/ico-yes.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
								else
            						return "<img src='<?=base_url()?>assets/admin/img/ico-standby.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
								
                              //return cellValue;
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                            	var val = $('#list1').jqGrid('getCell',rowId,'active');
                            	//var catname = $('#list1').jqGrid('getCell',rowId,'name');
                                
                            	//set inactive
                            	if(val == 1){
                            		var msg = "<?=lang('lset_inactive_confirmation')?>";
                            		var setval = 0;
                            	}else{
                            		//set active
                            		var msg = '<?=lang('lset_active_confirmation')?>';
                            		var setval = 1;
                            	}
                            	
                                $.confirm({
                                   title:"<?=lang('lchange_status_confirmation')?>",
                                   text: msg,
                                   confirmButton: "<?=lang('lok')?>",
                                   cancelButton: "<?=lang('lcancel')?>",
                                   confirm: function(button) {
                                      $.post("<?=site_url('admin/package/setActive')?>/"+setval+"/"+rowId, 
                                       function(data) {
                                            if(!data.is_error)
                                                var rv = '<div class="alert alert-success">'+data.message+'</div>';
                                            else
                                                var rv = '<div class="alert alert-error">'+data.message+'</div>';

                                               $('#show_message').html(rv);
                                               $('#show_message').slideDown('normal',function(){
                                                    setTimeout(function() {
                                                       $('#show_message').slideUp('normal',function(){
                                                          if(!data.is_error)
                                                              gridReload();
                                                       });	
                                                     }, <?=config_item('message_delay')?>);
                                               });	
                                           //reload grid
                                           //gridReload();

                                       },"json"
                                   );
                                   },
                                   cancel: function(button) {
                                      // alert("You cancelled.");
                                   }
                               });
                            	
                        }},
                        cellattr: function (rowId, cellValue, rawObject) {
                        	//column 2 , status 
                        	if(rawObject[2] == "1")	
                        	 	var attribute = ' title="<?=lang('lset_inactive')?>"' ;
                            else
                            	var attribute = ' title="<?=lang('lset_active')?>"' ;
                            	
                            return attribute ;
                        }
                  },
                        
                 {name:'edit',index:'edit', width:1, align:"left",sortable:false,align:"center",
                	formatter:'dynamicLink', 
      			 	formatoptions:{
      			 		 url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/package/edit')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
							}
					},
      			 	cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ledit')?>"' 
                            return attribute ;
                     }
      			 }
                        
	          ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?}?>
            width: 1024,
            height: <?=$rowHeight?>,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'price',
            toppager: true, 
            //shrinkToFit:false,
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url('admin/package/loadDataGrid')?>",
				page:1
			}).trigger("reloadGrid");
	}

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
		  <?php
      if(isAdmin()) { ?>
  			<div>
  				<a href="<?=site_url('admin/package/add')?>" class="btn btn-small btn-primary">
  					<i class="icon-plus"></i> <?=$ladd_button_title?>
  				</a>
  			</div>
      <?php } ?>
        <br/>
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->