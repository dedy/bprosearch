<?php
class Package_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
   	/* delete user*/
    function delete($data){
       	//-- delete records    	
    	$this->db->where('id', $data['id']);
		$this->db->delete('packages'); 
    }
    
    /* insert_types */
    function insert($values){
        $values["updated_by"]   = _userid();
        $values["updated_on"]   = date("Y-m-d H:i_s");
        $values["created_on"]   = date("Y-m-d H:i_s");
        $values["created_by"]   = _userid();
        
                 
		$this->db->insert("packages", $values); 
		return $this->db->insert_id();
	}
	
	function update($data,$id){
        $data["updated_by"]   = _userid();
        $data["updated_on"]   = date("Y-m-d H:i_s");
        $this->db->where('id', $id);
		$this->db->update('packages', $data); 
	}
	
    function update_category($id,$categories){
        //delete categoris first
        $this->delete_category($id);

        if(count($categories)){
            foreach($categories as $c){
                if($c){
                    $values['package_id'] = $id;
                    $values['category_id']  = $c;
                    $this->db->insert('package_categories',$values);
                }
            }
        }
    }

    //delete category by package id
    function delete_category($id){
        //-- delete records     
        $this->db->where('package_id', $id);
        $this->db->delete('package_categories'); 
    }

    function get_data_by_id($id){
        $data = $this->search(1,1,'name','asc',array("id"=>$id));
        if(count($data)){
            return $data[0];
        }else{
            return false;
        }
    }
    
    function search_simple(){
    	$query = $this->db->get('packages');
		$result = $query->result();
	
  		return $result;
		
    }

    function get_data_category($id){
        $this->db->where("package_id",$id);
        $query = $this->db->get('package_categories');
        $result = $query->result();
    
        return $result;
           
    }
    
    function getIdByName($name,$datas){
        if(count($datas)){
            foreach($datas as $data){
                if(strtoupper($data->name) == strtoupper($name)){
                    return $data->id;
                }
            }
        }
        
        return 0;
    }
        
    
    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('packages.id',$searchval['id']);
        }

        if(isset($searchval['name']) && $searchval['name']){
            $this->db->where('packages.name',$searchval['name']);
        }

        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            if(is_array($searchval['exclude_id'])) {
                $this->db->where_not_in('packages.id',$searchval['exclude_id']);
            }else{
                $this->db->where('packages.id <>',$searchval['exclude_id']);
            }
        }

        if(isset($searchval['active'])){
            $this->db->where('packages.active',$searchval['active']);
        }
        
          if(isset($searchval['include_id']) && $searchval['include_id']){
            $this->db->or_where('packages.id',$searchval['include_id']);
        }
 
    	if($this->is_count){
			$this->db->select("count(packages.id) as total");
		}else{
            $this->db->select("packages.*");
            $this->db->select("concat('Rp. ',format(price,0)) as price_fmt");
			$this->db->select("concat(duration_months,' bulan') as duration_str",false);
			$this->db->select("'' as edit");
			$this->db->select("'' as hapus");
       }
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('packages');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
     function setActive($id,$data){
		$data["updated_on"]	= date("Y-m-d H:i:s");
		$data["updated_by"]	= _userid();
	
  		$this->db->where('id', $id);
		$this->db->update('packages', $data); 
	}

    //check in table user_packages
    function is_used($id){
        //check in table files
        $this->db->select("count(types_id) as total");
        $this->db->where("types_id",$id);
		$query = $this->db->get('user_packages');
		$result = $query->result();
        
        if($result[0]->total > 0){
            return true;
        }
        
        return false;
    }
    
}