<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
        
        //user module only for admin
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
		$this->load->model('package_model');
	}
	
	function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('llist_packages');
		$this->viewparams['ladd_button_title']	= lang('ladd_package');
		
		$error_message = $this->session->flashdata('error_message');
		$this->viewparams['error_message']	= $error_message;
		parent::viewpage("vlist");
	}
    
    function setActive($value,$id){
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');

        $is_error = false;
        $message = "";

		if(!isAdmin()){
			$is_error = true;
			$message = lang('linvalid_permission');
		}else {

			$data['active']	= $value;
			

			$this->package_model->setActive($id,$data);
			
	        $data = $this->package_model->search(1,1,'','',array('id'=>$id));
	        
			$message = ($value == 1)?lang('ldata_isset_active'):lang('ldata_isset_inactive');
		}

		$result = array("is_error" => $is_error, "message" => $message);
  		echo json_encode($result);



	}

	
    function edit($id){
      /*  echo time();
        echo date("Y-m-d",time());
        */
       /* can not access this page without valid id*/
		$data = $this->package_model->get_data_by_id($id);
        
		if(!$data){
			redirect('admin/package');
		}
        
        //get package category by package id
        $data_category = $this->package_model->get_data_category($id);

		$this->viewparams['title_page']	= lang('lmodify_package');
		$this->viewparams['data']	= $data;
		$this->viewparams['data_categories']	= $data_category;
			
        /* user group list */
       // $user_groups = $this->user_model->getUserGroup();
		//$this->viewparams['user_groups'] = $user_groups;
            
        $this->form($id);
	}
    
    function form($id=0){
         $this->load->library(array('form_validation'));

         //category list
         $this->load->model('category/category_model');

         $searchval = array(
         	'active'	=> 1
         );

       	$categories = $this->category_model->get_childs(0,'display_order','asc',$searchval);
		$this->viewparams['categories']	= $categories;

		parent::viewpage("vform");
    }
	
	function add(){
		if(!isAdmin()){
			$this->session->set_flashdata('error_message', lang('linvalid_permission'));
			redirect('admin/package');
		}
      	$this->viewparams['title_page']	= lang('ladd_package');
	
        $this->form(0);
	}
	
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'price'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->package_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->package_model->countSearch($searchv);

		if(count($query)){
			$i=0;
			foreach($query as $q){
				$query[$i]->price = "Rp. ".number_format($query[$i]->price,0,',','.'); 
				$i++;
			}
		}

		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","active","name","price","duration_str","active","edit"),
			"id"		=> "id"
 		);
         
        parent::loadDataJqGrid();
		
	}
    
    //update_user
	function doupdate(){
		if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		/* check mandatory fields */
		$id		        = $this->input->post('id');
        
		$name           = $this->input->post('name');
		$price           = $this->input->post('price');
		$duration           = $this->input->post('duration');
		$active	          = $this->input->post('active');
		$all_category	          = $this->input->post('all_category');
		$categories	          = $this->input->post('categories');

		
		$message = array();
		$redirect = "";
		$is_error = false;
		
		if(!isAdmin()){
			$is_error = true;
			$message[] = lang('linvalid_permission');
		}else {

			//-- run form validation
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'lang:lname', 'required');
			$this->form_validation->set_rules('price', 'lang:lprice', 'numeric');
			$this->form_validation->set_rules('duration', 'lang:lduration', 'required|numeric');
			
			//If not category, at least has to select 1 category
			if(!$all_category){
				$this->form_validation->set_rules('categories[]', 'lang:lcategory', 'required');
			}
			$this->form_validation->set_error_delimiters('', '<br/>');
			
			if ($this->form_validation->run() == FALSE){
				$is_error = true;
				$message[] = validation_errors();
				
			}
			
			if(!$is_error){
				
	            /* add/update if no error */
	            if(!$is_error){
	        
		       		$data = array(
	                    "name"          => $name
	                    ,"price"          => $price
	                    ,"duration_months"          => $duration
					    ,"active"             => ($active)?$active:0
					    ,"all_category"             => ($all_category)?$all_category:0
					);
					
					//add
					if(!$id){
					 	$this->db->trans_begin();
						$id = $this->package_model->insert($data);

						//update category
						$this->package_model->update_category($id,$categories);
	                    if ($this->db->trans_status() === FALSE){
	        				$this->db->trans_rollback();
							$message[] = lang('ldata_failed_inserted');
						}
						else{
						    $this->db->trans_commit();
						    $message[] = lang('ldata_success_inserted');
						}
	                    
					}else{
					 	$this->db->trans_begin();
						                    
	                    $this->package_model->update($data,$id);
						$this->package_model->update_category($id,$categories);
	                    
	                    if ($this->db->trans_status() === FALSE){
	        				$this->db->trans_rollback();
							$message[] = lang('ldata_failed_updated');       				
						}
						else{
						    $this->db->trans_commit();
							$message[] = lang('ldata_success_updated');
						}
					}
					
					$redirect = site_url("admin/package");
				}
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
  
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
