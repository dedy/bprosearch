<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Package extends Public_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
    function index(){

        $this->viewparams['website_title']   		= "BPro Package - ".config_item('website_title');
        $this->viewparams['website_description']   	= "BPro Package";
        $this->viewparams['website_keyword']   		= "BPro Package";

        parent::viewpage("package/index");

    }
    
}
