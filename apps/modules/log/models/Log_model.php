<?php
class Log_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
    function get_data_by_id($id){
        $data = $this->search(1,1,'id','asc',array("id"=>$id));
        
        if(isset($data[0])){
            return $data[0];
        }else{
            return false;
        }
    }
    
	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where("id",$searchval['id']);    
        }
       	if($this->is_count){
			$this->db->select("count(email_logs.id) as total");
			
		}else{
            $this->db->select("email_logs.*");
            $this->db->select("from as log_from");
            $this->db->select("to as log_to");
            $this->db->select("cc as log_cc");
            $this->db->select("bcc as log_bcc");
            $this->db->select("subject as log_subject");
            $this->db->select("content as log_content");
      		$this->db->select("DATE_FORMAT(created_on,'%d/%m/%Y %H:%i:%s') as emails_created_on_fmt",false);
       }
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('email_logs');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
     function get_data_import_by_id($id){
        $data = $this->search_import(1,1,'id','asc',array("id"=>$id));
        
        if(isset($data[0])){
            return $data[0];
        }else{
            return false;
        }
    }
    
     /* countSearchUser */
    function countSearchImport($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search_import(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
    function search_import($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where("import_logs.id",$searchval['id']);    
        }
       	if($this->is_count){
			$this->db->select("count(import_logs.id) as total");
			
		}else{
            $this->db->select("import_logs.*");
            $this->db->select("users.full_name as created_by_fullname");
      		$this->db->select("DATE_FORMAT(import_logs.created_on,'%d/%m/%Y %H:%i:%s') as created_on_fmt",false);
      		$this->db->select("DATE_FORMAT(import_logs.finished_on,'%d/%m/%Y %H:%i:%s') as finished_on_fmt",false);
            $this->db->join('users',"users.id=import_logs.created_by","left outer");
            
        }
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('import_logs');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    
     /* countSearchUser */
    function countSearchImportDetail($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search_import_detail(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
    function search_import_detail($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
        if(isset($searchval['import_log_id']) && $searchval['import_log_id']){
            $this->db->where("import_log_id",$searchval['import_log_id']);    
        }
        
        if(isset($searchval['nomor_dokumen']) && $searchval['nomor_dokumen']){
            $this->db->like("nomor_dokumen",$searchval['nomor_dokumen']);    
        }

        if(isset($searchval['status']) && $searchval['status']){
            $this->db->like("status",$searchval['status']);    
        }
        
       	if($this->is_count){
			$this->db->select("count(import_logs_detail.id) as total");
			
		}else{
            $this->db->select("import_logs_detail.*");
      		$this->db->select("DATE_FORMAT(created_on,'%d/%m/%Y %H:%i:%s') as created_on_fmt",false);
        }
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('import_logs_detail');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
}