
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<style>
    .ui-jqgrid tr.jqgrow td { vertical-align: top; }
</style>

<link href='<?=base_url();?>assets/js/jquery/chosen/1.4.2/chosen.css' rel='stylesheet'>
<script src="<?php echo base_url(); ?>assets/js/jquery/chosen/1.4.2/chosen.jquery.min.js" type="text/javascript"></script>




<script type="text/javascript">
	 jQuery().ready(function (){
          //chosen - improves select
        $('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect: true});
      

        $("#submit-btn").click(function(e){
            e.preventDefault(); 
            $("#submit_spinner").show();
			gridReload();
		});	
        
        $("#submit_spinner").hide();
        
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url('log/import/loadDataGridDetail')?>/<?=$import_log_id?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','<?=lang('lnomor_dokumen')?>','<?=lang('lstatus')?>','<?=lang('ldescription')?>','<?=lang('lcreated_on')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:30, align:"right",sortable:false},
                {name:'id',index:'id', hidden: true},
                {name:'nomor_dokumen',index:'nomor_dokumen',align:"left",stype:'text',width:200,sortable:false},
                {name:'status',index:'status',align:"left",stype:'text',width:100,sortable:false},
                {name:'description',index:'description',align:"left",stype:'text',width:500,sortable:false},
                {name:'created_on',index:'created_on',align:"left",stype:'text',width:150,sortable:false}
	          ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?}?>
            width: 1024,
            height: <?=$rowHeight?>,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'created_on',
            sortorder: "desc" ,
            toppager: true, 
            shrinkToFit:false,
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
    
			jQuery("#list1").jqGrid('setGridParam',{
				url:'<?=site_url('log/import/loadDataGridDetail')?>/<?=$import_log_id?>/?nomor_dokumen='+$("#nomor_dokumen").val()+'&status='+$("#status").val(),
				page:1
			}).trigger("reloadGrid");
            $("#submit_spinner").hide();
	}

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li>
			<a href="<?=site_url('log/import')?>"><?=lang('limport_log')?></a> <span class="divider">/</span>
		</li>
	<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>


<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
            <table>
                <tr>
                    <td width="200px"><?=lang('limport_time')?></td>
                    <td>:&nbsp;<?=$data->created_on_fmt?></td>
                </tr>
                
                <tr>
                    <td><?=lang('limport_by')?></td>
                    <td>:&nbsp;<?=$data->created_by_fullname?></td>
                </tr>
            </table>
            <div>&nbsp;</div>
            
            
            
            <form id='input-form' class="form-horizontal" method="post">
    		    
                    <div class="row-fluid">
                    
                        <span class="span6">
                        	 <div class="control-group">
        						<label class="control-label" for="nomor_dokumen"><?=lang('lnomor_dokumen');?></label>
        						<div class="controls">
                                    <input class="input-xlarge" type="text" name='nomor_dokumen' id='nomor_dokumen' value="" maxlength='100'>
                              	</div>
        					</div>
                        </span>
                    </div>

                     <div class="row-fluid">
                    
                        <span class="span6">
                             <div class="control-group">
                                <label class="control-label" for="nomor_dokumen"><?=lang('lstatus');?></label>
                                <div class="controls">
                                    <select name='status' id='status' class="input-medium"  data-rel="chosen" data-placeholder="Pilih status">
                                    <option value=''></option>
                                    <option value='error'>Error</option>
                                    <option value='insert'>Insert</option>
                                    <option value='update'>Update</option>
                                    </select>
                                </div>
                            </div>
                        </span>
                    </div>
                
                    <div class="form-actions">
    					   <img src="<?=base_url()?>assets/admin/img/spinner-mini.gif" id="submit_spinner">
    					  	<button type="submit" class="btn btn-primary" id='submit-btn'>
                              <i class="icon-search"></i> <?=lang('lsearch')?>
                           </button>
                    </div><!--/form-actions-->
                        
                        
            </form>
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->