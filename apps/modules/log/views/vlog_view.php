
<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/log/email')?>"><?=$title_page?></a><span class="divider">/</span></li>
		<li><?=lang('lview')?></li>
	</ul>
</div>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
             <table class="table table-striped">
                  <tbody>
                      <tr><td width='100px'><?=lang('ldate');?></td><td>:&nbsp;<?=$data->emails_created_on_fmt?></td></tr>
                      <tr><td><?=lang('lsubject');?></td><td>:&nbsp;<?=$data->log_subject?></td></tr>
                      <tr><td><?=lang('lfrom');?></td><td>:&nbsp;<?=$data->log_from?></td></tr>
                      <tr><td><?=lang('lto');?></td><td>:&nbsp;<?=$data->log_to?></td></tr>
                      <tr><td><?=lang('lcc');?></td><td>:&nbsp;<?=$data->log_cc?></td></tr>
                      <tr><td><?=lang('lbcc');?></td><td>:&nbsp;<?=$data->log_bcc?></td></tr>
                      <tr><td><?=lang('lemail_content');?></td><td>:&nbsp;</td></tr>
                      <tr><td colspan="2"><?=$data->content?></td></tr>
                  </tbody>
             </table>
               <a href="<?=site_url('admin/log/email')?>">
                    <button class="btn btn-white" id='cancel-btn'><- <?=lang('lback')?></button>
                </a>
        </div>
    </div>
</div>
                 
            
             

       </div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->
