<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH.'vendor/asika/pdf2text/src/Pdf2text.php';

class Import extends Admin_Controller{
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
        parent::__construct();
        $this->load->model('log/log_model');
        
	}
	
    function test(){
        echo "sdf";
        $reader = new \Asika\Pdf2text;
        $filename = "/home/dedy/www/bpro/search/import/files/19831231_6_1983/19831231_6_1983.pdf";
        $output = $reader->decode($filename);
        echo $output;
    }
    
    function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('limport_log');
	
		parent::viewpage("vimport_list");
	}
    
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'created_on'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'desc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->log_model->search_import($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->log_model->countSearchImport($searchv);
        
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","filename","created_by_fullname","created_on_fmt","finished_on_fmt"),
			"id"		=> "id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
    function detail($id){
    	$this->viewparams['title_page']	= lang('limport_detail');
    	$this->viewparams['import_log_id']	= $id;
        
        //get import data
        $data = $this->log_model->get_data_import_by_id($id);
        if(!$data){
            redirect("log/import");
        }
        $this->viewparams['data']	= $data;
	
		parent::viewpage("vimportdetail_list");
	}
   
    function loadDataGridDetail($id){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'created_on'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'desc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array(
            "import_log_id" => $id
             ,"nomor_dokumen"    => $this->input->get('nomor_dokumen')
             ,"status"    => $this->input->get('status')
        );
      	$query = $this->log_model->search_import_detail($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->log_model->countSearchImportDetail($searchv);
          
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","nomor_dokumen","status","description","created_on_fmt"),
			"id"		=> "id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
    
	
	
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
