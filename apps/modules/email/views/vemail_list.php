
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<style>
    .ui-jqgrid tr.jqgrow td { vertical-align: top; }
</style>
<script type="text/javascript">
	 jQuery().ready(function (){
    	
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url('admin/email/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','<?=lang('lsubject')?>','<?=lang('ldescription')?>','<?=lang('ledit')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:12, align:"right",sortable:false},
                {name:'id',index:'id', hidden: true},
                {name:'subject',index:'subject',width:95,align:"left",stype:'text'},
                {name:'description',index:'description',align:"left",stype:'text'},
                 {name:'edit',index:'edit', width:12, align:"left",sortable:false,align:"center",
                	formatter:'dynamicLink', 
      			 	formatoptions:{
      			 		 url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/email/edit')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
							}
					},
      			 	cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ledit')?>"' 
                            return attribute ;
                     }
      			 }            
                        
	          ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?}?>
            width: 1024,
            height: <?=$rowHeight?>,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'subject',
            toppager: true, 
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url('admin/user/loadDataGrid')?>",
				page:1
			}).trigger("reloadGrid");
	}

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('home')?></a> <span class="divider">/</span>
		</li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>


<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->