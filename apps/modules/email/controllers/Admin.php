<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		
class Admin extends Admin_Controller{
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		//only superadmin can access
		if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
        parent::__construct();
        $this->load->model('email/email_model');
        
	}
	
    function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('llist_email');
	
		parent::viewpage("vemail_list");
	}
    
   
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'subject'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->email_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->email_model->countSearch($searchv);
        
		$this->firephp->log($this->db->last_query());
		$this->firephp->log("count : ".$count);
		for($i=0;$i<count($query);$i++){
			$query[$i]->edit = "";
		}
  
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","subject","description","edit"),
			"id"		=> "id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
	function edit($id){
    	$data = $this->email_model->get_data($id);
        
		if(!$data){
			redirect();
		}
		
		$this->viewparams['title_page']	= lang('lmodify_email');
		$this->viewparams['data']	= $data;
		
        $this->form($id);
	}
    
    function form($id=0){
         $this->load->library(array('form_validation'));
	      
		parent::viewpage("vemail_form");
    }
	
	function doupdate(){
		if(!$this->input->is_ajax_request()){
            redirect('auth/logout');
        }

		/* check mandatory fields */
		$id		        = $this->input->post('id');
        $subject        = $this->input->post('subject');
        
		$description  = $this->input->post('description');
		$content     = $this->input->post('content');
		
		$message = array();
		$redirect = "";
		$is_error = false;
		
		//-- run form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('subject', 'lang:lsubject', 'required');
		//$this->form_validation->set_rules('description', 'lang:ldescription', 'required');
		$this->form_validation->set_rules('content', 'lang:lcontent', 'required');
        
		
		$this->form_validation->set_error_delimiters('', '<br/>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
            $message[] = validation_errors();
			
		}
		
		if(!$is_error){
			
            /* add/update if no error */
            if(!$is_error){
        
	       		$data = array(
                    "subject"               => $subject
					,"description"		    => $description
					,"content"              => $content
					,"display_order"		=> 0
					,"updated_on"	     => date("Y-m-d H:i:s")
					,"updated_by"	     => _UserId()
				);
				
				//add
				if(!$id){
                   // $this->field_model->insert($id,$fields);
	
				//	if($id)	
        		//		$message[] = lang('ldata_success_inserted');
				//	else
						$message[] = lang('ldata_failed_inserted');
						
				}else{
                    $this->email_model->update($id, $data);
					$message[] = lang('ldata_success_updated');
				}
				
				$redirect = site_url("admin/email");
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
