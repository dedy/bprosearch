<?php
class Email_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
	function get_data($id){
		$searchval['id'] = $id;
		$result = $this->search(1,1,'subject','asc',$searchval);
		return $result[0];
	}
	
    
    /* insert_users */
    function insert($values){
		$this->db->insert("emails", $values); 
		return $this->db->insert_id();
	}
	
	function update($id,$data){
		$this->db->where('id', $id);
		$this->db->update('emails', $data); 
	}
	
   function send_mail($id_email,$to_email="",$cc_email=array(),$bcc_email=array(),$original=array(),$to_replace=array()){
        $sendmail = false;
      	if($to_email) {
	  		/** sending email notification **/
			$this->load->library('email');
			$this->load->config('email');
	
			$this->load->helper('text');
            
            //email id=2 transmit notification
            $email_data = $this->getDataById($id_email);
            
            //email configuration
           	$this->email->from(config_item('sender_mail'),config_item('sender_name'));
            $this->email->to($to_email);
            
            //cc
            if(count($cc_email)){
                $this->email->cc($cc_email);
            }
         
            //bcc
            if(count($bcc_email)){
                $this->email->bcc($bcc_email);
            }
            
            //subject is from the email title
		    $subject  = $email_data[0]->subject;
           
			$this->email->subject($subject);
            
            //replace the template word to data from the complaint 
            if($original && $to_replace) {
                $email_content = $this->email_content_replace($original,$to_replace,$email_data[0]->content);
            }else {
                $email_content = $email_data[0]->content;
            }
            
		    $this->email->message($email_content);
	        
          
            //insert into emails log
            $email_values = array(
                "from"          => config_item('sender_name')."< ".config_item('sender_mail')." >"
                ,"to"           => $to_email
                ,"bcc"          => (is_array($bcc_email) && $bcc_email)?implode(", ",$bcc_email):""
                ,"cc"          => (is_array($cc_email) && $cc_email)?implode(", ",$cc_email):""
                ,"subject"      => $subject
                ,"content"      => $email_content
                ,"created_on"   => date("Y-m-d H:i:s")
                ,"created_by"   => (_UserId())?_UserId():0
            );
            $this->db->insert("email_logs", $email_values); 
		
            //attachment
           /* if($attachment){
                $this->email->attach($attachment);
            }
            */            
            
	     	/* If sending email enable, then send the mail */
            if($this->config->item('sending_mail')) {
                if($this->email->send()){
                     $sendmail = true;
                }
            
                if($this->config->item('debug_email')) {
                    echo $this->email->print_debugger();
                }
            }
		}
		
		/* end sending mail */
		return $sendmail;
	}
    
    function email_content_replace($original,$to_replace,$content){
        
        $content = str_replace($original,$to_replace,$content);
        
        return $content;
    }
    
    
    function getDataById($id){
        return $this->search(1,1,'id','asc',array("id"=>$id));
    }
    
	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('emails.id',$searchval['id']);
        }

      	if($this->is_count){
			$this->db->select("count(emails.id) as total");
		}
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('emails');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
}