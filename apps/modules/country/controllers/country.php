<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();

		$this->load->model('country_model');
	}
	
	function topics($country_code){
		if(!$country_code)
			redirect();
		
		//check if country code is exists
		$data_country = $this->country_model->getInfoByCode($country_code);
		
		//if some random country code , that not ASEAN, redirect to home
		if(!count($data_country)){
			redirect();
		}
		
		$this->load->model('page/page_model');
		
		//get topics of first level
		$topics = $this->page_model->get_topics(0); 
		$this->viewparams['topics']	= $topics;
		
		$this->viewparams['country_code']	= $country_code;
		
		parent::view('country_topics');
	}
	
	
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
