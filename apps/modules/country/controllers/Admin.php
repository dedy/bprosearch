<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		
class Admin extends MY_Controller{
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		//only superadmin can access
		if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
        parent::__construct();
        $this->load->model('country/country_model');
        
	}
	
    function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('lcountry');
	
		parent::viewpage("vcountry_list");
	}
    
   
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'country_order asc, country_name'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->country_model->search_country($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->country_model->countSearchCountry($searchv);
        
		$this->firephp->log($this->db->last_query());
		$this->firephp->log("count : ".$count);
		for($i=0;$i<count($query);$i++){
			$query[$i]->delete =  $query[$i]->edit = "";
		}
  
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("country_id","country_code","country_name","country_order","edit","delete"),
			"id"		=> "country_id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
	function edit($id){
    	$data = $this->country_model->getInfoById($id);
		if(!$data){
			redirect('admin/country');
		}
		
		$this->viewparams['title_page']	= lang('lmodify_country');
		$this->viewparams['data']	= $data[0];
		
        $this->form($id);
	}
    
    function add(){
      	$this->viewparams['title_page']	= lang('ladd_country');
        $this->viewparams['default_order'] = $this->country_model->next_order();        
        $this->form(0);
	}
	
    function form($id=0){
         $this->load->library(array('form_validation'));
	      
		parent::viewpage("vcountry_form");
    }
	
	function doupdate(){
		if(!$this->input->is_ajax_request()){
            redirect('auth/logout');
        }

		/* check mandatory fields */
		$id		        = $this->input->post('id');
        $name           = $this->input->post('name');
        $code           = $this->input->post('code');
		$orders         = $this->input->post('orders');
        
		$message = array();
		$redirect = "";
		$is_error = false;
		
		//-- run form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'lang:lcountry_code', 'required');
		$this->form_validation->set_rules('name', 'lang:lname', 'required');
        $this->form_validation->set_rules('orders', 'lang:lorder', 'numeric');
		 
		
		$this->form_validation->set_error_delimiters('', '<br/>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
            $message[] = validation_errors();
			
		}
		
		if(!$is_error){
			
            /* add/update if no error */
            if(!$is_error){
        
	       		$data = array(
                    "orders"            => $orders
					,"code"             => $code
					,"name"             => $name
					,"updated_on"	    => date("Y-m-d H:i:s")
					,"updated_by"	    => _UserId()
				);
				
				//add
				if(!$id){
                    $id  = $this->country_model->insert($data);
	
					if($id)	{
        				$message[] = lang('ldata_success_inserted');
                    } else {
						$message[] = lang('ldata_failed_inserted');
                    }
				}else{
                    $this->country_model->update($data, $id);
					$message[] = lang('ldata_success_updated');
				}
				
				$redirect = site_url("admin/country");
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
    function delete($id){
        $success = true;
		
        //check if used in type and file
        if($this->country_model->is_used($id)){
            $this->firephp->log("sql : ".$this->db->last_query());
	
             $success = false;
             $message = lang('ldata_is_used');
        }else{
            $this->country_model->delete($id);
            if($this->db->affected_rows()){
                $message = lang('ldata_is_deleted');
            }
            else {
                $success = false;
                $message = lang('ldata_is_failed_deleted');
            }
        }
		
		$result = array("success"=>$success,"message" => $message);
  		echo json_encode($result);
	}
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
