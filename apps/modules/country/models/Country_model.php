<?php
class Country_model extends CI_Model {
	var $is_count = false;
	var $data = array();
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   	function getInfoById($id){
		$searchval['id']	= $id;
		return $this->search_country(1,1,'','',$searchval);
	
	}

	function getInfoByCode($code){
		$searchval['code']	= $code;
		return $this->search_country(1,1,'','',$searchval);
	
	}
	
    function get_country(){
        return $this->search_country(0,0,'country_order,country_name','asc');
    }
    
    function search_country($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
    	foreach($searchval as $key=>$val){
    		if(!is_array($val))
	    		$searchval[$key] = trim(urldecode($val));
		}
		
		if(isset($searchval['id']) && $searchval['id']){
			if(is_array($searchval['id']))
				$this->db->where_in("id",$searchval['id']);
			else
				$this->db->where("id",$searchval['id']);
		}
		
		if(isset($searchval['code'])){
			if(is_array($searchval['code']))
				$this->db->where_in("code",$searchval['code']);
			else
				$this->db->where("code",$searchval['code']);
		}
		
		if($this->is_count){
			$this->db->select("count(country.id) as total");
		}else{
			$this->db->select("country.*");
			$this->db->select("country.name as country_name");
			$this->db->select("country.code as country_code");
			$this->db->select("country.id as id_country");
			$this->db->select("country.id as country_id");
			$this->db->select("country.orders as country_order");
            
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
			
		$query = $this->db->get('country');
		$result = $query->result();
	
  		//echo $this->db->last_query();
  		if(!$this->is_count)
			return $result;
		else	
			return $result[0]->total;
		
    }
    
    
    function countSearchCountry($searchval=array()){
    	$this->is_count = true;
    	$res = $this->search_country(0,0,'','',$searchval);
    	
    	return $res;
	}
	
     function next_order(){
        $this->db->select("max(orders) as max_order");
		$query = $this->db->get('country');
		$result = $query->result();
	
        return $result[0]->max_order+1;
        
    }
    
     function insert($values){
		$this->db->insert("country", $values); 
		return $this->db->insert_id();
	}
	
	function update($data,$id){
		$this->db->where('id', $id);
		$this->db->update('country', $data); 
	}
	
    function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('country'); 
	}
    
    function is_used($id){
        //check in table user
        $this->db->select("count(country_id) as total");
        $this->db->where("country_id",$id);
		$query = $this->db->get('users');
		$result = $query->result();
        $this->firephp->log("is used : ".$this->db->last_query());
	
        if($result[0]->total > 0){
            return true;
        }else{
            //check in table files
            $this->db->select("count(country_id) as total");
            $this->db->where("country_id",$id);
            $query = $this->db->get('files');
            $result = $query->result();
            $this->firephp->log("is used 2 : ".$this->db->last_query());
	    if($result[0]->total > 0){
                return true;
            }
       
        }
        
        return false;
    }

}