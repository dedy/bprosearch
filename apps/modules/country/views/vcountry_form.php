<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/adapters/jquery.js"></script>

<link href='<?=base_url();?>assets/js/jquery/chosen/1.4.2/chosen.css' rel='stylesheet'>
<script>
	$(document).ready(function(){
	   	
		$('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/country');?>");
		});
		
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
			
	
	});
	
	function dopost(obj){
		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();

		$.post('<?=site_url('admin/country/doupdate');?>', 
				$("#input-form").serialize(),
				function(returData) {
					
					$('#loadingmessage').hide();
					obj.removeAttr('disabled');
     				
					$('#show_message').slideUp('normal',function(){
						
						if(returData.error){
							var rv = '<div class="alert alert-error">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							var rv = '<div class="alert alert-success">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, <?=config_item('message_delay')?>);
							});	
						}	
					});
					
				},'json');
	}
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=base_url()?>admin">Home</a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/country')?>"><?=lang('lcountry')?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
             <?php 
              $hidden = array(
                  "id"              => (isset($data->country_id))? $data->country_id:0
              );
              echo form_open("admin/country/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
            
	       <div class="row-fluid">
				<div class="span12">
		
                   	<div class="control-group">
						<label class="control-label" for="order"><?=lang('lorder');?></label>
						<div class="controls">
						<input class="input-mini" type="text" name='orders' id='orders' value="<? if(isset($data->country_order)) echo $data->country_order ; else echo $default_order?>" maxlength='2'>
						</div>
					</div>
					
                     <div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;<?=lang('lcountry_code');?></b></label>
						<div class="controls">
						<input class="input-mini" type="text" name='code' id='code' value="<? if(isset($data->country_code) && $data->country_code) echo $data->country_code?>" maxlength='3'>
						</div>
					</div>
                    <div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;<?=lang('lname');?></b></label>
						<div class="controls">
						<input class="input-large" type="text" name='name' id='name' value="<? if(isset($data->country_name) && $data->country_name) echo $data->country_name?>" maxlength='50'>
						</div>
					</div>
                   
			     </div><!--/ span12-->
               
        </div><!--/ row-fluid -->
        
        
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
					</div><!--/form-actions-->
					
				<?=form_close();?>    <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->


<script src="<?php echo base_url(); ?>assets/admin/js/jquery.chosen.min.js" type="text/javascript"></script>
