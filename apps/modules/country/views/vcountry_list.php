
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<!-- jquery confirm -->
<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<style>
    .ui-jqgrid tr.jqgrow td { vertical-align: top; }
</style>
<script type="text/javascript">
	 jQuery().ready(function (){
    	
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url('admin/country/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','<?=lang('lcountry_code')?>','<?=lang('lname')?>','<?=lang('lorder')?>','<?=lang('ledit')?>','<?=lang('ldel')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:12, align:"right",sortable:false},
                {name:'id',index:'id', hidden: true},
                {name:'code',index:'country_code',width:15,align:"left",stype:'text'},
                {name:'name',index:'country_name',width:45,align:"left",stype:'text'},
                {name:'order',index:'country_order', width:15,align:"center",stype:'text'},
                 {name:'edit',index:'edit', width:12, align:"left",sortable:false,align:"center",
                	formatter:'dynamicLink', 
      			 	formatoptions:{
      			 		 url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/country/edit')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
							}
					},
      			 	cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ledit')?>"' 
                            return attribute ;
                     }
      			 },
                  {name: 'delete', index: 'delete', width: 12,align:"center",
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/country/delete')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                  return "<img src='<?=base_url()?>assets/admin/img/ico-delete.png' id='delete-"+rowId+"' class='delete' border='0' width='16px' height='16px'>";
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                                $.confirm({
                                    title:"Delete country",
                                    text: "Are you sure to permanently delete this country?",
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    confirm: function(button) {
                                       $.post(
                                            "<?=site_url('admin/country/delete')?>/" + rowId, 
                                            function(data) {
                                                if(data.success)
                                                    var rv = '<div class="alert alert-success">'+data.message+'</div>';
                                                else
                                                    var rv = '<div class="alert alert-error">'+data.message+'</div>';

                                                    $('#show_message').html(rv);
                                                    $('#show_message').slideDown('normal',function(){
                                                         setTimeout(function() {
                                                            $('#show_message').slideUp('normal',function(){
                                                                if(data.success){
                                                                    gridReload();
                                                                }
                                                            });	
                                                          }, <?=config_item('message_delay')?>);
                                                    });	
                                                //reload grid
                                                //gridReload();
                                            },"json"
                                        );

                                    },
                                    cancel: function(button) {
                                       // alert("You cancelled.");
                                    }
                                });
                                
                            }
                        },
                        cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ldelete')?>"' 
                            return attribute ;
                        }}
                              
                 
                 
                        
	          ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?}?>
            width: 680,
            height: 480,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=lang('lcountry_list')?>",
            sortname: 'country_order,country_name',
            toppager: true, 
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url('admin/country/loadDataGrid')?>",
				page:1
			}).trigger("reloadGrid");
	}

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>


<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
            <div>
				<a href="<?=base_url()?>admin/country/add" class="btn btn-small btn-primary">
					<i class="icon-plus"></i> <?=lang('ladd_country')?>
				</a>
			</div>
			<br/>
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->