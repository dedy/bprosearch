<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Data Transmission Tools Login Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="DeCMS Administrator Panel">
	<meta name="author" content="Dedy">

    	<meta charset="utf-8">
        <link href="<?php echo base_url(); ?>assets/css/style.css?v=1.2" media="all" rel="stylesheet" type="text/css" />
	    
		<!-- jQuery -->
		<script src="<?=base_url();?>assets/js/jquery/jquery-1.9.1.min.js"></script>
	
    	<title><?=$website_title?></title>

    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="viewport" content="width=device-width, initial-scale=1" />
    	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
 </head>

<body class="inside_view v1">
	<div id="content" >
        <div class="row">
            <div class="column twelve">         
                    
                    <div class="row">
                    	<div class="column eight push-two">
                            <div class="search-area">
								<h3 align="center" style="padding:50px 0 0 0">Welcome to DTT Application</h3>

                           <strong>
                                Welcome to the ASEANstats Data Transmission Tool (DTT) <br/>

                            </strong>
                            <p>
                                For using DTT, a user must be registered by ASEANstats. <br>
                                To obtain a username and password, please contact stats@asean.org. <br><br>
                                Thank you.
                                <br><br>
                                <strong>
                                    Please click <a href='<?=site_url('login')?>'>here</a> to proceed to the DTT.
                                </strong>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
	</div>
</body>
</html>
