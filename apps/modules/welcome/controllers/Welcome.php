<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
        
        //if not logged in , display login form
		if ($this->ion_auth->logged_in()){
            redirect('auth');
        }
     
	}
	
    function index(){
        $this->viewparams['website_title']  = "DTT Application";
		$this->load->view("vwelcome.php",$this->viewparams);
    }
    
}
