<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
		parent::__construct();
		$this->load->model('page/page_model');
        $this->load->helper('string');
    }
	
    function index(){
    	$this->lists();
    }
    
    function lists(){
        $this->viewparams['title_page']	= lang('llist_page');
		$this->viewparams['ladd_button_title']	= lang('ladd_page');
		
		parent::viewpage("vpage_list");
    }
    
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'page_title'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->page_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->page_model->countSearch($searchv);
        
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("page_id","page_active","page_title","page_slug","page_description","page_active","edit",'hapus'),
			"id"		=> "page_id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
    function setActive($value,$id){
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		$data['active']	= $value;
		
		$this->page_model->setActive($id,$data);
		
		$success = true;
        if($this->db->affected_rows()){
           $message = ($value == 1)?lang('ldata_isset_active'):lang('ldata_isset_inactive');
           $success = true;
        }
        else {
            $success = false;
            $message = lang('ldata_change_status_is_failed');
        }

		$result = array("message" => $message,"success" => $success);
  		echo json_encode($result);
	}
    
    function delete($id){
    	//protected id : 1 and 2. about us and home
    	$protected_id = array(1,2);

    	if(!in_array($id,$protected_id)){

	        $data['id'] 	= $id;
	        $success = true;
	        
	        $this->page_model->delete($data);
	        if($this->db->affected_rows()){
	            $message = lang('ldata_is_deleted');
	        }
	        else {
	            $success = false;
	            $message = lang('ldata_is_failed_deleted');
	        }
		}else{
			$success = false;
	        $message = lang('ldata_is_protected');
	        
		}
		$result = array("success"=>$success,"message" => $message);
  		echo json_encode($result);
	}
    
    function add(){
    	$this->viewparams['title_page']	= lang('ladd_page');
	
        $this->form(0);
    }
   
    
	function edit($id=-1){
       /* can not access this page without valid id*/
		$data = $this->page_model->getDataByIdSimple($id);
		if(!$data){
			redirect('admin/page');
		}
        
		$this->viewparams['title_page']	= lang('lmodify_page');
		$this->viewparams['data']	= $data[0];
			
        $this->form($id);
	}
    
    function form($id=-1){
        $this->load->helper('form');
  
		parent::viewpage("vpage_form");
    }
    
   function doupdate(){

		$id 		        = $this->input->post('page_id');
		$title              = trim($this->input->post('title'));
		$description        = trim($this->input->post('description'));
        $content            = trim($this->input->post('content'));
		$meta_keyword       = trim($this->input->post('meta_keyword'));
		$meta_description   = trim($this->input->post('meta_description'));
		$date_posted 		= ($this->input->post('date_posted'))?$this->input->post('date_posted'):date("Y-m-d");
		$addnew				= ($this->input->post('addnew'))?$this->input->post('addnew'):0;
		$active 			= (isset($_POST['active']) && $_POST['active'] == 1 ) ? $_POST['active']:0;
		
		$message = array();
		$redirect = "";
		$is_error = false;
		
		//-- run form validation
		//name is mandatory
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'lang:ltitle', 'required');
		
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
			if(form_error('title'))
				$message[] = form_error('title'); 
			
		} else {
		
			if(!$is_error){

				$values = array(
					"title"			=> $title
					,"description"	=> $description
                    ,"active"		=> $active
                    ,"content"		=> $content
                    ,"meta_keyword"		=> $meta_keyword
                    ,"meta_description"		=> $meta_description
                    ,"date_posted"  => $date_posted
					,"updated_on" 	=> date("Y-m-d H:i:s")
					,"updated_by"	=> _userid()
				);
				
		
				
				//add
				if(!$id){
					$values["created_on"]  = date("Y-m-d H:i:s");
					$values["created_by"]	= _userid();
					$values["slug"] = $this->page_model->generate_slug($title,0);
					$id = $this->page_model->insert($values);
					//$this->page_model->update($values,$id);
					
					$message[] = lang('ldata_success_inserted');
				}else{
					//edit the topic 
				    $slug = $this->input->post('titleslug');
				    if(!$slug)
					   $values["slug"] = $this->page_model->generate_slug($title,$id);
                    else
                       $values["slug"] = $this->page_model->generate_slug($slug,$id);
                 	$this->page_model->update($values,$id);
				
					$message[] = lang('ldata_success_updated');
				}
				
				if($addnew)
					$redirect = site_url("admin/page/add");
				else
					$redirect = site_url("admin/page");
			}
		}
	
		$result = array(
			"message"	=> implode("<br/>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
