<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Public_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
        $this->load->model('page_model');
        
	}
	
	function show_404($page='',$log_error=true){
		// By default we log this, but allow a dev to skip it
		if ($log_error)
		{
			if(!$page)
				$page = $_SERVER['PHP_SELF'];
			log_message('error', '404 Page Not Found --> '.$page);
		}
		parent::viewpage('error_404');
	}
    
    function detail($slug='',$page_id=0){
    
    	if(!$slug){
      		echo modules::run('page/show_404');
			exit;
        }

		$data = $this->page_model->get_data_by_slug($slug,1);	//get active page
		if(!count($data)){   //no page found
        	echo modules::run('page/show_404');
			exit;
        }
        
        //if page please-login , must be not logged in user
        if($slug == "please-login"){
        	if(isMember()){
        		//if loggin member, don't display this page
        		//redirect to home
        		echo modules::run('page/show_404');
				exit;
        	}else{
        		//if not loggin, dan tidak dari page yg membutuhkan login, atau direct access, jg ga bisa akses halaman ini
        		//tidak ada flashdata not_loggedin
            	$not_loggedin = $this->session->flashdata('not_loggedin');
            	if(!$not_loggedin){
        			echo modules::run('page/show_404');
					exit;
            	}
        	}	
        }else
        if($slug == 'not-authorized'){
       		$sess = $this->session->flashdata('not_auth');
       		
            if(!$sess){
            	echo modules::run('page/show_404');
				exit;
            }
        }else
		if($slug == 'membership-expired'){
			$sess = $this->session->flashdata('membership_expired');
 			if(!$sess){
            	echo modules::run('page/show_404');
				exit;
            }
        }
             
        $this->viewparams['data']   			= $data[0];
        $this->viewparams['slug']   			= $slug;
      
        $this->viewparams['website_title']   		= $data[0]->page_title." - ".config_item('website_title');
        $this->viewparams['website_description']   	= ($data[0]->page_meta_description)?$data[0]->page_meta_description." - ".config_item('website_description'):config_item('website_description');
        $this->viewparams['website_keyword']   		= ($data[0]->page_meta_keyword)?$data[0]->page_meta_keyword." - ".config_item('website_keyword'):config_item('website_keyword');
  		
       	parent::viewpage("page_detail");
    }
  
    function view($id=''){
         if(!$id){
            redirect();
            exit;
        }
        $data = $this->page_model->getDataById($id,1);  //get active page
        
        if(!count($data)){   //no page found
        	echo modules::run('page/show_404');
			exit;
        }
        
        $content['data']   = $data[0];
        $content['website_title']   		= $data[0]->page_title." - ".config_item('website_title');
		$content['website_description']   	= ($data[0]->page_meta_description)?$data[0]->page_meta_description." - ".config_item('website_description'):config_item('website_description');
        $content['website_keyword']   		= ($data[0]->page_meta_keyword)?$data[0]->page_meta_keyword." - ".config_item('website_keyword'):config_item('website_keyword');
     
		$template['content'] = $this->load->view('page', $content, TRUE);
		$template['footer'] = $this->load->view('footer', $content, TRUE);
		$this->load->view('template', $template);
    }
    
    function getchild($id_topic){
    	//get topic info
    	$topic = $this->page_model->getDataById($id_topic,1);
    	$data = $this->page_model->get_total_child($id_topic,1) ; //get active child
    	$total = $data[0]->total;
    	
    	echo json_encode(array("total"=>$total , "description"=>$topic[0]->page_description));
    }
    
}

/* End of file oage.php */
/* Location: ./apps/modules/page/controllers/page.php */
