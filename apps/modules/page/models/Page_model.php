<?php
class Page_model extends CI_Model {
	var $is_count = false;
	var $data = array();
	var $id_topic_level1 = 0;
	var $ids = array();
		
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
	function getDataByIdSimple($id){
        $this->db->select("pages.*");
        $this->db->select("DATE_FORMAT(".$this->db->dbprefix."pages.date_posted,'%Y-%m-%d') as date_posted_fmt2",false);
         
		$this->db->where("id",$id);
    	$query = $this->db->get('pages');
		$result = $query->result();
		return $result;
	}
	
    function get_page_list($searchval=array(),$sidx='',$sord=''){
        
        return $this->search(0,0,$sidx,$sord,$searchval);    
    }
    
    function get_data_by_slug($slug,$active=''){
        $searchval['slug']  = $slug;
        
        
        if($active){
        	$searchval['active']  = $active;
        }
        
        return $this->search(1,1,'page_title','asc',$searchval);
    }
    
    function get_topics($id_parent=0){
    	$searchval = array(
    		"id_category"	=> 10  //topics id
    		,"status"		=> 1 //get active 
    		,"id_parent"	=> $id_parent
    	);
    	return $this->search(0,0,'page_order,page_title','asc',$searchval);
    }
    
    //search_page
    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	foreach($searchval as $key=>$val){
    		if(!is_array($val))
	    		$searchval[$key] = trim(urldecode($val));
		}
	
		if( isset($searchval['active']))
			$this->db->where("pages.active",$searchval['active']);
		
		if( isset($searchval['id']) && $searchval['id'])
			$this->db->where("pages.id",$searchval['id']);
		
		if( isset($searchval['slug']) && $searchval['slug'])
			$this->db->where("pages.slug",$searchval['slug']);
		
		if( isset($searchval['title']) &&  isset($searchval['description']) ) {
		 	$this->db->where("( ".$this->db->dbprefix."pages.title like '%".$searchval['title']."%' OR ".$this->db->dbprefix."pages.description like '%".$searchval['description']."%')" );
		}
				
		if($this->is_count){
			$this->db->select("count(".$this->db->dbprefix."pages.id) as total");
		}else{
			$this->db->select("pages.id as id_container");
            $this->db->select("pages.title as container");
            $this->db->select("CASE ".$this->db->dbprefix."pages.active WHEN 1 THEN '".lang('lpublish')."' ELSE '".lang('lunpublish')."' END page_status_str", false);
            
			$this->db->select("DATE_FORMAT(".$this->db->dbprefix."pages.date_posted,'%d/%m/%Y %H:%i:%s') as date_posted_fmt",false);
            $this->db->select("DATE_FORMAT(".$this->db->dbprefix."pages.date_posted,'%Y-%m-%d') as date_posted_fmt2",false);
            $this->db->select("DATE_FORMAT(".$this->db->dbprefix."pages.updated_on,'%d/%m/%Y %H:%i:%s') as updated_on_fmt",false);
			$this->db->select("pages.id as page_id");
			$this->db->select("pages.title as page_title");
            $this->db->select("pages.active as page_active");
            $this->db->select("pages.description as page_description");
            $this->db->select("pages.slug as page_slug");
            $this->db->select("pages.meta_keyword as page_meta_keyword");
            $this->db->select("pages.meta_description as page_meta_description");
            $this->db->select("pages.content as page_content");
            $this->db->select("'' as edit");
			$this->db->select("'' as hapus");
       
			
			$this->db->join("users b","b.id = ".$this->db->dbprefix."pages.updated_by","left outer");	
		}
	
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
			
		$query = $this->db->get('pages');
		$result = $query->result();
	
  		//echo $this->db->last_query();
  		if(!$this->is_count)
			return $result;
		else	
			return $result[0]->total;
		
    }
    
    /* countSearchPage */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res;
	}
    
     //deletePage
    function delete($data){
    	$this->db->where('id', $data['id']);
		$this->db->delete('pages'); 
    }
    
    //insertPage
    function insert($values){
		$this->db->insert("pages", $values); 
		return $this->db->insert_id();
	}
	
    //updatePage
	function update($values,$id){
		$this->db->where("id",$id);
		$this->db->update("pages", $values);
	}
	
	function get_titles($ids){
		$this->db->order_by("level","asc");
		$this->db->order_by("display_order","asc");
		
		$this->db->where_in("id",$ids);
		$query = $this->db->get('pages');
		$result = $query->result();
		return $result;
	}
    //getDataPageById
    //Status : 0=inactive, 1= active
	function getDataById($id,$status=-1){
		$searchval['id']	= $id;
		if($status > 0)
			$searchval['active']	= $status;
		
		return $this->search(1,1,'','',$searchval);
	}
    
    function slug_exists($slug,$exlude_id=0){
        $this->db->select("count(".$this->db->dbprefix."pages.id) as total");
        $this->db->where("slug",$slug);
        
        if($exlude_id)
            $this->db->where("id <>",$exlude_id);
			
		$query = $this->db->get('pages');
		$result = $query->result();
        
        if($result[0]->total)
            return true;
        else
            return false;
    }
    
    function generate_slug($title,$id){
        $slug = url_title($title, 'dash', true);
        $new_slug = $slug;
        $counter = 1;
        
        while($this->slug_exists($new_slug,$id)){
            $new_slug = $slug."-".$counter;
            $counter++;        
        }
        
        return $new_slug;
    }
    
	function get_footer(){
		$this->db->select("content");
		$this->db->where("status",1); //active page
        $this->db->where("title","footer");
			
		$query = $this->db->get('page');
		$result = $query->result();
        
        if(count($result))
            return $result[0]->content;
        else
            return false;
	}
 	
	function setActive($id,$data){
		$data["updated_on"]	= date("Y-m-d H:i:s");
		$data["updated_by"]	= _userid();
	
  		$this->db->where('id', $id);
		$this->db->update('pages', $data); 
	}
   
	
}