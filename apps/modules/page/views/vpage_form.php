<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/adapters/jquery.js"></script>


<script>
	$(document).ready(function(){
	   	
        var config = {
				height:<?=config_item('ck_height')?>,
				width:<?=config_item('ck_width')?> ,
				path: '<?php echo base_url(); ?>assets/js/ckeditor/' ,
				enterMode: CKEDITOR.ENTER_BR
				
			};
		CKFinder.setupCKEditor( null, '<?php echo base_url(); ?>assets/js/ckfinder/' );	
		$('textarea#content').ckeditor(config);
		
        
        //datepicker
		$('.datepicker').datepicker({ dateFormat: "yy-mm-dd"});

		$('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/page');?>");
		});
		
		$('#submit-add-btn').click(function(e){
			e.preventDefault(); 
			$('#addnew').val(1);
			dopost($(this));
		});
		
		$('#_date_posted').click(function(e){
			$('#date_posted').val('');
		});

        $('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
			
		//tooltip
		$('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});
	
	});
	
	function dopost(obj){
		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();

		$.post('<?=site_url('admin/page/doupdate');?>', 
				$("#input-form").serialize(),
				function(returData) {
					
					$('#loadingmessage').hide();
					obj.removeAttr('disabled');
     				
					$('#show_message').slideUp('normal',function(){
						
						if(returData.error){
							var rv = '<div class="alert alert-error">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							var rv = '<div class="alert alert-success">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, 1000);
							});	
						}	
					});
					
				},'json');
	}
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/page')?>"><?=lang('llist_page')?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
        
			 <?php 
              $hidden = array(
                  "page_id"             => (isset($data->id))? $data->id:0
                  ,"addnew"              => 0
              );
              echo form_open("admin/page/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
             <div class="row-fluid">
				<div class="span6">
		
                   	<div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;<?=lang('ltitle');?></b></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='title' id='title' value="<? if(isset($data->title) && $data->title) echo $data->title?>" maxlength='100'>
						</div>
					</div>
					
                    
                    <? if(isset($data)){?>
                   	<div class="control-group">
						<label class="control-label" for="slug"><?=lang('lslug');?></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='titleslug' id='slug' value="<? if(isset($data->slug) && $data->slug) echo $data->slug?>" maxlength='100'>
						</div>
					</div>
					<?}?>
					
					<div class="control-group">
						<label class="control-label" for="description"><?=lang('ldescription');?></label>
						<div class="controls">
							<textarea  name='description' id='description' class="input-xlarge"><? if(isset($data->description) && $data->description) echo $data->description?></textarea>
						</div>
					</div>
			     </div>
                 <div class="span6">
                    		<div class="control-group">
						<label class="control-label" for="meta_keyword"><?=lang('lmeta_keyword');?></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='meta_keyword' id='meta_keyword' value="<? if(isset($data->meta_keyword) && $data->meta_keyword) echo $data->meta_keyword?>" maxlength='255'>
						</div>
					</div>
	
					<div class="control-group">
						<label class="control-label" for="meta_description"><?=lang('lmeta_description');?></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='meta_description' id='meta_description' value="<? if(isset($data->meta_description) && $data->meta_description) echo $data->meta_description  ?>" maxlength='255'>
						</div>
					</div>
                    
                     <div class="control-group">
    						<label class="control-label"><?=lang('ldate_posted')?></label>
    						<div class="controls">
    						  <input  class="input-small datepicker" type="text" name='date_posted' id='date_posted' value="<? if(isset($data->date_posted_fmt2) && ($data->date_posted_fmt2 <> "0000-00-00")) echo $data->date_posted_fmt2?>">
    						  <img id="_date_posted" border="0" src="<?=base_url()?>assets/admin/img/b_del.gif">
    						 </div>
					</div>
                        
					<div class="control-group">
						<label class="control-label" for='status'><?=lang('lactive')?></label>
						<div class="controls">
						  <label class="checkbox inline">
							<input type="checkbox" id="status" name='active' value="1" <? if( (isset($data->active) && $data->active) || !isset($data) )echo "checked"?>> <?=lang('lyes')?>
						  </label>
						</div>
					  </div>
	         
                 </div><!--/ span6 -->
                 <div class="span12">
                        <div class="control-group">
						<?=lang('lcontent');?>
                        <br />
						<textarea name='content' id='content'>
                        <? if(isset($data->content) && $data->content) echo $data->content?>
                        </textarea> 
						
					</div>
                 </div>
        </div><!--/ row-fluid -->
        
        
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn btn-primary" id='submit-add-btn'><?=lang('lsave_changes_and_add_new')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
					</div><!--/form-actions-->
					
				</form>   <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->


<script src="<?php echo base_url(); ?>assets/admin/js/jquery.chosen.min.js" type="text/javascript"></script>
