<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("country/country_model");
		$this->load->model("user/user_model");
		
	}

     function get_by_country($country_id){
        if(!isLoggedIn()){
           redirect('auth/logout');
        }
        
        $result = $this->user_model->search_simple(0,1,'username,country_name','asc',array("country_id"=>$country_id));
        echo json_encode($result);
    }
    
    function dochange_password(){
        $this->load->library(array('form_validation'));
		
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        $user = $this->ion_auth->user()->row();
        $is_error = false;
        $redirect = "";
        
		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
		}else{
            $identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
                $is_error = false;
                $message =  $this->ion_auth->messages();
                $redirect = site_url('auth/logout');
                
			}
			else
			{
				$is_error = true;
                $message =  $this->ion_auth->errors();
			}
        }
        $result = array("error"=>$is_error,"message" => $message, "redirect" => $redirect);
        echo json_encode($result);

    }
    // change password
	function change_password(){
        if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
        $this->load->helper('form');
        $user = $this->ion_auth->user()->row();
        
        $this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
		$this->viewparams['old_password'] = array(
            'name' => 'old',
            'id'   => 'old',
            'type' => 'password',
        );
        $this->viewparams['new_password'] = array(
            'name'    => 'new',
            'id'      => 'new',
            'type'    => 'password',
            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
        );
        $this->viewparams['new_password_confirm'] = array(
            'name'    => 'new_confirm',
            'id'      => 'new_confirm',
            'type'    => 'password',
            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
        );

        $this->viewparams['title_page']	= lang('lchange_password');
		parent::viewpage("vuser_changepass");

	}
    
	function test_sendmail(){
		/** sending email notification **/
		$this->load->library('email');
		
		$mail_config = $this->settings;
		
		$email = "dedy.adhiewirawan@gmail.com";
		$email_message= "testing email bprosearch.com";
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from(config_item('sender_mail'), config_item('sender_name'));
					 
	    $this->email->subject('test mail');
		$this->email->message($email_message);
    	$this->email->send();
    	
		echo $this->email->print_debugger();	
	}
	
	function generate_password($password,$salt){
		echo md5($password.$salt);
	}

}
