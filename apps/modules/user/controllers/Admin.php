<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
        
        //user module only for admin and staff
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
		$this->load->model('user_model');
        $this->load->model('country/country_model');
        $this->load->model('package/package_model');
	}
	
	function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('llist_user');
		$this->viewparams['ladd_button_title']	= lang('ladd_user');
	
		parent::viewpage("vuser_list");
	}
    
    function setActive($value,$id){
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		$data['active']	= $value;
		
		$this->user_model->setActive($id,$data);
		
        $data = $this->user_model->search_simple(1,1,'','',array('id'=>$id));
        $this->firephp->log('get from active : '.$this->db->last_query());
		
		$message = ($value == 1)?lang('ldata_isset_active'):lang('ldata_isset_inactive');
		
		$result = array("message" => $message,"username"=>$data[0]->full_name);
  		echo json_encode($result);



	}

	function delete($id){
        $data['id'] 	= $id;
		$data['deleted'] = 1;
		$data['active'] = 0;
        
		//update deleted to 1
        //$this->user_model->update($data,$id);
        
        //can not delete admin , id = '1' 
        if($id <> 1) {
            $success = true;
            if(!$this->user_model->is_used($id)) {
                $this->user_model->delete($data);
                if($this->db->affected_rows()){
                    $message = lang('ldata_is_deleted');
                }
                else {
                    $success = false;
                    $message = lang('ldata_is_failed_deleted');
                }
            }else{
                $success = false;
                $message = lang('luser_already_has_data');
            }
        }else{
            $success = false;
            $message = lang('lthis_user_cannot_be_deleted');
        }
        
		
		
		$result = array("success"=>$success,"message" => $message);
  		echo json_encode($result);
	}
	
    function edit($id){
      /*  echo time();
        echo date("Y-m-d",time());
        */
       /* can not access this page without valid id*/
		$data = $this->user_model->get_user_data($id);
        
		if(!$data){
			redirect();
		}
        
		$this->viewparams['title_page']	= lang('lmodify_user');
		$this->viewparams['data']	= $data;
			
        /* user group list */
       // $user_groups = $this->user_model->getUserGroup();
		//$this->viewparams['user_groups'] = $user_groups;
            
        $this->form($id);
	}
    
    function form($id=0){
         $this->load->library(array('form_validation'));
		
		/* country list */    
   		$countries = $this->country_model->search_country(0,0,'orders,name',$sord='asc');
		$this->viewparams['countries'] = $countries;
        
        /* user group list */
        $user_groups = $this->user_model->getUserGroup();
        $this->viewparams['user_groups'] = $user_groups;  

        //packages list
        $searchval = array("active"=>"1"); 
        if($id){
			$data = $this->viewparams['data'];
        	
        	//edit mode, include package yg ga active
        	if($data->package_id){
        		$searchval['include_id'] = $data->package_id;
        	};
        }

        $packages = $this->package_model->search(0,0,'price','asc',$searchval);

        $this->viewparams['packages'] = $packages;  

		parent::viewpage("vuser_form");
    }
	
	function add(){
        if(!isAdmin()) {
            redirect('admin/user');
        }
      	$this->viewparams['title_page']	= lang('ladd_user');
	
        $this->form(0);
	}
	
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'full_name'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->user_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->user_model->countSearch($searchv);
        
		$this->firephp->log($this->db->last_query());
		$this->firephp->log("count : ".$count);
		for($i=0;$i<count($query);$i++){
			//$query[$i]->edit = $query[$i]->delete = "";
            $query[$i]->user_group_name = ucfirst($query[$i]->user_group_name);
		}
  
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("user_id","active","full_name","user_group_name","full_name","email","country_name","package_name","package_enddate_fmt","active","edit",'hapus'),
			"id"		=> "user_id"
 		);
            
        parent::loadDataJqGrid();
		
	}
    
    function check_required()
    {
        $choice = $this->input->post("fields");
        if(is_null($choice))
        {
            $choice = array();
        }
        
        $fields = implode(',', $choice);
        $this->firephp->log("check : ".$choice.";".$fields);
        if($fields != '')
            return true;
        else
            return false;   
    }
    
	//update_user
	function doupdate(){
		if(!$this->input->is_ajax_request()) {
            redirect('auth/logout');
        }
        
        if(!isAdmin()) {
           	$result = array(
                "message"	=> lang('linvalid_profile')
                ,"error"	=> true
                ,"redirect"	=> site_url('admin/user')
            );
		
            echo json_encode($result);
            return;
        }

		/* check mandatory fields */
		$id		        = $this->input->post('id');
        $group_id		= $this->input->post('group_id');
        
		$full_name        = $this->input->post('full_name');
        $username = $email 	= $this->input->post('email');
        
        $country          = $this->input->post('country');
		$mobile 	      = $this->input->post('mobile');
		$phone 	          = $this->input->post('phone');
		$active	          = $this->input->post('active');
		$package_id	          = $this->input->post('package_id');
		$package_enddate	          = $this->input->post('package_enddate');
		
		//mandatory in add mode
		$password		= $this->input->post('password');
		$retype_password	= $this->input->post('retype_password');
		
		$message = array();
		$redirect = "";
		$is_error = false;
		
		
		//-- run form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('full_name', 'lang:luser_fullname', 'required');
		//$this->form_validation->set_rules('username', 'lang:luser_name', 'required');
		$this->form_validation->set_rules('email', 'lang:lemail', 'required|valid_email');
		$this->form_validation->set_rules('country', 'lang:lcountry', 'required');
		
		//add mode
		if(!$id){
			$this->form_validation->set_rules('password', 'lang:lpassword', 'required|matches[retype_password]');
			$this->form_validation->set_rules('retype_password', 'lang:lretype_password', 'required');
		}else{
			if($password){
				$this->form_validation->set_rules('password', 'lang:lpassword', 'required|matches[retype_password]');
				$this->form_validation->set_rules('retype_password', 'lang:lretype_password', 'required');
			}
			
		}
		
		$this->form_validation->set_error_delimiters('', '<br/>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
            
			if($email) {
                //check unique email dan login name
                $data_exists = $this->user_model->email_exists($email,$id);
                $this->firephp->log($this->db->last_query());
                if($data_exists){
                    $message[] = lang('lemail_exists_alert'); 
                }
            }
            
			$message[] = validation_errors();
			
		}
		
		if(!$is_error){
			
            /* add/update if no error */
            if(!$is_error){
        
	       		$data = array(
                    "full_name"          => $full_name
					,"email"		     => $email
                    ,"country_id"           => $country
				    ,"mobile"            => $mobile
				    ,"phone"             => $phone
				    ,"active"             => ($active)?$active:0
				    ,"package_id"             => ($package_id)?$package_id:NULL
				    ,"package_enddate"             => ($package_enddate)?dbdate($package_enddate):NULL
                    ,"updated_by"       => _userid()
                    ,"updated_on"       => time()
                    
				);
				
                
				//add
				if(!$id){
				 	
                    $id = $this->ion_auth->register($username, $password, $email, $data, array($group_id));
                    
					if($id)	
						$message[] = lang('ldata_success_inserted');
					else
						$message[] = lang('ldata_failed_inserted');
						
				}else{
					//if edit mode, and password and filled in , will reset password
					if($password){
                        $data['password']       = $password;
                    }
                    
                    $this->ion_auth->update($id, $data);
                    
                    // to remove the user (#ID:123) from all of the assigned groups call this:
					$this->ion_auth->remove_from_group(false, $id);

					// to add the user (#ID:123) to the 'publisher' group call this:
					$this->ion_auth->add_to_group($group_id, $id);

					$message[] = lang('ldata_success_updated');
				}
				
				$redirect = site_url("admin/user");
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
	function dochange_password(){
        $this->load->library(array('form_validation'));
		
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        $user = $this->ion_auth->user()->row();
        $is_error = false;
        $redirect = "";
        
		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
		}else{
            $identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
                $is_error = false;
                $message =  $this->ion_auth->messages();
                $redirect = site_url('auth/logout');
                
			}
			else
			{
				$is_error = true;
                $message =  $this->ion_auth->errors();
			}
        }
        $result = array("error"=>$is_error,"message" => $message, "redirect" => $redirect);
        echo json_encode($result);

    }
    // change password
	function change_password(){
        if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
        $this->load->helper('form');
        $user = $this->ion_auth->user()->row();
        
        $this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
		$this->viewparams['old_password'] = array(
            'name' => 'old',
            'id'   => 'old',
            'type' => 'password',
        );
        $this->viewparams['new_password'] = array(
            'name'    => 'new',
            'id'      => 'new',
            'type'    => 'password',
            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
        );
        $this->viewparams['new_password_confirm'] = array(
            'name'    => 'new_confirm',
            'id'      => 'new_confirm',
            'type'    => 'password',
            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
        );

        $this->viewparams['title_page']	= lang('lchange_password');
		parent::viewpage("vuser_changepass");

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
