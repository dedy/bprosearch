<?php
class User_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function canAccess($slug,$id){
        //user can access page 2 , kalau membership active, dan package include category tsb
        $user_package = _UserPackage();
        if(!$user_package){
            //blom ada package , can not access
            return false;
        }else {
            //ada package, but not active, can not access
            if(!$this->MembershipIsActive()){
                return false;
            }else{
                //check package
                $CI =& get_instance();
                $CI->load->model('package/package_model');
                $package = $CI->package_model->get_data_by_id($user_package);
                
                //if all category, can access all 
                if($package->all_category == "1"){
                    return true;
                }else {
                    //if not all category, check if exists in category list
                    $package_categories = $CI->package_model->get_data_category($user_package);
                    if(count($package_categories)){
                        foreach($package_categories as $p){
                            //if the selected category exists in the package, return true, can access
                            if($p->category_id == $id){
                                return true;
                            }
                        }

                        //not exists , return false
                        return false;
                    }
                }
            }
        }
    }

    function MembershipIsActive(){
        $enddate = _UserPackageEnddate();
        if(!$enddate){
            return false;
        }
            
        $date_now = (new DateTime())->format('Y-m-d');
        $date2    = (new DateTime($enddate))->format('Y-m-d');

        //membership is active if membership enddate is bigger than today
        if($date2 >= $date_now){
            return true;
        }else {
            return false;
        }
    }


    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
	function get_user_data($id){
		$searchval['id'] = $id;
		$result = $this->search(1,1,'full_name','asc',$searchval);
		return $result[0];
	}
	
   	/* delete user*/
    function delete($data){
       	//-- delete records    	
    	$this->db->where('id', $data['id']);
		$this->db->delete('users'); 
    }
    
    /* insert_users */
    function insert($values){
		$this->db->insert("users", $values); 
		return $this->db->insert_id();
	}
	
	function update($data,$id){
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
	}
	

	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('users.id',$searchval['id']);
        }

        if(isset($searchval['country_id']) && $searchval['country_id']){
            $this->db->where('users.country_id',$searchval['country_id']);
        }
        
        if(isset($searchval['username'])){
            $this->db->where('users.username',$searchval['username']);
        }
        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            $this->db->where('users.id <>',$searchval['exclude_id']);
        }
        
		if($this->is_count){
			$this->db->select("count(users.id) as total");
		}else{
			$this->db->select("users.*");
			$this->db->select("users.id as user_id");
			$this->db->select("groups.name as user_group_name");
			$this->db->select("groups.id as group_id");
			$this->db->select("country.name as country_name");
            $this->db->select("country.id as country_id");
			$this->db->select("packages.name as package_name");
            $this->db->select("DATE_FORMAT(".$this->db->dbprefix."users.package_enddate,'%d/%m/%Y') as package_enddate_fmt",false);

			$this->db->select("'' as edit");
			$this->db->select("'' as hapus");
            
			$this->db->select("FROM_UNIXTIME(users.last_login ,'%d/%m/%Y %H:%i:%s') as last_login_fmt",false);
            
       }
		
        //exclude admin
       // $this->db->where("user_id <> ",'1');
        
        //and not deleted
        $this->db->where("deleted <>",'1');
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->join('users_groups',"users_groups.user_id = users.id","left outer");
		$this->db->join("groups","groups.id = users_groups.group_id");
        $this->db->join("country","country.id = users.country_id","left outer");
		$this->db->join("packages","packages.id = users.package_id","left outer");
        $this->db->from('users');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function setActive($id,$data){
		$data["updated_on"]	= date("Y-m-d H:i:s");
		$data["updated_by"]	= _userid();
	
  		$this->db->where('id', $id);
		$this->db->update('users', $data); 
	}

    function getUserGroup(){
       // $this->db->where("id >=",_groupid());
		$this->db->order_by("id","asc");
        $this->db->from('groups');
		
		$query = $this->db->get();
        
 		return $query->result();
    }
    
    function search_simple($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('users.id',$searchval['id']);
        }

        if(isset($searchval['country_id']) && $searchval['country_id']){
            $this->db->where('users.country_id',$searchval['country_id']);
        }
        
        if(isset($searchval['username'])){
            $this->db->where('users.username',$searchval['username']);
        }
        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            $this->db->where('users.id <>',$searchval['exclude_id']);
        }
        
		if($this->is_count){
			$this->db->select("count(users.id) as total");
		}else{
			$this->db->select("users.username");
			$this->db->select("users.full_name");
            $this->db->select("users.id as user_id");
            $this->db->select("users.package_id as user_package_id");
			$this->db->select("users.package_enddate as user_package_enddate");
			$this->db->select("country.name as country_name");
			$this->db->select("country.id as country_id");
            
        }
        
          //exclude admin
        $this->db->where("user_id <> ",'1');
        
        //and not deleted
        $this->db->where("deleted <>",'1');
      
		
        
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->join('users_groups',"users_groups.user_id = users.id","left outer");
		$this->db->join("groups","groups.id = users_groups.group_id");
		$this->db->join("country","country.id = users.country_id","left outer");
        $this->db->from('users');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function username_exists($username,$id){
        $this->db->where("username",$username);
        if($id){
            $this->db->where("id <>",$id);
        }
        $this->db->from('users');
		
		$result = $this->db->get()->result();
        
        if(count($result)){
            return true;
        } else {
            return false;
        }
    }
    
    function email_exists($email,$id=0){
        $this->db->where("email",$email);
        if($id){
            $this->db->where("id <>",$id);
        }
        $this->db->from('users');
		
		$result = $this->db->get()->result();
        
        if(count($result)){
            return true;
        } else {
            return false;
        }
    }
    
    function is_used($id){
        //check in table files
        $this->db->select("count(id) as total");
        $this->db->where("created_by",$id);
		$query = $this->db->get('regulasi');
		$result = $query->result();
        
        if($result[0]->total > 0){
            return true;
        }
        
        return false;
    }
}