<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/adapters/jquery.js"></script>


<script>
	$(document).ready(function(){
	   	$('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url();?>");
		});
        
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
			
	
	});
	
	function dopost(obj){
		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();

		$.post('<?=site_url('admin/user/dochange_password');?>', 
				$("#input-form").serialize(),
				function(returData) {
					
					$('#loadingmessage').hide();
					obj.removeAttr('disabled');
     				
					$('#show_message').slideUp('normal',function(){
						
						if(returData.error){
							var rv = '<div class="alert alert-error">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							var rv = '<div class="alert alert-success">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, 1500);
							});	
						}	
					});
					
				},'json');
	}
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
            <a href="<?=base_url()?>">Home</a> <span class="divider">/</span>
        </li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
            
            <?php echo form_open("admin/user/dochange_password",array('id'=>'input-form','class'=>'form-horizontal'))?>
            
	       <div class="row-fluid">
				<div class="span12">
		
                   	<div class="control-group">
						<label class="control-label" for="old_password"><b>*&nbsp;<?=lang('change_password_old_password_label');?></b></label>
						<div class="controls">
                            <?php echo form_input($old_password);?>
						</div>
					</div>
                    
                    
                    <div class="control-group">
						<label class="control-label" for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
						<div class="controls">
                            <?php echo form_input($new_password);?>
						</div>
					</div>
                    
                    <div class="control-group">
						<label class="control-label" for="new_password_confirm"><?php echo lang('change_password_new_password_confirm_label');?></label>
						<div class="controls">
                            <?php echo form_input($new_password_confirm);?>
						</div>
					</div>
                </div>
           </div>
        
        
            <div id='show_message' style="display: none;"></div> 

            <div class="form-actions">
               <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
                  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
              </div>

                <button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('change_password_submit_btn')?></button>
                <button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
            </div><!--/form-actions-->

				<?=form_close();?>    <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->


<script src="<?php echo base_url(); ?>assets/admin/js/jquery.chosen.min.js" type="text/javascript"></script>
