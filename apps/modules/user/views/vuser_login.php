<!-- jQuery -->
<script src="<?=base_url();?>assets/admin/js/jquery-1.7.2.min.js"></script>

<!-- library for advanced tooltip -->
<script src="<?=base_url();?>assets/admin/js/bootstrap-tooltip.js"></script>


<script>
$(document).ready(function(){
	$('#submit-btn').click(function(e){
		//$('form#login-form').submit();
		//return;
		e.preventDefault(); 
		$('#loadingmessage').show();
		
		$.post('<?=site_url('admin/user/dologin');?>', 
			$("#login-form").serialize(),
			function(returData) {
				$('#loadingmessage').hide();
				$('#show_message').slideUp('normal',function(){
					if(returData.error){
						var rv = '<div class="alert alert-error">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal');	
							
					}else{
						var rv = '<div class="alert alert-success">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal',function(){
							 setTimeout(function() {
							    $('#show_message').slideUp('normal',function(){
								    if(returData.redirect){
								    	window.location.replace(returData.redirect);
								    }
							    });	
							  }, 1000);
						});	
					}	
				});
				
			},'json');
	});
		
	//tooltip
	$('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

});
</script>	


<h2>User Login</h2>
<div class="row">
	<div class="column seven">
        <div class="search-area">
			<form id='login-form' class="form-horizontal" action="<?=site_url('admin/user/dologin');?>" method="post">
				      <div class="input-large inputwrap">
                    <input class="" id="email" name="email" placeholder="<?=lang('lemail')?>" type="text" value="<?=$email?>" >
                </div><!--firstname-->
                <div class="input-large inputwrap">
                    <input class="" id="password" name="password" placeholder="<?=lang('lpassword')?>" type="password" value="" >
                </div><!--email--> 
                <button type="submit" class="btn btn-primary btn-large btn-contrast right" id="submit-btn" style="float:right">
                    <?=lang('llogin')?>
                </button>
                <div id='show_message' style="display: none;"></div>
                <span id='loadingmessage' style="display:none;">
                    <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
                </span>   
			</form>
		</div>
	</div>
</div>
