<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<link href='<?=base_url();?>assets/js/jquery/chosen/1.4.2/chosen.css' rel='stylesheet'>

<script>
	$(document).ready(function(){
		//chosen - improves select
        $('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect: true});

        //datepicker
        $('.datepicker').datepicker({ dateFormat: "dd/mm/yy",changeMonth: true, changeYear: true,yearRange: ((new Date().getFullYear())-1)+':' + ((new Date().getFullYear())+1)});
        
        $("#_package_enddate").click(function(){
               $("#package_enddate").val("");
           })

        $('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/user');?>");
		});
		
		$('#submit-add-btn').click(function(e){
			e.preventDefault(); 
			$('#addnew').val(1);
			dopost($(this));
		});
		
		$('#submit-btn').click(function(e){
			e.preventDefault(); 
			dopost($(this));
		});
			
	
	});
	
	function dopost(obj){
		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();

		$.post('<?=site_url('admin/user/doupdate');?>', 
				$("#input-form").serialize(),
				function(returData) {
					
					$('#loadingmessage').hide();
					obj.removeAttr('disabled');
     				
					$('#show_message').slideUp('normal',function(){
						
						if(returData.error){
							var rv = '<div class="alert alert-error">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							var rv = '<div class="alert alert-success">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, <?=config_item('message_delay')?>);
							});	
						}	
					});
					
				},'json');
	}
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=base_url()?>">Home</a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/user')?>"><?=lang('llist_user')?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
              <?php 
              $hidden = array(
                  "id"              => (isset($data->user_id))? $data->user_id:0
              );
              echo form_open("admin/user/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
                           
            <div class="row-fluid">
				<div class="span6">
                   
                    <?php
                    
                    if(!isset($data->user_id) || ($data->user_id <> 1)){  ?>
                        <div class="control-group">
                           <label class="control-label" for="name"><strong>*&nbsp;<?=lang('luser_group');?></strong></label>
                             <div class="controls">
                                 <select name='group_id' id='group_id' class="input-xlarge"  data-rel="chosen" data-placeholder="<?=lang('lplease_select_group')?>" >
                                   <option value=''><?=lang('lplease_select_group')?></option>
                                  <?php 
                                     foreach($user_groups as $value){?>
                                         <option value='<?=$value->id?>' <?php if(isset($data->group_id) && ($data->group_id == $value->id)) { echo "selected"; } ?>><?=ucfirst($value->name)?></option>
                                     <?php }?>
                                 </select>

                             </div>
                         </div> 
                    <?php }else 
                     if(isset($data->user_id) && ($data->user_id == 1)){  ?>
                        <input type="hidden" name='group_id' value ='<?=$data->group_id?>'>
                       <div class="control-group">
                           <label class="control-label" for="name"><strong>*&nbsp;<?=lang('luser_group');?></strong></label>
                             <div class="controls">
                                <?=ucfirst($data->user_group_name)?>
                             </div>
                         </div> 
                    <?php }?>
                    
                    <div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;<?=lang('luser_fullname');?></b></label>
						<div class="controls">
							<input class="input-xlarge" type="text" name='full_name' id='full_name' value="<?php if(isset($data->full_name) && $data->full_name) echo $data->full_name ?>" maxlength='150'>
						</div>
					</div>
                    
				
                    <div class="control-group">
    						<label class="control-label" for="description"><b>*&nbsp;<?=lang('lemail');?></b></label>
    						<div class="controls">
    							<input class="input-xlarge" type="text" name='email' id='email'  value="<?php if(isset($data->email) && $data->email) echo $data->email?>" maxlength='100'>
    						</div>
    					</div>
                    
                   
                </div>
                <div class="span6">
                       
                  <div class="control-group">
                      <label class="control-label" for="name"><strong>*&nbsp;<?=lang('lcountry');?></strong></label>
						<div class="controls">
                        	<select name='country' id='country' class="input-xlarge"  data-rel="chosen" data-placeholder="<?=lang('lplease_select_country')?>" >
    		                  <option value=''><?=lang('lplease_select_country')?></option>
                 		     <?php 
                        		foreach($countries as $value){?>
                                    <option value='<?=$value->id?>' <?php if(isset($data->country_id) && ($data->country_id == $value->id)) { echo "selected"; } ?>><?=$value->name?></option>
                        		<?php }?>
                        	</select>
						  
						</div>
					</div>
                    
                     <div class="control-group">
						<label class="control-label" for="name"><?=lang('lphone');?></label>
						<div class="controls">
                            <input type="text" id='phone' name='phone' value='<?php if(isset($data->phone) && $data->phone) echo $data->phone;?>' maxlength="25"/>
						  
						</div>
					</div>
                    
                      <div class="control-group">
						<label class="control-label" for="name"><?=lang('lmobile');?></label>
						<div class="controls">
                            <input type="text" id='mobile' name='mobile' value='<?php if(isset($data->mobile)) echo $data->mobile;?>'maxlength="25" />						  
						</div>
					</div>

					 <div class="control-group">
                      <label class="control-label" for="name"><?=lang('lmember_package');?></label>
						<div class="controls">
                        	<select name='package_id' id='package_id' class="input-xlarge"  data-rel="chosen" data-placeholder="<?=lang('lplease_select_package')?>" >
    		                  <option value=''></option>
                 		     <?php 
                        		foreach($packages as $package){
                        			$package_price = ($package->price > 0)?" - Rp. ".number_format($package->price):"";
                        			?>
                                    <option value='<?=$package->id?>' <?php if(isset($data->package_id) && ($data->package_id == $package->id)) { echo "selected"; } ?>><?=$package->name?> <?=$package_price?></option>
                        		<?php }?>
                        	</select>
						  
						</div>
					</div>
                    
                    <div class="control-group">
                        <label class="control-label">
                            <?=lang('lpackage_end_date')?>
                          </label>
                        <div class="controls">
                          <input class="input-small datepicker" type="text" name='package_enddate' id='package_enddate' value="<? if(isset($data->package_enddate) && ($data->package_enddate <> "0000-00-00")) echo $data->package_enddate_fmt?>">
                          <img id="_package_enddate" border="0" src="<?=base_url()?>assets/admin/img/b_del.gif">
                         </div>
                    </div>
                    	
                	
                    	<div class="control-group">
    						<label class="control-label" for='status'><?=lang('lstatus')?></label>
    						<div class="controls">
    						  <label class="checkbox inline">
                                  <input type="checkbox" id="active" name='active' value="1" <?php if( (isset($data->active) && $data->active) || !isset($data->active)){ echo "checked";}?>> <?=lang('lactive')?>
    						  </label>
    						</div>
    					  </div>
				  
                    
		            </div> <!--/span12-->
                      
                  </div><!--/row-fluid-->
                  
                        <fieldset>
    						<legend>
    							<?php 
    							//if edit mode , reset the existing password
    							if(isset($data)){?>
    								<?=lang('lreset_password');?>
    							<?php }else{?>	
    								<?=lang('lpassword');?>
    							<?php }?>
    						</legend>
                            
    					<div class="control-group">
    						<label class="control-label" for="description"><b>*&nbsp;<?=lang('lpassword');?></b></label>
    						<div class="controls">
    							<input class="input-xlarge" type="password" name='password' id='password' value="" maxlength='50'>
    						</div>
    					</div>
    					
    					<div class="control-group">
    						<label class="control-label" for="description"><b>*&nbsp;<?=lang('lretype_password');?></b></label>
    						<div class="controls">
    							<input class="input-xlarge" type="password" name='retype_password' id='retype_password' value="" maxlength='50'>
    						</div>
    					</div>
    				</fieldset>
				
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
					</div><!--/form-actions-->
			</form>   <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->


<script src="<?php echo base_url(); ?>assets/js/jquery/chosen/1.4.2/chosen.jquery.min.js" type="text/javascript"></script>
