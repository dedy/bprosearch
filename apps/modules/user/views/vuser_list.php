
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>


<script type="text/javascript">
	 jQuery().ready(function (){
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url('admin/user/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','active','username','<?=lang('luser_group')?>','<?=lang('lname')?>','<?=lang('lemail')?>','<?=lang('lcountry')?>','<?=lang('lpackage')?>','<?=lang('lend_date')?>','<?=lang('lstatus')?>','<?=lang('ledit')?>','<?=lang('ldel')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:2, align:"right",sortable:false},
                {name:'id',index:'user_id', hidden: true},
                {name:'active',index:'active', hidden: true},
                {name:'username',index:'user_fulname', hidden: true},
                {name:'groupname',index:'user_group_name',align:"left",stype:'text',width:5,},
                {name:'full_name',index:'full_name',align:"left",stype:'text',width:8,hidden:false,
                	formatter: function (cellvalue, options, rowObject) {
					   return '<a href="<?=site_url('admin/user/edit')?>/' + rowObject[1] + '"><u>'+cellvalue+'</u></a>';
					}
               	},
            	 {name:'email',index:'email',align:"left",stype:'text',width:10,hidden:false,
                	formatter: function (cellvalue, options, rowObject) {
					   return '<a href="<?=site_url('admin/user/edit')?>/' + rowObject[1] + '"><u>'+cellvalue+'</u></a>';
					}
               	},
                {name:'country',index:'country_name',align:"left",stype:'text',width:7,sortable:false},
                {name:'package_name',index:'package_name',align:"left",stype:'text',width:7},
                {name:'package_enddate',index:'package_enddate_fmt',align:"left",stype:'text',width:5,sortable:false},
            	{name:'status_icon', index: 'active', width: 2,align:"center",
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '#';
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                            	if(cellValue == "1")
            						return "<img src='<?=base_url()?>assets/admin/img/ico-yes.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
								else
            						return "<img src='<?=base_url()?>assets/admin/img/ico-standby.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
								
                              //return cellValue;
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                            	var val = $('#list1').jqGrid('getCell',rowId,'active');
                            	var username = $('#list1').jqGrid('getCell',rowId,'username');
                                
                            	//set inactive
                            	if(val == 1){
                            		var msg = 'User \''+username+ '\' will no longer access to admin panel';
                            		var setval = 0;
                            	}else{
                            		//set active
                            		var msg = 'User \''+username+ '\' will now have access to admin panel';
                            		var setval = 1;
                            	}
                            	
                                if(rowId == 1){
                                    return;
                                }else{
                                    $.confirm({
                                       title:"<?=lang('lchange_user_access_confirmation')?>",
                                       text: msg,
                                       confirmButton: "<?=lang('lok')?>",
                                       cancelButton: "<?=lang('lcancel')?>",
                                       confirm: function(button) {
                                          $.post("<?=site_url('admin/user/setActive')?>/"+setval+"/"+rowId, 
                                           function(data) {
                                               var rv = '<div class="alert alert-success">Access for user '+data.username+' is changed</div>';
                                                   $('#show_message').html(rv);
                                                   $('#show_message').slideDown('normal',function(){
                                                        setTimeout(function() {
                                                           $('#show_message').slideUp('normal',function(){
                                                              gridReload();
                                                           });	
                                                         }, <?=config_item('message_delay')?>);
                                                   });	
                                               //reload grid
                                               //gridReload();

                                           },"json"
                                       );
                                       },
                                       cancel: function(button) {
                                          // alert("You cancelled.");
                                       }
                                   });
                                }
                            	
                        }},
                        cellattr: function (rowId, cellValue, rawObject) {
                        	//column 2 , status 
                        	if(rawObject[2] == "1")	
                        	 	var attribute = ' title="<?=lang('lset_inactive')?>"' ;
                            else
                            	var attribute = ' title="<?=lang('lset_active')?>"' ;
                            	
                            return attribute ;
                        }
                  },
                        
                 {name:'edit',index:'edit', width:2, align:"left",sortable:false,align:"center",
                	formatter:'dynamicLink', 
      			 	formatoptions:{
      			 		 url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/user/edit')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
							}
					},
      			 	cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ledit')?>"' 
                            return attribute ;
                     }
      			 },
      			  {name: 'delete', index: 'delete', width: 2,align:"center",
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/user/delete')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                  if(rowId == 1) {
                                      return "";
                                  }else {
                                    return "<img src='<?=base_url()?>assets/admin/img/ico-delete.png' id='delete-"+rowId+"' class='delete' border='0' width='16px' height='16px'>";
                                    }
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                                $.confirm({
                                    title:"Delete user",
                                    text: "<?=lang('ldelete_user_confirmation')?>",
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    confirm: function(button) {
                                       $.post(
                                            "<?=site_url('admin/user/delete')?>/" + rowId, 
                                            function(data) {
                                                if(data.success)
                                                    var rv = '<div class="alert alert-success">'+data.message+'</div>';
                                                else
                                                    var rv = '<div class="alert alert-error">'+data.message+'</div>';

                                                    $('#show_message').html(rv);
                                                    $('#show_message').slideDown('normal',function(){
                                                         setTimeout(function() {
                                                            $('#show_message').slideUp('normal',function(){
                                                                if(data.success){
                                                                    gridReload();
                                                                }
                                                            });	
                                                          }, <?=config_item('message_delay')?>);
                                                    });	
                                                //reload grid
                                                //gridReload();
                                            },"json"
                                        );

                                    },
                                    cancel: function(button) {
                                       // alert("You cancelled.");
                                    }
                                });
                                
                                
                            	
                            }
                        },
                        cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ldelete')?>"' 
                            return attribute ;
                        }}
                              
                        
	          ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?}?>
            width: 1024,
            height: <?=$rowHeight?>,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'full_name',
            toppager: true, 
            //shrinkToFit:false,
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url('admin/user/loadDataGrid')?>",
				page:1
			}).trigger("reloadGrid");
	}

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url("admin")?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
            <?
            if(isAdmin()) { ?>
			<div>
				<a href="<?=site_url("admin/user/add")?>" class="btn btn-small btn-primary">
					<i class="icon-plus"></i> <?=$ladd_button_title?>
				</a>
			</div>
            <?}?>
			<br/>
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->