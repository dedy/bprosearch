<?php
class Type_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
   	/* delete user*/
    function delete($data){
       	//-- delete records    	
    	$this->db->where('id', $data['id']);
		$this->db->delete('types'); 
    }
    
    /* insert_types */
    function insert($values){
        $values["updated_by"]   = _userid();
        $values["updated_on"]   = date("Y-m-d H:i_s");
        $values["created_on"]   = date("Y-m-d H:i_s");
        $values["created_by"]   = _userid();
        
                 
		$this->db->insert("types", $values); 
		return $this->db->insert_id();
	}
	
	function update($data,$id){
        $data["updated_by"]   = _userid();
        $data["updated_on"]   = date("Y-m-d H:i_s");
        $this->db->where('id', $id);
		$this->db->update('types', $data); 
	}
	
    function get_data_by_id($id){
        $data = $this->search(1,1,'name','asc',array("id"=>$id));
        if(count($data)){
            return $data[0];
        }else{
            return false;
        }
    }
    
    function get_data_type($exclude_id=array(),$is_active=1){
        
        return $this->search(0,1,'name','asc',array("exclude_id"=>$exclude_id,"active"=>$is_active));
                
                
    }

    function search_simple(){
    	$query = $this->db->get('types');
		$result = $query->result();
	
  		return $result;
		
    }
    
    function getIdByName($name,$datas){
        if(count($datas)){
            foreach($datas as $data){
                if(strtoupper($data->name) == strtoupper($name)){
                    return $data->id;
                }
            }
        }
        
        return 0;
    }
        
    
    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('types.id',$searchval['id']);
        }

        if(isset($searchval['name']) && $searchval['name']){
            $this->db->where('types.name',$searchval['name']);
        }

        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            if(is_array($searchval['exclude_id'])) {
                $this->db->where_not_in('types.id',$searchval['exclude_id']);
            }else{
                $this->db->where('types.id <>',$searchval['exclude_id']);
            }
        }
        
        if(isset($searchval['active'])){
            $this->db->where('types.active',$searchval['active']);
        }
        
      	if($this->is_count){
			$this->db->select("count(types.id) as total");
		}else{
			$this->db->select("types.*");
			$this->db->select("'' as edit");
			$this->db->select("'' as hapus");
       }
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('types');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function is_exist($name,$exclude_id){
        $data = $this->search(1,1,'name','asc',array("exclude_id"=>$exclude_id,"name"=>$name));
        if(count($data)){
            return $data[0];
        }else{
            return false;
        }
    }
    
    function setActive($id,$data){
		$data["updated_on"]	= date("Y-m-d H:i:s");
		$data["updated_by"]	= _userid();
	
  		$this->db->where('id', $id);
		$this->db->update('types', $data); 
	}

    function is_used($id){
        //check in table files
        $this->db->select("count(types_id) as total");
        $this->db->where("types_id",$id);
		$query = $this->db->get('category_types');
		$result = $query->result();
        
        if($result[0]->total > 0){
            return true;
        }
        
        return false;
    }
    
    function get_category_type_by_type($id){
        //check in table files
        $this->db->select("category.name as category_name");
        $this->db->where("types_id",$id);
        $this->db->join("category","category.id = category_types.category_id");
        $query = $this->db->get('category_types');
        $result = $query->result();
        
        return $result;
    }
    function get_data_by_category($category_id,$active=''){
         //check in table files
        $this->db->select("types.id as type_id");
        $this->db->select("types.name as type_name");
        if($active){
            $this->db->where("types.active",$active);
        }
        $this->db->where("category_id",$category_id);
        $this->db->order_by("category_types.id","asc");
        $this->db->join("types","types.id = category_types.types_id");
		$query = $this->db->get('category_types');
		$result = $query->result();
        return $result;
    }
}