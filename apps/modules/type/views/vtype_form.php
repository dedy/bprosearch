<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){
        
        $("#name").focus();
        $('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/type');?>");
		});
		
		$('#submit-add-btn').click(function(e){
			e.preventDefault(); 
			$('#addnew').val(1);
			dopost($(this));
		});
		
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
			
	});
	
	function dopost(obj){

		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();
		
		//$('form#input-form').submit();
		//return;
		//alert('lie');
		
		$('form#input-form').ajaxSubmit({
		 	URL:'<?=site_url('admin/type/doupdate');?>',
		 	dataType: 'json', 
		   	success: function(returData){
		   		//alert('hi');
		   		$('#loadingmessage').hide();
				obj.removeAttr('disabled');
 				
				$('#show_message').slideUp('normal',function(){
					
					if(returData.error){
						var rv = '<div class="alert alert-error">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal');	
							
					}else{
						var rv = '<div class="alert alert-success">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal',function(){
							 setTimeout(function() {
							    $('#show_message').slideUp('normal',function(){
								    if(returData.redirect){
								    	window.location.replace(returData.redirect);
								    }
							    });	
							  }, 2000);
						});	
					}	
				});
		      }
		  });
	}	
		
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/type')?>"><?=lang('llist_jenis_dokumen')?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
             <?php 
              $hidden = array(
                  "id"              => (isset($data->id))? $data->id:0
              );
              echo form_open("admin/type/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
					  <div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;<?=lang('ltype_name');?></b></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='name' id='name' value="<?php if(isset($data->name) && $data->name) echo $data->name ?>" maxlength='255'>
						</div>
					</div>
                    
                    <div class="control-group">
						<label class="control-label" for='status'><?=lang('lstatus')?></label>
						<div class="controls">
						  <label class="checkbox inline">
							<input type="checkbox" id="status" name='active' value="1" <? if( (isset($data->active) && $data->active) || !isset($data))echo "checked"?>> <?=lang('lactive')?>
						  </label>
						</div>
					  </div>
	
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
					</div><!--/form-actions-->
					
				</form>   <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->