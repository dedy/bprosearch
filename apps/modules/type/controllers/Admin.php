<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
        
        //user module only for admin
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
		$this->load->model('type_model');
		$this->load->model('category/category_model');
	}
	
	function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('llist_jenis_dokumen');
		$this->viewparams['ladd_button_title']	= lang('ladd_jenis_dokumen');
	
		parent::viewpage("vtype_list");
	}
    
    function setActive($value,$id){
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		$data['active']	= $value;
		
		$this->type_model->setActive($id,$data);
		
        $data = $this->type_model->search(1,1,'','',array('id'=>$id));
        
		$message = ($value == 1)?lang('ldata_isset_active'):lang('ldata_isset_inactive');
		
		$result = array("message" => $message,"name"=>$data[0]->name);
  		echo json_encode($result);



	}

	function delete($id){
        $data['id'] 	= $id;
		$data['active'] = 0;
        $is_error = false;
        
        //check if used 
        $datas = $this->type_model->get_category_type_by_type($id);

        if(!count($datas)) {
            $this->type_model->delete($data);
            if($this->db->affected_rows()){
                $message = lang('ldata_is_deleted');
                $is_error = false;
            }
            else {
                 $is_error = true;
                $message = lang('ldata_is_failed_deleted');
            }
        }else{
        	$category_names = array();
        	foreach($datas as $data){
        		array_push($category_names,$data->category_name);
        	}

            $is_error = true;
            $message = "Data sudah digunakan di 'Kategori Peraturan' : ".implode(", ", $category_names);
        }
		
		
		$result = array("is_error"=>$is_error,"message" => $message);
  		echo json_encode($result);
	}
	
    function edit($id){
      /*  echo time();
        echo date("Y-m-d",time());
        */
       /* can not access this page without valid id*/
		$data = $this->type_model->get_data_by_id($id);
        
		if(!$data){
			redirect('admin/type');
		}
        
		$this->viewparams['title_page']	= lang('lmodify_type');
		$this->viewparams['data']	= $data;
			
        /* user group list */
       // $user_groups = $this->user_model->getUserGroup();
		//$this->viewparams['user_groups'] = $user_groups;
            
        $this->form($id);
	}
    
    function form($id=0){
         $this->load->library(array('form_validation'));
		parent::viewpage("vtype_form");
    }
	
	function add(){
      	$this->viewparams['title_page']	= lang('ladd_type');
	
        $this->form(0);
	}
	
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'name'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array();
      	$query = $this->type_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->type_model->countSearch($searchv);

		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","active","name","active","edit",'hapus'),
			"id"		=> "id"
 		);
         
        parent::loadDataJqGrid();
		
	}
    
    function get_data($category_id){
        $exclude_data = $this->category_model->search_type(0,1,'name','asc',array('category_id'=>$category_id));
        
        $exclude_id = array();
        if(count($exclude_data)){
            foreach($exclude_data as $data){
                array_push($exclude_id,$data->types_id);
            }
        }
        //get active data
        $result = $this->type_model->get_data_type($exclude_id,1);
        //echo $this->db->last_query();
        echo json_encode($result);
        
    }
    
	//update_user
	function doupdate(){
		if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		/* check mandatory fields */
		$id		        = $this->input->post('id');
        
		$name           = $this->input->post('name');
		$active	          = $this->input->post('active');
		
		$message = array();
		$redirect = "";
		$is_error = false;
		
		
		//-- run form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'lang:lnama_jenis_dokumen', 'required');
		
		$this->form_validation->set_error_delimiters('', '<br/>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
			$message[] = validation_errors();
			
		}
		
		if(!$is_error){
			//check exist 
            $is_exist = $this->type_model->is_exist($name,$id);
            
            if($is_exist){
                $is_error = true;
                $message[] = lang('ljenis_dokumen_exists');
            }
            
            /* add/update if no error */
            if(!$is_error){
        
	       		$data = array(
                    "name"          => $name
				    ,"active"             => ($active)?$active:0
				);
				
				//add
				if(!$id){
				 	
                    $id = $this->type_model->insert($data);
                    
					if($id)	
						$message[] = lang('ldata_success_inserted');
					else
						$message[] = lang('ldata_failed_inserted');
						
				}else{
					                    
                    $this->type_model->update($data,$id);
                    
					$message[] = lang('ldata_success_updated');
				}
				
				$redirect = site_url("admin/type");
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
    function get_by_category($category_id){
        $data = $this->type_model->get_data_by_category($category_id);
        echo json_encode($data);
    }
    
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
