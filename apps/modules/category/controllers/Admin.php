<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
		//user module only for admin
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
		$this->load->model('category/category_model');
		$this->load->model('regulasi/regulasi_model');
		$this->load->helper('image');
	
	}
	
	function index(){
		$this->lists();
	}

    function lists(){
    	$this->viewparams['title_page']	= sprintf(lang('llist_str'),lang('lcategory'));
		$this->viewparams['ladd_button_title']	= sprintf(lang('ladd_str'),lang('lcategory'));
	
		parent::viewpage("vcategory_list");
	}
    	
    function deletetypes($id){
        $data['id'] 	= $id;
		
		$this->category_model->deleteCategoryTypes($id);
		
		//check affected rows
		$is_error =false;
		$afftectedRows = $this->db->affected_rows();
        if($afftectedRows){
        	$message = lang('ldata_is_deleted');
		} else {
            $is_error = true;
            $message = "Data gagal dihapus";
            
        }  
        $result = array(
			"message"	=> $message
			,"error"	=> $is_error
		);
  		echo json_encode($result);

    }

	function delete($id){
		
   		$data['id'] 	= $id;
		
		$is_error = false;
		$message = "";

		//check if used as parent , category(id_parent)
		$data_as_parent = $this->category_model->get_childs($id);

		if(count($data_as_parent)){
			$is_error = true;
			$message = "Data ini mempunyai sub kategori, harap hapus sub kategori terlebih dahulu";
		}else {
			//check if used in regulasi_category (category_id)
			$data_regulasi = $this->regulasi_model->get_data_by_category_id($id);
			//echo $this->db->last_query();
			if(count($data_regulasi)){
				$is_error = true;
				$message = "Data ini digunakan di 'Regulasi' : <br>";
				$i=0;
				foreach($data_regulasi as $d){
					$message .= "* <a  target='_blank' href='".site_url('admin/regulasi/edit')."/".$d->regulasi_id."'>".$d->nomor_dokumen."</a><br>";
					if($i > 10){
						$message .= "..... ";
						break;
					}
					$i++;
				}
			}
		}
		
		if(!$is_error){
			$this->category_model->deleteCategoryTypesByCategoryId($id);
			$this->category_model->deleteCategory($data);
			$message = lang('ldata_is_deleted');
		}
		
		$result = array("is_error" => $is_error,"message" => $message);
  		echo json_encode($result);


	}
    
    function get_type_list($id){
        
		$data_jenis_dokumen = $this->category_model->search_type(0,1,'id','asc',array("category_id"=>$id));
		$total_jenis_dokumen  = count($data_jenis_dokumen);
		$this->viewparams['data_jenis_dokumen']	= $data_jenis_dokumen;
		$this->viewparams['total_jenis_dokumen']	= $total_jenis_dokumen;
		parent::viewajax('vtype_list');
    }
	
	function edit($id){
        
        $id_parent = $this->input->get('id_parent');
       
		$data = $this->category_model->getCategoryById($id);
		
		if(!$data){
			redirect('admin/category');
		}
        
        //id parent parameter GET, must match with the id parent of the data
        if($id_parent){
            if($id_parent <> $data[0]->id_parent){
                redirect('admin/category');
            }
            $title_page = lang('ledit_sub_kategory');
        } else {
            $title_page = sprintf(lang('lmodify_str'),lang('lcategory_peraturan'));
        }
        
        
	
		$this->viewparams['title_page']	= $title_page;
		$this->viewparams['data']	= $data[0];
		$this->viewparams['id']	= $id;
        
        	
		$this->form();	
	}
	
	function add(){
        $id_parent = $this->input->get('id_parent');
        if($id_parent){
            $title_page = lang('ladd_sub_kategory');
        } else {
            $title_page = sprintf(lang('lstring_add'),lang('lcategory_peraturan'));
        }
              
        $this->viewparams['id']	= 0;
	
        $this->viewparams['title_page']	= $title_page;
		
		$this->form();	
	}
	
	function form(){
        $this->load->helper('form');
        $title_page2 = lang('lcategory');
		
        $id_parent = $this->input->get('id_parent');
        $parent_name = "";
        
        if($id_parent){
            //get name 
            $parent_data  = $this->category_model->getCategoryById($id_parent);
            $parent_name = ($parent_data[0]->category_name)?$parent_data[0]->category_name:"";
            
            if($parent_name == ""){
                redirect('admin/category');
            }
        }
        
        /*
        $sidx = 'display_order,name'; // get index row - i.e. user click to sort
        $sord = 'asc'; // get the direction

        $this->category_model->get_parents(0,array(),$sidx,$sord);
        $parent_list = $this->category_model->data;

        //-- give leveling string (add space in front of name)
        for($i=0;$i<count($parent_list);$i++){
            $parent_list[$i]->name = $this->category_model->getNameInTree($parent_list[$i]->name,$parent_list[$i]->level);
        }
        $this->viewparams['parent_list']	= $parent_list; */
		
		$this->viewparams['title_page2']	= $title_page2;
		$this->viewparams['parent_name']	= $parent_name;
		$this->viewparams['id_parent']	= $id_parent;
        
		parent::viewpage("vcategory_form");
	}
	
	function setActive($value,$id){
		$data['active']	= $value;
		
		$this->category_model->setActive($id,$data);
		
		$message = ($value == 1)?lang('ldata_isset_active'):lang('ldata_isset_inactive');
		
		$result = array("message" => $message);
  		echo json_encode($result);

	}

	function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = 'level_order'; // get index row - i.e. user click to sort
      	$sord = 'asc'; // get the direction
      	$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to 
        $searchv = array();
	
		$query = $this->category_model->search_view($limit,$page,$sidx,$sord,$searchv);
		$count = $this->category_model->countSearchView($searchv);
		
		for($i=0;$i<count($query);$i++){
			$query[$i]->category_prefix = $this->category_model->getPrefix($query[$i]->level);
			$query[$i]->category_image = ($query[$i]->category_image)?"<img src='".base_url()."uploads/category/".get_thumbname($query[$i]->category_image)."' border='0' width='80' height='80' >":"";
            
            //get category types by category id 
            $query[$i]->types_data = $this->category_model->get_type_str($query[$i]->category_id);
            
		}
		
		$columns = array("category_id","category_active","category_name","category_prefix","level","id_parent","category_name","types_data","display_order","category_active",'edit','ddelete','addsub');
       
		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> $columns,
			"id"		=> "category_id"
 		);
        
        parent::loadDataJqGrid();
		
	}
	
    function doupdatedokumen(){
        $category_id 		= $this->input->post('category_id');
		$types_id 	= $this->input->post('types_id');
		
		$message = "";
		$is_error = false;
        
        $values = array(
					"category_id"	=> $category_id
					,"types_id"     => $types_id
					,"created_on" 	=> date("Y-m-d H:i:s")
					,"created_by"	=> _userid()
		);
		
        $this->db->insert("category_types", $values); 
        $id = $this->db->insert_id();
       
        $afftectedRows = $this->db->affected_rows();
        if($afftectedRows){
            $message = lang('ldata_success_inserted');
        } else {
            $is_error = true;
            $message = lang('ldata_failed_inserted');
            
        }  
        
		$result = array(
			"message"	=> $message
			,"error"	=> $is_error
		);
		
		echo json_encode($result);
    }
    
    
	function doupdate(){
   		$this->load->helper('url');
		$id 		= $this->input->post('category_id');
		$id_parent 	= $this->input->post('id_parent');
		$name 		= trim($this->input->post('name'));
		$description= trim($this->input->post('description'));
		$status 	= ($this->input->post('status') == 1 ) ? $this->input->post('status'):0;
		$display_order = ($this->input->post('display_order')) ? $this->input->post('display_order'):0;
	
		$message = array();
		$redirect = "";
		$is_error = false;
		
		//-- run form validation
		//name is mandatory
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'lang:lcategory_name', 'required');
		$this->form_validation->set_rules('display_order', 'lang:ldisplay_order', 'numeric');
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
			if(form_error('name'))
				$message[] = form_error('name'); 
			
			if(form_error('display_order'))	
				$message[] = form_error('display_order'); 
		} else {
			if(!$is_error){
	            
				$level = 0;
				$level_parent = $this->category_model->getLevel($id_parent);
				$level = $level_parent+1;
				
				$values = array(
					"name"			=> $name
					,"description"	=> $description
					,"id_parent"	=> $id_parent
					,"level"		=> $level
					,"active"		=> $status
					,"display_order"	=> $display_order
					,"updated_on" 	=> date("Y-m-d H:i:s")
					,"updated_by"	=> _userid()
				);
				
				
				//add
				if(!$id){
					$values["slug"] 	= $this->category_model->generate_slug($name);
					$values["created_on"] = date("Y-m-d H:i:s");
					$values["created_by"]	= _userid();

				    $id = $this->category_model->insert_category($values);
					
					$message[] = lang('ldata_success_inserted');
				}else{
					$values["slug"] 	= $this->category_model->generate_slug($name,$id);
					$this->category_model->update_category($values,$id);
					$message[] = lang('ldata_success_updated');
				}
				
				$redirect = site_url("admin/category");
				
				//$redirect = '';
					
			}
		}
	
		$result = array(
			"message"	=> implode("<br/>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
