
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>


<script type="text/javascript">
	 jQuery().ready(function (){
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url('admin/category/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','active','catname','prefix','level','id_parent','<?=lang('lcategory')?>','<?=lang('ljenis_dokumen')?>','<?=lang('lorder')?>','<?=lang('lactive')?>','<?=lang('ledit')?>','<?=lang('ldel')?>',''],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:1, align:"right",sortable:false},
                {name:'id',index:'category_id', hidden: true},
                {name:'active',index:'active', hidden: true},
                {name:'catname',index:'catname', hidden: true},
                {name:'prefix',index:'prefix', hidden: true},
                {name:'level',index:'level', hidden: true},
                {name:'id_parent',index:'id_parent', hidden: true},
                {name:'category_name',index:'category_name',align:"left",stype:'text',width:6,sortable:false,hidden:false,
                	formatter: function (cellvalue, options, rowObject) {
					   return rowObject[4]+'<a href="<?=site_url('admin/category/edit')?>/' + rowObject[1] + '"><u>'+cellvalue+'</u></a>';
					},sortable:false
               	},
                {name:'types_data',index:'types_data',align:"left",stype:'text',width:8,sortable:false},
                {name:'order',index:'display_order', sortable:false, width:1,align:"center"},
                  
                {name:'status_icon', index: 'active', width: 1,align:"center",sortable:false,
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '#';
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                            	if(cellValue == "1")
            						return "<img src='<?=base_url()?>assets/admin/img/ico-yes.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
								else
            						return "<img src='<?=base_url()?>assets/admin/img/ico-standby.png' id='active-"+rowId+"' class='publish' border='0' width='16px' height='16px'>";
								
                              //return cellValue;
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                            	var val = $('#list1').jqGrid('getCell',rowId,'active');
                            	var catname = $('#list1').jqGrid('getCell',rowId,'catname');
                                
                            	//set inactive
                            	if(val == 1){
                            		var msg = 'Category \''+catname+ '\' will no longer active';
                            		var setval = 0;
                            	}else{
                            		//set active
                            		var msg = 'Category \''+catname+ '\' will become active';
                            		var setval = 1;
                            	}
                            	
                                $.confirm({
                                   title:"<?=lang('lchange_status_confirmation')?>",
                                   text: msg,
                                   confirmButton: "<?=lang('lok')?>",
                                   cancelButton: "<?=lang('lcancel')?>",
                                   confirm: function(button) {
                                      $.post("<?=site_url('admin/category/setActive')?>/"+setval+"/"+rowId, 
                                       function(data) {
                                           var rv = '<div class="alert alert-success"><?=lang('lstatus_is_changed')?></div>';
                                               $('#show_message').html(rv);
                                               $('#show_message').slideDown('normal',function(){
                                                    setTimeout(function() {
                                                       $('#show_message').slideUp('normal',function(){
                                                          gridReload();
                                                       });	
                                                     }, <?=config_item('message_delay')?>);
                                               });	
                                           //reload grid
                                           //gridReload();

                                       },"json"
                                   );
                                   },
                                   cancel: function(button) {
                                      // alert("You cancelled.");
                                   }
                               });
                            	
                        }},
                        cellattr: function (rowId, cellValue, rawObject) {
                        	//column 2 , status 
                        	if(rawObject[2] == "1")	
                        	 	var attribute = ' title="<?=lang('lset_inactive')?>"' ;
                            else
                            	var attribute = ' title="<?=lang('lset_active')?>"' ;
                            	
                            return attribute ;
                        }
                  },
                        
                 {name:'edit',index:'edit', width:1, align:"left",sortable:false,align:"center",
                	formatter:'dynamicLink', 
      			 	formatoptions:{
      			 		 url: function (cellValue, rowId, rowData) {
                                var id_parent = rowData[6];
                                if(id_parent > 0) { 
                                    return '<?=site_url('admin/category/edit')?>/' + rowId+'?id_parent='+id_parent;
                                }else{
                                    return '<?=site_url('admin/category/edit')?>/' + rowId;
                                }
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
							}
					},
      			 	cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ledit')?>"' 
                            return attribute ;
                     }
      			 },
      			  {name: 'delete', index: 'delete', width: 1,align:"center",
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '<?=site_url('admin/category/delete')?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-delete.png' id='delete-"+rowId+"' class='delete' border='0' width='16px' height='16px'>";
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                                $.confirm({
                                    title:"<?=lang('ldelete_category')?>",
                                    text: "<?=lang('lconfirm_delete')?>",
                                    confirmButton: "<?=lang('lok')?>",
                                    cancelButton: "<?=lang('lcancel')?>",
                                    confirm: function(button) {
                                       $.post(
                                            "<?=site_url('admin/category/delete')?>/" + rowId, 
                                            function(data) {
                                                if(!data.is_error)
                                                    var rv = '<div class="alert alert-success">'+data.message+'</div>';
                                                else
                                                    var rv = '<div class="alert alert-error">'+data.message+'</div>';

                                                    $('#show_message').html(rv);
                                                    $('#show_message').slideDown('normal',function(){
                                                        if(!data.is_error){
                                                            gridReload();
                                                        }
                                                    });	
                                                //reload grid
                                                //gridReload();
                                            },"json"
                                        );

                                    },
                                    cancel: function(button) {
                                       // alert("You cancelled.");
                                    }
                                });
                                
                                
                            	
                            }
                        },
                        cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ldelete')?>"' 
                            return attribute ;
                        }},
                        
                    {name:'addsub',index:'addsub',align:"center",width:1,
                        formatter: function (cellvalue, options, rowObject) {
                           var level = rowObject[5];
                           
                            if(level == 1) {
                                return '<a href="<?=site_url('admin/category/add')?>/?id_parent=' + rowObject[1] + '"><img title="<?=lang('ladd_sub_kategory')?>" src="<?=base_url()?>assets/admin/img/ico-plus.png" class="addsub" border="0" width="16px" height="16px"></a>';
                            } else {
                                return '' ;
                            }
                        },sortable:false
                    }    
                              
                        
	          ],
            rowNum:<?=$rowNum?>,
            <?if(isset($rowList) && $rowList){?>
              rowList:[<?=$rowList?>],
            <?}?>
            width: 1024,
            height: <?=$rowHeight?>,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'level_order',
            toppager: true, 
            //shrinkToFit:false,
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url('admin/category/loadDataGrid')?>",
				page:1
			}).trigger("reloadGrid");
	}

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
		
			<div>
				<a href="<?=site_url('admin/category/add')?>" class="btn btn-small btn-primary">
					<i class="icon-plus"></i> <?=$ladd_button_title?>
				</a>
			</div>
			<br/>
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->