<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<script src="<?php echo base_url(); ?>assets/js/jquery/chosen/1.4.2/chosen.jquery.min.js" type="text/javascript"></script>
<link href='<?=base_url();?>assets/js/jquery/chosen/1.4.2/chosen.css' rel='stylesheet'>

<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<script>
    
	$(document).ready(function(){
        
        $("#name").focus();
        //chosen - improves select
        $('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect: true});
        
        
        $('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/category');?>");
		});
		
		$('#submit-add-btn').click(function(e){
			e.preventDefault(); 
			$('#addnew').val(1);
			dopost($(this));
		});
		
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
		
          <?php
            if(isset($id) && $id){
              if($this->category_model->can_add_jenis_dokumen($id)) { ?>
                populate_types(<?=$id?>);    
                populate_jenis_dokumen(<?=$id?>);  
                
                $('#add_jenis_dokumen').click(function(e){
                    e.preventDefault(); 
                    var jenis_dokumen = $("#types_id").val();

                    if(jenis_dokumen == ""){
                        alert('<?=lang('lplease_select_jenis_dokumen')?>');
                    } else {
                        $("#types_spinner").show();
                        //post the data
                        $.ajax({
                          type: "POST",
                          url: "<?=site_url('admin/category/doupdatedokumen')?>",
                          data: {category_id : <?=$id?>, types_id : jenis_dokumen},
                          success: function(returData){
                                //alert('hi');
                                $("#types_spinner").hide();
                                $('#show_message_types').slideUp('normal',function(){

                                    if(returData.error){
                                        var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                                        $('#show_message_types').html(rv);
                                        $('#show_message_types').slideDown('normal');	

                                    }else{
                                        var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                                        $('#show_message_types').html(rv);
                                        $('#show_message_types').slideDown('normal',function(){
                                             setTimeout(function() {
                                                $('#show_message_types').slideUp('normal',function(){
                                                    //load the list
                                                    populate_types(<?=$id?>);    
                                                    populate_jenis_dokumen(<?=$id?>);  
          
                                                });	
                                              }, <?=config_item('message_delay')?>);
                                        });	
                                    }	
                                });
                            },
                          dataType: "json"
                        });

                    }

                    return false;

                });


          <?php
            }
          } ?>
                          
	});
    
     <?php
        if(isset($id) && $id){
              if($this->category_model->can_add_jenis_dokumen($id)) { ?>
                   function deletetypes(del_id){
                        $.confirm({
                                    title:"<?=lang('ldelete_jenis_dokumen')?>",
                                    text: "<?=lang('lconfirm_delete')?>",
                                    confirmButton: "<?=lang('lok')?>",
                                    cancelButton: "<?=lang('lcancel')?>",
                                    confirm: function(button) {
                                       $.post(
                                            "<?=site_url('admin/category/deletetypes')?>/" + del_id, 
                                            function(returData) {


                                                $('#show_message_types').slideUp('normal',function(){

                                                if(returData.error){
                                                    var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                                                    $('#show_message_types').html(rv);
                                                    $('#show_message_types').slideDown('normal'); 

                                                }else{
                                                    var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                                                    $('#show_message_types').html(rv);
                                                    $('#show_message_types').slideDown('normal',function(){
                                                        populate_types(<?=$id?>);
                                                        populate_jenis_dokumen(<?=$id?>);

                                                    }); 
                                                } 
                                            });
                                           },"json"
                                        );

                                    },
                                    cancel: function(button) {
                                       // alert("You cancelled.");
                                    }
                                });
                            return false;
                    }


                function populate_jenis_dokumen(id_category){
                    //show loading image
                    $("#list_jenis_dokumen").html('<img src="<?=base_url()?>assets/admin/img/spinner-mini.gif">'); 
                    $( "#list_jenis_dokumen" ).load( "<?=site_url('admin/category/get_type_list')?>/"+id_category, function() {
                        //hide loading image
                    });
                }
                
                function populate_types(id_category){
                    $("#types_spinner").show();
                    $.getJSON("<?=site_url('admin/type/get_data')?>/"+id_category,
                        function(j){
                          var options = '<option value=""></option>';

                          for (var i = 0; i < j.length; i++) {
                               options += '<option value="' + j[i].id + '">' + j[i].name+ '</option>';
                          }
                          $("select#types_id").html(options).trigger('chosen:updated');
                          $("#types_spinner").hide();
                        }
                    );  
                }
              <?php }
        }?>
	
	function dopost(obj){

		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();
		
		//$('form#input-form').submit();
		//return;
		//alert('lie');
		
		$('form#input-form').ajaxSubmit({
		 	URL:'<?=site_url('admin/category/doupdate');?>',
		 	dataType: 'json', 
		   	success: function(returData){
		   		//alert('hi');
		   		$('#loadingmessage').hide();
				obj.removeAttr('disabled');
 				
				$('#show_message').slideUp('normal',function(){
					
					if(returData.error){
						var rv = '<div class="alert alert-error">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal');	
							
					}else{
						var rv = '<div class="alert alert-success">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal',function(){
							 setTimeout(function() {
							    $('#show_message').slideUp('normal',function(){
								    if(returData.redirect){
								    	window.location.replace(returData.redirect);
								    }
							    });	
							  }, 2000);
						});	
					}	
				});
		      }
		  });
	}	
		
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/category')?>"><?=$title_page2?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
             <?php 
              $hidden = array(
                  "category_id"              => (isset($data->category_id))? $data->category_id:0
              );
              echo form_open("admin/category/doupdate",array('id'=>'input-form','class'=>'form-horizontal','enctype'=>'multipart/form-data'),$hidden);
              ?>
                    <?php
                        if($id_parent) { ?>
                             <div class="control-group">
                                <label class="control-label" for="id_parent"><?=lang('lparent')?></label>
                                <div class="controls">
                                       <input type="hidden" name='id_parent' value='<?=$id_parent?>'>
                                        <?=$parent_name?>
                                </div>
                            </div>
                     <?php }else { ?>
                        <input type="hidden" name='id_parent' value='0'>
                      <?php }?>
            
            
					<div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;
                        <?
                        if($id_parent) {
                            echo lang('lsub_kategory');
                        }else {
                            echo lang('lcategory_peraturan');
                        }?></b></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='name' id='name' value="<?php if(isset($data->category_name) && $data->category_name) echo $data->category_name ?>" maxlength='50'>
						</div>
					</div>
                    
					<div class="control-group">
						<label class="control-label" for="description"><?=lang('ldescription');?></label>
						<div class="controls">
							<textarea  name='description' id='description' class="input-xlarge"><?php if(isset($data->category_description) && $data->category_description) echo $data->category_description?></textarea>
						</div>
					</div>
					
					
					<div class="control-group">
						<label class="control-label" for="display_order"><?=lang('ldisplay_order');?></label>
						<div class="controls">
						<input class="input-mini focused" type="text" name='display_order' id='display_order' value="<?php if(isset($data->display_order) && $data->display_order) echo $data->display_order ?>">
						</div>
					</div>

                    <div class="control-group">
						<label class="control-label" for='status'><?=lang('lstatus')?></label>
						<div class="controls">
						  <label class="checkbox inline">
							<input type="checkbox" id="status" name='status' value="1" <? if( (isset($data->category_active) && $data->category_active) || !isset($data))echo "checked"?>> <?=lang('lactive')?>
						  </label>
						</div>
					  </div>
	
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
                       
					</div><!--/form-actions-->
					
				</form>   <!--/form-->
                
                <?php 
                if($this->category_model->can_add_jenis_dokumen($id)) { ?>
                      
                    <h2><?=lang('ljenis_dokumen')?>&nbsp;</h2>
                    <?php 
                    $hidden = array(
                        "category_id"              => $id
                    );
                    echo form_open("admin/category/type/doupdate",array('id'=>'input-form2','class'=>'form-horizontal'),$hidden);
                    ?>
                        <div class="control-group">
                            <label class="control-label" for='status'><?=lang('ljenis_dokumen')?></label>
                            <div class="controls">
                            <select name='types_id' id='types_id' class="input-xlarge" data-rel="chosen">
                            </select>
                            <img src="<?=base_url()?>assets/admin/img/spinner-mini.gif" id="types_spinner">
                          
                                <button  class="btn btn-small btn-primary" id='add_jenis_dokumen'>
                                    <i class="icon-plus"></i> <?=lang('ladd_jenis_dokumen')?>
                                </button>
                            </div>
                        </div>

                        <div id='show_message_types' style="display: none;"></div> 

                        
                    </form>
                    
                    <div id='list_jenis_dokumen'></div>
                    
                  
              
                <?php } ?>
                
                  
                
                
		</div><!--/box-content-->
        
        
	</div><!--/span-->

</div><!--/row-->