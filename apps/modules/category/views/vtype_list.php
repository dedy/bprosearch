<table class="table table-striped table-bordered bootstrap-datatable datatable">
    <th>No. </th>
    <th><?=lang("ljenis_dokumen")?></th>
    <th><?=lang("ldel")?></th>
    <tbody>
      <?php 
      $counter = 0;
      if($total_jenis_dokumen) {
          foreach($data_jenis_dokumen as $value){
              $counter++;
              $msg = "";
              $counter_message = 0;

              ?>
           <tr>
                <td width='5px'><?=$counter?>.</td>
                <td><?=$value->type_name?></td>
                <td>
                    <a title='<?=lang('ldelete')?>' class='delete_class' href='javascript:deletetypes(<?=$value->category_types_id?>)' >
                        <img src='<?=base_url()?>assets/admin/img/ico-delete.png' id='types-del-<?=$value->category_types_id?>' border='0' width='16px' height='16px'>
                    </a>        
                </td>    
                    
           </tr>
          <?php }
          }?>
      </tr>
    </tbody>
</table>