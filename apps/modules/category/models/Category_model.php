<?php
class Category_model extends CI_Model {
	var $is_count = false;
	var $data = array();
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function get_data_by_slug($slug){
    	$slug = strtolower(trim($slug));
   	    $searchval['slug']	= $slug;
   	    $searchval['active']	= '1';
		return $this->search_category(1,1,'','',$searchval);
    }   

    function get_data_by_category_id($id){
   	    $searchval['id']	= $id;
   	    $searchval['active']	= '1';
		return $this->search_category(1,1,'','',$searchval);
    }  
    
    function search_view($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
    	foreach($searchval as $key=>$val){
    		if(!is_array($val))
	    		$searchval[$key] = trim(urldecode($val));
		}
	
		if( isset($searchval['active']))
			$this->db->where("a.active",$searchval['active']);
		
		if( isset($searchval['id']) && $searchval['id']){
			if(is_array($searchval['id']))
				$this->db->where_in("a.id",$searchval['id']);
			else
				$this->db->where("a.id",$searchval['id']);
		}
		
		if( isset($searchval['id_parent']))
			$this->db->where("a.id_parent",$searchval['id_parent']);
	
		if( isset($searchval['level']))
			$this->db->where("a.level",$searchval['level']);
	    if( isset($searchval['category_name']) && $searchval['category_name'])
			$this->db->where("a.name",$searchval['category_name']);

 		if( isset($searchval['slug']) && $searchval['slug'])
			$this->db->where("a.slug",$searchval['slug']);


        if($this->is_count){
			$this->db->select("count(a.id) as total");
		}else{
			$this->db->select("a.*");
			$this->db->select("'' as edit");
			$this->db->select("'' as ddelete");
			$this->db->select("'' as addsub");
			
		  	$this->db->select("a.id as id_container");
            $this->db->select("a.name as container");
          
			$this->db->select("CASE a.active WHEN 1 THEN '".lang('lactive')."' ELSE '".lang('linactive')."' END status_desc", false);
			$this->db->select("users.username as updated_by_username");
			$this->db->select("DATE_FORMAT(a.updated_on,'%d/%m/%Y %H:%i:%s') as updated_on_fmt",false);
			$this->db->select("a.id as category_id");
			$this->db->select("a.id_parent as category_id_parent");
            $this->db->select("parent_name as category_parent_name");
            $this->db->select("a.name as category_name");
            $this->db->select("a.slug as category_slug");
            $this->db->select("a.description as category_description");
            $this->db->select("a.active as category_active");
            $this->db->select("a.image as category_image");
			$this->db->join("users","users.id = a.updated_by","left outer");	
		}
	
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
			
		$query = $this->db->get('category_view a');
		$result = $query->result();
	
  		//echo $this->db->last_query();
  		if(!$this->is_count)
			return $result;
		else	
			return $result[0]->total;
		
    }

    function search_category($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
    	foreach($searchval as $key=>$val){
    		if(!is_array($val))
	    		$searchval[$key] = trim(urldecode($val));
		}
	
		if( isset($searchval['active']))
			$this->db->where("category.active",$searchval['active']);
		
		if( isset($searchval['id']) && $searchval['id']){
			if(is_array($searchval['id']))
				$this->db->where_in("category.id",$searchval['id']);
			else
				$this->db->where("category.id",$searchval['id']);
		}
		
		if( isset($searchval['id_parent']))
			$this->db->where("category.id_parent",$searchval['id_parent']);
	
		if( isset($searchval['level']))
			$this->db->where("category.level",$searchval['level']);
	    if( isset($searchval['category_name']) && $searchval['category_name'])
			$this->db->where("category.name",$searchval['category_name']);

 		if( isset($searchval['slug']) && $searchval['slug'])
			$this->db->where("category.slug",$searchval['slug']);


        if($this->is_count){
			$this->db->select("count(category.id) as total");
		}else{
		  	$this->db->select("category.id as id_container");
            $this->db->select("category.name as container");
          
			$this->db->select("CASE category.active WHEN 1 THEN '".lang('lactive')."' ELSE '".lang('linactive')."' END status_desc", false);
			$this->db->select("users.username as updated_by_username");
			$this->db->select("DATE_FORMAT(".$this->db->dbprefix."category.updated_on,'%d/%m/%Y %H:%i:%s') as updated_on_fmt",false);
			$this->db->select("category.id as category_id");
			$this->db->select("category.id_parent as category_id_parent");
            $this->db->select("b.name as category_parent_name");
            $this->db->select("category.name as category_name");
            $this->db->select("category.slug as category_slug");
            $this->db->select("category.description as category_description");
            $this->db->select("category.active as category_active");
            $this->db->select("category.image as category_image");
            $this->db->select("category.*");
			
			$this->db->join("users","users.id = category.updated_by","left outer");	
			$this->db->join("category b","b.id = category.id_parent","left outer");	
		}
	
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
			
		$query = $this->db->get('category');
		$result = $query->result();
	
  		//echo $this->db->last_query();
  		if(!$this->is_count)
			return $result;
		else	
			return $result[0]->total;
		
    }
    
    function getIdByName($name,$datas){
        if(count($datas)){
            foreach($datas as $data){
                if(strtoupper($data->name) == strtoupper($name)){
                    return $data->id;
                }
            }
        }
        
        return 0;
    }
    
    function search_simple($filter=array()){
    	$this->db->select("category.*");
		$query = $this->db->get('category');
		$result = $query->result();
	
  		return $result;
		
    }
    
    function countSearchView($searchval=array()){
    	$this->is_count = true;
    	$res = $this->search_view(0,0,'','',$searchval);
    	
    	return $res;
	}

	function countSearchCategory($searchval=array()){
    	$this->is_count = true;
    	$res = $this->search_category(0,0,'','',$searchval);
    	
    	return $res;
	}


     
    function deleteCategory($data){
    	$this->db->where('id', $data['id']);
		$this->db->delete('category'); 
    }
    
    function deleteCategoryTypesByCategoryId($category_id){
        $this->db->where('category_id', $category_id);
		$this->db->delete('category_types');
    }

    function deleteCategoryTypes($id){
        $this->db->where('id', $id);
		$this->db->delete('category_types');
    }
    
    /*
    table category , field : type ==> 1=post , 2=product, 3=banner,4=menu, 5=brand,6=product type
    */
    function isUsedCategory($id){
		$this->db->select("count(".$this->db->dbprefix."category.id) as total");
		$this->db->where("id_parent",$id);
		$query = $this->db->get('category');
		$result = $query->result();
   		//echo $this->db->last_query();
   		if(count($result))
   			if($result[0]->total > 0)
   				return true;
   		
   		return false;
	}
	
	
	
	function isUsed($id,$table,$field='category_id'){
		$this->db->select("count(id) as total");
		$this->db->where($field,$id);
		$query = $this->db->get($table);
		$result = $query->result();
   		
   		if(count($result))
   			if($result[0]->total > 0)
   				return true;
   		
   		return false;
	}

	function getChildBySlug($slug){
		$this->db->where("category.active",1);
		$this->db->select("category.id as category_id");
		$this->db->select("category.id as id_container");
		$this->db->select("category.name as container");
        $this->db->select("category.slug as category_slug");
        $this->db->select("category.name as category_name");
        $this->db->select("category.image as category_image");
        $this->db->select("category.description as category_description");
        
        $this->db->join("category a","a.id = ".$this->db->dbprefix."category.id_parent and a.slug = '".$slug."' ");
        
		$this->db->order_by('category.display_order','asc');

    	$query = $this->db->get('category');
		$result = $query->result();
		//echo $this->db->last_query();
		return $result;
    }

	function getChildById($id){
		$this->db->where("category.active",1);
		$this->db->where("category.id_parent",$id);
		$this->db->select("category.id as category_id");
		$this->db->select("category.id as id_container");
		$this->db->select("category.name as container");
        $this->db->select("category.slug as category_slug");
        $this->db->select("category.name as category_name");
        $this->db->select("category.image as category_image");
        $this->db->select("category.description as category_description");
  		$this->db->order_by('category.display_order','asc');

    	$query = $this->db->get('category');
		$result = $query->result();
		//echo $this->db->last_query();
		return $result;
    }

	function get_childs($pid,$sidx='',$sord='',$searchval=array()){
    	
    	if(isset($searchval['active']))
    		$this->db->where("category.active",$searchval['active']);
    	
    	if(isset($searchval['include_id']) && $searchval['include_id']){
    		if(is_array($searchval['include_id'])){
    			$this->db->or_where_in("category.id",$searchval['include_id']);
    		}else{
    			$this->db->or_where("category.id",$searchval['include_id']);
    		}
    	}
    			
    	$this->db->where("category.id_parent",$pid);

		$this->db->select("CASE category.active WHEN 1 THEN '".lang('lactive')."' ELSE '".lang('linactive')."' END status_desc", false);
		$this->db->select("users.username as updated_by_username");
		$this->db->select("DATE_FORMAT(".$this->db->dbprefix."category.updated_on,'%d/%m/%Y %H:%i:%s') as updated_on_fmt",false);
		$this->db->select("category.id as category_id");
		$this->db->select("category.slug as category_slug");
        $this->db->select("category.name as category_name");
        $this->db->select("category.image as category_image");
        $this->db->select("category.active as category_active");
        $this->db->select("category.*");
                
        $this->db->join("users","users.id = ".$this->db->dbprefix."category.updated_by","left outer");	
		
		if($sidx)
			$this->db->order_by($sidx,$sord);

    	$query = $this->db->get('category');
        
		$result = $query->result();
		//echo $this->db->last_query();
		return $result;
    }
    
    function get_parents($pid,$found=array(),$sidx='',$sord='',$searchval=array()) {
	   $result = $this->get_childs($pid,$sidx,$sord,$searchval);
	  //var_dump($result);
		if(count($result)){
			//array_push($found,$result);
	    	foreach($result as $value){
	    		$this->data[] = $value;
	    		if(isset($searchval['exclude_id_parent']) && $searchval['exclude_id_parent'])
	    			if($value->category_id == $searchval['exclude_id_parent'])
	    				continue;

	    		$this->get_parents($value->category_id,$found,$sidx,$sord,$searchval);
	        }
	    }	        
	    return;
	}
	
	function insert_category($values){
		
        $this->db->insert("category", $values); 
        $id = $this->db->insert_id();
       
        //update the code
        //code = id parent + order + id + order 
         $values['code'] = $this->generate_code($values,$id);
         $this->update_category($values,$id);
        
        /*$this->db->trans_begin();
        $this->db->query('AN SQL QUERY...');
        $this->db->query('ANOTHER QUERY...');
        $this->db->query('AND YET ANOTHER QUERY...');
        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
        }else{
                $this->db->trans_commit();
        }*/



                    
		return $id ;
        
	}
	
    //can add if is no child 
    //and in edit 
    function can_add_jenis_dokumen($id){
        if($id){
            return true;
        }
        
        return false;
    }
    
    
    function get_type_str($category_id){
        $str = "";
        $this->is_count = false;
        $data = $this->search_type(0,1,'id','asc',array("category_id"=>$category_id));
        if(count($data)){
            foreach($data as $d){
                if($str){
                    $str .= ", ".$d->type_name;
                }else{
                    $str .= $d->type_name;
                }
            }
        }
        return $str;
    }
    
    function search_type($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['category_id']) && $searchval['category_id']){
            $this->db->where('category_types.category_id',$searchval['category_id']);
        }

      	if($this->is_count){
			$this->db->select("count(category_types.id) as total");
		}else{
			$this->db->select("category_types.*");
			$this->db->select("category_types.id as category_types_id");
			$this->db->select("types.name as type_name");
            $this->db->join("types","types.id = category_types.types_id");
       }
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('category_types');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function generate_code($values,$id){
        if($values['id_parent'] > 0 ) {
            $code = sprintf("%04d", $values['id_parent']).sprintf("%04d", $id);
        }else{
            $code = sprintf("%04d", $id);
        }
        return $code;
    }

    function update_category($values,$id){
        
		$values['code'] = $this->generate_code($values,$id);
        $this->db->where("id",$id);
		$this->db->update("category", $values);
	}
	
	function getCategoryById($id){
		$searchval['id']	= $id;
		return $this->search_category(1,1,'','',$searchval);
	}
	
	function getProductCategoryByIdActive($id,$status){
		$searchval['id']		= $id;
		$searchval['status']	= $status;
		$searchval['type']  	= 2;
		return $this->search_category(1,1,'','',$searchval);
	}
    
    
    function get_category_list($searchval){
        return $this->search_category(0,0,'display_order,name','asc',$searchval);    
 
    }
	
	function getLevel($id){
		if($id == 0)
			return 0;
			
		$data = $this->getCategoryById($id);	
		return $data[0]->level;
	}
    
     function slug_exists($slug,$exlude_id=0){
        $this->db->select("count(".$this->db->dbprefix."category.id) as total");
        $this->db->where("slug",$slug);
        
        if($exlude_id)
            $this->db->where("id <>",$exlude_id);
			
		$query = $this->db->get('category');
		$result = $query->result();
        
        if($result[0]->total)
            return true;
        else
            return false;
    }
    
    function generate_slug($title,$id=''){
    	$this->load->helper('url');
        $slug = url_title($title);
        $new_slug = $slug;
        $counter = 1;
        
        while($this->slug_exists($new_slug,$id)){
            $new_slug = $slug."-".$counter;
            $counter++;        
        }
        
        return $new_slug;
    }
    
    
    function getPrefix($level,$iprefix='---'){
		$prefix = "";
		if($level > 1){
			for($i=0;$i<$level;$i++){
				$prefix .= $iprefix;
			}
        }
		return $prefix;
	}   
 	
 	function getNameInTree($name,$level,$iprefix='---'){
		$prefix = "";
		if($level > 1){
			for($i=0;$i<$level;$i++){
				$prefix .= $iprefix;
			}
		}
		return $prefix."&nbsp;".$name;
	}   
	
	function get_product_category_by_name($name,$id_parent=0,$level=0){
		$searchval['type']			= 2; //2 = product cateogry
		$searchval['category_name'] = $name;
		$searchval['id_parent'] 	= $id_parent;
		if($level)
			$searchval['level'] 	= $level;
		
		return $this->search_category(0,0,'category_name','asc',$searchval);
	}
	
	function product_category_set_inactive(){
		$values['status'] 		= 0;
		$values['updated_by'] 	=  _userid();
		$values['updated_on']	= date("Y-m-d H:i:s");
		
		$this->db->where('type', 2);
		$this->db->update("category", $values);
		
	}
	
	function setActive($id,$data){
		$data["updated_on"]	= date("Y-m-d H:i:s");
		$data["updated_by"]	= _userid();
	
  		$this->db->where('id', $id);
		$this->db->update('category', $data); 
	}
}