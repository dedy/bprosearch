<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){
        
        $("#name").focus();
        $('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url('admin/group');?>");
		});
		
		$('#submit-add-btn').click(function(e){
			e.preventDefault(); 
			$('#addnew').val(1);
			dopost($(this));
		});
		
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
			
	});
	
	function dopost(obj){

		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();
		
		//$('form#input-form').submit();
		//return;
		//alert('lie');
		
		$('form#input-form').ajaxSubmit({
		 	URL:'<?=site_url('admin/type/doupdate');?>',
		 	dataType: 'json', 
		   	success: function(returData){
		   		//alert('hi');
		   		$('#loadingmessage').hide();
				obj.removeAttr('disabled');
 				
				$('#show_message').slideUp('normal',function(){
					
					if(returData.error){
						var rv = '<div class="alert alert-error">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal');	
							
					}else{
						var rv = '<div class="alert alert-success">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal',function(){
							 setTimeout(function() {
							    $('#show_message').slideUp('normal',function(){
								    if(returData.redirect){
								    	//indow.location.replace(returData.redirect);
								    }
							    });	
							  }, 2000);
						});	
					}	
				});
		      }
		  });
	}	
		
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url('admin/group')?>"><?=lang('llist_group')?></a></li><span class="divider">/</span>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
             <?php 
              $hidden = array(
                  "id"              => (isset($data->id))? $data->id:0
              );
              echo form_open("admin/group/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
					  <div class="control-group">
						<label class="control-label" for="name"><b>*&nbsp;<?=lang('lname');?></b></label>
						<div class="controls">
						<input class="input-large" type="text" name='name' id='name' value="<?php if(isset($data->name) && $data->name) echo $data->name ?>" maxlength='20'>
						</div>
					</div>
                     <div class="control-group">
						<label class="control-label" for="name"><?=lang('ldescription');?></label>
						<div class="controls">
						<input class="input-xlarge" type="text" name='description' id='description' value="<?php if(isset($data->description) && $data->description) echo $data->description ?>" maxlength='100'>
						</div>
					</div>
                    <div class="control-group">
						<label class="control-label" for='status'><?=lang('lpermissions')?></label>
						<div class="controls">
							<div class='span8'>
								<table class="table table-striped table-bordered bootstrap-datatable datatable responsive " >
								    <tr>
								        <th>Menu</th>
								        <th>C(reate)</th>
								        <th>R(ead)</th>
								        <th>U(pdate)</th>
								        <th>D(elete)</th>
								    </tr>
								 <?php
					                if(count($menus)){
					                	    foreach($menus as $menu){
					                        	//check menu for this group id
					                        	$c_selected = $r_selected = $u_selected = $d_selected = "";
					                        	
					                        	//edit mode 
					                        	if(isset($data->id)) {				                        	$group_menus = $this->group_model->getMenuByGroupId($data->id);
					                        		if(count($group_menus)){
					                        			foreach($group_menus as $group_menu){
					                        				if($group_menu->menu_id == $menu->menu_id){
																$permissions = json_decode($group_menu->permissions);

					                        						$c_selected = ($permissions->C)?"checked":"";
					                        						$r_selected = ($permissions->R)?"checked":"";
					                        						$u_selected = ($permissions->U)?"checked":"";
					                        						$d_selected = ($permissions->D)?"checked":"";

																break;
					                        				}	
					                        			}
					                        		}
					                        	}
					                        	?>
												<tr>
					                      			<td><?=$menu->menu_title?></td> 
					                      			<td width='30px'><input type="checkbox" id="create<?=$menu->menu_id?>" name='create<?=$menu->menu_id?>' value="1" <?=$c_selected?> ></td>
					                      			<td width='30px'><input type="checkbox" id="read<?=$menu->menu_id?>" name='read<?=$menu->menu_id?>' value="1" <?=$r_selected?>></td>
					                      			<td width='30px'><input type="checkbox" id="update<?=$menu->menu_id?>" name='update<?=$menu->menu_id?>' value="1" <?=$u_selected?>></td>
					                      			<td width='30px'><input type="checkbox" id="delete<?=$menu->menu_id?>" name='delete<?=$menu->menu_id?>' value="1" <?=$d_selected?>></td>

												</tr>
										<?php }
					                }?>
								</table>
							</div>
						</div>
					  </div>
	
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
					</div><!--/form-actions-->
					
				</form>   <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->