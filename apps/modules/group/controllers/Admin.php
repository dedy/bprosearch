<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
        
        //user module only for admin
        if(!isAdmin()){
        	redirect('auth/logout');
        }   
		
		$this->load->model('group_model');
		$this->load->model('menu/menu_model');
    	
	}
	
	function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('llist_group');
		$this->viewparams['ladd_button_title']	= lang('ladd_group');
	
		parent::viewpage("vgroup_list");
	}
    


    function edit($id){
    	//can not edit id 1 and 2 (admin dan member)
    	if(in_array($id,array(1,2))){
    		redirect('admin/group');
    	}

      /*  echo time();
        echo date("Y-m-d",time());
        */
       /* can not access this page without valid id*/
		$data = $this->group_model->get_data_by_id($id);
        
		if(!$data){
			redirect('admin/group');
		}
        
		$this->viewparams['title_page']	= lang('lmodify_group');
		$this->viewparams['data']	= $data;
			
       	$menus = $this->menu_model->getAdminMenus();
    	$this->viewparams['menus'] 		= $menus;
	    $this->form($id);
	}
    
    function form($id=0){
        $this->load->library(array('form_validation'));
		parent::viewpage("vgroup_form");
    }
	

	
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'name'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       
        $searchv = array(
        	'exclude_id'	=>	array('1','2')
        );

      	$query = $this->group_model->search($searchv);

      	$this->firephp->log($this->db->last_query());
		$count = count($query);

		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","name","description","edit"),
			"id"		=> "id"
 		);
         
        parent::loadDataJqGrid();
		
	}
    
    function get_data($category_id){
        $exclude_data = $this->category_model->search_type(0,1,'name','asc',array('category_id'=>$category_id));
        
        $exclude_id = array();
        if(count($exclude_data)){
            foreach($exclude_data as $data){
                array_push($exclude_id,$data->types_id);
            }
        }
        //get active data
        $result = $this->type_model->get_data_type($exclude_id,1);
        //echo $this->db->last_query();
        echo json_encode($result);
        
    }
    
	//update_user
	function doupdate(){
		if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		/* check mandatory fields */
		$id		        = $this->input->post('id');
        
		$name           = $this->input->post('name');
		$description	= $this->input->post('description');
		
		$message = array();
		$redirect = "";
		$is_error = false;
		
		
		//-- run form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'lang:lname', 'required');
		
		$this->form_validation->set_error_delimiters('', '<br/>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
			$message[] = validation_errors();
			
		}
		
		if(!$is_error){

            
            /* add/update if no error */
            if(!$is_error){
        
	       		$data = array(
                    "name"          => $name
                    ,"description"          => $description
				);
				
				//add
				if(!$id){
				 	
                    $id = $this->group_model->insert($data);
                    
					if($id)	
						$message[] = lang('ldata_success_inserted');
					else
						$message[] = lang('ldata_failed_inserted');
						
				}else{
					                    
                    $this->group_model->update($data,$id);
                    
                    //update permission
       				$menus = $this->menu_model->getAdminMenus();

       				//delete group menus
       				$this->menu_model->deleteByGroupId($id);

       				//update group menu 
       				if(count($menus)){
       					foreach($menus as $menu){
       						$create = "create".$menu->menu_id;
       						$read 	= "read".$menu->menu_id;
       						$update = "update".$menu->menu_id;
       						$delete = "delete".$menu->menu_id;
       						$permissions = array(
       							"C"		=> ($this->input->post($create))?1:0
       							,"R"	=> ($this->input->post($read))?1:0
       							,"U"	=> ($this->input->post($update))?1:0
       							,"D"	=> ($this->input->post($delete))?1:0
       						);

       						$data_menus = array(
       							"menu_id"		=> $menu->menu_id
       							,"group_id"		=> $id
       							,"permissions"	=> json_encode($permissions)
       						);

       						$this->menu_model->insert_group_menus($data_menus);
       					}
       				}
					$message[] = lang('ldata_success_updated');
				}
				
				$redirect = site_url("admin/group");
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
    function get_by_category($category_id){
        $data = $this->type_model->get_data_by_category($category_id);
        echo json_encode($data);
    }
    
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
