<?php
class Group_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
	function update($data,$id){
        $data["updated_by"]   = _userid();
        $data["updated_on"]   = date("Y-m-d H:i_s");
        $this->db->where('id', $id);
		$this->db->update('groups', $data); 
	}
	
    function get_data_by_id($id){
        $data = $this->search(array("id"=>$id));
        if(count($data)){
            return $data[0];
        }else{
            return false;
        }
    }
  
    function search($searchval=array()){

        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('groups.id',$searchval['id']);
        }
        
        if(isset($searchval['exclude_id'])){
            if(is_array($searchval['exclude_id'])){
                $this->db->where_not_in('groups.id',$searchval['exclude_id']);
            }else{
                $this->db->where('groups.id <>',$searchval['exclude_id']);
            }
        }

        $this->db->select("groups.*");
        $this->db->select("'' as edit");

    	$query = $this->db->get('groups');
		$result = $query->result();
	
  		return $result;
		
    }

    function getMenuByGroupId($group_id){

        $this->db->where('group_id',$group_id);
        
        $query = $this->db->get('group_menus');
        $result = $query->result();
    
        return $result;
        
    }
    
  
}