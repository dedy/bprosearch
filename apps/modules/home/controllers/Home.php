<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Public_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('page/page_model'); 
        $this->load->model('category/category_model'); 
        $this->load->model('regulasi/regulasi_model'); 
	}
	
    function index(){
        $this->load->helper('form');
        $this->load->helper('text');
        
		//get page data of 'Home'        
        $data = $this->page_model->get_data_by_slug('home','1');

        $this->viewparams['website_title']   		= $data[0]->page_title." - ".config_item('website_title');
        $this->viewparams['website_description']   	= ($data[0]->page_meta_description)?$data[0]->page_meta_description." - ".config_item('website_description'):config_item('website_description');
        $this->viewparams['website_keyword']   		= ($data[0]->page_meta_keyword)?$data[0]->page_meta_keyword." - ".config_item('website_keyword'):config_item('website_keyword');
	    
	    if(count($data)){
	    	$this->viewparams['data']  = $data[0];
	    } 
        
        //get category of parent only
        $categories = $this->category_model->get_childs(0,'display_order,category_name','asc',array('active'=>'1'));
        $this->viewparams['categories']	= $categories;
        
        //get latest regulation
        $limit = config_item('limit_latest_home');

        //exclude category kurs&sukubunga 39
        $latest_regulation = $this->regulasi_model->searchRegulasi($limit,1,'tanggal','desc',array('active'=>'1','exclude_category_id' => 39));

        $this->viewparams['latest_regulation'] = $latest_regulation;


        //get popular regulation (base on number_of_views)
        $limit = config_item('limit_latest_home');
        //$popular_regulation = $this->regulasi_model->search($limit,1,'number_of_views','desc',array('active'=>'1'));
        $popular_regulation = $this->regulasi_model->getPopularRegulasi();
        
        $this->viewparams['popular_regulation'] = $popular_regulation;

        parent::viewpage("home");

    }
    
}
