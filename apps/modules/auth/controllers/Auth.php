<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	// redirect if needed, otherwise display the user list
	function index()
	{
        //if not logged in , display login form
		if (!$this->ion_auth->logged_in())
		{
            $this->login();
		}else {
            $this->redirect();
            
        }
        
           
	}
    
    function checkLogin(){
		echo json_encode(array("isLoggedIn" => $this->ion_auth->logged_in()));
		
	}
    
    function get_redirect(){
        //if it's admin, redirect to user (later can be changed)
        if(isMember()){
            //member , redirect to home
            return site_url('/');
        }else{
            return site_url('admin/dashboard');
            //return site_url('auth/logout');
        }
    }
   
    function redirect(){
        $url = $this->get_redirect();    
        if($this->ion_auth->is_admin()){
            redirect($url, 'refresh');
        }else{
            redirect($url, 'refresh');
        }
    }

	// login page
	function login()
	{
        //echo  $this->ion_auth->hash_password("password", "");
         
        //$url = "https://www.google.com/recaptcha/api/siteverify?secret=6Lc9AxkTAAAAABxLOGY3Ve3f8o4X5O6ZtcL-smnc&remoteip=180.242.128.14&response=03AHJ_VuvBNKv_0FYvH1-zr8xfwP0zToTmVnRIxBWZycUoGctFRZNmWPQyXAQwi1sD4jUh4OFwYsIw3pfb4kp6k3Nfdw8os3otdF_8Jr1W_MkeAEWbtuXw_hUvtx42INBXw8t5CEhfhwIflh_iMIQ-MXV1b_qu1VYw-BMqN9N2FfS1ovF452kliYjvj5V58CsW9byB-OJwzXpzjKvZj7KrI_O06oI879w4hXEJQdHiHszppxeTi1wfvpFv11MJB68okGNgfzLzWD_prlW0lba1Ols_KYe2r90uzuX2N8TFURjxa9Drje0ZVoOs4ZvvEK7TSDH-6QiWI4PZJT5pQPPQjWCyfGM6L6UcpNm03-tpLg7lh3nJGNdI2NPgM345L5NcGxfonrQQ6XhP_4KG3mC9blImX9P94XFKO4DjLUBlI7rGrwmY5BuiRhmyyEtVMTySdMw1kPE_O2_YgZyQ2f3UhxnyNdJ1CTe2OUuCzVvxS0fWAo1cIgWIj45k6208W3Es5A8Gh2CwoKRqQIqiwVZ45ha87pHLpgKVbQx8w7FDYo1IcU8MDK8Tly9rj96cR8g2t_Y5iEBJdmWyaN-3THoGe_aCJoNlTfY7yU51qi4tjOiKLuGyeSrDwXK1avu21Dn9zchuzMnoZdifo5dWwsm6hZr81JRDwvDxpnA4xhGDGCRu6o-jdC-JOA-3PAKLEvVxI46nxoEITCX3Mpd49WvKa75TtbeoxI8JBHO63xWazfrSCIS0RQBGvxc8TFWhpWx5JkapBhfKR9hTzFUWWRCpi35FnSTsLweINFBU1U0pph98L-PnBMTmCwVIu4N_M8lhkMDuOz9qRrGtQlZc1KOcdSHh6YKRJSDscWh9vZAiXw59qJ7NcH_XuRKbYtRAXVagYZzvhlMVsityNlq3hCPDaJ6c_xEHQBbBjk6fNiXOYr-00KDo9IJG66wfQWBkHX08Yzt1ULlX4fG3fz7HF4cpkXZCwkMQqUC6bg";
        
        //echo "file : ".file_get_contents($url);
        
        // the user is not logging in so display the login page
        // set the flash data error message if there is one
        $this->viewparams['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $this->viewparams['identity'] = array('name' => 'identity',
            'id'    => 'identity',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('identity'),
        );
        $this->viewparams['password'] = array('name' => 'password',
            'id'   => 'password',
            'type' => 'password',
        );

        $this->viewparams['enable_recaptcha'] =  config_item('enable_recaptcha') ; 
              
		//view login page
		$this->load->view("vlogin", $this->viewparams);
     
	}
    
    function dologin(){
        
		//validate form input
		$this->form_validation->set_rules('identity', 'lang:login_username_label','required');
		$this->form_validation->set_rules('password', 'lang:lpassword', 'required');
        $is_error = false;
        $messages = "";
                
        $enable_recaptcha = config_item('enable_recaptcha');
        
        if($enable_recaptcha) {
            //verify google captcha
           $this->load->library('recaptcha');
           
         //  echo "site : ".config_item('recaptcha_site_key');
         //  echo ";server : ".config_item('recaptcha_secret_key');
    
           $recaptcha = $this->input->post('g-recaptcha-response');
           if (!empty($recaptcha)) {
               //echo $recaptcha;
               $response = $this->recaptcha->verifyResponse($recaptcha);
              //print_r($response);
               
               if (isset($response['success']) and $response['success'] === true) {
                   //do nothing, it's success
              //     echo "success";
                    $is_error = false;
       
               }else{
              //     echo "invalid captcha";
                   $is_error = true;
                   $messages = lang('linvalid_captcha');
               }
           }else{
              // echo "empty captcha";
               $is_error = true;
               $messages = lang('lplease_check_captcha');
           }
        }
        
        if(!$is_error) {
            $identity = $this->input->post('identity');
            if ($this->ion_auth->is_max_login_attempts_exceeded($identity))
            {
                $messages = lang('ltoo_many_login_attempts');
                $is_error = true;
            }else {

                if ($this->form_validation->run() == true)
                {
                    // check to see if the user is logging in
                    // check for "remember me"
                    $remember = (bool) $this->input->post('remember');

                    if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
                    {
                        //if the login is successful
                        //redirect them back to the home page
                        $messages = $this->ion_auth->messages();                 //lang("llogin_successful")

                        $this->ion_auth->clear_login_attempts($identity);

                        //add some session
                        $user = $this->ion_auth->user()->row();

                    }else
                    {
                        // if the login was un-successful
                        // redirect them back to the login page
                        $messages = $this->ion_auth->errors();
                        $is_error = true;
                    }

                }
                else
                {
                    // the user is not logging in so display the login page
                    // set the flash data error message if there is one
                    $messages  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                    $is_error = true;
                }
            }
        }
        
        echo json_encode(
            array(	"error"		=> $is_error,
                    "message"	=> $messages,
                    "redirect"	=> $this->get_redirect()
                )
            );
    }

	// log the user out
	function logout()
	{
        $isMember = isMember();

		// log the user out
		$logout = $this->ion_auth->logout();

        if($isMember){
    		redirect('/', 'refresh');
        }else{
            redirect('/admin', 'refresh');
       }
	}

	// forgot password
	function forgot_password()
	{
        
        $this->viewparams['type'] = $this->config->item('identity','ion_auth');
        // setup the input
        $this->viewparams['identity'] = array('name' => 'identity',
            'id' => 'identity',
        );

        if ( $this->config->item('identity', 'ion_auth') != 'email' ){
            $this->viewparams['identity_label'] = $this->lang->line('forgot_password_username_label');
        }
        else
        {
            $this->viewparams['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
        }
        
        $this->viewparams['flash_message']  = $this->session->flashdata('message');	

        //view forgot passpage
		$this->load->view("auth/vforgot_password", $this->viewparams);
	}
    
    function doforgot(){
        $redirect = $messages = "";
        $is_error = false;
        
        // setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_username_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}
        
        //validate the input
        if ($this->form_validation->run() == true){
            //search username
            $identity_column = $this->config->item('identity','ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            //if not found , then error
            if(empty($identity)) {

                if($this->config->item('identity', 'ion_auth') != 'email')
                {
                    $this->ion_auth->set_error('forgot_password_username_label');
                }
                else
                {
                   $this->ion_auth->set_error('forgot_password_email_not_found');
                }

                $redirect = "admin";//we should display a confirmation page here instead of the login page
                $messages = lang('lforgot_password_message');
                //  $messages = $this->ion_auth->errors();
                //$is_error = true;
            }else{
                
                // run the forgotten password method to email an activation code to the user
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

                if ($forgotten)
                {
                    //if username is found, then send the email 
                    $username = $forgotten['identity'];
                    $passcode = $forgotten['forgotten_password_code'];
                    
                    $this->load->model('email/email_model');
                    $id_email = 1;
                    $to_email = $forgotten['email'];
                    $original = array("{full_name}","{reset_url}");
                    $to_replace = array($forgotten['full_name'],"<a href='".site_url('auth/reset_password')."/".$passcode."'>This link</a>");
                    
                    $this->email_model->send_mail($id_email,$to_email,array(),array(),$original,$to_replace);
                                                
                    // if there were no errors
                    // $messages = $this->ion_auth->messages();
                     $messages = lang('lforgot_password_message');
                     $redirect = "admin";//we should display a confirmation page here instead of the login page
                }
                else
                {
                    $redirect = "admin";//we should display a confirmation page here instead of the login page
                    $messages = lang('lforgot_password_message');
                     
                    //$messages = $this->ion_auth->errors();
                    //$is_error = true;
                
                }
            }
        }else{
            //form validation error
            $messages  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
        }
        
         echo json_encode(
            array(	"error"		=> $is_error,
                    "message"	=> $messages,
                    "redirect"	=> $redirect
                )
            );
    }//end function doforgot_password
    
    function doreset_password(){
        $code = $this->input->post('code');
        $user = $this->ion_auth->forgotten_password_check($code);

        // if the code is valid then display the password reset form
        $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

        $messages = $redirect = "";
        $is_error = false;
        if ($this->form_validation->run() == false){
             //form validation error
            $messages  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
        }else  {

            // do we have a valid request?
           /* if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
            {

                // something fishy might be up
                $this->ion_auth->clear_forgotten_password_code($code);
                $messages  = $this->lang->line('error_csrf');
                $is_error = true;
            }
            else
            {*/
                // finally change the password
                $identity = $user->{$this->config->item('identity', 'ion_auth')};

                $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                if ($change)
                {
                    // if the password was successfully changed
                    $messages  = $this->ion_auth->messages();
                    $redirect = site_url('admin');
                }
                else
                {
                     $messages  = $this->ion_auth->errors();
                     $is_error = true;
                     $redirect = site_url('auth/reset_password/')."/". $code;
                }
          //  }
        }
        
          echo json_encode(
            array(	"error"		=> $is_error,
                    "message"	=> $messages,
                    "redirect"	=> $redirect
                )
            );
    }
    
    
	// reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
				// display the form

				// set the flash data error message if there is one
				$this->viewparams['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->viewparams['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
				);
				$this->viewparams['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'type'    => 'password',
					'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
				);
				$this->viewparams['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->viewparams['csrf'] = $this->_get_csrf_nonce();
				$this->viewparams['code'] = $code;

                $this->viewparams['flash_message']  = $this->session->flashdata('message');	

				// render
                $this->load->view("auth/reset_password", $this->viewparams);
		}
		else
		{
            // if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
            //$this->session->set_flashdata('message', lang('linvalid_forgot_password_code'));
			redirect("forgot_password", 'refresh');
		}
	}


	// activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	// deactivate the user
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	// create a new user
	function create_user()
    {
        $this->data['title'] = "Create User";

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => $this->input->post('company'),
                'phone'      => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("auth", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->_render_page('auth/create_user', $this->data);
        }
    }

	// edit a user
	function edit_user($id)
	{
		$this->data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->_render_page('auth/edit_user', $this->data);
	}

	// create a new group
	function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth/create_group', $this->data);
		}
	}

	// edit a group
	function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth/edit_group', $this->data);
	}


	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}
    
    function testcaptcha(){
           //verify google captcha
           $this->load->library('recaptcha');

           $recaptcha = "ssss";
           $response = $this->recaptcha->verifyResponse($recaptcha);
          print_r($response);
    }

}
