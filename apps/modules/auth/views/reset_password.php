<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DTT Forgot Password Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="DeCMS Administrator Panel">
	<meta name="author" content="Dedy">

    	<meta charset="utf-8">
        <link href="<?php echo base_url(); ?>assets/css/style.css?v=1.2" media="all" rel="stylesheet" type="text/css" />
	    
		<!-- jQuery -->
		<script src="<?=base_url();?>assets/js/jquery/jquery-1.9.1.min.js"></script>
	
    	<title><?=$website_title?></title>

    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

      	<meta name="description" content="<?=$website_description?>">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        
<script>
	$(document).ready(function(){
        $('#identity').focus();
        
        <?php
            if($flash_message) { ?>
            var rv = '<div class="alert alert-danger"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_red"></i></span><?=$flash_message?></div>';
                $('#show_message').html(rv);
                $('#show_message').slideDown('normal');	
        
        <?php }?>
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
            
             //disable the button to prevent double submit
             $(this).attr('disabled', 'disabled');
             
           
			$('#loadingmessage').show();
			
			$.post('<?=site_url('auth/doreset_password');?>', 
				$("#login-form").serialize(),
				function(returData) {
					$('#loadingmessage').hide();
					$('#show_message').slideUp('normal',function(){
                      //  alert(returData.message);
						if(returData.error){
							var rv = '<div class="alert alert-danger"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_red"></i></span>'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
                             $('#submit-btn').removeAttr('disabled');
                            
						}else{
							var rv = '<div class="alert alert-success"><span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span>'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, 2000);
							});	
						}	
					});
					
				},'json');
		});
		
	});
	</script>	
</head>

<body class="inside_view v1">
	<div id="content" >
        <div class="row">
            <div class="column twelve">         
                    
                    <div class="row">
                    	<div class="column six push-three">
                            <div class="search-area">
									<h3 align="center" style="padding:50px 0 0 0">Reset Password Page</h3>

                                <div class="alert alert-info">
                                   <?=lang('reset_password_heading')?>

                               </div>   
                                    


<div id="infoMessage"><?php echo $message;?></div>

   <?php echo form_open("auth/doreset_password",array('id'=>'login-form','class'=>'form-horizontal'),array("code"=>$code));?>

	<p>
		<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
		<?php echo form_input($new_password);?>
	</p>

	<p>
		<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
		<?php echo form_input($new_password_confirm);?>
	</p>

	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

	   <p><?php echo form_submit(array("id"=>"submit-btn","name"=>"submit" , "class"=>"btn btn-primary","value"=>lang('forgot_password_submit_btn')));?></p>
        <div id='loadingmessage' style="display: none">
              <img src='<?=base_url()?>/assets/images/spinner-mini.gif'/>
        </div>
        <div id='show_message' style="margin:5px 0 0 0"></div>

     <?php echo form_close();?>

    
    
                        </div>
                    </div>
                </div>
            </div>
        </div>    
	</div>
</Body>
</html>
