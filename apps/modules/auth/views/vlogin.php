<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=config_item('backend_header_title')?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="DeCMS Administrator Panel">
	<meta name="author" content="Dedy">

    	<meta charset="utf-8">
        <link href="<?php echo base_url(); ?>assets/css/style.css?v=1.2" media="all" rel="stylesheet" type="text/css" />
	    
		<!-- jQuery -->
		<script src="<?=base_url();?>assets/js/jquery/jquery-1.9.1.min.js"></script>
	
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

      	<meta name="description" content="<?=$website_description?>">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    	<!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">-->
        <?php
        if($enable_recaptcha) { ?>
            <script src='https://www.google.com/recaptcha/api.js'></script>
        <?php } ?>
<script>
	$(document).ready(function(){
        $('#identity').focus();
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			$('#loadingmessage').show();
			
			$.post('<?=site_url('auth/dologin');?>', 
				$("#login-form").serialize(),
				function(returData) {
					$('#loadingmessage').hide();
					$('#show_message').slideUp('normal',function(){
                      //  alert(returData.message);
						if(returData.error){
							var rv = '<div class="alert alert-danger"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_red"></i></span>'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
                            <?php
                                if($enable_recaptcha) { ?>
                                    grecaptcha.reset();
                                <?php }?>
						}else{
							var rv = '<div class="alert alert-success"><span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span>'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, 1500);
							});	
						}	
					});
					
				},'json');
		});
		
	});
	</script>	
</head>

<body class="inside_view v1">
	<div id="content" >
        <div class="row">
            <div class="column twelve">         
                    
                    <div class="row">
                    	<div class="column six push-three">
                            <div class="search-area">
                                
                                
								<h3 align="center" style="padding:50px 0 0 0"><?=config_item('backend_welcome_title')?></h3>

                            <div class="alert alert-info">
                                <?=lang('lplease_login')?>

                            </div>
                                
                            <?php echo form_open("auth/dologin",array('id'=>'login-form','class'=>'form-horizontal'));?>
                                <fieldset>

                                    <p>
                                      <?php echo lang('lemail', 'identity');?>
                                      <?php echo form_input($identity);?>
                                    </p>

                                    <p>
                                      <?php echo lang('login_password_label', 'password');?>
                                      <?php echo form_input($password);?>
                                    </p>

                                    <!-- <p>
                                      <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                                        <?php echo lang('login_remember_label', 'remember');?>
                                      
                                    </p>-->
                                    
                                    <?php
                                     if($enable_recaptcha) { ?>
                                    <p>
                                       <div class="g-recaptcha" data-sitekey="<?=config_item('recaptcha_site_key')?>"></div>
                                    </p>
                                     <?php } ?>
                                    <p>         
                                        <button id='submit-btn' type="submit" class="btn btn-primary"><?=lang('llogin')?></button>
                                    </p>
                                     <div id='loadingmessage' style="display: none">
                                          <img src='<?=base_url()?>/assets/images/spinner-mini.gif'/>
                                  </div>
                                    <div id='show_message' style="margin:5px 0 0 0"></div>
                                </fieldset>
                            <?php echo form_close();?>

                             <p><a href="<?=site_url('forgot_password')?>"><?php echo lang('login_forgot_password');?></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
	</div>
</Body>
</html>
