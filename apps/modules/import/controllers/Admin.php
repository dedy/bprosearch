<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';

class Admin extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    

	public function __construct()
	{
		parent::__construct();
        
        //user module only for admin
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
   }
	
	function index(){
        $this->load->helper('form');
        $this->load->model('regulasi/regulasi_model');
        $folder = FCPATH."import";
        
        $files = scandir($folder,1);
        
        $import_files = array();
        
        if(count($files)){
            foreach ($files as $file){
                if($file != "." && $file != ".."  ){
                    $ext = pathinfo($folder."/".$file, PATHINFO_EXTENSION);
                    if($ext == "xlsx" || $ext == "xls"){
                        $data = array("filename"=>$file,"filesize"=>  $this->regulasi_model->FileSizeConvert(filesize($folder."/".$file)));
                        array_push($import_files,$data);
                    }
                }
            }
        }
        
        $this->viewparams['import_files']        = $import_files;
        $this->viewparams['total_import_file']	= count($import_files);
        $this->viewparams['title_page']     = lang('limport');
		
        parent::viewpage("vimport_form");
	}
	
    function doimport(){
        $folder = FCPATH."import";
        
        $files = scandir($folder,1); //order by character
        
        $import_files = array();
        
        if(count($files)){
            foreach ($files as $file){
                if($file != "." && $file != ".."  && $file ){
                    $ext = pathinfo($folder."/".$file, PATHINFO_EXTENSION);
                    if($ext == "xlsx" || $ext == "xls" || $ext == "ods"){
                        if(file_exists($folder."/".$file)) {
                            //read the file
                            $result = $this->readfile($folder."/".$file);
                        }
                    }
                }
            }
        }
        
		echo json_encode($result);
    }
    
    private function readfile($file){
        $folder = FCPATH."import/files";
        $filename = basename($file);
        
        $this->load->model('import/import_model');
        $this->load->model('regulasi/regulasi_model');
        $this->load->model('category/category_model');
        $this->load->model('type/type_model');
        
        $cols = array(
            "peraturan_utama"
            ,"kategori_1"
            ,"kategori_2"
            ,"kategori_3"
            ,"kategori_4"
            ,"kategori_5"
            ,"kategori_6"
            ,"jenis_dokumen"
            ,"nomor_dokumen"
            ,"tanggal_dokumen"
            ,"perihal"
            ,"nama_dokumen"
            ,"status"
            ,"peraturan_pengganti"
            ,"nomor_urut"
        );
        
        $Reader = new SpreadsheetReader($file);
        $sheets = $Reader->Sheets();
        $Reader -> ChangeSheet(0);

        $iRow = 0; 
        
        $categories = $this->category_model->search_simple();
        $types = $this->type_model->search_simple();
        
        //start import , insert into import log
        $values = array(
            "filename"          => $filename
        );
        $import_log_id = $this->import_model->insert($values);
        
        $totalInsert = $totalUpdate = $totalError = $totalRow = 0;
        
        foreach ($Reader as $Row){
            $errors = array();
            
            if(!isset($Row[8])){
                break;
            }
            
            $nomor_dokumen = $Row[8];
           
            if(trim($nomor_dokumen) == ""){
                break;
            }
            
            if($iRow == 0){
                $iRow++;
                continue;
            }
            
            $totalRow++;
            $iCol=0;
            $data = array();
            $data_category = $upload_data = $upload_data_lampiran = $upload_data_en = $upload_data_lampiran_en = array();
           
            $is_update = false;
            $id = 0;
            
            $data['active'] = '1';
            foreach($cols as $col){
                $Row[$iCol] = (isset($Row[$iCol]))?trim($Row[$iCol]):"";
                switch ($col){
                     case "nomor_urut" :
                        if($Row[$iCol] <> "") {
                            $data[$col] =  $Row[$iCol];
                        }
                    break;
                     case "peraturan_utama":
                        if(strtoupper($Row[$iCol]) == "PAJAK"){
                            //do nothing, read next kategori 1-6
                        }else {
                            //get kategori id
                            $id_category = $this->category_model->getIdByName($Row[$iCol],$categories);
                            if($id_category) {
                                array_push($data_category,$id_category);
                            }else{
                                $errors[] = "Kategori '".$Row[$iCol]."' tidak ditemukan";
                            }
                        }
                    break;
                    case "kategori_1":
                    case "kategori_2":
                    case "kategori_3":
                    case "kategori_4":
                    case "kategori_5":
                    case "kategori_6":
                        // if(strtoupper($Row[0]) == "PAJAK" && $Row[$iCol]){
                         if($Row[$iCol]){
                             //get kategori id
                            $id_category = $this->category_model->getIdByName($Row[$iCol],$categories);
                             if($id_category) {
                                array_push($data_category,$id_category);
                            }else{
                                $errors[] = "Kategori '".$Row[$iCol]."' tidak ditemukan";
                            }
                             
                         }
                    break;
                    case "jenis_dokumen" :
                        //get type_id
                        $id_type = $this->type_model->getIdByName($Row[$iCol],$types);
                        if($id_type) {
                            $data['types_id'] = $id_type ;
                        }else{
                            $errors[] = "Jenis Dokumen '".$Row[$iCol]."' tidak ditemukan";
                        }
                    break;
                       
                    case "nomor_dokumen" :
                        //check if nomor dokumen exist/not
                        //if exist, update, else insert
                        $data_regulasi = $this->regulasi_model->nomor_exists($Row[$iCol]);
                        if(isset($data_regulasi->regulasi_id) && $data_regulasi->regulasi_id){
                            $is_update = true;
                            $id = $data_regulasi->regulasi_id;
                        }
                        $data['nomor_dokumen'] =  $Row[$iCol];
                    break;
                    case "tanggal_dokumen" :
                        $data['tanggal'] = ($Row[$iCol])?$Row[$iCol]:"0000-00-00";
                        if($data['tanggal'] == "0000-00-00"){
                            $errors[] = "Tanggal dokumen kosong ";
                        }
                    break;
                    
                    case "perihal" :
                        if($Row[$iCol] <> "") {
                            $data['judul'] = $Row[$iCol];
                        }
                       // if($data['judul'] == "0000-00-00"){
                        //    $errors[] = "Perihal kosong ";
                       // }
                    break;
                    
                     case "nama_dokumen" : 
                         //insert/update the pdf file if exists
                         if($Row[$iCol]){
                             //check if the file exists
                             $pdfFilePath = $folder."/".$Row[$iCol]."/".$Row[$iCol].".pdf"; 
                             $lampiranFilePath = $folder."/".$Row[$iCol]."/".$Row[$iCol]."_LMP.docx"; 
                             
                             if(is_file($pdfFilePath)){
                                    $upload_data['full_path']       = $pdfFilePath;
                                    $data['nama_file_regulasi']     = $Row[$iCol].".pdf";
                                    $data['ukuran_file_regulasi']   = filesize($pdfFilePath);
                                    $data['type_file_regulasi']     = mime_content_type($pdfFilePath);
                             }
                             
                             if(is_file($lampiranFilePath)){
                                    $upload_data_lampiran['full_path']  = $lampiranFilePath;
                                    $data['nama_file_lampiran']     = $Row[$iCol]."_LMP.docx";
                                    $data['ukuran_file_lampiran']   = filesize($lampiranFilePath);
                                    $data['type_file_lampiran']     = mime_content_type($lampiranFilePath);
                             }

                             //check if the file english exists
                             $pdfFilePathEn = $folder."/".$Row[$iCol]."/".$Row[$iCol]."_en.pdf"; 
                             $lampiranFilePathEn = $folder."/".$Row[$iCol]."/".$Row[$iCol]."_LMP_en.docx"; 
                             
                             if(is_file($pdfFilePath)){
                                    $upload_data_en['full_path']       = $pdfFilePathEn;
                                    $data['nama_file_regulasi_en']     = $Row[$iCol]."_en.pdf";
                                    $data['ukuran_file_regulasi_en']   = filesize($pdfFilePathEn);
                                    $data['type_file_regulasi_en']     = mime_content_type($pdfFilePathEn);
                             }
                             
                             if(is_file($lampiranFilePathEn)){
                                    $upload_data_lampiran_en['full_path']  = $lampiranFilePathEn;
                                    $data['nama_file_lampiran_en']     = $Row[$iCol]."_LMP_en.docx";
                                    $data['ukuran_file_lampiran_en']   = filesize($lampiranFilePathEn);
                                    $data['type_file_lampiran_en']     = mime_content_type($lampiranFilePathEn);
                             }

                         }
                         break;
                     
                    case "status":
                        $data['berlaku'] = (strtoupper($Row[$iCol]) == "YA")?"1":"0";
                    break;
                    
                    case "peraturan_pengganti" : 
                        if($Row[$iCol]) { 
                            $data_regulasi_pengganti = $this->regulasi_model->nomor_exists($Row[$iCol]);
                            if(isset($data_regulasi_pengganti->regulasi_id) && $data_regulasi_pengganti->regulasi_id){
                                $data['id_regulasi_pengganti'] = $data_regulasi_pengganti->regulasi_id;
                            }else{
                                $errors[] = "Peraturan Pengganti Nomor '".$Row[$iCol]."' tidak ditemukan";
                            }
                        }
                    break;
                    default:
                        break;
                    
                }
                $iCol++;
            }
            
            //array unique for data_category
            $data_category = array_unique($data_category);
            $status = $desc = "";
            
            //if error, no update/insert
            if(count($errors)){
                $status = "error";
                $desc = implode(";",$errors);
                $totalError++;
            }else{
                if($is_update){
                    $status = "update";
                    $desc = "id:".$id;
                    
                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
                    
                    $this->regulasi_model->update($data,$id);
                    $this->regulasi_model->update_category($id,$data_category);
                    
                    //if upload file, then update the regulasi_file table
                    $this->regulasi_model->update_file($id,$upload_data,$upload_data_lampiran,false);
                    $this->regulasi_model->update_file_en($id,$upload_data_en,$upload_data_lampiran_en,false);
              
                    $this->db->trans_complete(); # Completing transaction
                    
                    if ($this->db->trans_status() === FALSE) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $status = "error";
                        $desc = "db update transaction error";
                    } 
                    else {
                        # Everything is Perfect. 
                        # Committing data to the database.
                        $this->db->trans_commit();
                        $totalUpdate++;
       
                    }

						
                }else{
                    $status = "insert";
                    
                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

                    $id = $this->regulasi_model->insert($data);
                    $this->regulasi_model->update_category($id,$data_category);
                    
                    //if upload file, then update the regulasi_file table
                    $this->regulasi_model->update_file($id,$upload_data,$upload_data_lampiran,false); //not unlink the file
                    
                    $this->db->trans_complete(); # Completing transaction
                    
                    if ($this->db->trans_status() === FALSE) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $status = "error";
                        $desc = "db insert transaction error";
                    } 
                    else {
                        # Everything is Perfect. 
                        # Committing data to the database.
                        $this->db->trans_commit();
                        $totalInsert++;
                    }
                    
                }
                
            }
            
            
            //if error found, no insert/update
            unset($values);
           
            $values = array(
                "import_log_id"     => $import_log_id
                ,"nomor_dokumen"    => $data['nomor_dokumen']
                ,"status"           => $status 
                ,"description"      => $desc
            );
            $this->import_model->insert_detail($values);
           
            $iRow++;
        }
                     
        //update finished time
        unset($values);
        $values['finished_on'] = date("Y-m-d H:i:s");
        $this->db->where("id",$import_log_id);
		$this->db->update("import_logs", $values);
        
        $message  = "Import Selesai.<BR> Total Baris data : ".$totalRow."<BR>";
        $message .= "Total Insert : ".$totalInsert."<BR> ";
        $message .= "Total Update : ".$totalUpdate."<BR> ";
        $message .= "Total Error : ".$totalError."<BR>";
        $message .= "Anda dapat melihat log import ini di <a target='_blank' href='".site_url('log/import/detail')."/".$import_log_id."'>sini</a>";
        
        
        
        $result = array(
			"message"	=> $message
            ,"error" => false
		);
		
        return $result;
    }
   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
