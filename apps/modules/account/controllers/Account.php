<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Account extends Public_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user/user_model');
    }

    public function doforgot()
    {
        $this->load->library(array('form_validation'));
        $redirect = $messages = "";
        $is_error = false;

        // setting validation rules by checking wheather identity is username or email
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_username_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }

        //validate the input
        if ($this->form_validation->run() == true) {
            //search username
            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            //if not found , then error
            if (empty($identity)) {

                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_username_label');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }

                $redirect = "admin/login";//we should display a confirmation page here instead of the login page
                $messages = lang('lforgot_password_message');
                //  $messages = $this->ion_auth->errors();
                //$is_error = true;
            } else {

                // run the forgotten password method to email an activation code to the user
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

                if ($forgotten) {
                    //if username is found, then send the email
                    $username = $forgotten['identity'];
                    $passcode = $forgotten['forgotten_password_code'];

                    $this->load->model('email/email_model');
                    $id_email = 1;
                    $to_email = $forgotten['email'];
                    $original = array("{full_name}", "{reset_url}");
                    $to_replace = array($forgotten['full_name'], "<a href='" . site_url('account/reset_password') . "/" . $passcode . "'>This link</a>");

                    $this->email_model->send_mail($id_email, $to_email, array(), array(), $original, $to_replace);

                    // if there were no errors
                    // $messages = $this->ion_auth->messages();
                    $messages = lang('lforgot_password_message');
                    //$redirect = "admin/login";//we should display a confirmation page here instead of the login page
                } else {
                    //$redirect = "";//we should display a confirmation page here instead of the login page
                    $messages = lang('lforgot_password_message');

                    //$messages = $this->ion_auth->errors();
                    //$is_error = true;

                }
            }
        } else {
            //form validation error
            $xmessages = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $xmessages = str_replace('</p>', '', $xmessages);
            $message = explode('<p>', $xmessages);
            $messages = '';
            foreach ($message as $msg) {
                if (!empty($msg))
                    $messages .= "<span class=\"vd_alert-icon\"><i class=\"fa fa-exclamation-triangle vd_red\"></i>  " . $msg . "</span><br/>";
            }
            $is_error = true;
        }

        echo json_encode(
            array("error" => $is_error,
                "message" => $messages,
                //"redirect"	=> $redirect
            )
        );
    }//end function doforgot_password

    // reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        $this->load->library(array('form_validation'));
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            // display the form

            // set the flash data error message if there is one
            $this->viewparams['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $this->viewparams['new_password'] = array(
                'name' => 'new',
                'class' => 'w-input field',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $this->viewparams['min_password_length'] . '}.*$',
            );
            $this->viewparams['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'class' => 'w-input field',
                'type' => 'password',
                'pattern' => '^.{' . $this->viewparams['min_password_length'] . '}.*$',
            );
            $this->viewparams['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );
            $this->viewparams['csrf'] = $this->_get_csrf_nonce();
            $this->viewparams['code'] = $code;

            $this->viewparams['flash_message'] = $this->session->flashdata('message');

            // render
            parent::viewpage("account/account-reset-password");
            //$this->load->view("account/account-reset-password", $this->viewparams);
        } else {
            // if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            //$this->session->set_flashdata('message', lang('linvalid_forgot_password_code'));
            redirect("/", 'refresh');
        }
    }

    public function doreset_password()
    {
        $this->load->library(array('form_validation'));
        $code = $this->input->post('code');
        $user = $this->ion_auth->forgotten_password_check($code);

        // if the code is valid then display the password reset form
        $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

        $messages = $redirect = "";
        $is_error = false;
        if ($this->form_validation->run() == false) {
            //form validation error
            $messages = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
        } else {

            // finally change the password
            $identity = $user->{$this->config->item('identity', 'ion_auth')};
            $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

            if ($change) {
                // if the password was successfully changed
                $messages = $this->ion_auth->messages();
                $redirect = site_url('/');
            } else {
                $messages = $this->ion_auth->errors();
                $is_error = true;
                $redirect = site_url('account/reset_password') . "/" . $code;
            }
            //  }
        }

        echo json_encode(
            array("error" => $is_error,
                "message" => $messages,
                "redirect" => $redirect
            )
        );
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function register()
    {
        if(isMember()){
            redirect('/');
            exit;
        }
     
        $this->viewparams['title_page'] = lang('lregister');
        $this->load->helper('form');
        
        //terms and condition modal 
        $this->load->model('page/page_model');

        $data = $this->page_model->get_data_by_slug('terms-condition');
        $this->viewparams['terms_title']    = $data[0]->page_title;
        $this->viewparams['terms_content']  = $data[0]->page_content;

        parent::viewpage("account/account-register");
	}

    function dologin()
    {
        $this->load->library(array('form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');

        //validate form input
        $this->form_validation->set_rules('email', 'lang:lemail', 'required');
        $this->form_validation->set_rules('password', 'lang:lpassword', 'required');
        $is_error = false;
        $messages = $redirect = "";

        if (!$is_error) {
            $identity = $this->input->post('email');
            if ($this->ion_auth->is_max_login_attempts_exceeded($identity)) {
                $messages = lang('ltoo_many_login_attempts');
                $is_error = true;
            } else {

                if ($this->form_validation->run() == true) {
                    // check to see if the user is logging in
                    // check for "remember me"
                    //$remember = (bool) $this->input->post('remember');
                    $remember = false;

                    if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember)) {
                        //if the login is successful
                        //redirect them back to the home page
                        //$str_replace = array('<p>', '</p>');
                        $messages = str_replace("<p>", "", $this->ion_auth->messages());
                        $messages = str_replace("</p>", "", $messages);
                        //$messages = lang("llogin_successful");

                        $this->ion_auth->clear_login_attempts($identity);

                        //set user session for package type and membership date
                        $user = $this->ion_auth->user()->row();
                        $this->session->set_userdata('user_package_id', $user->package_id);
                        $this->session->set_userdata('user_package_enddate', $user->package_enddate);


                        $redirect = site_url('account/profile');

                    } else {
                        // if the login was un-successful
                        // redirect them back to the login page
                        $xmessages = $this->ion_auth->errors();
                        $xmessages = str_replace('</p>', '', $xmessages);
                        $message = explode('<p>', $xmessages);
                        $messages = '';
                        foreach ($message as $msg) {
                            if (!empty($msg))
                                $messages .= "<span class=\"vd_alert-icon\"><i class=\"fa fa-exclamation-triangle vd_red\"></i>  " . $msg . "</span><br/>";
                        }
                        //$messages = str_replace("<p>","",$messages);
                        //$messages = str_replace("</p>","",$messages);
                        $is_error = true;
                    }

                } else {
                    // the user is not logging in so display the login page
                    // set the flash data error message if there is one
                    $xmessages = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                    $xmessages = str_replace('</p>', '', $xmessages);
                    //$messages = str_replace("<p>","",$messages);
                    //$messages = str_replace("</p>","",$messages);
                    $message = explode('<p>', $xmessages);
                    $messages = '';
                    foreach ($message as $msg) {
                        if (!empty($msg))
                            $messages .= "<span class=\"vd_alert-icon\"><i class=\"fa fa-exclamation-triangle vd_red\"></i>  " . $msg . "</span><br/>";
                    }

                    $is_error = true;
                }
            }
        }

        echo json_encode(
            array("error" => $is_error,
                "message" => $messages,
                "redirect" => $redirect
            )
        );
    }

    //update_user
    function doupdate()
    {
        if (!$this->input->is_ajax_request()) {
            //do nothing
            exit;
        }

        //only logged in user can display this
        if (!$this->account_model->is_authenticated()) {
            //do nothing
            exit;
        }

        /* check mandatory fields */
        $id = $this->ion_auth->get_user_id();
        $nickname = $this->input->post('nickname');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');

        $full_name = $this->input->post('full_name');
        $address = $this->input->post('address');
        $zipcode = $this->input->post('zipcode');
        $city = $this->input->post('city');
        $country = $this->input->post('country');

        //mandatory in add mode
        $password = $this->input->post('Password');
        $retype_password = $this->input->post('Password2');

        $message = array();
        $redirect = "";
        $is_error = false;


        //-- run form validation
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nickname', 'lang:lnickname', 'required');
        $this->form_validation->set_rules('full_name', 'lang:lname', 'required');
        $this->form_validation->set_rules('city', 'lang:lcity', 'required');
        $this->form_validation->set_rules('country', 'lang:lcountry', 'required');

        $this->form_validation->set_rules('email', 'lang:lemail_address', 'required|valid_email');
        $this->form_validation->set_rules('phone', 'lang:lphone', 'required');

        //resest password mode
        if ($password) {
            $this->form_validation->set_rules('Password', 'lang:lpassword', 'required|matches[Password2]');
            $this->form_validation->set_rules('Password2', 'lang:lretype_password', 'required');
        }

        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == FALSE) {
            $is_error = true;
            $message[] = validation_errors();

        }

        if (!$is_error) {
            if ($nickname) {
                //check unique nickname
                $is_nickname_exists = $this->user_model->is_nickname_exists($nickname,$id);
                $this->firephp->log($this->db->last_query());
                if ($is_nickname_exists) {
                    $is_error = true;
                    $message[] = lang('lnickname_exists_alert');
                }
            }
        }


        /* add/update if no error */
        if (!$is_error) {

            $data = array(
                "full_name" => $full_name
            , "nickname" => $nickname
            , "address" => $address
            , "zipcode" => $zipcode
            , "city" => $city
            , "country_id" => $country
            //, "email" => $email
            , "phone" => $phone
            , "updated_by" => _userid()
            , "updated_on" => time()

            );


            //if edit mode, and password and filled in , will reset password
            if ($password) {
                $data['password'] = $password;
            }

            $this->ion_auth->update($id, $data);

            $message[] = lang('ldata_success_updated');

        }

        $result = array(
            "message" => implode("<br>", $message)
        , "error" => $is_error
        , "redirect" => $redirect
        );

        echo json_encode($result);
    }

    function doregister()
    {
        if (!$this->input->is_ajax_request()) {
            return; //do nothing
        }

        $email = $this->input->post('Email');
        $phone = $this->input->post('Phone');
        $name= $this->input->post('Name');
        $active = 1; //automatic active

        $password = $this->input->post('Password');
        $retype_password = $this->input->post('Password2');

        $message = "";
        $redirect = "";
        $is_error = false;

        //-- run form validation
        /* check mandatory fields */
        $this->load->library('form_validation');
        $this->form_validation->set_rules('Name', 'lang:lfullname', 'required');
        $this->form_validation->set_rules('Email', 'lang:lemail', 'required|valid_email');
        $this->form_validation->set_rules('Phone', 'lang:lphone', 'required');

        $this->form_validation->set_rules('Password', 'lang:lpassword', 'required');
        $this->form_validation->set_rules('Password2', 'lang:lretype_password', 'required|matches[Password]');

        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == FALSE) {
            $is_error = true;
            $xmessage = validation_errors();
            $xmessage = str_replace('  \n', '', $xmessage);
            $xmessage = explode('<br/>', $xmessage);

            foreach ($xmessage as $msg) {
                //$msg = str_replace('<br/>', '', $msg);
                //$msg = str_replace(' ', '', $msg);
                //$msg = str_replace('\n', '', $msg);

                if (strlen($msg) > 10)
                    $message .= "<span class=\"vd_alert-icon\"><i class=\"fa fa-exclamation-triangle vd_red\"></i>  " . $msg . "</span><br/>";
                //$message.=$msg;
            }
        }

        if (!$is_error) {
            if ($email) {
                //check unique email dan login name
                $data_exists = $this->user_model->email_exists($email);
                $this->firephp->log($this->db->last_query());
                if ($data_exists) {
                    $is_error = true;
                    //$message[] = lang('lemail_exists_alert');
                    $msg = lang('lemail_exists_alert');
                    $message .= "<span class=\"vd_alert-icon\"><i class=\"fa fa-exclamation-triangle vd_red\"></i>  " . $msg . "</span><br/>";
                }
            }
        }

      
        /* if no error then register it */
        if (!$is_error) {
            $this->load->model('package/package_model');
            $default_package = config_item('default_package');
            $data_package = $this->package_model->get_data_by_id($default_package);
            $duration = $data_package->duration_months;

            /*
                Calculate package end date 
            */
            $package_enddate = date("Y-m-d", strtotime(" +".$duration." months"));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
            $data = array(
            "email"         => $email
            , "full_name"   => $name
            , "phone"       => $phone
            , "active"      => ($active) ? $active : 0
            , "package_id"  => $default_package //default package when register is 1 = FREE TRIAL , for 3 month(get from db)
            ,"package_enddate"  => $package_enddate
            , "updated_on" => time()
            );

            /*
            Default Group id = 2 (member)
            */
            $id = $this->ion_auth->register($email, $password, $email, $data, array(2));
            $package_enddate_fmt  = iddate($package_enddate);
            
            /** Send Email for success registration */
            $this->load->model('email/email_model');
            $id_email = 2;
            $to_email = $email;
            $original = array("{full_name}","{email}", "{username}", "{password}","{package_enddate}");
            $to_replace = array($name,$to_email, $to_email, $password,$package_enddate_fmt);
            $this->email_model->send_mail($id_email, $to_email, array(), array(), $original, $to_replace);
            /** Send Email for success registration */

            if ($id) {
                $message[] = lang('lmember_register_success');

                //logged in 
                if ($this->ion_auth->login($email, $password, false)) {
                    $redirect = site_url("/");
                }
            } else {
                $xmessage = lang('lemail_exists_alert');
                $msg = $xmessage;
                //$message[] = lang('lmember_register_failed');
                $message .= "<span class=\"vd_alert-icon\"><i class=\"fa fa-exclamation-triangle vd_red\"></i>  " . $msg . "</span><br/>";
            }
        }

        $result = array(
            "message" => $message
        , "error" => $is_error
        , "redirect" => $redirect
        );

        echo json_encode($result);
    }

    function profile()
    {
        //only logged in user can display this
        if (!isMember()) {
            redirect('auth/logout');
        }
        $this->load->helper('form');

        $this->viewparams['title_page'] = lang('lprofile');
        
       //get user data
        $user = $this->ion_auth->user()->row();
        $this->viewparams['user_data'] = $user;
       

        //echo $this->db->last_query();
		parent::viewpage("account/account-profile");
	}


    public function publish_my_ads()
    {
        //only logged in user can delete this
        if (!$this->account_model->is_authenticated()) {
            redirect('account/logout');
        }

        $id = $this->input->post("id");
        //check if used in type and file
        if (!$this->ads_model->is_my_ads($id)) {
            $this->firephp->log("sql : " . $this->db->last_query());
            $success = false;
            $message = lang('ldata_is_failed_publish');
        } else {
            //update ads
            $row = $this->ads_model->getRow(array('id' => $id));
            $success = true;

            if ($row['active'] == 0) {
                $publish = 1;
                $message = lang("lpublish");
            } else {
                $publish = 0;
                $message = lang("lunpublish");
            }

            $this->ads_model->update($id, array('active' => $publish));
        }
        //update result in json
        $result = array("success" => $success, "message" => $message);
        echo json_encode($result);
    }

    public function sold_my_ads()
    {
        //only logged in user can delete this
        if (!$this->account_model->is_authenticated()) {
            redirect('account/logout');
        }

        $id = $this->input->post("id");
        //check if used in type and file
        if (!$this->ads_model->is_my_ads($id)) {
            $this->firephp->log("sql : " . $this->db->last_query());
            $success = false;
            $message = lang('ldata_is_failed_sold');
        } else {
            //update ads
            $row = $this->ads_model->getRow(array('id' => $id));
            $success = true;

            if ($row['sold'] == 0) {
                $sold = 1;
                $message = lang("lset_ready");
                $price = lang('lsold');
            } else {
                $sold = 0;
                $message = lang("lset_sold");
                $price = number_format($row['price'], 0);
            }

            $this->ads_model->update($id, array('sold' => $sold));
        }
        //update result in json
        $result = array("success" => $success, "message" => $message, 'price' => $price);
        echo json_encode($result);
    }

    /**
     * Delete My Ads
     */
    public function delete_my_ads()
    {
        //only logged in user can delete this
        if (!$this->account_model->is_authenticated()) {
            redirect('account/logout');
        }

        $id = $this->input->post("id");
        //check if used in type and file
        if (!$this->ads_model->is_my_ads($id)) {
            $this->firephp->log("sql : " . $this->db->last_query());
            $success = false;
            $message = lang('ldata_is_failed_deleted');
        } else {
            //delete master ads
            $this->ads_model->delete($id);

            //unlink photo
            $photos = $this->ads_photo_model->getRowsById($id);
            $user_directory = $this->config->item('upload_path');
            if ($photos) {
                foreach ($photos as $key => $photo) {
                    $file = $user_directory . $photo['storage_location'];
                    unlink($file);
                }
            }

            //delete bulk ads_model_photo where ads_id
            $this->ads_photo_model->delete($id);

            $success = true;
            $message = lang('ldata_is_deleted');

        }
        //delete
        $result = array("success" => $success, "message" => $message);
        echo json_encode($result);
    }

    /**
     * Logout for web redirect example
     *
     * @return  [type]  [description]
     */
    public function logout()
    {
        $redirect = '';
         // log the user out
        $this->ion_auth->logout();

           redirect('/', $redirect);
    }

}

/* End of file category.php */
/* Location: ./apps/modules/category/controllers/category.php */
