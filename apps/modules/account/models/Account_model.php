<?php
class Account_model extends CI_Model {
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
   
    function is_authenticated(){
        $CI =& get_instance();
        
        //check fb authenticated
        if($CI->facebook->is_authenticated()){
            return true;
        }    
        
        //member and admin can access front end
        if(isLoggedIn()){
            return true;
        }
        
        return false;

    }
   
    function logout_url(){
        $CI =& get_instance();
        if($CI->facebook->is_authenticated()){
            return $this->facebook->logout_url();
        }else{
            return base_url()."account/logout";
        }
    }
    
    /*
     * $user['id'] = facebook_id
     * $user['email'] = email
     */
    function member_exists($user){
        $this->db->where("facebook_id",$user['id']);
        $this->db->or_where("email",$user['email']);
		$query = $this->db->get('users');
		$result = $query->row();
	
        return $result;
    }
    
}