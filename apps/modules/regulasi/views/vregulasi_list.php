
<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/js/jquery/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo base_url(); ?>assets/js/jquery/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<link href='<?=base_url();?>assets/js/jquery/chosen/1.4.2/chosen.css' rel='stylesheet'>

<script src="<?php echo base_url(); ?>assets/js/jquery/chosen/1.4.2/chosen.jquery.min.js" type="text/javascript"></script>
<?php
$end = date('Y-m-d', strtotime('+5 years'));
//echo $end;
?>

<script type="text/javascript">
        
        
	 jQuery().ready(function (){
         $("#submit-btn").click(function(e){
            e.preventDefault(); 
            $("#submit_spinner").show();
			gridReload();
              $("#submit_spinner").hide();
      
		});	
        $("#type_spinner").hide();

        $("#category_id").change(function(){
            populate_type($(this).val(),0);
        });

        $("#submit_spinner").hide();
          $('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect: true});
    	 jQuery("#list1").jqGrid({
            url:'<?=site_url("admin/regulasi/loadDataGrid")?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id','active','nomor_dokumen',"<?=lang('ldate')?>",'<?=lang('lcategory')?>',"<?=lang('ljenis_dokumen')?>","<?=lang('lnomor_dokumen')?>",
                '<?=lang('lperihal')?>',"<?=lang('lberlaku')?>",'<?=lang('ledit')?>','<?=lang('ldel')?>'],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:1, align:"right",sortable:false},
                {name:'id',index:'category_id', hidden: true},
                {name:'active',index:'active', hidden: true},
                {name:'nomor_dokumen_raw',index:'nomor_dokumen_raw', hidden: true},
                {name:'tanggal',index:'tanggal', align:"left",width:3,sortable:false},
                {name:'category_name',index:'category_name', align:"left",width:4},
                {name:'type_name',index:'type_name', align:"left",width:4},
                {name:'nomor_dokumen',index:'nomor_dokumen',align:"left",stype:'text',width:4,
                	formatter: function (cellvalue, options, rowObject) {
					   return '<a href="<?=base_url()?>admin/regulasi/edit/' + rowObject[1] + '"><u>'+cellvalue+'</u></a>';
					},sortable:false
                 },
                {name:'judul',index:'judul',align:"left",stype:'text',width:8,sortable:false},
                {name:'berlaku_str',index:'berlaku_str',sortable:false, align:"center",width:1},
                 {name:'edit',index:'edit', width:1, align:"left",sortable:false,align:"center",width:1,
                	formatter:'dynamicLink', 
      			 	formatoptions:{
      			 		 url: function (cellValue, rowId, rowData) {
                                return '<?=site_url("admin/regulasi/edit")?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-edit.png' id='edit-"+rowId+"' class='edit' border='0' width='16px' height='16px'>";
							}
					},
      			 	cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ledit')?>"' 
                            return attribute ;
                     }
      			 },
      			  {name: 'delete', index: 'delete', width: 1,align:"center",width:1,
                        formatter: 'dynamicLink',
                        formatoptions: {
                            url: function (cellValue, rowId, rowData) {
                                return '<?=site_url("admin/regulasi/delete")?>/' + rowId;
                            },
                            cellValue: function (cellValue, rowId, rowData) {
                                return "<img src='<?=base_url()?>assets/admin/img/ico-delete.png' id='delete-"+rowId+"' class='delete' border='0' width='16px' height='16px'>";
                            },
                            onClick: function (rowId, iRow, iCol, cellValue, e) {
                                 var nomer = $('#list1').jqGrid('getCell',rowId,'nomor_dokumen_raw');
                             
                                $.confirm({
                                    title:"<?=lang('ldelete')?>",
                                    text: "<?=lang('lconfirm_delete')?> " + nomer,
                                    confirmButton: "<?=lang('lok')?>",
                                    cancelButton: "<?=lang('lcancel')?>",
                                    confirm: function(button) {
                                       $.post(
                                            "<?=site_url("admin/regulasi/delete")?>/" + rowId, 
                                            function(data) {
                                                if(!data.is_error)
                                                    var rv = '<div class="alert alert-success">'+data.message+'</div>';
                                                else
                                                    var rv = '<div class="alert alert-error">'+data.message+'</div>';

                                                    $('#show_message').html(rv);
                                                    $('#show_message').slideDown('normal',function(){
                                                         setTimeout(function() {
                                                            $('#show_message').slideUp('normal',function(){
                                                                if(!data.is_error){
                                                                    gridReload();
                                                                }
                                                            });	
                                                          }, <?=config_item('message_delay')?>);
                                                    });	
                                                //reload grid
                                                //gridReload();
                                            },"json"
                                        );

                                    },
                                    cancel: function(button) {
                                       // alert("You cancelled.");
                                    }
                                });
                                
                                
                            	
                            }
                        },
                        cellattr: function (rowId, cellValue, rawObject) {
                            var attribute = ' title="<?=lang('ldelete')?>"' 
                            return attribute ;
                        }}
                              
                        
	          ],
            rowNum:50,
            <?if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?}?>
            width: 1024,
            height: 1280,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'tanggal',
            sortorder: 'desc',
            toppager: true, 
            //shrinkToFit:false,
			loadComplete: function(data) {
				$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
    });
    
    
    function gridReload(){
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url("admin/regulasi/loadDataGrid")?>/?nomor_dokumen="+$("#nomor_dokumen").val()+"&category="+$("#category_id").val()+"&types="+$("#types_id").val(),
				page:1
			}).trigger("reloadGrid");
	}

 function populate_type(category_id,id_select){
        if(category_id) {
            //alert(id_select);
            $("#type_spinner").show();
            $.getJSON("<?=site_url('admin/type/get_by_category')?>/"+category_id+"/"+id_select,
                function(j){
                  var options = '<option value=""></option>';
          
                  for (var i = 0; i < j.length; i++) {
                    var selected  = "";
                    if(j[i].type_id == id_select)
                        selected = "selected";
                    options += '<option value="' + j[i].type_id + '" '+selected+'>' + j[i].type_name+ '</option>';
                  }
                  $("select#types_id").html(options).trigger('chosen:updated');
                  $("#type_spinner").hide();
                }
            );
        }else{
            var options = '<option value=""></option>';
            $("select#types_id").html(options).trigger('chosen:updated');
        }
        
        
    }
    

    
</script>

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('admin')?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-list"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<div class="box-content">
            <form id='input-form' class="form-horizontal" method="post">
    		    
                    <div class="row-fluid">
                    
                        <span class="span6">
                        	 <div class="control-group">
        						<label class="control-label" for="nomor_dokumen"><?=lang('lnomor_dokumen');?></label>
        						<div class="controls">
                                    <input class="input-xlarge" type="text" name='nomor_dokumen' id='nomor_dokumen' value="" maxlength='100'>
                              	</div>
        					</div>
                        </span>
                    </div>

                     <div class="row-fluid">
                    
                        <span class="span6">
                             <div class="control-group">
                                <label class="control-label" for="nomor_dokumen"><?=lang('lcategory');?></label>
                                <div class="controls">
                                        <select name='category_id' id='category_id' class="input-xxlarge"  data-rel="chosen" data-placeholder="<?=lang('lplease_select_category')?>" >
                                        <option value=''></option>
                                       <?php 
                                            foreach($data_categories as $data_category){
                                               $category_selected = "";
                                               
                                               $data_childs = $this->category_model->get_childs($data_category->id,'display_order,category_name','asc');
                                               ?>
                                                  <option value='<?=$data_category->id?>' ><?=$data_category->name?></option>

                                                  <?
                                                   if(count($data_childs)){
                                               ?>
                                                  <?    
                                                   //get child 
                                                      foreach($data_childs as $data_child){
                                                           ?><option value='<?=$data_child->category_id?>' <?php echo $category_selected;?>>
                                                               &nbsp;&nbsp;&nbsp; -&nbsp;<?=$data_child->category_name?>
                                                             </option>
                                                       <?} 
                                                    ?>
                                                <?}
                                            }?>
                                      </select>

                                

                                </div>
                            </div>

                           <div class="control-group">
                                <label class="control-label" for="name"><?=lang('ljenis_dokumen');?></label>
                                <div class="controls">
                                       <select id="types_id" name='types_id' data-rel="chosen" class='input-xxlarge' data-placeholder="<?=lang('lplease_select_jenis_dokumen')?>">
                                         <option value=''></option>
                                      </select> 
                                      <img src="<?=base_url()?>assets/admin/img/spinner-mini.gif" id="type_spinner">

                                  </div>
                            </div>




                        </span>
                    </div>



                           
                       

                
                    <div class="form-actions">
    					   <img src="<?=base_url()?>assets/admin/img/spinner-mini.gif" id="submit_spinner">
    					  	<button type="submit" class="btn btn-primary" id='submit-btn'>
                              <i class="icon-search"></i> <?=lang('lsearch')?>
                           </button>
                           
                           <a href="<?=site_url("admin/regulasi/add")?>" class="btn btn-small btn-primary">
                            <i class="icon-plus"></i> <?=$ladd_button_title?>
                        </a>
                    </div><!--/form-actions-->
                        
                        
            </form>
			<br/>
			<div id='show_message' style="display: none;"></div> 
			<table id="list1"></table> <!--Grid table-->
			<div id="pager1"></div>  <!--pagination div-->
		</div>
	</div><!--/span-->

</div><!--/row-->
