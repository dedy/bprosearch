Total data : <?=$total_data_terkait?>
<table class="table table-striped table-bordered bootstrap-datatable datatable" >
    <th>No. </th>
    <th><?=lang("ljenis_dokumen")?></th>
    <th><?=lang("lnomor_dokumen")?></th>
    <th><?=lang("ldate")?></th>
    <th width='25px'> <img src='<?=base_url()?>assets/admin/img/ico-delete.png' border='0' width='16px' height='16px'></th>
    <tbody>
      <?php 
      $counter = 0;
      if($total_data_terkait) {
          foreach($data_regulasi_terkait as $value){
              $counter++;
              $msg = "";
              $counter_message = 0;

              ?>
           <tr>
                <td width='5px'><?=$counter?>.</td>
                <td><?=$value->types_name?></td>
                <td><a href='<?=site_url('admin/regulasi/edit')?>/<?=$value->regulasi_id_terkait?>' target="_blank"><?=$value->nomor_terkait?></td>
                <td><?=$value->tanggal_terkait_fmt?></td>
                <td>
                    <a title='<?=lang('ldelete')?>' class='delete_class' href="javascript:deleteterkait('<?=$value->regulasi_id?>','<?=$value->regulasi_id_terkait?>','<?=$value->nomor_terkait?>')" >
                        <img src='<?=base_url()?>assets/admin/img/ico-delete.png' id='del-<?=$value->id_terkait?>' border='0' width='16px' height='16px'>
                    </a>        
                </td>    
                    
           </tr>
          <?php }
          }?>
      </tr>
    </tbody>
</table>