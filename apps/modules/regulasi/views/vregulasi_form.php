<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<link href='<?=base_url();?>assets/js/jquery/chosen/1.4.2/chosen.css' rel='stylesheet'>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/adapters/jquery.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery/chosen/1.4.2/chosen.jquery.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/select2/js/select2.full.js"></script>
<link href="<?php echo base_url(); ?>assets/js/jquery/select2/css/select2.min.css" type="text/css" rel="stylesheet" />


<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<script>
	$(document).ready(function(){
        var config = {
            height:<?=config_item('ck_height')?>,
            width:740 ,
            path: '<?php echo base_url(); ?>assets/js/ckeditor/' ,
            enterMode: CKEDITOR.ENTER_BR

        };
        
         <?
          if(isset($data)) { ?>

            populate_terkait(<?=$data->id?>);

            $('#submit-btn2').click(function(e){
                e.preventDefault(); 
                $("#terkait_spinner").show();
                //post the data
                $.ajax({
                  type: "POST",
                  url: "<?=site_url('regulasi/terkait/doupdate')?>",
                  data: $("#input-form2").serialize(),
                    success: function(returData){
                        //alert('hi');
                        $("#terkait_spinner").hide();
                        $('#showmessage2').slideUp('normal',function(){

                            if(returData.error){
                                var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                                $('#showmessage2').html(rv);
                                $('#showmessage2').slideDown('normal'); 

                            }else{
                                var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                                $('#showmessage2').html(rv);
                                $('#showmessage2').slideDown('normal',function(){
                                     setTimeout(function() {
                                        $('#showmessage2').slideUp('normal',function(){ 
                                            //load the list
                                            populate_terkait(<?=$data->id?>);
                                            //empty the form
                                            //$('input:hidden[name=id]').val(0);
                                        }); 
                                      }, <?=config_item('message_delay')?>);
                                }); 
                            } 
                        });
                    },
                  dataType: "json"
                });

                return false;

            });


        <?}?>


        $("input[name='berlaku']").change(function(){
            var val = $(this).val();
            if(val == 1){
                 //$("#id_regulasi_pengganti").val('').trigger('change');
                 $('#id_regulasi_pengganti').prop('disabled', true);
            }else{
                 $('#id_regulasi_pengganti').prop('disabled', false);
            }
        });
        
        <?
        //jika berlaku, tampilkan pilihan regulasi pengganti
        if(isset($data->berlaku) && !$data->berlaku) { ?>
               $('#id_regulasi_pengganti').prop('disabled', false);
        <? }else {
            //
            ?>
               $('#id_regulasi_pengganti').prop('disabled', true);
        <? } ?>
        
        $("#pdf-btn").click(function(e){
            e.preventDefault(); 
            var obj = $(this);
            obj.attr('disabled','disabled');
            $('#loadingmessage').show();

            //$('form#input-form').submit();
            //return;
            //alert('lie');
            $('form#input-form').ajaxSubmit({
                URL:'<?=site_url('admin/regulasi/doupdate');?>',
                dataType: 'json', 
                success: function(returData){
                    //alert('hi');
                    $('#loadingmessage').hide();
                    obj.removeAttr('disabled');

                    $('#show_message').slideUp('normal',function(){

                        if(returData.error){
                            var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal');	

                        }else{
                            var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal',function(){
                                 setTimeout(function() {
                                    $('#show_message').slideUp('normal',function(){
                                         window.open("<?=site_url('admin/regulasi/viewpdf');?>/"+returData.id, "popupWindow", "width=600,height=600,scrollbars=yes");
                                        <? 
                                        if(!isset($data)) { ?>
                                        window.location.href = "<?=site_url('admin/regulasi/edit');?>/"+returData.id;
                                        <?}?>
                                        
                                    });	
                                  },  <?=config_item('message_delay')?>);
                            });	
                        }	
                    });
                  }
              });

           
        });    
            
        $("#id_regulasi_pengganti").select2({
            minimumInputLength: 4,
            ajax: {
                url: "<?=site_url('admin/regulasi/ajax_get')?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term
                    ,id:<?=$id_regulasi?>
                  };
                },
                results: function (data) {
                    return {
                        results: data
                    };
                },
                type: "GET"
            }
        });
        
        $("#id_regulasi_terkait").select2({
            minimumInputLength: 4,
            ajax: {
                url: "<?=site_url('admin/regulasi/ajax_get')?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term
                    ,id:<?=$id_regulasi?>
                  };
                },
                results: function (data) {
                    return {
                        results: data
                    };
                },
                type: "GET"
            }
        });

		CKFinder.setupCKEditor( null, '/assets/js/ckfinder/' );	
    $('textarea#isi_dokumen').ckeditor(config);
		$('textarea#isi_dokumen_en').ckeditor(config);
		
        <?
        //populate type dokumen
        if(isset($data->types_id)) {
            if(isset($data_regulasi_category[0]->category_id)) {?>
             populate_type(<?=$data_regulasi_category[0]->category_id?>,<?=$data->types_id?>);
        <?}
        }?>
        //chosen - improves select
        $('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect: true});
        $("#type_spinner").hide();
        //populate_type(0,0);
        
        //datepicker
        $('.datepicker').datepicker({ dateFormat: "dd/mm/yy",changeMonth: true, changeYear: true,yearRange: '1950:' + new Date().getFullYear()});
        
        $("#_tanggal").click(function(){
               $("#tanggal").val("");
           })
            
        $('#cancel-btn').click(function(e){
			e.preventDefault(); 
			window.location.replace("<?=site_url("admin/regulasi");?>");
		});
		
		$('#submit-add-btn').click(function(e){
			e.preventDefault(); 
			$('#addnew').val(1);
			dopost($(this));
		});
		
		$('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
		});
		
        $("#category1").change(function(){
            populate_type($(this).val(),0);
        });
        
	});
	
  function populate_terkait(id){
        //show loading image
        $("#list_terkait").html('<img src="<?=base_url()?>assets/admin/img/spinner-mini.gif">'); 
        $( "#list_terkait" ).load( "<?=site_url('regulasi/terkait/get_data')?>/"+id, function() {
            //hide loading image
        });
    }


    function populate_type(category_id,id_select){
        if(category_id) {
            //alert(id_select);
            $("#type_spinner").show();
            $.getJSON("<?=site_url('admin/type/get_by_category')?>/"+category_id+"/"+id_select,
                function(j){
                  var options = '<option value=""></option>';
          
                  for (var i = 0; i < j.length; i++) {
                    var selected  = "";
                    if(j[i].type_id == id_select)
                        selected = "selected";
                    options += '<option value="' + j[i].type_id + '" '+selected+'>' + j[i].type_name+ '</option>';
                  }
                  $("select#types_id").html(options).trigger('chosen:updated');
                  $("#type_spinner").hide();
                }
            );
        }else{
            var options = '<option value=""></option>';
            $("select#types_id").html(options).trigger('chosen:updated');
        }
        
        
    }
    
	function dopost(obj){

		obj.attr('disabled','disabled');
 		$('#loadingmessage').show();
		
		//$('form#input-form').submit();
		//return;
		//alert('lie');
		$('form#input-form').ajaxSubmit({
		 	URL:'<?=site_url("admin/regulasi/doupdate");?>',
		 	dataType: 'json', 
		   	success: function(returData){
		   		//alert('hi');
		   		$('#loadingmessage').hide();
				obj.removeAttr('disabled');
 				
				$('#show_message').slideUp('normal',function(){
					
					if(returData.error){
						var rv = '<div class="alert alert-error">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal');	
							
					}else{
						var rv = '<div class="alert alert-success">'+returData.message+'</div>';
						$('#show_message').html(rv);
						$('#show_message').slideDown('normal',function(){
							 setTimeout(function() {
							    $('#show_message').slideUp('normal',function(){
								    if(returData.redirect){
								    	window.location.replace(returData.redirect);
								    }
							    });	
							  },  <?=config_item('message_delay')?>);
						});	
					}	
				});
		      }
		  });
	}	
	
  <?
    if(isset($data)) { ?>
        function deleteterkait(regulasi_id,regulasi_id_terkait,regulasi_terkait_nomor){
            $.confirm({
                        title:"<?=lang('ldelete_terkait')?> : "+regulasi_terkait_nomor,
                        text: "<?=lang('lconfirm_delete')?>",
                        confirmButton: "<?=lang('lok')?>",
                        cancelButton: "<?=lang('lcancel')?>",
                        confirm: function(button) {
                           $.post(
                                "<?=site_url('regulasi/terkait/delete')?>/" + regulasi_id+'/'+regulasi_id_terkait, 
                                function(returData) {

                                     $('#showmessage2').slideUp('normal',function(){

                                      if(returData.error){
                                          var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                                          $('#showmessage2').html(rv);
                                          $('#showmessage2').slideDown('normal'); 

                                      }else{
                                          var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                                          $('#showmessage2').html(rv);
                                          $('#showmessage2').slideDown('normal',function(){
                                               setTimeout(function() {
                                                  $('#showmessage2').slideUp('normal',function(){ 
                                                      //load the list
                                                      populate_terkait(<?=$data->id?>);
                                                      //empty the form
                                                      //$('input:hidden[name=id]').val(0);
                                                  }); 
                                                }, <?=config_item('message_delay')?>);
                                          }); 
                                      } 
                                  });


                                     populate_terkait(<?=$data->id?>);
                                },"json"
                            );

                        },
                        cancel: function(button) {
                           // alert("You cancelled.");
                        }
                    });
                return false;
        }
    <?}?>

	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url("admin")?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
		<li><a href="<?=site_url("admin/regulasi")?>"><?=lang('llist_regulasi')?></a></li><span class="divider">/</span>
        <li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
             <?php 
              $hidden = array(
                  "id"              => (isset($data->id))? $data->id:0
              );
              echo form_open_multipart("admin/regulasi/doupdate",array('id'=>'input-form','class'=>'form-horizontal'),$hidden);
              ?>
                   <div class="row-fluid">
                       <div class="span6">
                           
                           <div class="control-group">
                                <label class="control-label" for="name">
                                   <b>*&nbsp;<?=lang('lcategory')?> 1</b>
                                </label>
                                <div class="controls">
                                        <select name='category_id[]' id='category1' class="input-xlarge"  data-rel="chosen" data-placeholder="<?=lang('lplease_select_category')?> 1" >
                                        <option value=''></option>
                                       <?php 
                                            foreach($data_categories as $data_category){
                                               $category_selected = "";
                                               
                                               //hanya pajak yg punya child
                                              //all cat can have subcat
                                                 //get child 
                                                   $data_childs = $this->category_model->get_childs($data_category->id,'display_order,category_name','asc');
                                                   
                                                   if(count($data_childs)){
                                                    ?>
                                                    <optgroup label="<?=$data_category->name?>">
                                                   <?    
                                                       foreach($data_childs as $data_child){
                                                           $category_selected = "";
                                                            if(isset($data_regulasi_category[0]->category_id) && ($data_regulasi_category[0]->category_id == $data_child->category_id)){
                                                                $category_selected = "selected";
                                                            }
                                                           
                                                           ?><option value='<?=$data_child->category_id?>' <?php echo $category_selected;?>>
                                                               &nbsp;&nbsp;&nbsp; -&nbsp;<?=$data_child->category_name?>
                                                             </option>
                                                       <?} 
                                                    ?>
                                                    </optgroup>
                                                    <?} else {
                                                         $category_selected = "";
                                                         if(isset($data_regulasi_category[0]->category_id) && ($data_regulasi_category[0]->category_id == $data_category->id)){
                                                             $category_selected = "selected";
                                                         }

                                                    ?>
                                                    <option value='<?=$data_category->id?>' <?php echo $category_selected;?>>
                                                    <?=$data_category->name?></option>
                                          <?php }
                                            }?>
                                      </select>
                                  </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="name"><b>*&nbsp;<?=lang('ljenis_dokumen');?></b></label>
                                <div class="controls">
                                       <select id="types_id" name='types_id' data-rel="chosen" class='input-xlarge' data-placeholder="<?=lang('lplease_select_type')?>">
                                         <option value=''></option>
                                      </select>	
                                      <img src="<?=base_url()?>assets/admin/img/spinner-mini.gif" id="type_spinner">

                                  </div>
                            </div>
                       
                           <div class="control-group">
                                <label class="control-label">
                                    <b>*&nbsp;<?=lang('ldate')?></b>
                                  </label>
                                <div class="controls">
                                  <input class="input-small datepicker" type="text" name='tanggal' id='tanggal' value="<? if(isset($data->tanggal) && ($data->tanggal <> "0000-00-00")) echo $data->tanggal_fmt ?>">
                                  <img id="_tanggal" border="0" src="<?=base_url()?>assets/admin/img/b_del.gif">
                                 </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">
                                    <?=lang('lnomor')?>
                                  </label>
                                <div class="controls">
                                  <input class="input-small" type="text" name='nomor_urut' id='nomor_urut' value="<? if(isset($data->nomor_urut)) echo $data->nomor_urut ?>">
                                 </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="name"><b>*&nbsp;<?=lang('lnomor_dokumen');?></b></label>
                                <div class="controls">
                                <input class="input-xlarge" type="text" name='nomor_dokumen' id='nomor_dokumen' value="<?php if(isset($data->nomor_dokumen) && $data->nomor_dokumen) echo $data->nomor_dokumen ?>" maxlength='100'>
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label" for="name"><b>*&nbsp;<?=lang('lperihal');?></b></label>
                                <div class="controls">
                                    <textarea id='judul' name='judul' cols='50' rows='4'><? if(isset($data->judul) && $data->judul) echo $data->judul?></textarea>
                                </div>
                            </div>

                           
                           <!--  <div class="control-group">
                                <label class="control-label" for="name"><b>*&nbsp;<?=lang('lnama_file_dokumen');?></b></label>
                                <div class="controls">
                                <input class="input-xlarge" type="text" name='nama_file_regulasi' id='nama_file_regulasi' value="<?php if(isset($data->nama_file_regulasi) && $data->nama_file_regulasi) echo $data->nama_file_regulasi ?>" maxlength='255'>
                                </div>
                            </div>-->
                           
                            <div class="control-group">
                                <label class="control-label" for="name"><?=lang('ldokumen_regulasi');?></label>
                                <div class="controls">
                                <input class="input-xlarge" type="file" name='file_regulasi' id='file_regulasi' >
                                <input type="hidden" name='file_regulasi_old' value='<?php if(isset($data->nama_file_regulasi) && $data->nama_file_regulasi) { echo $data->nama_file_regulasi;}?>'>
                                    <?php if(isset($data->nama_file_regulasi) && $data->nama_file_regulasi) { ?>
                                    <br>
                                    <a href='<?=site_url('admin/regulasi/getfile/pdf')?>/<?=$data->id?>' target='_blank'><?=$data->nama_file_regulasi?></a>
                                <?}?>
                                                                
                                </div>
                            </div>
                           
                            <div class="control-group">
                                <label class="control-label" for="name"><?=lang('ldokumen_lampiran');?></label>
                                <div class="controls">
                                <input class="input-xlarge" type="file" name='file_lampiran' id='file_regulasi' >
                                <input type="hidden" name='file_lampiran_old' value='<?php if(isset($data->nama_file_lampiran) && $data->nama_file_lampiran) { echo $data->nama_file_lampiran;}?>'>
                                    <?php if(isset($data->nama_file_lampiran) && $data->nama_file_lampiran) { ?>
                                    <br>
                                    <a href='<?=site_url('admin/regulasi/getfile/lampiran')?>/<?=$data->id?>' target='_blank'><?=$data->nama_file_lampiran?></a>
                                <?}?>
                                                                
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label" for="name"><?=lang('ldokumen_regulasi');?> (EN)</label>
                                <div class="controls">
                                <input class="input-xlarge" type="file" name='file_regulasi_en' id='file_regulasi_en' >
                                <input type="hidden" name='file_regulasi_en_old' value='<?php if(isset($data->nama_file_regulasi_en) && $data->nama_file_regulasi_en) { echo $data->nama_file_regulasi_en;}?>'>
                                    <?php if(isset($data->nama_file_regulasi_en) && $data->nama_file_regulasi_en) { ?>
                                    <br>
                                    <a href='<?=site_url('admin/regulasi/getfileen/pdf')?>/<?=$data->id?>' target='_blank'><?=$data->nama_file_regulasi_en?></a>
                                <?}?>
                                                                
                                </div>
                            </div>
                           
                            <div class="control-group">
                                <label class="control-label" for="name"><?=lang('ldokumen_lampiran');?> (EN)</label>
                                <div class="controls">
                                <input class="input-xlarge" type="file" name='file_lampiran_en' id='file_lampiran_en' >
                                <input type="hidden" name='file_lampiran_en_old' value='<?php if(isset($data->nama_file_lampiran_en) && $data->nama_file_lampiran_en) { echo $data->nama_file_lampiran_en;}?>'>
                                    <?php if(isset($data->nama_file_lampiran_en) && $data->nama_file_lampiran_en) { ?>
                                    <br>
                                    <a href='<?=site_url('admin/regulasi/getfileen/lampiran')?>/<?=$data->id?>' target='_blank'><?=$data->nama_file_lampiran_en?></a>
                                <?}?>
                                                                
                                </div>
                            </div>
                           
                           
                            <div class="control-group" >
                                <label class="control-label" for='status'><b>*&nbsp;<?=lang('lberlaku')?></b></label>
                                <div class="controls">

                                    <label class="radio inline">
                                        <input type="radio" name="berlaku" id="berlaku1" value="1" <? if(!isset($data->berlaku) || (isset($data->berlaku) && $data->berlaku == 1)) echo "checked"; ?> > <?=lang('lyes')?>
                                    </label>
                                    <label class="radio inline">
                                    <input type="radio" name="berlaku" id="berlaku2" value="0" <? if(isset($data->berlaku) && $data->berlaku <> 1) echo "checked"; ?> > <?=lang('ltidak_berlaku')?>
                                    </label>

                                </div>
                              </div>
                           
                           
                           <div class="control-group" id="dv_regulasi_pengganti">
                                <label class="control-label" for="name"><b>*&nbsp;<?=lang('lperaturan_pengganti');?></b></label>
                                <div class="controls">
                                       <select id="id_regulasi_pengganti" name='id_regulasi_pengganti' class='input-xlarge' data-placeholder="<?=lang('lplease_select_regulasi_pengganti')?>">
                                       <?
                                       if(isset($data) && $data->id_regulasi_pengganti){
                                           ?>
                                           <option value='<?=$data->id_regulasi_pengganti?>' selected="selected"><?=$data->nomor_pengganti?> - <?=$data->tanggal_pengganti_fmt?></option>
                                        <?}?>
                                           <option value=''></option>
                                      </select>	
                                  </div>
                            </div>
                           
                           
                       </div>
                       <div class="span6">
                             <?
                                for ($i = 1; $i < config_item('number_of_category');$i++) { ?>
                                    <div class="control-group">
                                        <label class="control-label" for="name">
                                            <?
                                            //first one is mandatory
                                            if($i == 0 ) {
                                                echo "<b>*&nbsp;".lang('lcategory')." ".($i+1)."</b>";
                                            }else {
                                                echo lang('lcategory')." ".($i+1);
                                            }
                                            ?>
                                        </label>
                                        <div class="controls">
                                                <select name='category_id[]' id='category<?=$i+1?>' class="input-xlarge"  data-rel="chosen" data-placeholder="<?=lang('lplease_select_category')?> <?=$i+1?>" >
                                                <option value=''></option>
                                                
                                                <?php 
                                            foreach($data_categories as $data_category){
                                               $category_selected = "";
                                               
                                               //get child 
                                                   $data_childs = $this->category_model->get_childs($data_category->id,'display_order,category_name','asc');
                                                   if(count($data_childs)){
                                                 
                                               ?>
                                                    <optgroup label="<?=$data_category->name?>">
                                                   <?    
                                                     foreach($data_childs as $data_child){
                                                            $category_selected = "";
                                                            if(isset($data_regulasi_category[$i]->category_id) && ($data_regulasi_category[$i]->category_id == $data_child->id)){
                                                                $category_selected = "selected";
                                                            }
                                                           
                                                           ?>

                                                           <option value='<?=$data_child->category_id?>' <?php echo $category_selected;?>>
                                                               &nbsp;&nbsp;&nbsp; -&nbsp;<?=$data_child->category_name?>
                                                              
                                                             </option>
                                                       <?} 
                                                    ?>
                                                    </optgroup>
                                                <?} else {
                                                   $category_selected = "";
                                                   if(isset($data_regulasi_category[$i]->category_id) && ($data_regulasi_category[$i]->category_id == $data_category->id)){
                                                           $category_selected = "selected";
                                                       }

                                                    ?>
                                                  <option value='<?=$data_category->id?>' <?php echo $category_selected;?>>
                                                <?=$data_category->name?></option>
                                          <?php }
                                            }?>
                                              </select>
                                          </div>
                                    </div>

                                   
                                <?php 
                                    } ?>


                                    <div class="control-group">
                                <label class="control-label" for="name"><?=lang('lperihal');?> (EN)</label>
                                <div class="controls">
                                    <textarea id='judul_en' name='judul_en' cols='50' rows='4'><? if(isset($data->judul_en) && $data->judul_en) echo $data->judul_en?></textarea>
                                </div>
                            </div>
                           
                       </div>
                       
                       <div class='span12'>
                              <div class="control-group">
                                <?=strtoupper(lang('lisi_dokumen'));?>
                                <br />
                                <textarea name='isi_dokumen' id='isi_dokumen'>
                                <? if(isset($data->isi_dokumen) && $data->isi_dokumen) echo $data->isi_dokumen?>
                                </textarea> 
                            </div>  
                           
                       </div>

                        <div class='span12'>
                              <div class="control-group">
                                <?=strtoupper(lang('lisi_dokumen'));?> (EN)
                                <br />
                                <textarea name='isi_dokumen_en' id='isi_dokumen_en'>
                                <? if(isset($data->isi_dokumen_en) && $data->isi_dokumen_en) echo $data->isi_dokumen_en?>
                                </textarea> 
                            </div>  
                           
                       </div>
                   </div>   
           
                   
	
					<div id='show_message' style="display: none;"></div> 
				
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsave_changes_and_close')?></button>
					  	<button class="btn" id='cancel-btn'><?=lang('lcancel')?></button>
                        
                       <!--  <button type="button" class="btn btn-primary" id='pdf-btn'><?=lang('lview_pdf')?></button>-->
					</div><!--/form-actions-->
					
				</form>   <!--/form-->
        <?
        //edit mode
        if(isset($data)) { ?>
          <hr>
                <h2><?=lang('lperaturan_terkait')?>&nbsp;</h2>
                <?php 
                $hidden = array(
                    "regulasi_id"              => $data->id,
                    "id"              => ""
                );
                echo form_open("farm/market/doupdate",array('id'=>'input-form2','class'=>'form-horizontal'),$hidden);
                ?>

                <div class="control-group" >
                  <label class="control-label" for="name"><strong>*&nbsp;<?=lang('lregulasi');?></strong></label>
                  <div class="controls">
                         <select id="id_regulasi_terkait" name='id_regulasi_terkait' class='input-xlarge' data-placeholder="<?=lang('lplease_select_regulasi_terkait')?>">
                        
                             <option value=''></option>
                        </select>  
                      <button type="submit" class="btn btn-primary" id='submit-btn2'><?=lang('lsave')?></button>

                      <img src="<?=base_url()?>assets/admin/img/spinner-mini.gif" id="terkait_spinner" style="display: none;">
                    </div>
              </div>
               <div id='showmessage2'></div>


              </form>
            
            <div class='span8'>
              <div id='list_terkait'></div>
              </div>
          <?php }?>


		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->

