<?php
class Regulasi_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
   }
    
    //get popular last 30 days 
    //exclude category  kurs&suku bunga id 39
    function getPopularRegulasi(){
        $limit = config_item('limit_popular_home');
 
        $this->db->order_by('total_views','desc');
        $this->db->select("regulasi.judul");
        $this->db->select("regulasi.nomor_dokumen");
        $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_fmt",false);
        $this->db->join('regulasi','regulasi.id = popular_regulasi.regulasi_id',"left outer");
        $this->db->from('popular_regulasi');
        $this->db->limit($limit,0);
        
        $query = $this->db->get();
        
        return $query->result();
   } 

    function insert_view_history($id,$page){
        $values["regulasi_id"]  = $id;
        $values["page"]  = $page;
        $values['time']         = date("Y-m-d H:i:s");
        $values["ip_address"]   = $this->input->ip_address();
        $values["user_id"]   = (isLoggedIn())?_UserId():NULL;
                         
        $this->db->insert("regulasi_view_histories", $values); 
    }

    //get previous data yg tidak berlaku
    //berdasar id_regulasi_pengganti = $id
    function get_regulasi_history($id){
        $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_fmt",false);
        $this->db->select("regulasi.id as regulasi_id");
        $this->db->select("nomor_dokumen");
        $this->db->select("category_name_parents");
        $this->db->select("category_id_parents");
        $this->db->select("category_ids");
        $this->db->select("category_names");
        $this->db->select("judul");
        $this->db->select("judul_en");
        $this->db->select("berlaku");
        $this->db->where("id_regulasi_pengganti",$id);
        $this->db->from('regulasi');
        
        $query = $this->db->get();
        
        $datas = $query->result();
        //echo $this->db->last_query()."<BR><BR>";
        if(count($datas)){
            foreach($datas as $data){
                array_push($this->datas,$data);
                $this->get_regulasi_history($data->regulasi_id);
            }
        }else{
            return;
        }
    }

    //get previous data yg tidak berlaku
    //berdasar id_regulasi_pengganti = $id
    function get_regulasi_pengganti($id){
        $id_regulasi_pengganti = $this->get_id_regulasi_pengganti($id);

        if($id_regulasi_pengganti) {
            $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_fmt",false);
            $this->db->select("regulasi.id as regulasi_id");
            $this->db->select("nomor_dokumen");
            $this->db->select("category_name_parents");
            $this->db->select("category_id_parents");
            $this->db->select("category_ids");
            $this->db->select("category_names");
            $this->db->select("judul");
            $this->db->select("judul_en");
            $this->db->select("berlaku");
            $this->db->where("regulasi.id",$id_regulasi_pengganti);
            $this->db->from('regulasi');
            
            $query = $this->db->get();
            
            $datas = $query->result();
            //echo $this->db->last_query()."<BR><BR>";
            if(count($datas)){
                foreach($datas as $data){
                    array_push($this->datas,$data);
                    $this->get_regulasi_pengganti($data->regulasi_id);
                }
            }else{
                return;
            }
        }
    }

    function get_id_regulasi_pengganti($id){
        $this->db->select("id_regulasi_pengganti");
        $this->db->where("id",$id);
        $this->db->from('regulasi');
        
        $query = $this->db->get();
        
        $data = $query->result();
 
        return $data[0]->id_regulasi_pengganti;
    }

    function regulasi_history_is_exists($id){

        $this->db->select("count(regulasi.id) as total");
        $this->db->where("id_regulasi_pengganti",$id);
        $this->db->from('regulasi');
        
        $query = $this->db->get();
        
        $datas = $query->result();
        //echo $this->db->last_query()."<BR><BR>";
        if(count($datas)){
            if($datas[0]->total > 0){
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }


    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    

    /* countSearchUser */
    function countSearchRegulasi($searchval=array()){
        $this->is_count = true;
        
        $res = $this->searchRegulasi(0,0,'','',$searchval);
        
        return $res[0]->total;
    }

   	/* delete user*/
    function delete($data){
       	//-- delete records    	
    	$this->db->where('id', $data['id']);
		$this->db->delete('regulasi'); 
    }
    
    /* insert_types */
    function insert($values){
        $values["updated_by"]   = _userid();
        $values["updated_on"]   = date("Y-m-d H:i:s");
        $values["created_on"]   = date("Y-m-d H:i:s");
        $values["created_by"]   = _userid();
        
                 
		$this->db->insert("regulasi", $values); 
		$id = $this->db->insert_id();
        
        //insert regulasi file
        unset($values);
        $values['regulasi_id'] = $id;
        $this->db->insert("regulasi_file", $values); 
        
        return $id;
		
	}
	
    function get_category($regulasi_id){
        $this->db->order_by("id","asc");
        $this->db->where("regulasi_id",$regulasi_id);
        $this->db->from('regulasi_category');
		
		$query = $this->db->get();
		
 		return $query->result();
    }

    function delete_category($regulasi_id){
        //-- delete records    	
    	$this->db->where('regulasi_id', $regulasi_id);
		$this->db->delete('regulasi_category');
    }
    
    function delete_terkait($data){
        //-- delete records     
        $this->db->where('regulasi_id', $data['regulasi_id']);
        $this->db->where('regulasi_id_terkait', $data['regulasi_id_terkait']);
        $this->db->delete('regulasi_terkait');
    }

    function update_regulasi_terkait($id,$id_regulasi_terkait){
         //delete dulu
        $this->db->where('regulasi_id', $id);
		$this->db->delete('regulasi_terkait');
        
         //delete dulu
        $this->db->where('regulasi_id_terkait', $id);
		$this->db->delete('regulasi_terkait');
        
        //insert 
        if(count($id_regulasi_terkait)){
            foreach($id_regulasi_terkait as $regid){
                if($regid) {
                    $values = array(
                        "regulasi_id"   => $id
                         ,"regulasi_id_terkait"  => $regid
                    );
                    $this->db->insert("regulasi_terkait", $values); 
                    
                    $values = array(
                        "regulasi_id"   => $regid
                         ,"regulasi_id_terkait"  => $id
                    );
                    $this->db->insert("regulasi_terkait", $values); 
                    
                }
            }
        }
    }
                 
    /* Update parent ids and names, cat ids and names, in table regulasi also */
    function update_category($id,$category_id){
        $CI =& get_instance();
        $CI->load->model('category/category_model');


        //delete dulu
        $this->delete_category($id);
        
        //insert 
        if(count($category_id)){

            //glue the category
            $category_id_parents = $category_name_parents = $category_ids = $category_names = "";
            foreach($category_id as $catid){
                if($catid) {
                    //get info, include parent nya
                    $data_category = $CI->category_model->search_category(1,1,'category_id','asc',array("id"=>$catid));

                    $category_id_parents .= ";".($data_category[0]->category_id_parent)?";".$data_category[0]->category_id_parent:"";
                    $category_name_parents .= ";".($data_category[0]->category_parent_name)?";".$data_category[0]->category_parent_name:"";
                    $category_ids .= ";".($data_category[0]->category_id)?";".$data_category[0]->category_id:"";
                    $category_names .= ";".($data_category[0]->category_name)?";".$data_category[0]->category_name:"";


                    $values = array(
                        "regulasi_id"   => $id
                         ,"category_id"  => $catid
                    );
                    $this->db->insert("regulasi_category", $values); 
                }
            }

            $category_id_parents .= ";";
            $category_name_parents .= ";";
            $category_ids .= ";";
            $category_names .= ";";

            //update regulasi
            $v['category_id_parents'] = $category_id_parents;
            $v['category_name_parents'] = $category_name_parents;
            $v['category_ids'] = $category_ids;
            $v['category_names'] = $category_names;
            $this->db->where('id', $id);
            $this->db->update('regulasi', $v); 
        }
    }
    
    
	function update($data,$id){
        $data["updated_by"]   = _userid();
        $data["updated_on"]   = date("Y-m-d H:i_s");
        $this->db->where('id', $id);
		$this->db->update('regulasi', $data); 
	}
	
    function get_active_data_by_id($id){
        $data = $this->search(1,1,'judul','asc',array("id"=>$id,"active"=>"1"));
        if(count($data)){
            return $data[0];
        }else{
            return false;
        }
    }

    function get_data_by_id($id){
        $data = $this->search(1,1,'judul','asc',array("id"=>$id));
        if(count($data)){
            return $data[0];
        }else{
            return false;
        }
    }
    
    function getRegulasiByNomorDokumen($searchval){
        if(isset($searchval['nomor_dokumen']) && $searchval['nomor_dokumen']){
            $this->db->like('nomor_dokumen',$searchval['nomor_dokumen']);
        }
        
        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            $this->db->where('id <>',$searchval['exclude_id']);

            //dan exclude yg di table terkait
            $this->db->where("id not in (select regulasi_id_terkait from regulasi_terkait where regulasi_id = '".$searchval['exclude_id']."') ");

        }
        
        if(isset($searchval['active'])){
            $this->db->where(" active = '".$searchval['active']."'");
        }

        $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_fmt",false);
        $this->db->select("id,nomor_dokumen,tanggal");
		$this->db->order_by("tanggal","desc");
		$this->db->order_by("nomor_dokumen","desc");
        $this->db->from('regulasi');
		      
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    
    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('regulasi.id',$searchval['id']);
        }

        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            $this->db->where_not_in('regulasi.id',$searchval['exclude_id']);
        }

        if(isset($searchval['exclude_category_id']) && $searchval['exclude_category_id']){
            $this->db->not_like('regulasi.category_id_parents',$searchval['exclude_category_id']);
        }

        
        if(isset($searchval['active'])){
            $this->db->where('regulasi.active',$searchval['active']);
        }
        
        if(isset($searchval['nomor_dokumen']) && $searchval['nomor_dokumen']){
            $this->db->like("regulasi.nomor_dokumen",$searchval['nomor_dokumen']);    
        }

        if(isset($searchval['nomor_start']) && $searchval['nomor_start']){
            $this->db->like("regulasi.nomor_urut",$searchval['nomor_start']);    
        }

        if(isset($searchval['nomor_end']) && $searchval['nomor_end']){
            $this->db->like("regulasi.nomor_urut",$searchval['nomor_end']);    
        }

        if(isset($searchval['optiontanggal']) && $searchval['optiontanggal']){
            if($searchval['optiontanggal'] == "tanggal"){
                if(isset($searchval['date_from']) && $searchval['date_from']){
                    $this->db->where("regulasi.tanggal >=",$searchval['date_from']);    
                }
                
                if(isset($searchval['date_to']) && $searchval['date_to']){
                    $this->db->where("regulasi.tanggal <=",$searchval['date_to']);    
                }
            }else
            if($searchval['optiontanggal'] == "tahun"){
                if(isset($searchval['tahun']) && $searchval['tahun']){
                    $this->db->where("YEAR(regulasi.tanggal)",$searchval['tahun']);    
                }

            }            
          
        }


        if(isset($searchval['keyword']) && $searchval['keyword']){
            if(isset($searchval['searchMethod']) && $searchval['searchMethod']){
                switch ($searchval['searchMethod']) {
                    case 'm1': //kalimat
                        # code...
                        $this->db->like("regulasi.isi_dokumen",$searchval['keyword']);
                        break;
                    
                    case 'm2': //judul
                        # code...
                        $this->db->like("regulasi.judul",$searchval['keyword']);
                        break;

                    case 'm3': //both
                        # code...
                        $this->db->where("( regulasi.isi_dokumen like '%".$searchval['keyword']."%' or regulasi.judul like '%".$searchval['keyword']."%' ) ");
                        break;

                    default:
                        # code...
                        break;
                }

            }
       }

        if(isset($searchval['category_id']) && $searchval['category_id']){

            if(is_array($searchval['category_id'])){
                $this->db->where_in("regulasi_category.category_id",$searchval['category_id']);    
            }else{
                $this->db->where("regulasi_category.category_id",$searchval['category_id']);    
            }
        }
   
        if(isset($searchval['types_id']) && $searchval['types_id']){
            $this->db->where("regulasi.types_id",$searchval['types_id']);    
        }

      	if($this->is_count){
			$this->db->select("count(regulasi_category.id) as total");
		}else{
			$this->db->select("regulasi.*");
            $this->db->select("regulasi.id as regulasi_id");
            $this->db->select("category.name as category_name");
            $this->db->select("category.id as category_id");
            $this->db->select("category.slug as category_slug");
            $this->db->select("c.id as parent_category_id");
            $this->db->select("c.name as parent_category_name");
            $this->db->select("c.slug as parent_category_slug");
			$this->db->select("types.name as type_name");
			$this->db->select("types.id as type_id");
			$this->db->select("'' as edit");
			$this->db->select("'' as hapus");
            $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_fmt",false);
            $this->db->select("DATE_FORMAT(b.tanggal,'%d/%m/%Y') as tanggal_pengganti_fmt",false);
            $this->db->select("b.nomor_dokumen as nomor_pengganti");
            $berlaku_sql = "CASE regulasi.berlaku ";
            $berlaku_sql .= "WHEN '1' THEN '".lang('lyes')."' ";
            $berlaku_sql .= " ELSE '".lang('lno')."' END as berlaku_str";
            
            $this->db->select($berlaku_sql,false);
            
			$this->db->join("types","types.id = regulasi.types_id","left outer");
			$this->db->join("regulasi b","b.id = regulasi.id_regulasi_pengganti","left outer");
		
       }
        $this->db->join("regulasi_category","regulasi_category.regulasi_id = regulasi.id","left outer");
        $this->db->join("category","category.id = regulasi_category.category_id","left outer");
        $this->db->join("category c","c.id = category.id_parent","left outer");

		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx){
                if(is_array($sidx)){
                    foreach($sidx as $a => $b){
                        $this->db->order_by($a,$b);
                    }
                }else {
    				$this->db->order_by($sidx,$sord);
                }
            }
		}
		
        $this->db->from('regulasi');
		
		$query = $this->db->get();
		
 		return $query->result();
    }

     function searchRegulasi($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
        
        if(isset($searchval['id']) && $searchval['id']){
            $this->db->where('regulasi.id',$searchval['id']);
        }

        if(isset($searchval['exclude_id']) && $searchval['exclude_id']){
            $this->db->where_not_in('regulasi.id',$searchval['exclude_id']);
        }
        
      if(isset($searchval['exclude_category_id']) && $searchval['exclude_category_id']){
            $this->db->not_like('regulasi.category_id_parents',$searchval['exclude_category_id']);
        }

        if(isset($searchval['active'])){
            $this->db->where('regulasi.active',$searchval['active']);
        }
        
        if(isset($searchval['nomor_dokumen']) && $searchval['nomor_dokumen']){
            $this->db->like("regulasi.nomor_dokumen",$searchval['nomor_dokumen']);    
        }

        if(isset($searchval['nomor_start']) && $searchval['nomor_start']){
            $this->db->like("regulasi.nomor_urut",$searchval['nomor_start']);    
        }

        if(isset($searchval['nomor_end']) && $searchval['nomor_end']){
            $this->db->like("regulasi.nomor_urut",$searchval['nomor_end']);    
        }

        if(isset($searchval['optiontanggal']) && $searchval['optiontanggal']){
            if($searchval['optiontanggal'] == "tanggal"){
                if(isset($searchval['date_from']) && $searchval['date_from']){
                    $this->db->where("regulasi.tanggal >=",$searchval['date_from']);    
                }
                
                if(isset($searchval['date_to']) && $searchval['date_to']){
                    $this->db->where("regulasi.tanggal <=",$searchval['date_to']);    
                }
            }else
            if($searchval['optiontanggal'] == "tahun"){
                if(isset($searchval['tahun']) && $searchval['tahun']){
                    $this->db->where("YEAR(regulasi.tanggal)",$searchval['tahun']);    
                }

            }            
          
        }


        if(isset($searchval['keyword']) && $searchval['keyword']){
            if(isset($searchval['searchMethod']) && $searchval['searchMethod']){
                switch ($searchval['searchMethod']) {
                    case 'm1': //kalimat
                        # code...
                        $this->db->where("( regulasi.isi_dokumen like '%".$searchval['keyword']."%' or regulasi.isi_dokumen_en like '%".$searchval['keyword']."%' ) ");

                        break;
                    
                    case 'm2': //judul
                        # code...
                        $this->db->where("( regulasi.judul like '%".$searchval['keyword']."%' or regulasi.judul_en like '%".$searchval['keyword']."%' ) ");

                        break;

                    case 'm3': //both
                        # code...
                        $this->db->where("( regulasi.isi_dokumen like '%".$searchval['keyword']."%' or regulasi.isi_dokumen_en like '%".$searchval['keyword']."%' or regulasi.judul like '%".$searchval['keyword']."%'  or regulasi.judul_en like '%".$searchval['keyword']."%' ) ");

                        break;

                    default:
                        # code...
                        break;
                }

            }
       }

       /* cari ke field category_ids 
            format value : ;2;3;4;
       */
        if(isset($searchval['category_id']) && $searchval['category_id']){
            if(is_array($searchval['category_id'])){
                $i = 0;
                $q = "( ";
                foreach($searchval['category_id'] as $c){
                    if($i == 0){
                        $q .= " regulasi.category_ids like '%;".$c.";%'";
                    }else{
                        $q .= " OR regulasi.category_ids like '%;".$c.";%'";
                    }
                    $i++;
                }
                $q .= ") ";
                $this->db->where($q);
            }else{
                $this->db->like("regulasi.category_ids",";".$searchval['category_id'].";");    
            }
        }

   
        if(isset($searchval['types_id']) && $searchval['types_id']){
            $this->db->where("regulasi.types_id",$searchval['types_id']);    
        }

        if($this->is_count){
            $this->db->select("count(regulasi.id) as total");
        }else{
            $this->db->select("regulasi.*");
            $this->db->select("regulasi.id as regulasi_id");
            
            $this->db->select("types.name as type_name");
            $this->db->select("types.id as type_id");
            $this->db->select("'' as edit");
            $this->db->select("'' as hapus");
            
            $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_fmt",false);
            $this->db->select("DATE_FORMAT(b.tanggal,'%d/%m/%Y') as tanggal_pengganti_fmt",false);
            $this->db->select("b.nomor_dokumen as nomor_pengganti");
            $berlaku_sql = "CASE regulasi.berlaku ";
            $berlaku_sql .= "WHEN '1' THEN '".lang('lyes')."' ";
            $berlaku_sql .= " ELSE '".lang('lno')."' END as berlaku_str";
            
            $this->db->select($berlaku_sql,false);
            
            $this->db->join("types","types.id = regulasi.types_id","left outer");
            $this->db->join("regulasi b","b.id = regulasi.id_regulasi_pengganti","left outer");
        
        }
      

        
        if(!$this->is_count){
            if($limit&&$page) {
                $offset = ($page-1)*$limit;
                if(!$offset)
                    $offset = 0;
                $this->db->limit($limit,$offset);
            }
                
            if($sidx){
                if(is_array($sidx)){
                    foreach($sidx as $a => $b){
                        $this->db->order_by($a,$b);
                    }
                }else {
                    $this->db->order_by($sidx,$sord);
                }
            }
        }
        
        $this->db->from('regulasi');
        
        $query = $this->db->get();
        
        return $query->result();
    }

    function get_newest_data(){
        $this->db->select("DATE_FORMAT(updated_on,'%d/%m/%Y') as updated_on_fmt",false);
        $this->db->order_by("updated_on","desc");
        $this->db->limit(1);
        $this->db->from('regulasi');
        
        $query = $this->db->get();
        
        return $query->result();

    }
  
    //get blob 
    function get_file_regulasi($id){ 
        $this->db->select('nama_file_regulasi');
        $this->db->select('type_file_regulasi');
        $this->db->select('ukuran_file_regulasi');
        $this->db->select('regulasi_file_utama'); //file blob
       // $this->db->select('nama_file_regulasi_en');
       // $this->db->select('type_file_regulasi_en');
       // $this->db->select('ukuran_file_regulasi_en');
       // $this->db->select('regulasi_file_utama_en'); //file blob
        $this->db->where('regulasi_file.regulasi_id', $id);
        $this->db->join('regulasi','regulasi.id = regulasi_file.regulasi_id');
        $this->db->from('regulasi_file');
		$query = $this->db->get();
        return $query->result();
    }
    
        //get blob 
    function get_file_lampiran($id){ 
        $this->db->select('nama_file_lampiran');
        $this->db->select('type_file_lampiran');
        $this->db->select('ukuran_file_lampiran');
        $this->db->select('regulasi_file_lampiran'); //file blob
      //  $this->db->select('nama_file_lampiran_en');
      //  $this->db->select('type_file_lampiran_en');
      //  $this->db->select('ukuran_file_lampiran_en');
      //  $this->db->select('regulasi_file_lampiran_en'); //file blob
        $this->db->where('regulasi_file.regulasi_id', $id);
        $this->db->join('regulasi','regulasi.id = regulasi_file.regulasi_id');
        $this->db->from('regulasi_file');
		$query = $this->db->get();
        return $query->result();
    }

      //get blob 
    function get_file_regulasi_en($id){ 
        $this->db->select('nama_file_regulasi_en');
        $this->db->select('type_file_regulasi_en');
        $this->db->select('ukuran_file_regulasi_en');
        $this->db->select('regulasi_file_utama_en'); //file blob
        $this->db->where('regulasi_file_en.regulasi_id', $id);
        $this->db->join('regulasi','regulasi.id = regulasi_file_en.regulasi_id');
        $this->db->from('regulasi_file_en');
        $query = $this->db->get();
        return $query->result();
    }
    
        //get blob 
    function get_file_lampiran_en($id){ 
        $this->db->select('nama_file_lampiran_en');
        $this->db->select('type_file_lampiran_en');
        $this->db->select('ukuran_file_lampiran_en');
        $this->db->select('regulasi_file_lampiran_en'); //file blob
        $this->db->where('regulasi_file_en.regulasi_id', $id);
        $this->db->join('regulasi','regulasi.id = regulasi_file_en.regulasi_id');
        $this->db->from('regulasi_file_en');
        $query = $this->db->get();
        return $query->result();
    }
    
    //to get data from content to pdf
    function get_data_pdf($id){
        $this->db->select("isi_dokumen");
        $this->db->select("nama_file_regulasi");    
        $this->db->where('regulasi.id',$id);
      
        $this->db->from('regulasi');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function nomor_exists($nomor){
        $this->db->select("regulasi.id as regulasi_id");    
        $this->db->where('nomor_dokumen',$nomor);
        $this->db->from('regulasi');
		$query = $this->db->get();
        $data = $query->result();
        
        if(count($data)){
            return $data[0];
        }else {
            return false;
        }
    }
    
    function get_data_by_category_id($category_id){
        $this->db->select("regulasi.id as regulasi_id");    
        $this->db->select("regulasi.nomor_dokumen as nomor_dokumen");    
        $this->db->where('category_id',$category_id);
        $this->db->from('regulasi_category');
        $this->db->join('regulasi','regulasi.id = regulasi_category.regulasi_id');

        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    function row_file_en_exists($id){
        $this->db->select("id");    
        $this->db->where('regulasi_id',$id);
        $this->db->from('regulasi_file_en');
        $query = $this->db->get();
        $data = $query->result();
        
        if(count($data)){
            return true;
        }else {
            return false;
        }
    }
        function row_file_exists($id){
        $this->db->select("id");    
        $this->db->where('regulasi_id',$id);
        $this->db->from('regulasi_file');
		$query = $this->db->get();
        $data = $query->result();
        
        if(count($data)){
            return true;
        }else {
            return false;
        }
    }
    
    function update_file($id,$data_upload,$data_upload_lampiran,$unlink=true){
        //if not exist, insert 
        $row_file_exists = $this->row_file_exists($id);
        
        //relasi 1-1 table regulasi
        $data = array(
               'regulasi_id'            => $id,
        );
        if (isset($data_upload['full_path']) && $data_upload['full_path']) {
            $data['regulasi_file_utama'] = file_get_contents($data_upload['full_path']);
        }
        
        if (isset($data_upload_lampiran['full_path']) && $data_upload_lampiran['full_path']) {
            $data['regulasi_file_lampiran'] = file_get_contents($data_upload_lampiran['full_path']);
        }

        // if data not exist, then insert
        // else , update
        if(!($row_file_exists)){
              $this->db->insert("regulasi_file", $data); 
        }else {
            $this->db->where('regulasi_id', $id);
            $this->db->update('regulasi_file', $data); 
        }
        
        //unlink the file
        if($unlink) {
            @unlink($data_upload['full_path']);
            @unlink($data_upload_lampiran['full_path']);
        }
    }

    function update_file_en($id,$data_upload,$data_upload_lampiran,$unlink=true){
        //if not exist, insert 
        $row_file_exists = $this->row_file_en_exists($id);
        
        //relasi 1-1 table regulasi
        $data = array(
               'regulasi_id'            => $id,
        );
       // print_r($data_upload);
       // print_r($data_upload_lampiran);
        if (isset($data_upload['full_path']) && $data_upload['full_path']) {
            $data['regulasi_file_utama_en'] = file_get_contents($data_upload['full_path']);
        }
        
        if (isset($data_upload_lampiran['full_path']) && $data_upload_lampiran['full_path']) {
            $data['regulasi_file_lampiran_en'] = file_get_contents($data_upload_lampiran['full_path']);
        }

        // if data not exist, then insert
        // else , update
        if(!($row_file_exists)){
              $this->db->insert("regulasi_file_en", $data); 
        }else {
            $this->db->where('regulasi_id', $id);
            $this->db->update('regulasi_file_en', $data); 
        }
       // echo $this->db->last_query();

        //unlink the file
        if($unlink) {
            @unlink($data_upload['full_path']);
            @unlink($data_upload_lampiran['full_path']);
        }
   }

    function get_regulasi_terkait($id){
        $this->db->select("regulasi.*");
        $this->db->select("DATE_FORMAT(regulasi.tanggal,'%d/%m/%Y') as tanggal_terkait_fmt",false);
        $this->db->select("nomor_dokumen as nomor_terkait");
        $this->db->select("judul as judul_terkait");
        $this->db->select("judul_en as judul_terkait_en");
        $this->db->select("berlaku as berlaku_terkait");
        $this->db->select("types.name as types_name");
        $this->db->select("regulasi_terkait.id as id_terkait, regulasi_id, regulasi_id_terkait");
        
        $this->db->where('regulasi_id',$id);
      
        $this->db->from('regulasi_terkait');
        $this->db->join('regulasi','regulasi.id = regulasi_terkait.regulasi_id_terkait');
        $this->db->join('types','types.id = regulasi.types_id');
		$this->db->order_by("regulasi.tanggal","desc");
		$query = $this->db->get();
		
 		return $query->result();
    }
        
    function terkait_exists($regulasi_id,$id_regulasi_terkait){
        $this->db->where('regulasi_id',$regulasi_id);
        $this->db->where('regulasi_id_terkait',$id_regulasi_terkait);
        $this->db->from('regulasi_terkait');
        $num_results = $this->db->count_all_results();
        if($num_results){
            return true;
        }else{
            return false;
        }
    }

    function regulasi_terkait_exists($regulasi_id){
        $this->db->where('regulasi_id',$regulasi_id);
         $this->db->from('regulasi_terkait');
        $num_results = $this->db->count_all_results();
        if($num_results){
            return true;
        }else{
            return false;
        }
    }


    function is_used($id){
        return false;
        
        //check in table files
        /*$this->db->select("count(id) as total");
        $this->db->where("id",$id);
		$query = $this->db->get('regulasi');
		$result = $query->result();
        
        if($result[0]->total > 0){
            return true;
        }
        
        return false; */
    }

      function insert_terkait($values){
        $this->db->insert("regulasi_terkait", $values); 
        return $this->db->insert_id();
    }
    

    function update_terkait($data,$id){
        $this->db->where('id', $id);
        $this->db->update('regulasi_terkait', $data); 
    }
    

    
    
    /** 
    * Converts bytes into human readable file size. 
    * 
    * @param string $bytes 
    * @return string human readable file size (2,87 Мб)
    * @author Mogilev Arseny 
    */ 
    function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
                ),
            );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }

}