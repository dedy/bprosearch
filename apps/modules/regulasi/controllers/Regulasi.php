<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Regulasi extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('category/category_model');
        $this->load->model('regulasi/regulasi_model');
        $this->load->model('type/type_model');
        $this->load->model('user/user_model');

        //load library
        $this->load->library(array(
            'pagination',
            
        ));
        
        $this->load->helper('text');
    }
        

    function test(){
        $data = "Pajak;Pajak;;;";
        $arrayData = explode(";",$data);
        print_r($arrayData);
    }
    
    /*
        2017 03 16
        Patch untuk update field category id, name dan parent di table regulasi
        Untuk kepentingan display front end
        fields : separated by semicolon
            category_id_parents
            category_name_parents
            category_ids
            category_names
    */
    function patch(){
        if(!$this->input->is_cli_request()){
            echo "Not CLI ";
            exit;
        }

        $this->db->select("regulasi.id as regulasi_id");
        $this->db->from('regulasi');
        $this->db->order_by('regulasi_id','asc');
        $query = $this->db->get();
        $datas = $query->result();

        $i=0;
        foreach($datas as $data){
            //get categories
            $this->db->select("a.id as category_id");
            $this->db->select("a.name as category_name");
            $this->db->select("b.id as parent_category_id");
            $this->db->select("b.name as parent_category_name");
            $this->db->from('regulasi_category r');
            $this->db->join('category a','a.id = r.category_id');
            $this->db->join('category b','b.id = a.id_parent','left outer');
            $this->db->where("regulasi_id",$data->regulasi_id);
            $this->db->order_by("r.id","asc");
            $query2 = $this->db->get();
            $datas2 = $query2->result();
            if(count($datas2)){
                //glue the category
                $category_id_parents = $category_name_parents = $category_ids = $category_names = "";

                foreach($datas2 as $data2){
                    $category_id_parents .= ";".($data2->parent_category_id)?";".$data2->parent_category_id:"";
                    $category_name_parents .= ";".($data2->parent_category_name)?";".$data2->parent_category_name:"";
                     $category_ids .= ";".($data2->category_id)?";".$data2->category_id:"";
                    $category_names .= ";".($data2->category_name)?";".$data2->category_name:"";
                }
                $category_id_parents .= ";";
                $category_name_parents .= ";";
                $category_ids .= ";";
                $category_names .= ";";

                //update the field
                $v['category_id_parents'] = $category_id_parents;
                $v['category_name_parents'] = $category_name_parents;
                $v['category_ids'] = $category_ids;
                $v['category_names'] = $category_names;
                $v['updated_on'] = date("Y-m-d H:i:s");
                $id = $data->regulasi_id;
                
                $this->db->where('id', $id);
                $this->db->update('regulasi', $v); 
            }
            echo $i.":".$id." || ".$category_id_parents." - ".$category_name_parents." || ".$category_ids." - ".$category_names.PHP_EOL;
            $i++;
        }
        echo "Selesai";        
    }

    function search($slug='',$id=''){
        if($slug && !$id){
            show_404();
        }

        $this->load->helper('form');
        $s = $this->input->get('s');
        $q = $this->input->get('q');


        //paging
        $per_page = $this->input->get("page");
        if(!$per_page){
            $per_page = 1;
        }

        //access ke halaman 2 ke atas
        if($per_page > 1 ) {
            //if member is not logged in , can not see page 2
            if(!isMember() ){
                $this->session->set_flashdata('not_loggedin', true);
                redirect('please-login');
            }else
            if(isMember()){
                //if member login 
                //check member package membership type (free / silver / gold / platinum) , and membership date end
                if(!$this->user_model->MembershipIsActive()){
                    $this->session->set_flashdata('membership_expired', true);
                    redirect('membership-expired','refresh');
                }

                if($s == "c"){ //dari halaman category , check akses kategory nya
                    //dari salah satu category
                    //if user not has access , ga bisa view halaman > 1  
                    if(!$this->user_model->canAccess($slug,$id)){
                        $this->session->set_flashdata('not_auth', true);
                        redirect('not-authorized','refresh');
                    }
                }
            }
        }

        $limit = config_item('limit_perpage');
        $offset = ($per_page  == 1 || $per_page == null ) ? 0 : ($per_page * $limit) - $limit;
        
        $sidx = 'tanggal';
        $sord = 'desc' ;

    	/*
    	$s = source , c(ategory) or g(lobal)
    	*/

        switch ($s) {
            //from home, category search
            case 'c':
                # code...
                if(!$slug){
                    show_404();
                }else{
                    //search from category front end
                    $data = $this->category_model->get_data_by_category_id($id);
                    if(!$data){
                        show_404();
                    }
                    $this->viewparams['data']   = $data[0];

                    //check if it has subcat
                    $subcats = $this->category_model->getChildById($id);
                    $display_category   = false;
                    if(count($subcats)){
                        $display_category   = true;
                    }
                    $this->viewparams['display_category']   = $display_category;
                    $this->viewparams['subcats']   = $subcats;


                    //get jenis document (type)
                    //base category
                    $types = $this->type_model->get_data_by_category($data[0]->category_id,'1');
                    $this->viewparams['types']   = $types;

                    //default is tahun checked
                    $optiontanggal = $this->input->get('optiontanggal');
                    $optiontahun_checked = $optiontanggal_checked = "";
                    $date_to_disabled = $date_from_disabled = $tahun_disabled = "";

                    if($optiontanggal == "tanggal"){
                        $optiontanggal_checked = "checked";

                        //disable tahun
                        $tahun_disabled = "disabled";
                    }else
                    if($optiontanggal == "tahun"){
                        $optiontahun_checked = "checked";
                        
                        //disable tanggal
                        $date_to_disabled = $date_from_disabled = "disabled";

                    }else{
                        $optiontahun_checked = "checked";
                        //disable tanggal
                        $date_to_disabled = $date_from_disabled = "disabled";
                    }

                    $tahun = $this->input->get('tahun');

                    $this->viewparams['optiontahun_checked'] = $optiontahun_checked;
                    $this->viewparams['optiontanggal_checked'] = $optiontanggal_checked;
                    $this->viewparams['date_to_disabled'] = $date_to_disabled;
                    $this->viewparams['date_from_disabled'] = $date_from_disabled;
                    $this->viewparams['tahun_disabled'] = $tahun_disabled;
                    $this->viewparams['tahun'] = $tahun;

                    //default method checked is m1 
                    $m1_checked = $m2_checked = $m3_checked = "";
                    $searchMethod = $this->input->get('searchMethod');
                    switch ($searchMethod) {
                        case 'm1': //by kalimat
                            # code...
                            $m1_checked = "checked";
                            break;
                        
                        case 'm2': //by judul
                            # code...
                            $m2_checked = "checked";

                            break;

                        case 'm3': //by judul dan kalimat
                            # code...
                            $m3_checked = "checked";
                            break;

                        default:
                            # code...
                            $m1_checked = "checked";
                            break;
                    }
                    $this->viewparams['m1_checked'] = $m1_checked;
                    $this->viewparams['m2_checked'] = $m2_checked;
                    $this->viewparams['m3_checked'] = $m3_checked;

                    //get param
                    $scat = $this->input->get('scat');
                    $type = $this->input->get('type');
                    $date_from = $this->input->get('date_from');
                    $date_to = $this->input->get('date_to');

                    //validate date format
                    if($date_from){
                        list($y,$m,$d) = explode("-", $date_from);
                        if(!checkdate($m,$d,$y)){
                            $date_from = "";
                        }
                    }

                    if($date_to){
                        list($y,$m,$d) = explode("-", $date_to);
                        if(!checkdate($m,$d,$y)){
                            $date_to = "";
                        }

                    }

                    //validate nomor 
                    $nomor_start = $this->input->get('nomor_start');
                    $nomor_end = $this->input->get('nomor_end');

                    if($nomor_start){
                        if(!is_numeric($nomor_start)){
                            $nomor_start = "";
                        }
                    }

                    if($nomor_end){
                        if(!is_numeric($nomor_end)){
                            $nomor_end = "";
                        }
                    }

                    $this->viewparams['scat'] = $scat;
                    $this->viewparams['stype'] = $type;
                    $this->viewparams['q'] = $q;
                    $this->viewparams['date_from'] = $date_from;
                    $this->viewparams['date_to'] = $date_to;
                    $this->viewparams['nomor_end'] = $nomor_end;
                    $this->viewparams['nomor_start'] = $nomor_start;

                    /*
                    If no sub category selected, check if it has sub cat 
                    */
                    if($data[0]->category_id && !$scat){
                        $id_categories  = array();
                        $subcats = $this->category_model->get_childs($data[0]->category_id,'','',array("active"=>"1"));

                        if(count($subcats)){
                            foreach($subcats as $subcat){
                                $id_categories[]  = $subcat->category_id;
                            }
                        }else{
                            $id_categories[]  = $data[0]->category_id;

                        }
                    }else
                    if($scat) {
                        $id_categories = $scat;
                    }

                    //get regulasi data
                    $searchval = array(
                        "category_id"       => $id_categories
                        ,"sub_category_id"  => $scat
                        ,"types_id"          => $type
                        ,"keyword"          => $q
                        ,"searchMethod"     => $searchMethod
                        ,"optiontanggal"    => $optiontanggal
                        ,"date_from"        => $date_from
                        ,"date_to"          => $date_to
                        ,"tahun"            => $tahun
                        ,"nomor_start"      => $nomor_start
                        ,"nomor_end"        => $nomor_end
                        ,"active"           => "1"
                    );

                    //pagination
                    $base_url = site_url('regulasi/search')."/".$slug."/".$id;

                    $config['base_url'] = $base_url.'/?'.http_build_query(array(
                            's'         => $s
                            ,'scat'     => $scat
                            ,'type'     => $type
                            ,'q'        => $q //keyword
                            ,'searchMethod'    => $searchMethod
                            ,'optiontanggal'    => $optiontanggal
                            ,"date_from"        => $date_from
                            ,"date_to"          => $date_to
                            ,"tahun"            => $tahun
                            ,"nomor_start"      => $nomor_start
                            ,"nomor_end"        => $nomor_end
                        ),'',"&amp;");


               }
               break;
            
            //from home, global search
            case 'g':
                $sortBy = $this->input->get('sortBy');

                if(!$sortBy){
                    //default index order adalah tanggal , desc
                    $sidx = "tanggal";
                }else{
                    //if sidx exists , and not in array, default adalah tanggal
                    if($sortBy <> "tanggal" && $sortBy <> "category"){
                        $sidx = "tanggal";
                    }else{
                        if($sortBy ==  "category"){
                            $sidx = $sortBy;
                            $sord = 'asc' ;
                        }
                    }
                }

                $searchMethod = "m3";
                # code...
                    $this->viewparams['q']   = $q;
                    $searchval = array(
                        "keyword"          => $q
                        ,"searchMethod"    => $searchMethod
                        ,"active"           => "1"

                    );
                    $base_url = site_url('regulasi/search');

                    $config['base_url'] = $base_url.'/?'.http_build_query(array(
                            's'         => $s
                            //,'searchMethod'    => $searchMethod
                            ,'q'        => $q //keyword
                            ,'sortBy'   => $sidx
                        ),'',"&amp;");

                break;

            default:
                show_404();

                # code...
                break;
        }
        
       //kalau ga ada keyword (dari home, popular/latest), exclude kurs
        if(!strlen($q)){
            $searchval['exclude_category_id'] = 39;
        }

        $datas = $this->regulasi_model->searchRegulasi($limit,$per_page,$sidx,$sord,$searchval);
        //echo $this->db->last_query();
        $total_data = $this->regulasi_model->countSearchRegulasi($searchval);
        //echo "<BR><BR>";
        //echo $this->db->last_query();

        $this->viewparams['datas']   = $datas;
        $this->viewparams['offset']   = $offset;
        $this->viewparams['total_data']   = $total_data;
        $config['first_link'] = '&laquo;';
        $config['last_link'] = '&raquo;';
        $config['total_rows'] =  $total_data;
        $config['per_page'] = $this->config->item("limit_perpage");
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 1    ;

        $this->pagination->initialize($config);

        $this->viewparams['s']   = $s;
        $this->viewparams['slug']   = $slug;
        $this->viewparams['id']   = $id;

        //get last modified record from db
        $newest_data = $this->regulasi_model->get_newest_data();
        
        $this->viewparams['last_updated']   = $newest_data[0]->updated_on_fmt;

        parent::viewpage("regulasi/search");

    }

    function detail($id='',$lang='id'){
        if(!$id){
            show_404();
        }

        $lang = $this->input->get('lang');
        $offset = $this->input->get('offset');

        $lang = ($lang == "en")?"en":"id";
        $offset = ($offset)?$offset:1;

        $data = $this->regulasi_model->get_active_data_by_id($id);

        //kalau ga ada data, display page not found
        if(!$data){
            show_404();
        }

        $this->viewparams['data']   = $data;
        $this->viewparams['lang']   = $lang;
        $this->viewparams['offset']   = $offset;


        //get history data
        $this->regulasi_model->get_regulasi_pengganti($id);
        
        //reverse order
        if(count($this->regulasi_model->datas)){
            $tmpdata = $this->regulasi_model->datas;
            $this->regulasi_model->datas = array();
            for($i = (count($tmpdata)-1);$i>=0;$i--){
                $this->regulasi_model->datas[]  = $tmpdata[$i];
            }
        }

        $this->regulasi_model->get_regulasi_history($id);
        $this->viewparams['regulasi_histories']   = $this->regulasi_model->datas;                

        //get related regulasi
        $related_datas = $this->regulasi_model->get_regulasi_terkait($id);
        $this->viewparams['related_datas']   = $related_datas;

        //insert into view history
        $this->regulasi_model->insert_view_history($id,'detail');

       // print_r($this->regulasi_model->datas);
        parent::viewiframe("regulasi/detail");


    }

   
    function rterkait($id=''){
       if(!$id){
            show_404();
        }
        $data = $this->regulasi_model->get_active_data_by_id($id);

        if(!$data){
            show_404();
        }

        //get related data :
        $related_datas = $this->regulasi_model->get_regulasi_terkait($id);
        $this->viewparams['related_datas']   = $related_datas;

       // print_r($this->regulasi_model->datas);
        parent::viewiframe("regulasi/terkait");

    }


    function history($id=''){
        if(!$id){
            show_404();
        }

        $data = $this->regulasi_model->get_active_data_by_id($id);

        if(!$data){
            show_404();
        }

        //get history : 
        $this->regulasi_model->get_regulasi_history($id);
        $this->viewparams['regulasi_histories']   = $this->regulasi_model->datas;

       // print_r($this->regulasi_model->datas);
        parent::viewiframe("regulasi/history");

    }

    public function getfile($type='pdf',$id){
         //insert into view history
        $this->regulasi_model->insert_view_history($id,'lampiran');
        switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran;
                $filetype = $data[0]->type_file_lampiran;
                $filename = $data[0]->nama_file_lampiran;
                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama; 
                $filetype = $data[0]->type_file_regulasi;
                $filename = $data[0]->nama_file_regulasi;
            break;
        
        }
        if(count($data)){

            header("Content-type: ".$filetype);
            header('Content-disposition: inline; filename="'.$filename.'"');
            //header('Content-length: ' . ($data[0]->ukuran_file_regulasi*1024));
            echo $content;
        }
    }

    public function download($type='pdf',$id){
         //insert into view history
        $this->regulasi_model->insert_view_history($id,'download');
     
        switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran;
                $filetype = $data[0]->type_file_lampiran;
                $filename = $data[0]->nama_file_lampiran;
                $filesize = $data[0]->ukuran_file_lampiran;

                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama; 
                $filetype = $data[0]->type_file_regulasi;
                $filename = $data[0]->nama_file_regulasi;
                $filesize = $data[0]->ukuran_file_regulasi;
            break;
        
        }
        if(count($data)){
            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: ".$filetype);
            header("Content-Transfer-Encoding: Binary");
            //header("Content-Length:".($data[0]->ukuran_file_regulasi*1024));
            header("Content-Length:".($filesize*1024));
            header('Content-disposition: attachment; filename="'.$filename.'"');
            echo $content;
        }
    }

    public function getfileen($type='pdf',$id){
        //insert into view history
        $this->regulasi_model->insert_view_history($id,'lampiran_en');
         switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran_en($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran_en;
                $filetype = $data[0]->type_file_lampiran_en;
                $filename = $data[0]->nama_file_lampiran_en;
                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi_en($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama_en; 
                $filetype = $data[0]->type_file_regulasi_en;
                $filename = $data[0]->nama_file_regulasi_en;
            break;
        
        }
        if(count($data)){

            header("Content-type: ".$filetype);
            header('Content-disposition: inline; filename="'.$filename.'"');
            //header('Content-length: ' . ($data[0]->ukuran_file_regulasi*1024));
            echo $content;
        }
    }

    public function downloaden($type='pdf',$id){
         //insert into view history
        $this->regulasi_model->insert_view_history($id,'download_en');
     
        switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran_en($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran_en;
                $filetype = $data[0]->type_file_lampiran_en;
                $filename = $data[0]->nama_file_lampiran_en;
                $filesize = $data[0]->ukuran_file_lampiran_en;
                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi_en($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama_en; 
                $filetype = $data[0]->type_file_regulasi_en;
                $filename = $data[0]->nama_file_regulasi_en;
                $filesize = $data[0]->ukuran_file_regulasi_en;

            break;
        
        }
        if(count($data)){
            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: ".$filetype);
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:".($filesize*1024));
            header('Content-disposition: attachment; filename="'.$filename.'"');
            echo $content;
        }
    }
}