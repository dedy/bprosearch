<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Terkait extends Admin_Controller {
    var $is_export = false;

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
	   	parent::__construct();
        
        //user module only for admin
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
		}   
		
		$this->load->model('regulasi/regulasi_model');
	}
	
	function get_data($id){
        $data_regulasi_terkait = $this->regulasi_model->get_regulasi_terkait($id);
        $total_data_terkait = count($data_regulasi_terkait);
        $this->viewparams['data_regulasi_terkait']    = $data_regulasi_terkait;
        $this->viewparams['total_data_terkait']  = $total_data_terkait;
        parent::viewajax('vregulasi_terkait_list');
    }

       
	//update_banner
	function doupdate(){
		if(!isLoggedIn() && !is_ajax()){
        	redirect('auth/logout');
            exit;
        }
        
        /* check mandatory fields */
		$id		= $this->input->post('id');
		$id_regulasi_terkait		= $this->input->post('id_regulasi_terkait');
		$regulasi_id		= $this->input->post('regulasi_id');
		
		$message = $similar = array();
		$redirect = "";
		$is_error = false;
		
		
		//-- run form validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id_regulasi_terkait', 'lang:lregulasi', 'required');
		    
	//	$this->form_validation->set_rules('farm_email', 'lang:lemail', 'valid_email');
		
		$this->form_validation->set_error_delimiters('', '<br/>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
			$message[] = validation_errors();
		}
		
        $id_select=0;
			
        /* add/update if no error */
        if(!$is_error){
        	//check if already exists in regulasi terkait
        	$is_exists = $this->regulasi_model->terkait_exists($regulasi_id,$id_regulasi_terkait);

        	if($is_exists){
        		$is_error = true;
        		$message[] = lang('lregulasi_terkait_exists');
        	}

        	if(!$is_error) {
	            $values = array(
	                "regulasi_id"           => $regulasi_id
	                ,"regulasi_id_terkait"        => $id_regulasi_terkait
	                ,"updated_on"       => date("Y-m-d H:i:s")
	                ,"updated_by"       => _userid()

	            );

	            //add
	            if(!$id){
	                //insert terkait 1
	                $id = $this->regulasi_model->insert_terkait($values);

	                //insert terkait 2
	                $values["regulasi_id"] = $id_regulasi_terkait;
	                $values["regulasi_id_terkait"] = $regulasi_id;

	                $id = $this->regulasi_model->insert_terkait($values);

	                $this->firephp->log($this->db->last_query());

	                //$this->commodity_model->insert_farm_commodity($id,$farm_commodity);

	                if($id)	{
	                    $message[] = lang('ldata_success_inserted');
	                    $id_select = $id;
	                }
	                else
	                    $message[] = lang('ldata_failed_inserted');

	            }else{
	                $this->regulasi_model->update_terkait($values,$id);
	                $this->firephp->log($this->db->last_query());

	                $message[] = lang('ldata_success_updated');
	            }
	        }

            //$redirect = "";
        }
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"error"	=> $is_error
		);
                    
		echo json_encode($result);
	}

	function delete($regulasi_id,$regulasi_id_terkait){
		$success = true;
		$error = false;
        $data['regulasi_id'] 	= $regulasi_id;
        $data['regulasi_id_terkait'] 	= $regulasi_id_terkait;
		
        //delete farm
        $this->regulasi_model->delete_terkait($data);
      	$this->firephp->log($this->db->last_query());
        
        $data['regulasi_id'] 	= $regulasi_id_terkait;
        $data['regulasi_id_terkait'] 	= $regulasi_id;
		$this->regulasi_model->delete_terkait($data);
      	$this->firephp->log($this->db->last_query());

        //error handler
        if($this->db->affected_rows() < 1){  
            $success = false;
			$error = true;
	        $message = lang('ldata_is_failed_deleted');
		}else{
    		$message = lang('ldata_is_deleted');
		}
		
		$result = array("error"=>$error,"success"=>$success,"message" => $message);
  		echo json_encode($result);


	}
	
	function edit($id){
		 
        /* can not access this page without valid id*/
		$data = $this->farm_model->get_data_market($id);
		
		if(!$data){
			redirect();
		}
		
        echo json_encode($data);
        
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
