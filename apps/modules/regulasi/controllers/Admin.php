<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/dompdf/dompdf/autoload.inc.php';

class Admin extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    //use Dompdf\Dompdf;

	public function __construct()
	{
		parent::__construct();
        
        //user module only for admin
        if(!isAdmin() && !isStaff()){
        	redirect('auth/logout');
        }   
		
		$this->load->model('type/type_model');
		$this->load->model('regulasi/regulasi_model');
		$this->load->model('category/category_model');
   }
	
	function index(){
		$this->lists();
	}
	
    function lists(){
    	$this->viewparams['title_page']	= lang('llist_regulasi');
		$this->viewparams['ladd_button_title']	= lang('ladd_regulasi');
	   
         //$this->category_model->get_parents(0,array(),"display_order,category_name","asc");
        $data_categories = $this->category_model->search_category(0,0,'display_order,category_name','asc',array('id_parent'=>0));
        //echo $this->db->last_query(0);
        $this->viewparams['data_categories']    = $data_categories;
   
		parent::viewpage("vregulasi_list");
	}
    
    function setActive($value,$id){
        if(!$this->input->is_ajax_request())
            redirect('auth/logout');

		$data['active']	= $value;
		
		$this->type_model->setActive($id,$data);
		
        $data = $this->type_model->search(1,1,'','',array('id'=>$id));
        
		$message = ($value == 1)?lang('ldata_isset_active'):lang('ldata_isset_inactive');
		
		$result = array("message" => $message,"name"=>$data[0]->name);
  		echo json_encode($result);



	}

	function delete($id){
        $data['id'] 	= $id;
		$data['active'] = 0;
        $is_error = false;
           
        if(!$this->regulasi_model->is_used($id)) {
            $this->regulasi_model->delete($data);
            if($this->db->affected_rows()){
                $message = lang('ldata_is_deleted');
                $is_error = false;
            }
            else {
                 $is_error = true;
                $message = lang('ldata_is_failed_deleted');
            }
        }else{
            $is_error = true;
            $message = lang('ldata_is_used');
        }
		
		
		$result = array("is_error"=>$is_error,"message" => $message);
  		echo json_encode($result);
	}
	
    function edit($id){
      /*  echo time();
        echo date("Y-m-d",time());
        */
       /* can not access this page without valid id*/
		$data = $this->regulasi_model->get_data_by_id($id);
        
		if(!$data){
			redirect('admin/type');
		}
        
        //get data category
        $data_regulasi_category = $this->regulasi_model->get_category($id);
      //  $data_regulasi_terkait = $this->regulasi_model->get_regulasi_terkait($id);
        
		$this->viewparams['title_page']	= lang('lmodify_type');
		$this->viewparams['data']	= $data;
		$this->viewparams['data_regulasi_category']	= $data_regulasi_category;
        //print_r($data_regulasi_category);

		//$this->viewparams['data_terkait']	= $data_regulasi_terkait;
        /* user group list */
       // $user_groups = $this->user_model->getUserGroup();
		//$this->viewparams['user_groups'] = $user_groups;
            
        $this->form($id);
	}
    
    function form($id=0){
         $this->load->library(array('form_validation'));
         
        //get category list
        $this->load->model('category/category_model');
        
        //$this->category_model->get_parents(0,array(),"display_order,category_name","asc");
		$data_categories = $this->category_model->search_category(0,0,'display_order,category_name','asc',array('id_parent'=>0));
		//echo $this->db->last_query(0);
        $this->viewparams['data_categories']	= $data_categories;
        $this->viewparams['id_regulasi']	= isset($this->viewparams['data'])?$this->viewparams['data']->regulasi_id:0 ;
        parent::viewpage("vregulasi_form");
    }
	
	function add(){
      	$this->viewparams['title_page']	= lang('ladd_regulasi');
	
        $this->form(0);
	}
	
    function loadDataGrid(){
		
		$page = isset($_POST['page'])?$_POST['page']:1;
      	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'tanggal'; // get index row - i.e. user click to sort
      	$sord = isset($_POST['sord'])?$_POST['sord']:'desc'; // get the direction
   		$limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid

        $id_category = $this->input->get('category');

        $id_categories  = array();

        if($id_category) {
            $subcats = $this->category_model->get_childs($id_category);

            if(count($subcats)){
                foreach($subcats as $subcat){
                    $id_categories[]  = $subcat->category_id;
                }
            }else{
                $id_categories[]  = $id_category;

            }
        }


        $searchv = array( 
            "nomor_dokumen"    => $this->input->get('nomor_dokumen')
            ,"category_id"    => $id_categories
            ,"types_id"    => $this->input->get('types')
        );

         switch($sidx){
            case "tanggal" : 
                $sidx = array(
                    "tanggal"=>"desc"
                );
            break;
            case "category_name" :
                $sidx = array(
                    "category_name" => $sord
                    ,"type_name"    => "asc"
                    ,"tanggal"      => "desc"
                );
            break;
            case "type_name" :
                $sidx = array(
                    "category_name"    => "asc"
                    ,"type_name"         => $sord
                    ,"tanggal"          => "desc"
            );
                break;
        }
      	$query = $this->regulasi_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		$count = $this->regulasi_model->countSearch($searchv);

		$this->DataJqGrid = array(
        	"page"		=> $page,
        	"sidx"		=> $sidx,
        	"sord"		=> $sord,
        	"query" 	=> $query,
        	"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> array("id","active","nomor_dokumen","tanggal_fmt","category_name","type_name","nomor_dokumen","judul","berlaku_str","edit",'hapus'),
			"id"		=> "id"
 		);
         
        parent::loadDataJqGrid();
		
	}
    
    /* 
     * Get data jenis dokumen by category id
     */
    function get_data($category_id){
        $exclude_data = $this->category_model->search_type(0,1,'name','asc',array('category_id'=>$category_id));
        
        $exclude_id = array();
        if(count($exclude_data)){
            foreach($exclude_data as $data){
                array_push($exclude_id,$data->types_id);
            }
        }
        //get active data
        $result = $this->type_model->get_data_type($exclude_id,1);
        //echo $this->db->last_query();
        echo json_encode($result);
        
    }
    
    public function getfile($type='pdf',$id){
        switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran;
                $filetype = $data[0]->type_file_lampiran;
                $filename = $data[0]->nama_file_lampiran;
                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama; 
                $filetype = $data[0]->type_file_regulasi;
                $filename = $data[0]->nama_file_regulasi;
            break;
        
        }
        if(count($data)){

            header("Content-type: ".$filetype);
            header('Content-disposition: inline; filename="'.$filename.'"');
            //header('Content-length: ' . ($data[0]->ukuran_file_regulasi*1024));
            echo $content;
        }
    }
    
    public function getfileen($type='pdf',$id){
        switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran_en($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran_en;
                $filetype = $data[0]->type_file_lampiran_en;
                $filename = $data[0]->nama_file_lampiran_en;
                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi_en($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama_en; 
                $filetype = $data[0]->type_file_regulasi_en;
                $filename = $data[0]->nama_file_regulasi_en;
            break;
        
        }
        if(count($data)){

            header("Content-type: ".$filetype);
            header('Content-disposition: inline; filename="'.$filename.'"');
            //header('Content-length: ' . ($data[0]->ukuran_file_regulasi*1024));
            echo $content;
        }
    }

    public function downloaden($type='pdf',$id){
        switch($type){
            case "lampiran" :
                $data = $this->regulasi_model->get_file_lampiran($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_lampiran_en;
                $filetype = $data[0]->type_file_lampiran_en;
                $filename = $data[0]->nama_file_lampiran_en;
                $filesize = $data[0]->ukuran_file_lampiran_en;
                break;
            case "pdf" :
            default:
                $data = $this->regulasi_model->get_file_regulasi($id);
                if(!count($data)){
                    redirect('admin/regulasi');
                }
                $content = $data[0]->regulasi_file_utama_en; 
                $filetype = $data[0]->type_file_regulasi_en;
                $filename = $data[0]->nama_file_regulasi_en;
                $filesize = $data[0]->ukuran_file_regulasi_en;

            break;
        
        }
        if(count($data)){
            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: ".$filetype);
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:".($filesize*1024));
            header('Content-disposition: attachment; filename="'.$filename.'"');
            echo $content;
        }
    }
	//update_user
	function doupdate(){
		/*
        if(!$this->input->is_ajax_request()){
            redirect('auth/logout');
        }
*/
		/* check mandatory fields */
		$id                         = $this->input->post('id');
		$types_id                   = $this->input->post('types_id');
		$category_id		        = $this->input->post('category_id');
		$tanggal                    = $this->input->post('tanggal');
        $nomor_urut                  = trim($this->input->post('nomor_urut'));
		$nomor_dokumen                  = trim($this->input->post('nomor_dokumen'));
        $judul                          = trim($this->input->post('judul'));
		$judul_en                          = trim($this->input->post('judul_en'));
		//$nama_file_regulasi		        = trim($this->input->post('nama_file_regulasi'));
        $isi_dokumen                = trim($this->input->post('isi_dokumen'));
		$isi_dokumen_en		        = trim($this->input->post('isi_dokumen_en'));
		$berlaku                    = $this->input->post('berlaku');
		$id_regulasi_pengganti                   = $this->input->post('id_regulasi_pengganti');
		$id_regulasi_terkait                   = $this->input->post('id_regulasi_terkait');
        
        
		$message = array();
		$redirect = "";
		$is_error = false;
		
		
		//-- run form validation
		$this->load->library('form_validation');
        $this->form_validation->set_rules('category_id[0]', 'Kategori 1', 'required');
		$this->form_validation->set_rules('types_id', 'lang:ljenis_dokumen', 'required');
		$this->form_validation->set_rules('tanggal', 'lang:ldate', 'required');
        $this->form_validation->set_rules('nomor_urut', 'lang:lnomor', 'numeric');
		$this->form_validation->set_rules('nomor_dokumen', 'lang:lnomor_dokumen', 'required');
		$this->form_validation->set_rules('judul', 'lang:lperihal', 'required');
		//$this->form_validation->set_rules('nama_file_regulasi', 'lang:lnama_file_dokumen', 'required');
		$this->form_validation->set_rules('berlaku', 'lang:lberlaku', 'required');
		
        //kalau tidak berlaku, wajib isi peraturan pengganti
        if(!$berlaku){
            $this->form_validation->set_rules('id_regulasi_pengganti', 'lang:lperaturan_pengganti', 'required');
		            
        }
		$this->form_validation->set_error_delimiters('', '<br>');
		
		if ($this->form_validation->run() == FALSE){
			$is_error = true;
			$message[] = validation_errors();
			
		}
		
		if(!$is_error){
			//upload file if exists
            
            $config['upload_path']      = config_item('temp_path');
            $config['allowed_types']    = config_item('regulasi_file_type_allowed');;
            $config['max_size']         = config_item('max_size_regulasi_upload');

            if(!is_dir($config['upload_path']))
                mkdir($config['upload_path'],'0755');
            $this->load->library('upload', $config);
            $this->firephp->log(config_item('upload_path'));	

            // Alternately you can set preferences by calling the initialize function. Useful if you auto-load the class:
            $this->upload->initialize($config);
            
            $upload_data = array();
            /* upload the file if exists to tmp folder */
            if(isset($_FILES['file_regulasi']['name']) && $_FILES['file_regulasi']['name']) {
                if ( ! $this->upload->do_upload('file_regulasi')){
                    $this->firephp->log('error attachmen');	

                    $message[] = $this->upload->error_msg[0];
                    $this->firephp->log('error msg : '.$this->upload->error_msg[0].";".$_FILES['file_regulasi']['type']);	

                    $is_error = true;
                }else{
                    $this->firephp->log('no error attachmen');	
                    //$message[] = 
                    $upload_data = $this->upload->data();
                    $is_error = false;

                }
            }
            
			$upload_data_lampiran = array();
             /* upload the file if exists to tmp folder */
            if(isset($_FILES['file_lampiran']['name']) && $_FILES['file_lampiran']['name']) {
                if ( ! $this->upload->do_upload('file_lampiran')){
                    $this->firephp->log('error attachmen');	

                    $message[] = $this->upload->error_msg[0];
                    $this->firephp->log('error msg : '.$this->upload->error_msg[0].";".$_FILES['file_lampiran']['type']);	

                    $is_error = true;
                }else{
                    $this->firephp->log('no error attachmen');	
                    //$message[] = 
                    $upload_data_lampiran = $this->upload->data();
                    $is_error = false;

                }
            }

            /* Upload English Version */
            $upload_data_en = array();
            /* upload the file if exists to tmp folder */
            if(isset($_FILES['file_regulasi_en']['name']) && $_FILES['file_regulasi_en']['name']) {
                if ( ! $this->upload->do_upload('file_regulasi_en')){
                    $this->firephp->log('error attachmen'); 

                    $message[] = $this->upload->error_msg[0];
                    $this->firephp->log('error msg : '.$this->upload->error_msg[0].";".$_FILES['file_regulasi']['type']);   

                    $is_error = true;
                }else{
                    $this->firephp->log('no error attachmen');  
                    //$message[] = 
                    $upload_data_en = $this->upload->data();
                    $is_error = false;

                }
            }
            
            $upload_data_lampiran_en = array();
             /* upload the file if exists to tmp folder */
            if(isset($_FILES['file_lampiran_en']['name']) && $_FILES['file_lampiran_en']['name']) {
                if ( ! $this->upload->do_upload('file_lampiran_en')){
                    $this->firephp->log('error attachmen'); 

                    $message[] = $this->upload->error_msg[0];
                    $this->firephp->log('error msg : '.$this->upload->error_msg[0].";".$_FILES['file_lampiran']['type']);   

                    $is_error = true;
                }else{
                    $this->firephp->log('no error attachmen');  
                    //$message[] = 
                    $upload_data_lampiran_en = $this->upload->data();
                    $is_error = false;

                }
            }
            
                
            /* add/update if no error */
            if(!$is_error){
		        $data = array(
                    "types_id"              => $types_id
                    ,"active"               => '1' //all is active by def
                    ,"tanggal"              => dbdate($tanggal)
                    ,"judul"                => $judul
                    ,"judul_en"                => $judul_en
                    ,"nomor_urut"           => $nomor_urut
                  //  ,"nama_file_regulasi"        => $nama_file_regulasi
                    ,"isi_dokumen"          => $isi_dokumen
                    ,"isi_dokumen_en"          => $isi_dokumen_en
                    ,"nomor_dokumen"          => $nomor_dokumen
                    ,"berlaku"              => ($berlaku)?$berlaku:0
                    ,"id_regulasi_pengganti"              => $id_regulasi_pengganti
				);
                
                 //if upload file, then update the regulasi_file table
                if(isset($upload_data) && $upload_data){
                    $data['nama_file_regulasi']     = $upload_data['file_name'];
                    $data['ukuran_file_regulasi']   = $upload_data['file_size'];
                    $data['type_file_regulasi']     = $upload_data['file_type'];
                }
                
                
                 //if upload file, then update the regulasi_file table
                if(isset($upload_data_lampiran) && $upload_data_lampiran){
                    $data['nama_file_lampiran']     = $upload_data_lampiran['file_name'];
                    $data['ukuran_file_lampiran']   = $upload_data_lampiran['file_size'];
                    $data['type_file_lampiran']     = $upload_data_lampiran['file_type'];
                }

                  //if upload file, then update the regulasi_file table
                if(isset($upload_data_en) && $upload_data_en){
                    $data['nama_file_regulasi_en']     = $upload_data_en['file_name'];
                    $data['ukuran_file_regulasi_en']   = $upload_data_en['file_size'];
                    $data['type_file_regulasi_en']     = $upload_data_en['file_type'];
                }
                
                
                 //if upload file, then update the regulasi_file table
                if(isset($upload_data_lampiran_en) && $upload_data_lampiran_en){
                    $data['nama_file_lampiran_en']     = $upload_data_lampiran_en['file_name'];
                    $data['ukuran_file_lampiran_en']   = $upload_data_lampiran_en['file_size'];
                    $data['type_file_lampiran_en']     = $upload_data_lampiran_en['file_type'];
                }
                
                
                //add
				if(!$id){
				 	$this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

                    $id = $this->regulasi_model->insert($data);

                    $this->regulasi_model->update_category($id,$category_id);
                   // $this->regulasi_model->update_regulasi_terkait($id,$id_regulasi_terkait);
                    
                    //if upload file, then update the regulasi_file table
                    if(isset($upload_data) && $upload_data){
                        $this->regulasi_model->update_file($id,$upload_data,$upload_data_lampiran,true);
                    }

                    //if upload file en, then update the regulasi_file table
                    if(isset($upload_data_en) && $upload_data_en){
                        $this->regulasi_model->update_file_en($id,$upload_data_en,$upload_data_lampiran_en,true);
                    }
                    
                    $this->db->trans_complete(); # Completing transaction
                    
                    if ($this->db->trans_status() === FALSE) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $message[] = lang('ldata_failed_inserted');
                        $is_error  = true;
                    } 
                    else {
                        # Everything is Perfect. 
                        # Committing data to the database.
                        $this->db->trans_commit();
                        $message[] = lang('ldata_success_inserted');
                    }

						
				}else{
					$this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
                    
                    $this->regulasi_model->update($data,$id);
                    $this->regulasi_model->update_category($id,$category_id);
                    //$this->regulasi_model->update_regulasi_terkait($id,$id_regulasi_terkait);
                    
                    //if upload file, then update the regulasi_file table
                    $this->regulasi_model->update_file($id,$upload_data,$upload_data_lampiran,true);
              
                    $this->regulasi_model->update_file_en($id,$upload_data_en,$upload_data_lampiran_en,true);
              
                    $this->db->trans_complete(); # Completing transaction
                    if ($this->db->trans_status() === FALSE) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $message[] = lang('ldata_failed_inserted');
                        $is_error  = true;
                    } 
                    else {
                        # Everything is Perfect. 
                        # Committing data to the database.
                        $this->db->trans_commit();
                        $message[] = lang('ldata_success_updated');
                    }
				}
				
				$redirect = site_url("admin/regulasi");
			}
		}
		
		$result = array(
			"message"	=> implode("<br>",$message)
			,"id"	=> $id
			,"error"	=> $is_error
			,"redirect"	=> $redirect
		);
		
		echo json_encode($result);
	}
	
	function ajax_get(){
        $searchval['nomor_dokumen'] = $this->input->get('q');
        $searchval['exclude_id']  = $this->input->get('id');
        
        $searchval['active'] = 1;
        $result = $this->regulasi_model->getRegulasiByNomorDokumen($searchval);
       //           
        $this->firephp->log($this->db->last_query());
        $results = $ret = array();
        $total = count($result);
        if($total){
            for($i=0;$i<count($result);$i++){
                 $results['id'] = $result[$i]->id;
                 $results['text'] = htmlspecialchars($result[$i]->nomor_dokumen." - ".$result[$i]->tanggal_fmt);
                 $ret['results'][$i] = $results;
        	}
        }else{
            $results['id']      = 0;
            $results['text']    = "";
            $ret['results'][0]  = $results;
        }
       // $hasil['total_count'] = $total;
       // $hasil['incomplete_result'] = false;
        
        //echo $total;	
        //echo $this->db->last_query();
        echo json_encode($ret);
        
    }
    
    function viewpdf($id){
        //https://github.com/dompdf/dompdf/wiki/DOMPDF-and-Composer-Quick-start-guide
        
        //get data content and filename
        $data = $this->regulasi_model->get_data_pdf($id);
        
        if(!$data){
            redirect('admin/regulasi');
        }
        
        $content = $data[0]->isi_dokumen;
        $filename = $data[0]->nama_file_regulasi;
        
        //generate some PDFs!
        $dompdf = new Dompdf\Dompdf();//if you use namespaces you may use new \DOMPDF()
        $dompdf->loadHtml($content);
        
      
        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'portrait'); //or landscape
        
        $dompdf->render();
        $dompdf->add_info('Author','test'.config_item('pdf_author'));
        $dompdf->stream($filename,array('Attachment'=>0));//stream to browser
        //$dompdf->stream('my.pdf');//save to file

    /*    $content = $this->input->post('isi_dokumen');
        $filename = $this->input->post('nama_file_regulasi');
        
        //Load the library
        $this->load->library('html2pdf');

        //Set folder to save PDF to
        $this->html2pdf->folder('./upload/pdf/');

        //Set the filename to save/download as
        $this->html2pdf->filename($filename);

        //Set the paper defaults
        $this->html2pdf->paper('a4', 'portrait');

        //Load html view
        $this->html2pdf->html("asdasdas");
        
        //Create the PDF
        $path = $this->html2pdf->create('save'); //or 'download'
        echo $path;*/
 
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
