<?php
class Menu_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
    
	function get_menu(){
        if(!isAdmin()){
            //get menu base on group permission
            $searchval = array(
                "group_id"  => userGroupId()
                ,"active"   => '1'
            );

            $menus =  $this->search(0,0,'display_order','asc',$searchval);
            return $menus;
        }else{
            return $this->getAdminMenus();
        }
    }
    
    function getAdminMenus(){
        $this->db->where('a.active','1');
        $this->db->select("a.id as menu_id");
        $this->db->select("a.title as menu_title");
        $this->db->select("a.url as menu_url");
        $this->db->select("a.display_order as menu_display_order");
        
        $this->db->order_by('display_order','asc');
        $this->db->from('menus a');
        
        $query = $this->db->get();
        
        return $query->result();
    }

    function deleteByGroupId($group_id){
        $this->db->where('group_id', $group_id);
        $this->db->delete('group_menus'); 
    }

    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
        if(isset($searchval['group_id']) && $searchval['group_id']){
            $this->db->where('b.group_id',$searchval['group_id']);
        }

        if(isset($searchval['active'])){
            $this->db->where('a.active',$searchval['active']);
        }
        
       if($this->is_count){
			$this->db->select("count(group_menus.id) as total");
		}else{
            $this->db->select("a.id as menu_id");
            $this->db->select("a.title as menu_title");
            $this->db->select("a.url as menu_url");
            $this->db->select("a.display_order as menu_display_order");
            $this->db->select("b.permissions");
       }
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
        $this->db->from('group_menus b');
        $this->db->join('menus a','a.id = b.menu_id');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    function insert_group_menus($datas){
        $this->db->insert("group_menus", $datas); 
        return $this->db->insert_id();
    }   
}